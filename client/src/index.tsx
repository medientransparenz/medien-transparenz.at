import React, {Suspense} from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';

// import i18n (needs to be bundled ;)) 
import './i18n';

import { AuthProvider } from './context/auth-context';
import { userSlice } from './pages/list-users';
import { configureStore } from '@reduxjs/toolkit';
import { zipCodeSlice } from './pages/list-zipcodes';
import { overviewSlice } from './pages/overview';
import { topSlice } from './pages/top/top-transfers';
import { infoSlice } from './App';
import { flowsSlice } from './pages/flows/flows-transfers';
import { searchSlice } from './pages/search';
import { mapSlice } from './pages/map';
import { groupingSlice } from './pages/list-groups';
import { organisationsSlice } from './pages/list-organisations';
import { transfersSlice } from './pages/list-transfers';
import { aboutUsSlice } from './pages/about_us';
import { topFundingSlice } from './pages/top/top-fundings';
import { flowsFundingSlice } from './pages/flows/flows-funding';
import { flowsMixedSlice } from './pages/flows/flows-mixed';
/*
const rootReducer = combineReducers({
  formBuilder: formBuilderReducer
})
*/


export const store = configureStore({
  reducer: {
    users: userSlice.reducer,
    zipCodes: zipCodeSlice.reducer,
    overview: overviewSlice.reducer,
    top: topSlice.reducer,
    info: infoSlice.reducer,
    flow: flowsSlice.reducer,
    search: searchSlice.reducer,
    maps: mapSlice.reducer,
    grouping: groupingSlice.reducer,
    organisations: organisationsSlice.reducer,
    transfers: transfersSlice.reducer,
    aboutUs: aboutUsSlice.reducer,
    topFundings: topFundingSlice.reducer,
    flowFundings: flowsFundingSlice.reducer,
    flowsMixed: flowsMixedSlice.reducer
  }
})
export type AppState = ReturnType<typeof store.getState>

const container = document.getElementById('root');
const root = ReactDOM.createRoot(container!)


root.render(
  <Provider store={store}>
    <React.StrictMode>
    <AuthProvider>
      <Suspense fallback={<div>Loading...</div>}>
        <App />
      </Suspense>  
    </AuthProvider>
    </React.StrictMode>
  </Provider>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
