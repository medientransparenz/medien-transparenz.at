import React from 'react'
import { ToggleSwitch } from '../components/three-state/three-state-switch';
import { ComponentMeta, ComponentStory } from '@storybook/react';

export default {
  title: "Components/ToggleSwitch",
  component: ToggleSwitch
} as ComponentMeta<typeof ToggleSwitch>

const Template: ComponentStory<typeof ToggleSwitch> = args => <ToggleSwitch {...args} />
export const SimpleThreeState = Template.bind({}) 
SimpleThreeState.args = {
    values: [{value: "V1", label: "Label 1"},{value: "V2", label: "Label 2"},{value: "V3", label: "Label 3"}],
    selected: 'V1',
    onChange: (v)=>{console.log(v)}
}