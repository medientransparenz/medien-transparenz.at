import React from 'react'
import { BuildForm } from '../components/form-builder/standard-element-factory';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import {
    BrowserRouter as Router,
  } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import { IGrouping } from '../../../server/models/grouping';
import { orgTypes } from '../pages/organisation-new';

export default {
    title: "Components/Forms",
    component: BuildForm,
    decorators: [
        (Story) => (
          <Router>
            <Story />
          </Router>
        ),
      ],
  } as ComponentMeta<typeof BuildForm>
  
const t = text => text
const search = type => Promise.resolve([`Searching ${type}`])

const initial:IGrouping = {
    name:"",
    type: 'media',
    group_type: 'group',
    region: 'AT',
    isActive: true,
    members: [],
    serverSide: true
}

const Template: ComponentStory<typeof BuildForm> = args => <BuildForm {...args} />
export const NewGroupFrom = Template.bind({}) 
NewGroupFrom.args = {
    initialState: initial,
    fields: [
        {name: "name", type: "text", label: "Name", 
            validators: { required: "required"}, help: t("Pick a name for this group")},
        {name: "type", type: "radiobutton", label: "Type", help: t("Select the type of this group"), 
        options:['media','org'], disabled:true},
        {name: "isActive", type: "checkbox", label: t("Active"),
            help: t("Activate or deactivate this group")}, 
        {name: "group_type", type: "radiobutton", label: t("Group type"), 
            options:['group','class']
        },{
            name: "region", type: "select", label: t("Region"),
            options: ["EU", "AT", "AT-1", "AT-2", "AT-3", "AT-4", "AT-5", "AT-6", "AT-7", "AT-8", "AT-9"]
        }, 
        { name: "members", type: "multiselect", onSearch: (a)=>search(a),
             emptyRecordMsg: t('Start typing'), placeHolder: ""}      
    ],
    name: "Edit Group",
    submitHandler: (group_) =>{ 
        console.log(group_)
    },
    submitAction: { label: t("Create Group"),  nextRoute:"/groups"},
    cancelAction: { label: t("Cancel"),  nextRoute:'back'},
}

export const NewOrganisationFrom = Template.bind({}) 
NewOrganisationFrom.args = {
    fields: [
        {
            name: "name", type: "text", label: t("Name"),
            validators: { required: "required" }
        },
        {
            name: "type", type: "select", label: t("Type"), help: t("Select the type of this organisation"),
            options: orgTypes, disabled: false
        },
        {
            name: "street", type: "text", label: t("Street"),
            validators: { required: "required" }
        },
        {
            name: "zipCode", type: "text", label: t("Zip Code"),
            validators: { required: "required" }
        },
        {
            name: "city_de", type: "text", label: t("City"),
            validators: { required: "required" }
        },
        {
            name: "federalState", type: "select", label: t("Federal State"),
            options: ["EU", "AT", "AT-1", "AT-2", "AT-3", "AT-4", "AT-5", "AT-6", "AT-7", "AT-8", "AT-9"],
            labels: [t("EU"), t("AT"), t("AT-1"), t("AT-2"), t("AT-3"), t("AT-4"),
                     t("AT-5"), t("AT-6"), t("AT-7"), t("AT-8"), t("AT-9")], 
            disabled: false
        },
        {
            name: "country_de", type: "text", label: t("Country"),
            validators: { required: "required" }
        },
    ],
    name: t("New Organisation"),
    submitHandler: (organisation_) => 
        console.log(organisation_)
    ,
    submitAction: { label: t("Create Organisation"), nextRoute: "/organisation/list" },
    cancelAction: { label: t("Cancel"), nextRoute: 'back' }
}