import { faWindowClose, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import Button from "react-bootstrap/esm/Button";
import Modal, { ModalProps } from "react-bootstrap/esm/Modal";
import { useTranslation } from "react-i18next";
import mongoose from 'mongoose';

export const ConfirmDelete = <T extends mongoose.Document>(props: ModalProps & 
    { onDelete: ()=>any, elementToDelete: T | null , propertyToPresent: keyof T} ) => {
    const { t } = useTranslation();
    
    return (
      <Modal
       { ...props }
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {t("Attention!")}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            {t('Do you really want to delete the following element?')}
          </p>
          <p><strong>{ props.elementToDelete && (props.elementToDelete[props.propertyToPresent] as any).toString()}</strong></p>
          <p>{props.propertyToPresent.toString()}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.onHide}><FontAwesomeIcon icon={faWindowClose}/> {t('Cancel')}</Button>
          <Button variant="danger" onClick={props.onDelete}><FontAwesomeIcon icon={faTrashAlt}/> {t('Delete')}</Button>
        </Modal.Footer>
      </Modal>
      );
    }