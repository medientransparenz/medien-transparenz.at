import { faUser, faSignOutAlt, faExclamationTriangle, faQuestionCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../context/auth-context';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';
import { useTranslation } from 'react-i18next';
import Card from 'react-bootstrap/esm/Card';
import Toast from 'react-bootstrap/esm/Toast';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';


export const ShowError = ({ error, onClose, header = 'Error' }) =>
  error ? <Toast onClose={onClose}>
    <Toast.Header>
      <FontAwesomeIcon icon={faExclamationTriangle} />
      <strong className="mr-auto">{header}</strong>
    </Toast.Header>
    <Toast.Body>{error}</Toast.Body>
  </Toast> : null

export const LoginButton: React.FC<{ logoutText: string, loginText: string }> = (texts = { logoutText: 'Logout', loginText: 'Login' }) => {
  const { user, isLoggedIn, signout } = useContext(AuthContext);
  const navigate = useNavigate();
  if (isLoggedIn()) {
    return <><ul className="navbar-nav ms-auto">
      <li className="nav-item user-info">
        <span className="navbar-text"><FontAwesomeIcon icon={faUser} />{user?.username}</span>
      </li>
      <li className="nav-item">
        <span className="nav-link" onClick={() => { signout(); navigate('/') }}><FontAwesomeIcon icon={faSignOutAlt} /> {texts.logoutText} </span>
      </li>
    </ul></>
  } else {
    return <>{/*<ul className="navbar-nav ml-auto">
      <li className="nav-item">
        <Link className="nav-link" to="/login">
          <FontAwesomeIcon icon={faSignInAlt} /> {texts.loginText}
        </Link>
      </li>
  </ul>*/}</>
  }
}

export const IfNoError: React.FC<{ error: string, children: React.ReactNode }> = ({ children, error }) => {
  const { t } = useTranslation()
  return error ? <Card>
    <Card.Body>
      <Card.Header>{t('Error')}</Card.Header>
      <Card.Text>
        <div className="text-danger"><FontAwesomeIcon icon={faExclamationTriangle} /> {error}</div>
      </Card.Text>
    </Card.Body>


  </Card> : <div>{children}</div>
}

export const Render: React.FC<{ when: boolean, children: React.ReactNode }> = ({ children, when }) =>
  <>{when ? children : null}</>


export const ModalLoader: React.FC<{ isPending: boolean }> = ({ isPending }) => {
  const { t } = useTranslation()
  return <Modal show={isPending} animation={false} centered>
    <Modal.Body>
      <Spinner animation="border" role="status">
      </Spinner>
      <span>   {t('Loading Data')}...</span>
    </Modal.Body>
  </Modal>
}

export const Help = ({ text, placement = "bottom" }) => {
  const { t } = useTranslation()
  return <OverlayTrigger overlay={<Tooltip id="tooltip-disabled" >{t(text)}</Tooltip>}>
    <span className="d-inline-block">
      <FontAwesomeIcon icon={faQuestionCircle} />
    </span>
  </OverlayTrigger>
}

export const Info = ({ text }) => {
  return <div className="text-right info">
    <FontAwesomeIcon icon={faInfoCircle} /> {text}
  </div>
}

export const InfoTooltip = ({ text }) => {
  const { t } = useTranslation()
  return <OverlayTrigger  placement="bottom" overlay={<Tooltip id="tooltip-disabled" >{t(text)}</Tooltip>}>
    <span className="d-inline-block">
      <FontAwesomeIcon icon={faInfoCircle} />
    </span>
  </OverlayTrigger>
}

