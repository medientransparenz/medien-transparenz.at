import React from 'react'
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExcel } from '@fortawesome/free-solid-svg-icons';

export const ExportToExcel = ({ data, fileName }) => {
    const fileType =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ".xlsx";
  
    const exportToCSV = (apiData, fileName_) => {
      const ws = XLSX.utils.json_to_sheet(apiData);
      const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
      const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
      const data_ = new Blob([excelBuffer], { type: fileType });
      FileSaver.saveAs(data_, fileName_ + fileExtension);
    };
  
    return (
      <button className="btn btn-info btn-sm" onClick={(e) => exportToCSV(data, fileName)}><FontAwesomeIcon icon={faFileExcel} /> Export</button>
    );
  };