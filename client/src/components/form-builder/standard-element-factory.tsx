import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, useState, useEffect } from 'react';
import { FieldValues, useForm, UseFormSetValue, UseFormRegister, UseFormGetValues } from 'react-hook-form';
import {
    CheckBoxFieldDescription, FieldDescriptionType, FormAction,
    FormDescription, MultiCheckBoxFieldDescription, TextFieldDescription,
    RadioButtonFieldDescription,
    MultiSelectFieldDescription, BaseField, SelectFieldDescription, HtmlEditorFieldDescription
} from './field-types';
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { AuthContext } from '../../context/auth-context';
import { Multiselect } from 'multiselect-react-dropdown';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Form from 'react-bootstrap/Form';
import { zip } from '../../helpers/helpers';
import { Help } from '../helper-components';

/* eslint react-hooks/exhaustive-deps: 0 */

type FieldProps<T extends FieldValues, S extends BaseField<T>> = {
    description: S,
    setValue: UseFormSetValue<T>,
    getValues: UseFormGetValues<T>,
    reg: UseFormRegister<T>,
    errors: any,
    initialState?: T
}

const MultiCheckBoxField = <T extends FieldValues>({ description, reg, setValue, initialState }: FieldProps<T, MultiCheckBoxFieldDescription<T>>) => {
    useEffect(() => {
        setValue(description.name, initialState && initialState[description.name.toString()])
    }, [initialState])

    return <div className="form-group">
        {description.label ? <label>{description.label}</label> : null}
        <div>
            {description.options.map(o =>
                <div className="form-check form-check-inline" key={o}>
                    <input value={o}
                        defaultChecked={initialState && initialState[description.name.toString()].includes(o)}
                        {...reg(description.name)}
                        type="checkbox"
                        className="form-check-input"
                        id={o} />
                    <label htmlFor={o}>{o}</label>
                </div>
            )}
        </div>
    </div>
}


const HtmlEditorField = <T extends FieldValues>({ description, reg, setValue, errors, initialState }: FieldProps<T, HtmlEditorFieldDescription<T>>) => {
    useEffect(() => {
        setValue(description.name, initialState && initialState[description.name.toString()])
    }, [initialState])
    const {onChange:onChange_, onBlur:onBlur_, ...rest } = reg(description.name)
    return <div className="form-group">
        {description.label ? <label>{description.label} {
            description.validators && 'required' in description.validators ? <sup>*</sup> : null}</label> : null}
        <div>
            <CKEditor
                editor={ClassicEditor}
                onChange={ (event, editor) => 
                        setValue(description.name, editor.getData())
                }
                onBlur={ (event, editor) => 
                    setValue(description.name, editor.getData())
                }
                {...rest}                
                data={(initialState && initialState[description.name.toString()]) || ""}
                id={description.name.toString()} 
            />
            {description.help ? <small id={`${description.name}Help`} className="form-text text-muted">{description.help}</small> : null}
            <ShowError errors={errors} field={description.name.toString()} />
        </div>
    </div>       
}

const selectionBoxStyle = {
    //chips: { background: "red" },
    searchBox: {
        border: "none",
        "borderBottom": "1px solid blue",
        "borderRadius": "0px",
        "backgroundColor": "white"
    }
}

const MAX_OPTION_LENGTH = 40
const valueDecorator = (v: string) => 
    v.length > MAX_OPTION_LENGTH ? <span>{`${v.substring(0,MAX_OPTION_LENGTH)}...`}<Help text={v} /></span>  : v

const MultiSelectField = <T extends FieldValues>({ description, reg,
    setValue, initialState }: FieldProps<T, MultiSelectFieldDescription<T>>) => {
    const [options, setOptions] = useState<string[]>([])
    const [isLoading, setLoading] = useState(false)
    const [selectedValues, setSelectedValues] = useState<string[]>(initialState?.members ?? [])
    const onSearch = (query: string) => {
        if (query.length === 3) {
            setLoading(true)
            description.onSearch(query).then(results => {
                setOptions(results)
            }
            ).finally(() => setLoading(false))
        } else if (query.length < 3) {
            setOptions([])
        }
    }
    const updateValues = (values: string[]) => {
        values.sort();
        setSelectedValues(values);
        setValue(description.name as any, values as any)
    }
    useEffect(() => {
        if (initialState?.members) { setSelectedValues(initialState.members) }
    }, [initialState])
    return <div className="form-group" key={description.name.toString()}>
        <div className="label">{description.label}</div>
        <Multiselect onSearch={onSearch}
            id={description.name.toString()}
            //name={description.name.toString()}
            options={options}
            isObject={false}
            selectedValues={selectedValues}
            loading={isLoading}
            style={selectionBoxStyle}
            emptyRecordMsg={description.emptyRecordMsg}
            onSelect={updateValues}
            onRemove={updateValues}
            //onChange={e => console.log(e)}
            placeholder={description.placeHolder}
            selectedValueDecorator={valueDecorator}
        //ref={reg()}
        />
    </div>
}

const SelectField = <T extends FieldValues>({ description, reg, errors, initialState, setValue, getValues }: FieldProps<T, SelectFieldDescription<T>>) => {
    useEffect(() => {
        setValue(description.name, initialState && initialState[description.name.toString()])
    }, [initialState]) // eslint-disable-line react-hooks/exhaustive-deps
    return <FloatingLabel controlId="floatingSelect" label={
        `${description.label}${description.validators && 'required' in description.validators ? '*' : ''}`}>
        <Form.Select aria-label={description.label}
            {...reg(description.name)} disabled={description.disabled}>
            {zip(description.options, description.labels ? description.labels : description.options).map(([value,label], i) =>
                <option key={i} value={value}>{label}</option>
            )}
        </Form.Select>
    </FloatingLabel>
}

const RadioButtonField = <T extends FieldValues>({ description, reg, errors, initialState, setValue, getValues }: FieldProps<T, RadioButtonFieldDescription<T>>) => {
    const [localValue, setLocalValue] = useState<any>()
    useEffect(() => {
        setValue(description.name, initialState && initialState[description.name.toString()])
        setLocalValue(initialState && initialState[description.name.toString()])
    }, [initialState]) // eslint-disable-line react-hooks/exhaustive-deps
    return <div className="form-group">
        {description.label ? <label>{description.label}</label> : null}
        <div>
            {description.options.map((o, i) =>
                <div className="form-check" key={`${o}_${i}`}>
                    <input className="form-check-input" type="radio" value={o}
                        checked={localValue === o}
                        onClick={e => setLocalValue(o)}
                        disabled={description.disabled}
                        id={o}
                        {...reg(description.name)} />
                    <label className="form-check-label" htmlFor={o}>
                        {o}
                    </label>

                </div>
            )}
        </div>
    </div>
}

const CheckBoxField = <T extends FieldValues>({ description, reg, errors, setValue, initialState }: FieldProps<T, CheckBoxFieldDescription<T>>) => {
    useEffect(() => {
        setValue(description.name, initialState && initialState[description.name.toString()])
    }, [initialState])
    return (<div className="form-check" key={description.name.toString()}>
        <input
            defaultChecked={initialState && initialState[description.name.toString()]}
            {...reg(description.name, description.validators)}
            type={description.type}
            className="form-check-input"
            id={description.name.toString()} />
        {description.label ? <label htmlFor={description.name.toString()}>{description.label}{
            description.validators && 'required' in description.validators ? <sup>*</sup> : null}</label> : null}
        {description.help ? <small id={`${description.name}Help`} className="form-text text-muted">{description.help}</small> : null}
        <ShowError errors={errors} field={description.name.toString()} />
    </div>)
}

const TextField = <T extends FieldValues>({ description, reg, errors, initialState, setValue }: FieldProps<T, TextFieldDescription<T>>) => {
    useEffect(() => setValue(description.name, initialState && initialState[description.name.toString()]), [initialState]) // eslint-disable-line react-hooks/exhaustive-deps

    return (<div className="form-group" key={description.name.toString()}>
        {description.label ? <label htmlFor={description.name.toString()}>{description.label}{
            description.validators && 'required' in description.validators ? <sup>*</sup> : null}</label> : null}
        <input
            defaultValue={initialState && initialState[description.name.toString()]}
            placeholder={description.defaultValue ?? description.defaultValue}
            {...reg(description.name, description.validators)}
            type={description.type}
            className="form-control"
            id={description.name.toString()} />
        {description.help ? <small id={`${description.name}Help`} className="form-text text-muted">{description.help}</small> : null}
        <ShowError errors={errors} field={description.name.toString()} />
    </div>)
}

export const email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

type CreateProps<T extends FieldValues> = {
    description: FieldDescriptionType<T>,
    setValue: UseFormSetValue<T>,
    getValues: UseFormGetValues<T>,
    reg: UseFormRegister<T>,
    errors: any,
    initialState?: T
}

const Field = <T extends FieldValues>({ description, ...props }: CreateProps<T>) => {
    switch (description.type) {
        case 'multicheckbox': return <MultiCheckBoxField description={description} {...props} />
        case 'checkbox': return <CheckBoxField description={description} {...props} />
        case 'radiobutton': return <RadioButtonField description={description as RadioButtonFieldDescription<T>} {...props} />
        case 'multiselect': return <MultiSelectField description={description as MultiSelectFieldDescription<T>} {...props} />
        case 'select': return <SelectField description={description} {...props} />
        case 'html': return <HtmlEditorField description={description as HtmlEditorFieldDescription<T>} {...props} />
        default: return <TextField description={description} {...props} />
    }
}

const Spinner: React.FC<{ visible: boolean, msg: string }> = ({ visible, msg }) =>
    visible ? (<div className="alert alert-primary" role="alert"><div className="spinner-border" role="status">
        <span className="sr-only">{msg}</span>
    </div> {msg}</div>) : null

const ShowError: React.FC<{ errors: any, field: string }> = ({ errors, field }) => {
    if (field in errors) {
        return <p className="text-danger">{errors[field]?.message ?? errors[field]?.type}</p>
    } else { return null }
}

const ActionButton: React.FC<FormAction & { actionType: 'submit' | 'cancel', disabled: boolean }> = ({ label, icon, nextRoute, actionType, disabled }) => {
    const navigate = useNavigate();
    if (actionType === 'submit') {
        return <button type="submit" disabled={disabled}
            className="btn btn-primary">
            {icon ? <FontAwesomeIcon icon={icon} /> : null} {label}
        </button>
    } else {
        return <button type="button"
            onClick={() => nextRoute === 'back' ? navigate(-1) : navigate(nextRoute ?? '/')}
            className="btn btn-secondary">
            {icon ? <FontAwesomeIcon icon={icon} /> : null} {label}
        </button>
    }

}

const Toast: React.FC<{ message: string, header?: string }> = ({ message, header }) => { //eslint-disable-line @typescript-eslint/no-unused-vars
    return (
        <div className="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div className="toast-header">
                <img src="..." className="rounded mr-2" alt="..." />
                <strong className="mr-auto">{header ?? 'Info'}</strong>
                <small className="text-muted">11 mins ago</small>
                <button type="button" className="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div className="toast-body">
                {message}
            </div>
        </div>)
}


export const BuildForm = <R, T extends FieldValues>
    (descriptor: FormDescription<T> & { Presenter?: React.FC<R> }) => {
    const [submitError, setSubmitError] = useState<string | null>(null);
    const { signout } = useContext(AuthContext)
    const [result, setResult] = useState<R | null>();
    const [isPending, setPending] = useState(false);
    const { fields, name, submitAction, cancelAction, Presenter,
        submitHandler, pendingMessage, initialState, hiddenFields } = descriptor
    const { register, handleSubmit, setValue, getValues, formState: { errors } } = useForm<T>();
    const navigate = useNavigate();
    const { t } = useTranslation();
    useEffect(() => {
        descriptor.fields
            .filter(d => d.type === "multiselect")
            .forEach(d => register(d.name as any))
              
    }, [register])
    useEffect(()=>{
        hiddenFields?.forEach(f => {
            if (initialState && initialState[f]) {
                setValue(f, initialState[f]) 
            }
        }) 
    },[initialState])  // eslint-disable-line react-hooks/exhaustive-deps
    const handler = (data: any) => {
        setResult(null)
        setPending(true);
        setSubmitError(null);
        console.log("Pending ON");
        return Promise.resolve()
            .then(() => submitHandler(data))
            .then((success) => {
                if (submitAction.nextRoute) {
                    return (submitAction.nextRoute === 'back' ? navigate(-1) : navigate(submitAction.nextRoute ?? '/'))
                } else {
                    setResult(success as R)
                }
            })
            .catch(err => {
                if (err?.message === "Request failed with status code 401") {
                    signout().then(() => navigate("/login"))
                }
                setSubmitError(err?.response?.data?.message?.message ?? err?.message ?? err.toString());
                console.log("Error!");
                console.log(JSON.stringify(err));
            })
            .finally(() => {
                setPending(false);
            })
    }
    return (
        <div id={`${name.replace(" ", "_")}_form`}>
            <h1>{name}</h1>
            {result ? (<div className="alert alert-success" role="alert">
                {Presenter ? <Presenter {...result} /> : JSON.stringify(result)}
            </div>) : null
            }
            {submitError ? (<div className="alert alert-danger" role="alert">
                {submitError}</div>) : null}
            <Spinner visible={isPending} msg={pendingMessage ?? t("sending")} />
            <form onSubmit={handleSubmit(handler)}>
                 
                {fields.map(f => <Field setValue={setValue} getValues={getValues} description={f}
                  reg={register} errors={errors} initialState={initialState} key={f.name.toString()} />)}
                
                {fields.filter(f => f.validators && 'required' in f.validators).length > 0 ? <p className="text-info"><small><sup>*</sup>{t("required")}</small></p> : null}
                {/*<p>isDirty={isDirty ? "Yes":"No"}, isValid={isValid ? "Yes":"No"}, isPending={isPending ? "Yes":"No"}</p> */}
                
                <ActionButton {...submitAction} actionType='submit' disabled={isPending} />
                {cancelAction ? (<span>  <ActionButton {...cancelAction} actionType='cancel' disabled={false} /></span>) : null}
                
            </form>
        </div>

    );
}

