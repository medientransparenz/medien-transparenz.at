import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { RegisterOptions, FieldValues, SubmitHandler, Path } from 'react-hook-form';

type TextFieldTypes = "number" | "time" | "text" | "date" | "email" | "password" | "search" | "tel" | "url" | "file"

export type FormAction = {
    label: string,
    icon?: IconDefinition,
    nextRoute?: string | 'back'
}

export interface BaseField<T extends FieldValues> {
    name: Path<T>;
    label?: string;
    defaultValue?: any;
    validators?: RegisterOptions;
    help?: string;
    disabled?: boolean;
}

export interface TextFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: TextFieldTypes;
}
export interface CheckBoxFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: 'checkbox';
}
export interface MultiCheckBoxFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: 'multicheckbox';
    options: Array<any>;
}

export interface HtmlEditorFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: 'html';

}

export interface MultiSelectFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: 'multiselect';
    //options: string[];
    onSearch: (v:string)=>Promise<string[]>;
    emptyRecordMsg: string;
    //selectedValues: string[];
    //onSelect: (v:string[])=>any;
    //onRemove: (v:string[])=>any;
    placeHolder: string;
    //loading: boolean;
}

export interface RadioButtonFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: 'radiobutton';
    options: Array<any>;
}
export interface SelectFieldDescription<T extends FieldValues> extends BaseField<T> {
    type: 'select';
    options: Array<any>;
    labels?: Array<any>
}

export type FieldDescriptionType<T extends FieldValues> = TextFieldDescription<T> | 
    CheckBoxFieldDescription<T> |
    MultiCheckBoxFieldDescription<T> |
    RadioButtonFieldDescription<T> |
    MultiSelectFieldDescription<T> |
    HtmlEditorFieldDescription<T> |
    SelectFieldDescription<T>

// Description of the entire form that is used for building
export interface FormDescription<T extends FieldValues> {
    fields: FieldDescriptionType<T>[];
    name: string;
    debug?: boolean;
    submitHandler: SubmitHandler<T>;
    initialState?: T;
    pendingMessage? :string;
    submitAction: FormAction;
    cancelAction?: FormAction;
    hiddenFields?: Array<Path<T>>;
}
