import React from 'react';
import styled from 'styled-components';
//import { Switch, SwitchLabel, SwitchRadio, SwitchSelection } from "./switch-styles.js";
import { useState } from 'react';

export const Switch = styled.div`
  font-family: "Lucida Grande", Tahoma, Verdana, sans-serif;
  position: relative;
  height: 38px;
  width: 210px;
  background-color: #e4e4e4;
  border-radius: 3px;
  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
`;

export const SwitchRadio = styled.input`
  display: none;
`;

export const SwitchSelection = styled.span`
  display: block;
  position: absolute;
  z-index: 1;
  top: 0px;
  left: 0px;
  width: 70px;
  height: 38px;
  background: #216BA5;
  border-radius: 3px;
  transition: left 0.25s ease-out;
  box-shadow: inset 0 2px 2px rgba(0,0,0,0.16), 0 2px 2px rgba(0,0,0,0.45);
`;

export const SwitchLabel = styled.label`
  position: relative;
  z-index: 2;
  float: left;
  width: 70px;
  line-height: 38px;
  font-size: 11px;
  color: rgba(0, 0, 0, 0.6);
  text-align: center;
  cursor: pointer;

  ${SwitchRadio}:checked + &{
    transition: 0.15s ease-out;
    color: #fff;
  }
  
`;

const titleCase = str =>
    str.split(/\s+/).map(w => w[0].toUpperCase() + w.slice(1)).join(' ');

const ClickableLabel = ({ title, onChange, id, value }) =>
    <SwitchLabel onClick={() => onChange(value)} className={id}>
        {titleCase(title)}
    </SwitchLabel>;

const ConcealedRadio = ({ value, selected }) =>
    <SwitchRadio type="radio" name="switch" default={selected === value} />;

export type TSwitchProps = {
    values: {
        value: any,
        label: string
    }[],
    selected: any,
    onChange: (v:any)=>void
}

export const ToggleSwitch = ({ values, selected, onChange }: TSwitchProps) => {
    const [selected_, setSelected] = useState(selected);
    const valuesList = values.map(v=>v.value);
    const handleChange = val => {
        setSelected(val);
        onChange(val)
    };

    const selectionStyle = () => {
        return {
            left: `${valuesList.indexOf(selected_) / 3 * 100}%`,
        };
    };
    return (
        <Switch>
            {values.map(val => {
                return (
                    <span>
                        <ConcealedRadio value={val.value} selected={selected_} />
                        <ClickableLabel title={val.label} value={val.value} onChange={handleChange} id={val.label} />
                    </span>
                );
            })}
            <SwitchSelection style={selectionStyle()} />
        </Switch>
    );
}



