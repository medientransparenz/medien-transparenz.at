import config from "./server-config";
import axios from "axios";
import { createAuthenticationHeader } from "./security";
import { IZipCodeDocument } from "../../../server/models/zipcodes";
import {
  TOverViewResult,
  TOverViewTransferResult,
} from "../../../server/controllers/transfer";
import { TTopQuery, TTopResult } from "../pages/top/top-transfers";
import {
  TFlowsQuery,
  TFlowResult,
  TTimelineResult,
} from "../pages/flows/flows-transfers";
import { TSearchResult } from "../pages/search";
import { TMapQuery, TFederalStateResultRecord } from "../pages/map";
import {
  IGrouping,
  IGroupingDocument,
  IStoredGrouping,
} from "../../../server/models/grouping";
import {
  IOrganisationDocument,
  IOrganisation,
  IStoredOrganisation,
} from "../../../server/models/organisations";
import { IStoredTransfer } from "../../../server/models/transfer";
import { IDifference } from "../../../server/models/differences";
import { IDuplicate } from "../../../server/models/duplicates";
import { mergeTransferAndFundingSearchResult } from "../helpers/search-helper";
import { TTopFundingQuery, TTopFundingResult } from "../pages/top/top-fundings";
import { TOverViewFundingResult } from "../../../server/controllers/funding";
import { IBlogDocument } from "../../../server/models/blog";
import { Blog, TBlogResult } from "../pages/blog/blog";
import {
  TFlowsFundingQuery,
  TFundingFlowsResult,
  TTimelineFundingResult,
} from "../helpers/flows-funding.helper";

const endpoint = axios.create({
  baseURL: config.host,
  responseType: "json",
});

export type GroupListEntry = {
  _id: string;
  isActive: boolean;
  name: string;
  type: string;
  group_type: string;
};

export const objectToParams = (obj: { [key: string]: any }): string => {
  //console.log(JSON.stringify(obj, null, 2));
  return Object.entries(obj)
    .reduce(
      (acc, [k, v]) => [
        ...acc,
        ...(Array.isArray(v)
          ? v
              .filter((v_) => (v_.length ? v_.length > 0 : true))
              .map((v_) => `${k}=${encodeURIComponent(v_)}`)
          : [`${k}=${encodeURIComponent(v)}`]),
      ],
      [] as any
    )
    .join("&");
};

export const fileUpload = (url: string) => (files: FileList) => {
  const formData = new FormData();
  formData.append("file", files[0]);
  //const Authorization = JSON.stringify(createAuthenticationHeader())
  return endpoint.post(url, formData, {
    headers: {
      ...createAuthenticationHeader(),
      "Content-Type": "multipart/form-data",
    },
  });
};

export const countEntities = (url: string, query?: { [key: string]: any }) =>
  endpoint
    .get<number>(
      `${url}${
        query && Object.keys(query).length > 0
          ? "?" + objectToParams(query)
          : ""
      }`,
      {
        headers: {
          ...createAuthenticationHeader(),
        },
      }
    )
    .then(({ data }) => data);

const deleteEntity = (url: string) => (id: string) =>
  endpoint
    .delete(`${url}/${id}`, {
      headers: {
        ...createAuthenticationHeader(),
      },
    })
    .then(({ data }) => data);

const getEntitiesPaginated =
  <T>(url: string) =>
  (
    page: number,
    size: number,
    sortBy: string = "",
    sortOrder: "asc" | "desc" = "asc",
    query?: { [key: string]: any }
  ) =>
    endpoint
      .get<T>(
        `${url}?page=${page}&size=${size}&sortBy=${sortBy}&sortOrder=${sortOrder}${
          query && Object.keys(query).length > 0
            ? "&" + objectToParams(query)
            : ""
        }`,
        {
          headers: {
            ...createAuthenticationHeader(),
          },
        }
      )
      .then(({ data }) => data);

export const deleteGroup = deleteEntity("/groupings");
export const deleteOrganisation = deleteEntity("/organisations");

export const getZipCodes =
  getEntitiesPaginated<IZipCodeDocument[]>("/zipcodes");
export const getOrganisations =
  getEntitiesPaginated<IOrganisationDocument[]>("/organisations");
export const getAllGroups =
  getEntitiesPaginated<IGroupingDocument[]>("/groupings/all");

export const getDifferences = () =>
  endpoint.get<IDifference[]>("/differences").then(({ data }) => data);
export const getDuplicates = () =>
  endpoint.get<IDuplicate[]>("/duplicates").then(({ data }) => data);

export const getTransferOverview = () =>
  endpoint
    .get<TOverViewTransferResult>("/transfers/overview")
    .then(({ data }) => data);

export const getFundingOverview = () =>
  endpoint
    .get<TOverViewFundingResult>("/fundings/overview")
    .then(({ data }) => data);

export const getOverview = () =>
  Promise.all([getTransferOverview(), getFundingOverview()]).then((results) => {
    return {
      transfers: results[0],
      fundings: results[1],
    } as TOverViewResult;
  });

export const getTop = (query: TTopQuery) =>
  endpoint
    .get<TTopResult>("/transfers/top?" + objectToParams(query))
    .then(({ data }) => data);

export const getPeriods = () =>
  endpoint.get<number[]>("/transfers/periods").then(({ data }) => data);

export const getFlow = (query: TFlowsQuery) =>
  endpoint
    .get<TFlowResult>("/transfers/flows?" + objectToParams(query))
    .then(({ data }) => data);

export const getTimeline = (query: TFlowsQuery) =>
  endpoint
    .get<TTimelineResult>("/transfers/timeline?" + objectToParams(query))
    .then(({ data }) => data);

export const searchTranferNames = (name: string, orgType: "org" | "media") =>
  endpoint
    .get<string[]>(
      `/transfers/searchNames?name=${encodeURIComponent(name)}&type=${orgType}`
    )
    .then(({ data }) => data);

export const searchFundingNames = (name: string) =>
  endpoint
    .get<string[]>(
      `/fundings/searchNames?name=${encodeURIComponent(name)}&type=receiver`
    )
    .then(({ data }) => data);

export const searchNames = (name: string, orgType: "org" | "media") =>
  Promise.all([searchTranferNames(name, orgType), searchFundingNames(name)])
    .then((results) => results.flat().sort())
    .then((results) => new Set(results))
    .then((results) => Array.from(results));

export const search = (name: string) =>
  Promise.all([searchTransfers(name), searchFundings(name)]).then((results) => {
    return mergeTransferAndFundingSearchResult(results[0], results[1]);
  });

export const searchTransfers = (name: string) =>
  endpoint
    .get<TSearchResult>(`/transfers/search?name=${encodeURIComponent(name)}`)
    .then(({ data }) => data);

export const searchFundings = (name: string) =>
  endpoint
    .get<TSearchResult>(`/fundings/search?name=${encodeURIComponent(name)}`)
    .then(({ data }) => data);

export const getMapData = (query: TMapQuery) =>
  endpoint
    .get<TFederalStateResultRecord[]>(
      "/transfers/federalStates?" + objectToParams(query)
    )
    .then(({ data }) => data);

// TODO: Maybe return IGroupingDocument from put and transform it into IStoredGrouping afterwards
export const updateGroup = (group: IStoredGrouping) =>
  endpoint.put<IStoredGrouping, IStoredGrouping>(
    `/groupings/${group._id}`,
    group,
    {
      headers: {
        ...createAuthenticationHeader(),
      },
    }
  );

export const updateOrganisation = (organisation: IStoredOrganisation) =>
  endpoint.put<IStoredOrganisation, IStoredOrganisation>(
    `/organisations/${organisation._id}`,
    organisation,
    {
      headers: {
        ...createAuthenticationHeader(),
      },
    }
  );

export const updateTransfer = (transfer: IStoredTransfer) =>
  endpoint.put<IStoredTransfer, IStoredTransfer>(
    `/transfers/${transfer._id}`,
    transfer,
    {
      headers: {
        ...createAuthenticationHeader(),
      },
    }
  );

const getEntity =
  <T>(url: string) =>
  (id: string) =>
    endpoint.get<T>(`/${url}/${id}`).then(({ data }) => data);

const insertEntity =
  <T1, T2>(url: string) =>
  (entity: T1) =>
    endpoint.post<T1, T2>(url, entity, {
      headers: {
        ...createAuthenticationHeader(),
      },
    })
    

export const createGroup =
  insertEntity<IGrouping, IGroupingDocument>("groupings");

export const getGroup = getEntity<IStoredGrouping>("groupings");

export const getOrganisation = getEntity<IStoredOrganisation>("organisations");
export const getTransfer = getEntity<IStoredTransfer>("transfers");
export const getTransfers =
  getEntitiesPaginated<IStoredTransfer[]>("/transfers");
export const createOrganisation =
  insertEntity<IOrganisation, IOrganisationDocument>("organisations");

export const getGroupList = () =>
  endpoint.get<GroupListEntry[]>(`/groupings/list`).then(({ data }) => data);

export const getFundings = () => endpoint.get("/fundings").then();

export const getTopFundings = (query: TTopFundingQuery) =>
  endpoint
    .get<TTopFundingResult>("/fundings/top?" + objectToParams(query))
    .then(({ data }) => data);

export const getPeriodsFunding = () =>
  endpoint
    .get<number[]>("/fundings/periods")
    .then(({ data }) =>
      Array.from(Array(data[1] - data[0] + 1).keys()).map((x) => x + data[0])
    );

export const getFlowFunding = (
  query: TFlowsFundingQuery,
  otherReceiverDisabled: boolean,
  showFundingBasis: boolean
) =>
  endpoint
    .get<TFundingFlowsResult>("/fundings/flows?" + objectToParams(query))
    .then(({ data }) =>
      otherReceiverDisabled
        ? data
        : data.filter(
            (item) =>
              item.receiver.toLowerCase() !== "other receivers" &&
              item.fundingType.toLowerCase() !== "other fundingtypes"
          )
    )
    .then((data) => data.flatMap((d) => ({ ...d, showFundingBasis })));

export const getTimelineFundings = (query: TFlowsFundingQuery) =>
  endpoint
    .get<TTimelineFundingResult>("/fundings/timeline?" + objectToParams(query))
    .then(({ data }) => data);

export const addBlog = insertEntity<Blog, IBlogDocument>("blogs");

export const getActiveBlogData = () =>
  endpoint.get<TBlogResult>("/blogs").then(({ data }) => data)


export const getBlogData = () =>
  endpoint
    .get<TBlogResult>("/blogs/all?sortBy=createDate&sortOrder=desc", { headers: createAuthenticationHeader() })
    .then(({ data }) => data)

export const deleteBlog = deleteEntity("/blogs");

export const updateBlog = (blog: Blog) =>
  endpoint.put<Blog, IBlogDocument>(`/blogs/${blog._id}`, blog, {
    headers: {
      ...createAuthenticationHeader(),
    },
  });
