class ServerConfig {
    private _host = "/api";
    private _loginURI = `${this._host}/login`;
    private _registerURI = `${this._host}/users`;
    private _tripURI = `${this._host}/trips`;
    private _myTripURI = `${this._host}/trips/mine`;
    private _poiURL = `${this._host}/pois`;
    private _usersURL = `${this._host}/users`;

    public get host(): string {
        return this._host
    };

    public get loginURI(): string {
        return this._loginURI
    };

    public get registerURI(): string {
        return this._registerURI
    };

    public get tripURI(): string {
        return this._tripURI
    };

    public get myTripURI(): string {
        return this._myTripURI
    };

    public get poiURL(): string {
        return this._poiURL
    };

    public get usersURL(): string {
        return this._usersURL
    };

}

export default new ServerConfig()