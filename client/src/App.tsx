import { useContext, useEffect } from "react";
import "./App.scss";
//import "bootstrap/dist/css/bootstrap.min.css"
//import "bootstrap/dist/js/bootstrap.bundle"
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { Login } from "./components/login_form";
import { useTranslation } from "react-i18next";
import { Register } from "./components/register_form";
import { AuthContext } from "./context/auth-context";
import { ShowUsers } from "./pages/list-users";
import { Render, LoginButton } from "./components/helper-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFileUpload,
  faMailBulk,
  faObjectGroup,
  faSearch,
  faUsersCog,
  faCreditCard,
  faBlog,
} from "@fortawesome/free-solid-svg-icons";
import { EditUser } from "./pages/user-edit";
import { FileUpload } from "./pages/file-upload";
import { ListZipCodes } from "./pages/list-zipcodes";
import { Overview } from "./pages/overview";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/esm/NavDropdown";
import Container from "react-bootstrap/Container";
import { TopTransfers } from "./pages/top/top-transfers";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import {
  getPeriods,
  GroupListEntry,
  getGroupList,
  getPeriodsFunding,
} from "./services/data-service";
import { FlowsTransfers } from "./pages/flows/flows-transfers";
import { Search } from "./pages/search";
import { Impress } from "./pages/impress";
import { AboutUs } from "./pages/about_us";
import { ShowAllGroups } from "./pages/list-groups";
import { EditGroup } from "./pages/group-edit";
import { NewGroup } from "./pages/group-new";
import { ListOrganisations } from "./pages/list-organisations";
import { Home } from "./pages/home";
import { EditOrganisation } from "./pages/organisation-edit";
import { NewOrganisation } from "./pages/organisation-new";
import { ListTransfers } from "./pages/list-transfers";
import { EditTransfer } from "./pages/transfer-edit";
import { TopFundings } from "./pages/top/top-fundings";
import { FlowsFunding } from "./pages/flows/flows-funding";
import { Top } from "./pages/top/top";
import { Flows } from "./pages/flows/flows";
import { FlowsMixed } from "./pages/flows/flows-mixed";
import { BlogPage } from "./pages/blog/blog";

export type TInfoState = {
  periods: number[];
  groups: GroupListEntry[];
  pending: boolean;
  groupsEnabled: boolean;
  otherMediaDisabled: boolean;
  periodsFunding: number[];
  mixedPeriods: number[];
  showPaymentTypeDetail: boolean;
  fundingGroupsEnabled: boolean;
  showPaymentType: boolean;
  otherReceiverFundingEnabled: boolean;
  showPaymentTypeFundingDetail: boolean;
};

export const infoSlice = createSlice({
  name: "info",
  initialState: {
    periods: [],
    groups: [],
    groupsEnabled: true,
    otherMediaDisabled: false,
    showPaymentTypeDetail: false,
    showPaymentType: false,
    pending: true,
    periodsFunding: [],
    mixedPeriods: [],
    fundingGroupsEnabled: true,
    otherReceiverFundingEnabled: false,
    showPaymentTypeFundingDetail: false,
  } as TInfoState,
  reducers: {
    setPeriods: (state, action: PayloadAction<number[]>) => ({
      ...state,
      periods: action.payload,
    }),
    setPeriodsFunding: (state, action: PayloadAction<number[]>) => ({
      ...state,
      periodsFunding: action.payload,
    }),
    setMixedPeriods: (state, action: PayloadAction<number[]>) => ({
      ...state,
      mixedPeriods: action.payload,
    }),
    setGroups: (state, action: PayloadAction<GroupListEntry[]>) => ({
      ...state,
      groups: action.payload,
    }),
    setPending: (state, action: PayloadAction<boolean>) => ({
      ...state,
      pending: action.payload,
    }),
    setGroupsEnabled: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      groupsEnabled: action.payload,
    }),
    setOtherMediaDisabledEnabled: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      otherMediaDisabled: action.payload,
    }),
    setFundingGroupsEnabled: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      fundingGroupsEnabled: action.payload,
    }),
    setShowPaymentType: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      showPaymentType: action.payload,
    }),
    setShowPaymentTypeDetail: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      showPaymentTypeDetail: action.payload,
    }),
    setOtherReceiverFundingEnabled: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      otherReceiverFundingEnabled: action.payload,
    }),
    setShowPaymentTypeFundingDetail: (state: TInfoState, action: PayloadAction<boolean>) => ({
      ...state,
      showPaymentTypeFundingDetail: action.payload,
    }),
  },
});

const { setPeriods, setPending, setGroups, setPeriodsFunding, setMixedPeriods } = infoSlice.actions;

const mergePeriods = (transferPeriod, fundingPeriods) => {
  let periodsForTimeline: number[] = [];
  const transferYears = transferPeriod.map(it => +(it.toString()).substring(0, 4)).concat(fundingPeriods.filter(it => it !== 0));

    const minPeriod = Math.min.apply(Math, transferYears);
    const maxPeriod = Math.max.apply(Math, transferYears);

    if(isFinite(minPeriod)) {
      for(let i = minPeriod; i <= maxPeriod; i++)
        periodsForTimeline.push(i);
    }
    return periodsForTimeline;
}

/*
const SearchField: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  return <Form inline>
    <FormControl type="text" placeholder="Search"
      onChange={e => {
        dispatch(searchActions.setName(e.target.value))
      }}
      onKeyPress={e => {
        console.log(e)
        if (e.key === 'Enter') {
          dispatch(searchActions.setNeedsUpdate(true))
          history.push('/search')
        }
      }} className="mr-sm-2" />
    <Button variant="outline-success">Search</Button>
  </Form>
}
*/

const App = () => {
  const { t } = useTranslation();
  const { hasRole } = useContext(AuthContext);
  const dispatch = useDispatch();

  useEffect(() => {
    Promise.all([getPeriods(), getGroupList(), getPeriodsFunding()])
      .then(([periods, groups, periodsFunding]) => {
        dispatch(setPeriods(periods));
        dispatch(setGroups(groups));
        dispatch(setPeriodsFunding(periodsFunding));
        dispatch(setMixedPeriods(mergePeriods(periods, periodsFunding)))
      })
      .finally(() => dispatch(setPending(false)));
  }, [dispatch]);
  return (
    <>
      <div className="App">
        <Router>
          <Navbar bg="light" expand="lg" data-test-id="navbar">
            <Container fluid>
              <Link to="/" className="navbar-brand">
              <img alt="" src="/img/Logo_oben.png" height="50px" />
              </Link>
              <Navbar.Toggle aria-controls="basic-navbar-nav" data-test-id="mobileNavigationToggle"/>
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                  <Link to="/overview" className="nav-link" data-test-id="overview">
                    {t("overview")}
                  </Link>
                  <Link to="/top" className="nav-link" data-test-id="topValues">
                    {t("Top Values")}
                  </Link>
                  <Link to="/flows" className="nav-link" data-test-id="moneyFlows">
                    {t("Money Flows")}
                  </Link>
                  <Render when={hasRole("admin")}>
                    <NavDropdown
                      title={t("Administration")}
                      id="basic-nav-dropdown"
                    >
                      <Link className="dropdown-item" to="/users">
                        <FontAwesomeIcon icon={faUsersCog} /> {t("Users")}
                      </Link>
                      <Link className="dropdown-item" to="/zip/upload">
                        <FontAwesomeIcon icon={faFileUpload} />{" "}
                        {t("Zip Code Upload")}
                      </Link>
                      <Link className="dropdown-item" to="/zip/list">
                        <FontAwesomeIcon icon={faMailBulk} /> {t("Zip Codes")}
                      </Link>
                      <NavDropdown.Divider />
                      <Link className="dropdown-item" to="/organisation/list">
                        <FontAwesomeIcon icon={faMailBulk} />{" "}
                        {t("Organisations")}
                      </Link>
                      <Link className="dropdown-item" to="/organisation/upload">
                        <FontAwesomeIcon icon={faFileUpload} />{" "}
                        {t("Organisation Upload")}
                      </Link>
                      <NavDropdown.Divider />
                      <Link className="dropdown-item" to="/transfer">
                        <FontAwesomeIcon icon={faCreditCard} /> {t("Transfers")}
                      </Link>
                      <Link className="dropdown-item" to="/transfer/upload">
                        <FontAwesomeIcon icon={faFileUpload} />{" "}
                        {t("Transfers Upload")}
                      </Link>
                      <Link className="dropdown-item" to="/fundings/upload">
                        <FontAwesomeIcon icon={faFileUpload} />{" "}
                        {t("Fundings Upload")}
                      </Link>
                      <NavDropdown.Divider />
                                            <Link className="dropdown-item" to="/blog">
                        <FontAwesomeIcon icon={faBlog} />{" "}
                        {t("Blog")}
                      </Link>
                    </NavDropdown>
                  </Render>
                  <Render when={hasRole("group")}>
                    <NavDropdown title={t("Grouping")} id="basic-nav-dropdown">
                      <Link className="dropdown-item" to="/groups">
                        <FontAwesomeIcon icon={faObjectGroup} />{" "}
                        {t("Groupings")}
                      </Link>
                    </NavDropdown>
                  </Render>
                  <Link to="/about" className="nav-link">
                    {t("About us")}
                  </Link>
                </Nav>
                <Nav>
                  <LoginButton
                    loginText={t("Login")}
                    logoutText={t("Logout")}
                  />
                  <Link to="/search" className="nav-link" data-test-id="search">
                    <FontAwesomeIcon icon={faSearch} />
                  </Link>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
          <header className="App-header"></header>
          <div id="content">
            <Routes>
              <Route path="/top" element={<Top />} >
                <Route index element={<TopTransfers />} />
                <Route path="transfers" element={<TopTransfers />} />
                <Route path="fundings" element={ <TopFundings />} />
              </Route>
              <Route path="/flows" element={<Flows />} >
                <Route index element={<FlowsTransfers />} />
                <Route path="transfers" element={<FlowsTransfers />} />
                <Route path="fundings" element={ <FlowsFunding />} />
                <Route path="mixed" element={ <FlowsMixed />} />
              </Route>
              <Route path="/overview" element={<Overview />} />
              <Route path="/users/:id" element={<EditUser />} />
              <Route path="/edit_groups/:id" element={<EditGroup />} />
              <Route path="/users" element={<ShowUsers />} />
              <Route path="/login_" element={<Login />} />
              <Route path="/register_" element={<Register />} />
              <Route
                path="/zip/upload"
                element={
                  <FileUpload url="/zipcodes/import" name="Zip Code Upload" />
                }
              />
              <Route path="/zip/list" element={<ListZipCodes />} />
              <Route
                path="/organisation/list"
                element={<ListOrganisations />}
              />
              <Route
                path="/organisation/upload"
                element={
                  <FileUpload
                    url="/organisations/import"
                    name="Organisations Upload"
                  />
                }
              />
              <Route path="/organisation/:id" element={<EditOrganisation />} />
              <Route path="/organisation" element={<NewOrganisation />} />
              <Route
                path="/transfer/upload"
                element={
                  <FileUpload url="/transfers/import" name="Transfers Upload" />
                }
              />
              <Route
                path="/fundings/upload"
                element={
                  <FileUpload url="/fundings/import" name="Fundings Upload" />
                }
              />
              <Route path="/transfer/:id" element={<EditTransfer />} />
              <Route path="/transfer" element={<ListTransfers />} />
              <Route path="/search" element={<Search />} />
              <Route path="/impress" element={<Impress />} />
              <Route path="/groups" element={<ShowAllGroups />} />
              <Route
                path="/newgroup/media"
                element={<NewGroup default="media" />}
              />
              <Route
                path="/newgroup/org"
                element={<NewGroup default="org" />}
              />
              <Route path="/about" element={<AboutUs />} />
              <Route path="/blog" element={<BlogPage />} />
              <Route path="/" element={<Home />} />
            </Routes>
          </div>
          <footer>
            <Link to="/impress">{t("Impress")}</Link>
            <div className="float-right">© FH JOANNEUM, Graz</div>
          </footer>
        </Router>
      </div>
    </>
  );
};

export default App;
