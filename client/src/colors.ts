const palettes = require("nice-color-palettes");
const pallete = require("color-interpolate")(palettes[81]);
const palletePF = require("color-interpolate")(palettes[20]);

export const getColor = (idx: number, count: number, offset: number = 0.25) =>
  pallete(((idx + 1) / count) * (1 - 2 * offset) + offset);

export const getPFColor = (idx: number, count: number, offset: number = 0.25) =>
  palletePF(((idx + 1) / count) * (1 - 2 * offset) + offset);
