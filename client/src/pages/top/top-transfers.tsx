import { useState, useEffect, useMemo } from 'react'
import Card from 'react-bootstrap/Card'
import { useTranslation } from 'react-i18next';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../../index';
import { getTop } from '../../services/data-service';
import BootstrapSwitchButton from 'bootstrap-switch-button-react'

import './top.css'
import { Help, IfNoError, ModalLoader, Render } from '../../components/helper-components';
import { TInfoState, infoSlice } from '../../App';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { isTouchSupported, periodToString } from '../../helpers/helpers';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import { useNavigate } from 'react-router-dom';
import { flowsSlice } from '../flows/flows-transfers';

import { Region, regions } from '../../models/models';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { ToggleSwitch } from '../../components/three-state/three-state-switch';
import Row from 'react-bootstrap/esm/Row';
import ReactECharts from 'echarts-for-react';
import { MTATooltip } from '../../components/tooltip';



/* eslint react-hooks/exhaustive-deps: 0 */

const { createSliderWithTooltip } = Slider;
const Range = createSliderWithTooltip(Slider.Range);



type TopRecord = {
    organisation: string;
    total: number;
    isGrouping?: boolean;
    children?: TopRecord[]
}

export type TTopResult = {
    top: TopRecord[],
    all: number,
    groupings?: [{
        members: string[];
        name: string;
        owner?: string;
    }]
}

export type TTopQuery = {
    groupType?: 'group' | 'class',
    federalState?: Region,
    from?: number,
    to?: number,
    pType: 2 | 4 | 31 | number[]
    resultType: 'org' | 'media',
    orgCategories?: string | string[],
    x: number
}

type TTopState = {
    data?: TTopResult,
    chartModel?: TopRecord[],
    sunburstModel: ReturnType<typeof resultToSunburstModel>,
    pending: boolean,
    groupsEnabled: boolean,
    query: TTopQuery,
    needsUpdate: boolean;
    switchState: number;
}

const resultToChartModel = (data: TTopResult): TopRecord[] => [
    ...data.top,
    {
        organisation: 'Others',
        total: (data.all - data.top.reduce((acc, { total }) => acc + total, 0))
    }
]

const resultToSunburstModel = (data: TTopResult) => [
    ...data.top.map(t => t.children ? ({
        name: t.organisation,
        value: t.total,
        percent: Math.round(t.total / data.all * 10000) / 100,
        children: t.children.map(c => ({ name: c.organisation, value: c.total, percent: Math.round(c.total / data.all * 10000) / 100 }))
    }) : ({
        name: t.organisation,
        percent: Math.round(t.total / data.all * 10000) / 100,
        value: t.total
    })),
    {
        name: 'Others',
        value: (data.all - data.top.reduce((acc, { total }) => acc + total, 0)),
        percent: Math.round((data.all - data.top.reduce((acc, { total }) => acc + total, 0)) / data.all * 10000) / 100,
    }
]


export const topSlice = createSlice({
    name: 'top',
    initialState: {
        pending: true,
        switchState: 2,
        query: {
            resultType: 'org',
            pType: 2,
            x: 10,
            groupType: 'group'
        },
        sunburstModel: [],
        groupsEnabled: true,
        needsUpdate: true
    } as TTopState,
    reducers: {
        setData: (state: TTopState, action: PayloadAction<TTopResult>) => ({
            ...state, data: action.payload,
            chartModel: resultToChartModel(action.payload),
            sunburstModel: resultToSunburstModel(action.payload),
            pending: false
        }),
        setQuery: (state: TTopState, action: PayloadAction<TTopQuery>) => ({
            ...state, query: action.payload, needsUpdate: true,
            switchState: Array.isArray(action.payload.pType) ? 0 : action.payload.pType
        }),
        setPending: (state: TTopState, action: PayloadAction<boolean>) => ({
            ...state, pending: action.payload
        }),
        setNeedsUpdate: (state: TTopState, action: PayloadAction<boolean>) => ({
            ...state, needsUpdate: action.payload
        }),

    }
})


const { setData, setQuery, setPending, setNeedsUpdate } = topSlice.actions




const flowsActions = flowsSlice.actions;


export const TopTransfers = () => {
    const { setGroupsEnabled } = infoSlice.actions
    const { t, i18n } = useTranslation();
    const [error, setError] = useState('')
    const { query, pending, needsUpdate, switchState, sunburstModel } = useSelector<AppState, TTopState>(state => state.top);
    const [[pStart, pEnd], setPeriods] = useState<number[]>([query?.from ?? 0, query?.to ?? 0]);
    const { periods, groupsEnabled } = useSelector<AppState, TInfoState>(state => state.info)
    const dispatch = useDispatch();
    const navigate = useNavigate()
    const fieldName = (type: 'org' | 'media', isGrouping?: boolean) => {
        if (type === 'org') {
            return isGrouping ? 'orgGroups' : 'organisations'
        } else {
            return isGrouping ? 'mediaGroups' : 'media'
        }
    }


    const sunBurstLabelFormatter = ({ name, value, data, status }) => {
        if (name === "Others") {
            name = t(name)
        }
        switch (status) {
            case 'emphasis':
                return `${name}\n${value.toLocaleString(i18n.language, {
                    style: 'currency',
                    currency: 'EUR',
                })} (${data.percent}%)`
            case 'normal':
                return name.length > 30 ? `${name.substring(0, 29)}...` : `${name}\n`
            default:
                return status
        }
    }

    const sunBurstOptions = useMemo(() => (
        {
            series: {
                type: 'sunburst',
                nodeClick: false,
                data: sunburstModel,
                radius: [30, '70%'],
                levels: [
                    {},
                    {
                        r0: '15%',
                        r: '70%',
                        label: {
                            rotate: 'radial',
                            align: 'right',
                            overflow: 'truncate',
                            formatter: sunBurstLabelFormatter
                        },
                        emphasis: {
                            rotate: 0,
                            focus: 'ancestor',
                            label: {
                                formatter: sunBurstLabelFormatter
                            }
                        },
                        downplay: {
                            label: {
                                rotate: 'radial',
                                formatter: sunBurstLabelFormatter
                            }
                        },
                        itemStyle: {
                            borderWidth: 3
                        }
                    }, {
                        r0: '70%',
                        r: '80%',
                        label: {
                            position: 'outside',
                            rotate: 0,
                            padding: 3,
                            silent: false,
                            formatter: sunBurstLabelFormatter
                        },
                        emphasis: {
                            rotate: 0,
                            focus: 'ancestor',
                            label: {
                                formatter: sunBurstLabelFormatter
                            }
                        },
                        downplay: {
                            label: {
                                rotate: 0,
                                formatter: sunBurstLabelFormatter
                            }
                        },
                        itemStyle: {
                            borderWidth: 3
                        }
                    }
                ],
            },
            animation: process.env.NODE_ENV !== "development"
        }
    ), [sunburstModel])

    const openDetails_ = ({ name, children }: {name:string, children?: []}) => {
        const isGrouping = children && children.length > 0
        dispatch(flowsActions.setQuery({
            organisations: [],
            media: [],
            orgGroups: [],
            mediaGroups: [],
            [fieldName(query.resultType, isGrouping)]: [name],
            pType: query.pType,
            from: query.from,
            to: query.to
        }));
        navigate('/flows')

    }


    useEffect(() => {
        if (groupsEnabled) {
            dispatch(setQuery({ ...query, groupType: 'group' }))
        } else {
            let newQuery = { ...query }
            if ("groupType" in newQuery) {
                delete newQuery.groupType
                dispatch(setQuery(newQuery))
            }
        }
    }, [groupsEnabled])
    useEffect(() => {
        if (query.from !== periods[pStart] || query.to !== periods[pEnd]) {
            setPeriods([query?.from ? periods.indexOf(query.from) : 0, query?.to ? periods.indexOf(query.to) : 0])
        }
    }, [query])
    useEffect(() => {
        if (periods && periods.length > 0) {
            //console.log("Setting from and to!!!")
            //console.log("Periods: " + JSON.stringify(periods, null, 2))
            const maxPeriodIndex = periods.length - 1
            const maxPeriod = periods[maxPeriodIndex]
            //console.log("Maxperiod: " + maxPeriod)
            if (!query.from || !query.to) {
                dispatch(setQuery({
                    ...query,
                    from: maxPeriod,
                    to: maxPeriod
                }))
                setPeriods([maxPeriodIndex, maxPeriodIndex])
            } else {
                dispatch(setNeedsUpdate(true))
            }
        }
    }, [periods])
    useEffect(() => {
        if (needsUpdate && query.to && query.from) {
            Promise.resolve(dispatch(setPending(true)))
                .then(() => getTop(query))
                .then(result => dispatch(setData(result)))
                .catch(err => {
                    setError(err?.response?.data ?? err.message);
                })
                .finally(() => { dispatch(setPending(false)); dispatch(setNeedsUpdate(false)) })
        }
    }, [needsUpdate, query])
    const updateTimescale = (v: number[]) => {
        const from = periods[v[0]];
        const to = periods[v[1]]
        if (query.from !== from || query.to !== to) {
            dispatch(setQuery({ ...query, from, to }))
        }
    }
    const updateX = (x: number) => {
        if (query.x !== x) {
            dispatch(setQuery({ ...query, x }))
        }
    }
    const updateFederalState = (federalState: Region) => {
        if (query.federalState !== federalState) {
            dispatch(setQuery({ ...query, federalState }))
        }
    }
    const toggleStates = [
        { label: t('Advertising'), value: 2 },
        { label: t('Funding'), value: 4 },
        { label: t('Both'), value: 0 },
    ];
    const handleStyle = {
        border: "5px solid #96dbfa",
        width: "20px",
        height: "20px"
    }
    return <>
        {/*<pre>{JSON.stringify(sunburstModel, null, 2)}</pre>*/}

        {/*<pre>{JSON.stringify(periods, null, 2)}</pre>*/}
        <ModalLoader isPending={pending} />
        <IfNoError error={error}>
            <Card className="top">
                <Card.Title>{t('Top Player')} ({t('from')}: {periodToString(query.from ?? 19991)}, {t('to')}: {periodToString(query.to ?? 19991)})</Card.Title>
                <Card.Body>
                    <Card.Text>
                        <div className="settings" data-test-id="topPlayerSettings">
                            <div className="row justify-content-between" >
                                <Form className="control-form">
                                    <Row>
                                        <Col xs={10} sm={4} md={3} xl={2} data-test-id="payersOrBeneficiariesToggle">
                                            <div className="label">{t('Payers or Beneficiaries')}</div>
                                            <BootstrapSwitchButton
                                                checked={query.resultType === 'org'}
                                                onlabel={t('Payers')}
                                                width={150}
                                                onstyle="info" offstyle="secondary"
                                                offlabel={t('Recipients')}
                                                onChange={(checked: boolean) => {
                                                    dispatch(setQuery({ ...query, resultType: !checked ? 'media' : 'org' }))
                                                }}
                                            />
                                        </Col>
                                        <Col xs={10} sm={8} md={4} xl={3} data-test-id="advertisingOrFundingToggle">
                                            <div className="label">{t('Advertising or Funding')}</div>
                                            <ToggleSwitch values={toggleStates} selected={switchState}
                                                onChange={pT => dispatch(setQuery({ ...query, pType: pT === 0 ? [2, 4] : pT }))}
                                            />
                                            {/* <BootstrapSwitchButton
                                                checked={query.pType == 2}
                                                onlabel={t('Advertising')}
                                                width={180}
                                                onstyle="info" offstyle="secondary"
                                                offlabel={t('Funding')}
                                                onChange={(checked: boolean) => {
                                                    dispatch(setQuery({ ...query, pType: !checked ? 4 : 2 }))
                                                }}
                                            />*/}
                                        </Col>
                                        <Col xs={10} sm={4} md={3} xl={2}>
                                            <div className="label">{t('Number')}</div>
                                            <Form.Control as="select" value={query.x} onChange={e => updateX(parseInt(e.target.value))}>
                                                <option value={5}>Top 5</option>
                                                <option value={10}>Top 10</option>
                                                <option value={15}>Top 15</option>
                                                <option value={20}>Top 20</option>
                                            </Form.Control>
                                        </Col>
                                        <Col xs={10} sm={6} md={6} xl={3} data-test-id="regionToggle">
                                            <div className="label">{t('Region')}</div>
                                            <Form.Control as="select" value={query.federalState} onChange={e => updateFederalState(e.target.value as Region)}>
                                                {regions.map(r => <option value={r}>{t(r)}</option>)}
                                            </Form.Control>
                                        </Col>
                                        <Col xs={10} md={3} xl={2} data-test-id="groupingsToggle">
                                            <div className="label">{t('Groupings')} <Help text="Allows using pre-configured groupings" /></div>
                                            <BootstrapSwitchButton checked={groupsEnabled}
                                                width={120}
                                                onstyle="info" offstyle="secondary"
                                                onlabel={t("Enabled")}
                                                onChange={(enabled: boolean) => dispatch(setGroupsEnabled(enabled))}
                                                offlabel={t("Disabled")} />
                                        </Col>
                                    </Row>
                                </Form>


                            </div>
                            <div className="row justify-content-between">
                                <div className="col-12" data-test-id="periodRange">
                                    <div className="label">{t('Period')}</div>
                                    <Render when={query.from !== undefined && query.from > 0}>
                                        <Range vertical={false} max={periods.length - 1}
                                            railStyle={{ backgroundColor: "#999" }}
                                            onAfterChange={updateTimescale}
                                            tipFormatter={index => periodToString(periods[index])}
                                            handleStyle={[handleStyle, handleStyle]}
                                            onChange={setPeriods}
                                            marks={periods.reduce((acc, v, i) => v.toString().endsWith("1") || [pStart, pEnd].includes(i) ? { ...acc, [i]: periodToString(v) } : acc, {})}
                                            value={[pStart, pEnd]} />
                                    </Render>
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <Render when={sunburstModel.length <= 1}>
                                <div className="col-12">
                                    <div className="alert alert-warning" role="alert">
                                        {t('No Data found that matches your settings')}
                                    </div>
                                </div>
                            </Render>
                            <Render when={sunburstModel.length > 1}>
                                <div className="col-12">
                                    <div className="info text-end" data-test-id="tooltipHint">
                                        <FontAwesomeIcon icon={faInfoCircle} /> {t(`${isTouchSupported() ? "Double click" : "Click"} on the chart to get more information`)}
                                    </div>
                                    {/*<pre>{JSON.stringify(query,null,2)}</pre>
                                    <pre>{JSON.stringify(periods,null,2)}</pre>*/}

                                    <MTATooltip openLink={openDetails_}>
                                        <ReactECharts option={sunBurstOptions} style={{ height: 900 }} />
                                    </MTATooltip>
                                </div>
                            </Render>
                        </div>

                    </Card.Text>
                </Card.Body>
            </Card>
        </IfNoError>
    </>
}
