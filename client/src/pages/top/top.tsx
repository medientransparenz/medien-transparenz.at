import { useState, useEffect } from "react";
import { Tab, Nav, Card } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { Link, Outlet, useLocation } from "react-router-dom";
import { InfoTooltip } from "../../components/helper-components";
import "./top.css";

export const Top = () => {
  const { t } = useTranslation();
  const [key, setKey] = useState("transfers");
  const location = useLocation();

  useEffect(() => {
    if (location.pathname.includes("fundings")) {
      setKey("fundings");
    } else {
      setKey("transfers");
    }
  }, [location]);
  return (
    <>
      <Card className="top">
        <Card.Title>
          {t("Top Player")}{" "}
          {key === "transfers" ? t("Media Transparency") : t("Fundings")}
        </Card.Title>
        {/*<Card.Title>
            {t("Top Player")} ({t("from")}: {query.from ?? 1999}, {t("to")}: {query.to ?? 1999})
</Card.Title>*/}
        <Card.Body style={{ padding: "0 1rem 1rem 1rem" }}>
          <Tab.Container
            activeKey={key}
            onSelect={(k) => setKey(k ?? "transfers")}
          >
            <Nav variant="tabs">
              <Nav.Item>
                <Nav.Link eventKey="transfers" as={Link} to={`transfers`}>
                  {t("Media Transparency")}<span style={{margin: "0 0 0 .25em"}}><InfoTooltip text="about_MT" /></span>
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="fundings" as={Link} to={`fundings`}>
                  {t("Fundings")}<span style={{margin: "0 0 0 .25em"}}><InfoTooltip text="about_PF" /></span>
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Tab.Container>
          <Tab.Content>
            <Outlet />
          </Tab.Content>
        </Card.Body>
      </Card>
    </>
  );
};
