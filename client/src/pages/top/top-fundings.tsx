import Slider from "rc-slider";
import { useEffect, useState, useMemo } from "react";
import Card from "react-bootstrap/esm/Card";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { AppState } from "../..";
import { infoSlice, TInfoState } from "../../App";
import {
  Help,
  IfNoError,
  ModalLoader,
} from "../../components/helper-components";
import { isTouchSupported } from "../../helpers/helpers";
import { Form, Col, Row } from "react-bootstrap/esm";
import BootstrapSwitchButton from "bootstrap-switch-button-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { MTATooltip } from "../../components/tooltip";
import Multiselect from "multiselect-react-dropdown";
import { getTopFundings } from "../../services/data-service";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import ReactECharts from "echarts-for-react";
import { flowsFundingSlice } from "../flows/flows-funding";

/* eslint react-hooks/exhaustive-deps: 0 */

const { createSliderWithTooltip } = Slider;
const Range = createSliderWithTooltip(Slider.Range);

type TopFundingRecord = {
  receiver: string;
  total: number;
  isGrouping?: boolean;
  children?: TopFundingRecord[];
};

export type TTopFundingResult = {
  top: TopFundingRecord[];
  all: number;
};

export type TTopFundingQuery = {
  groupType?: "group" | "class";
  from?: number;
  to?: number;
  fundingType: number[];
  x: number;
};

type TTopFundingState = {
  data?: TTopFundingResult;
  chartModel?: TopFundingRecord[];
  sunburstModel: ReturnType<typeof resultToSunburstModel>;
  pending: boolean;
  query: TTopFundingQuery;
  needsUpdate: boolean;
};

const resultToSunburstModel = (data: TTopFundingResult) => [
  ...data.top.map((t) =>
    t.children
      ? {
          name: t.receiver,
          value: t.total,
          percent: Math.round((t.total / data.all) * 10000) / 100,
          children: t.children.map((c) => ({
            name: c.receiver,
            value: c.total,
            percent: Math.round((c.total / data.all) * 10000) / 100,
          })),
        }
      : {
          name: t.receiver,
          percent: Math.round((t.total / data.all) * 10000) / 100,
          value: t.total,
        }
  ),
  {
    name: "Others",
    value: data.all - data.top.reduce((acc, { total }) => acc + total, 0),
    percent:
      Math.round(
        ((data.all - data.top.reduce((acc, { total }) => acc + total, 0)) /
          data.all) *
          10000
      ) / 100,
  },
];

const resultToChartModel = (data: TTopFundingResult): TopFundingRecord[] => [
  ...data.top,
  {
    receiver: "Others",
    total: data.all - data.top.reduce((acc, { total }) => acc + total, 0),
  },
];

const MAX_OPTION_LENGTH = 40;
const valueDecorator = (v: string) =>
  v.length > MAX_OPTION_LENGTH ? (
    <span>
      {`${v.substring(0, MAX_OPTION_LENGTH)}...`}
      <Help text={v} />
    </span>
  ) : (
    v
  );

export const topFundingSlice = createSlice({
  name: "topFunding",
  initialState: {
    pending: true,
    sunburstModel: [],
    query: {
      x: 5,
      fundingType: [0,1,2,3,4],
      groupType: "group",
    },
    needsUpdate: true,
  } as TTopFundingState,
  reducers: {
    setFundingData: (
      state: TTopFundingState,
      action: PayloadAction<TTopFundingResult>
    ) => ({
      ...state,
      data: action.payload,
      chartModel: resultToChartModel(action.payload),
      sunburstModel: resultToSunburstModel(action.payload),
      pending: false,
    }),
    setPending: (state: TTopFundingState, action: PayloadAction<boolean>) => ({
      ...state,
      pending: action.payload,
    }),
    setQuery: (
      state: TTopFundingState,
      action: PayloadAction<TTopFundingQuery>
    ) => ({
      ...state,
      query: action.payload,
      needsUpdate: true,
    }),
    setNeedsUpdate: (
      state: TTopFundingState,
      action: PayloadAction<boolean>
    ) => ({
      ...state,
      needsUpdate: action.payload,
    }),
  },
});

const { setFundingData, setPending, setQuery, setNeedsUpdate } =
  topFundingSlice.actions;

const flowsFundingActions = flowsFundingSlice.actions;

export const TopFundings = () => {
  const { setFundingGroupsEnabled } = infoSlice.actions;
  const { t, i18n } = useTranslation();
  const [error, setError] = useState("");
  const { query, pending, sunburstModel, needsUpdate } = useSelector<
    AppState,
    TTopFundingState
  >((state) => state.topFundings);
  const [[pStart, pEnd], setPeriods] = useState<number[]>([0, 0]);
  const { periodsFunding, fundingGroupsEnabled } = useSelector<
    AppState,
    TInfoState
  >((state) => state.info);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const updateX = (x: number) => {
    if (query.x !== x) {
      dispatch(setQuery({ ...query, x }));
    }
  };

  const updateFundingType = (list: string[]) => {
    setError("");
    dispatch(
      setQuery({
        ...query,
        fundingType: list.length > 0 ? list
          .filter((v) => v.length > 0)
          .map((x) => fundingBases.indexOf(x) ): fundingBases.map((x) => fundingBases.indexOf(x)),
      })
    );
    //dispatch(setNeedsUpdate())
  };

  const sunBurstLabelFormatter = ({ name, value, data, status }) => {
    if (name === "Others") {
      name = t(name);
    }
    switch (status) {
      case "emphasis":
        return `${name}\n${value.toLocaleString(i18n.language, {
          style: "currency",
          currency: "EUR",
        })} (${data.percent}%)`;
      case "normal":
        return name.length > 30 ? `${name.substring(0, 29)}...` : `${name}\n`;
      default:
        return status;
    }
  };

  const sunBurstOptions = useMemo(
    () => ({
      series: {
        type: "sunburst",
        nodeClick: false,
        data: sunburstModel,
        radius: [30, "70%"],

        levels: [
          {},
          {
            r0: "15%",
            r: "70%",
            label: {
              rotate: "radial",
              align: "right",
              overflow: "truncate",
              formatter: sunBurstLabelFormatter,
            },
            emphasis: {
              rotate: 0,
              focus: "ancestor",
              label: {
                formatter: sunBurstLabelFormatter,
              },
            },
            downplay: {
              label: {
                rotate: "radial",
                formatter: sunBurstLabelFormatter,
              },
            },
            itemStyle: {
              borderWidth: 3,
            },
          },
          {
            r0: "70%",
            r: "80%",
            label: {
              position: "outside",
              rotate: 0,
              padding: 3,
              silent: false,
              formatter: sunBurstLabelFormatter,
            },
            emphasis: {
              rotate: 0,
              focus: "ancestor",
              label: {
                formatter: sunBurstLabelFormatter,
              },
            },
            downplay: {
              label: {
                rotate: 0,
                formatter: sunBurstLabelFormatter,
              },
            },
            itemStyle: {
              borderWidth: 3,
            },
          },
        ],
      },
    }),
    [sunburstModel]
  );

  const openDetails_ = ({
    name,
    children,
  }: {
    name: string;
    children?: [];
  }) => {
    const isGrouping = children && children.length > 0;

    dispatch(
      flowsFundingActions.setQuery({
        fundingType: query.fundingType,
        receiver: [],
        receiverGroups: [],
        [isGrouping ? "receiverGroups" : "receiver"]: [name],
        from: query.from,
        to: query.to,
      })
    );
    navigate("/flows/fundings");
  };

  useEffect(() => {
    if (fundingGroupsEnabled) {
      dispatch(setQuery({ ...query, groupType: "group" }));
    } else {
      let newQuery = { ...query };
      if ("groupType" in newQuery) {
        delete newQuery.groupType;
        dispatch(setQuery(newQuery));
      }
    }
  }, [fundingGroupsEnabled]);
  useEffect(() => {
    if (
      query.from !== periodsFunding[pStart] ||
      query.to !== periodsFunding[pEnd]
    ) {
      setPeriods([
        query?.from ? periodsFunding.indexOf(query.from) : 0,
        query?.to ? periodsFunding.indexOf(query.to) : 0,
      ]);
    }
  }, [query]);

  useEffect(() => {
    if (periodsFunding && periodsFunding.length > 0) {
      const maxPeriodIndex = periodsFunding.length - 1;
      const maxPeriod = periodsFunding[maxPeriodIndex];
      if (!query.from || !query.to) {
        dispatch(
          setQuery({
            ...query,
            from: maxPeriod,
            to: maxPeriod,
          })
        );
        setPeriods([maxPeriodIndex, maxPeriodIndex]);
      } else {
        dispatch(setNeedsUpdate(true));
      }
    }
  }, [periodsFunding]);

  useEffect(() => {
    if (needsUpdate && query.to && query.from) {
      Promise.resolve(dispatch(setPending(true)))
        .then(() => getTopFundings(query))
        .then((result) => dispatch(setFundingData(result)))
        .catch((err) => {
          setError(err?.response?.data ?? err.message);
        })
        .finally(() => {
          dispatch(setPending(false));
          dispatch(setNeedsUpdate(false));
        });
    }
  }, [needsUpdate, query]);

  const fundingBases = [
    "Presseförderung",
    "Publizistikförderung",
    "Privatrundfunkfonds",
    "Nichtkommerzieller Rundfunkfonds",
    "Fernsehfonds",
  ];

  const selectionBoxStyle = {
    //chips: { background: "red" },
    searchBox: {
      border: "none",
      "border-bottom": "1px solid blue",
      "border-radius": "0px",
      "background-color": "white",
    },
  };
  const updateTimescale = (v: number[]) => {
    const from = periodsFunding[v[0]];
    const to = periodsFunding[v[1]];
    if (query.from !== from || query.to !== to) {
      dispatch(setQuery({ ...query, from, to }));
    }
  };

  const handleStyle = {
    border: "5px solid #96dbfa",
    width: "20px",
    height: "20px",
  };

  return (
    <>
      <ModalLoader isPending={pending} />
      <IfNoError error={error}>
        <Card.Body>
          <Card.Text>
            <div className="settings">
              <div className="row justify-content-between">
                <Form className="control-form">
                  <Row>
                    <Col xs={12} sm={12} md={7} lg={7} xl={7}>
                      <div className="label">{t("Funding type")}</div>
                      <Multiselect
                        id="fundingBase"
                        options={fundingBases}
                        isObject={false}
                        style={selectionBoxStyle}
                        emptyRecordMsg={t("Start typing")}
                        placeholder={t("Select a funding type")}
                        selectedValueDecorator={valueDecorator}
                        onSelect={updateFundingType}
                        onRemove={updateFundingType}
                        showCheckbox={true}
                        selectedValues={query.fundingType.map(
                          (x) => fundingBases[x]
                        )}
                      />
                    </Col>
                    <Col xs={4} sm={4} md={2} lg={2} xl={2}>
                      <div className="label">{t("Number")}</div>
                      <Form.Control
                        as="select"
                        onChange={(e) => updateX(parseInt(e.target.value))}
                      >
                        <option value={5}>Top 5</option>
                        <option value={10}>Top 10</option>
                        <option value={15}>Top 15</option>
                        <option value={20}>Top 20</option>
                      </Form.Control>
                    </Col>
                    <Col xs={8} sm={8} md={3} lg={3} xl={3}>
                      <div className="label">
                        {t("Groupings")}{" "}
                        <Help text="Allows using pre-configured groupings" />
                      </div>
                      <BootstrapSwitchButton
                        checked={fundingGroupsEnabled}
                        width={120}
                        onstyle="info"
                        offstyle="secondary"
                        onlabel={t("Enabled")}
                        onChange={(enabled: boolean) =>
                          dispatch(setFundingGroupsEnabled(enabled))
                        }
                        offlabel={t("Disabled")}
                      />
                    </Col>
                  </Row>
                </Form>
              </div>
              <div className="row justify-content-between">
                <div className="col-12">
                  <div className="label">{t("Period")}</div>
                  <Range
                    vertical={false}
                    max={periodsFunding.length - 1}
                    railStyle={{ backgroundColor: "#999" }}
                    onAfterChange={updateTimescale}
                    tipFormatter={(index) => periodsFunding[index]}
                    handleStyle={[handleStyle, handleStyle]}
                    onChange={setPeriods}
                    marks={periodsFunding.reduce(
                      (acc, v, i) =>
                        v % 2 === 0 || [pStart, pEnd].includes(i)
                          ? { ...acc, [i]: v }
                          : acc,
                      {}
                    )}
                    value={[pStart, pEnd]}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-12">
                <div className="info text-end">
                  <FontAwesomeIcon icon={faInfoCircle} />{" "}
                  {t(
                    `${
                      isTouchSupported() ? "Double click" : "Click"
                    } on the chart to get more information`
                  )}
                </div>
                {/*<pre>{JSON.stringify(query,null,2)}</pre>
                                <pre>{JSON.stringify(periods,null,2)}</pre>*/}
                <MTATooltip openLink={openDetails_}>
                  <ReactECharts
                    option={sunBurstOptions}
                    style={{ height: 900 }}
                  />
                </MTATooltip>
              </div>
            </div>
          </Card.Text>
        </Card.Body>
      </IfNoError>
    </>
  );
};
