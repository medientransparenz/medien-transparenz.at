import React, { MutableRefObject, useRef, useEffect, useState, CSSProperties } from "react";
import * as THREE from 'three';
import { FederalStateCode } from '../models/models';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from "..";
import { TInfoState } from "../App";
import * as d3 from 'd3';
import { TrackballControls } from 'three-trackballcontrols-ts';


import { getMapData } from '../services/data-service';
import { ModalLoader, Render, ShowError } from "../components/helper-components";
import Card from "react-bootstrap/esm/Card";
import { useTranslation } from 'react-i18next';
import { ExtendedFeatureCollection } from "d3";
import centroid from '@turf/centroid'
import * as d3Geo from 'd3-geo';

import './map.css'
import Col from "react-bootstrap/esm/Col";
import Form from "react-bootstrap/esm/Form";
import { ToggleSwitch } from "../components/three-state/three-state-switch";
import { periodToString } from "../helpers/helpers";
import Slider from "rc-slider";
import { topSlice } from './top/top-transfers';
import { useNavigate } from 'react-router-dom';
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Row from "react-bootstrap/esm/Row";

const { createSliderWithTooltip } = Slider;
const Range = createSliderWithTooltip(Slider.Range);

const handleStyle = {
    border: "5px solid #96dbfa",
    width: "20px",
    height: "20px"
}

const orgTypes = [
    'Company',
    'Association',
    'Chamber',
    'Education',
    'Foundation',
    'Municipality',
    'Fund',
    'Undetermined',
    'Policy-relevant',
    'Ministry',
    'City',
    'Federal state',
    'Agency',
    'Museum'
]

const topActions = topSlice.actions;

// return scaling factor that fit bounds within width/height
function fit(bounds, width, height) {
    var topLeft = bounds[0];
    var bottomRight = bounds[1];

    var w = bottomRight[0] - topLeft[0];
    var h = bottomRight[1] - topLeft[1];

    var hscale = width / w;
    var vscale = height / h;

    // pick the smallest scaling factor
    var scale = (hscale < vscale) ? hscale : vscale;

    return scale;
}

// D3.js projection from GeoJSON bounds
function getProjection(geojson, width, height) {
    // From:
    //   http://stackoverflow.com/questions/14492284/center-a-map-in-d3-given-a-geojson-object?answertab=active#tab-top
    // We are using Turf to compute centroid (turf.centroid) and bounds (turf.envelope) because
    // D3's geo.centroid and path.bounds function expect clockwise polygons, which we cannot always guarantee:
    //   https://github.com/mbostock/d3/wiki/Geo-Paths#_path

    var center = centroid(geojson).geometry.coordinates;

    var projection = d3Geo.geoMercator() //d3.geo.mercator()
        .center(center as [number, number])
        .scale(1)
        .translate([0, 0]);

    // Create the path
    var path = d3Geo.geoPath().projection(projection);

    // Using the path determine the bounds of the current map and use
    // these to determine better values for the scale and translation

    // var env = turf.envelope(geojson);
    var bounds = path.bounds(geojson);

    var scale = fit(bounds, width, height);

    // New projection
    projection = d3Geo.geoMercator()
        .center(center as [number, number])
        .scale(scale)
        .translate([0, 0]);

    return projection;
}

// Use D3.js projection to create array of Three.js points/vectors from GeoJSON ring
function ringToPoints(ring, projection) {
    return ring.map(function (point) {
        var projected = projection(point);
        return new THREE.Vector2(projected[0], projected[1]);
    });
}

// Create Three.js polygon from GeoJSON Polygon
function createPolygonShape(polygon, projection) {
    var outerRing = polygon[0];
    var points = ringToPoints(outerRing, projection);
    var polygonShape = new THREE.Shape(points);

    polygon.slice(1).forEach(function (hole) {
        points = ringToPoints(hole, projection);
        var holeShape = new THREE.Shape(points);
        polygonShape.holes.push(holeShape);
    });

    return polygonShape;
}



export type TFederalStateResultRecord = {
    amount: number
    federalState: FederalStateCode
}

export type TMapQuery = {
    from?: number;
    to?: number;
    orgTypes: string[];
    pType: [2 | 4];
}

export type MapState = {
    mapData: TFederalStateResultRecord[];
    transferSums: { [key: string]: { amount: number, height: number, percent: number } }
    query: TMapQuery;
    switchState: number
    needsUpdate: boolean;
}

const mapDataToSums = (mapData: TFederalStateResultRecord[]) => {
    const total = mapData.reduce((acc, v) => acc + v.amount, 0);
    const maxAmount = Math.max(...mapData.map(v => v.amount));
    return mapData.reduce((acc, e) => ({
        ...acc, [e.federalState]: {
            amount: e.amount,
            height: e.amount / maxAmount,
            percent: e.amount / total
        }
    }), {})
}


export const mapSlice = createSlice({
    name: "maps",
    initialState: {
        transferSums: {} as { [key: string]: { amount: number, height: number, percent: number } },
        mapData: [],
        switchState: 2,
        query: {
            orgTypes: [],
            pType: [2]
        },
        needsUpdate: false,
    } as MapState,
    reducers: {
        setData: (state, action: PayloadAction<TFederalStateResultRecord[]>) => {
            state.mapData = action.payload;
            state.transferSums = mapDataToSums(action.payload);
            state.needsUpdate = false;
        },
        setNeedsUpdate: (state, action: PayloadAction<boolean>) => {
            state.needsUpdate = action.payload;
        },
        setQuery: (state, action: PayloadAction<TMapQuery>) => {
            state.query = action.payload;
            state.switchState = Array.isArray(action.payload.pType) ? 0 : action.payload.pType;
            state.needsUpdate = true;
        }

    }
})

const { setData, setNeedsUpdate, setQuery } = mapSlice.actions;

export const SpendingMap = () => {
    const [error, setError] = useState('');
    const mapContainerRef: MutableRefObject<HTMLDivElement | null> = useRef(null);
    const [pending, setPending] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { t, i18n } = useTranslation();
    const { periods } = useSelector<AppState, TInfoState>(state => state.info);
    const [[pStart, pEnd], setPeriods] = useState<number[]>([0, 0]);
    const [selection, setSelection] = useState<{ federalState: FederalStateCode, x: number, y: number } | null>(null)
    const { needsUpdate, query, transferSums, switchState } = useSelector<AppState, MapState>(state => state.maps);

    const goToTop = (fState) => {
        dispatch(topActions.setQuery({
            resultType: 'org', from: query.from, to: query.to,
            pType: query.pType, x: 10, federalState: fState
        }))
        navigate('/top')
    }

    const toggleStates = [
        { label: t('Advertising'), value: 2 },
        { label: t('Funding'), value: 4 },
        { label: t('Both'), value: 0 },
    ];

    const defaults = {
        color: function (d) {
            switch (d.iso) {
                case "AT-1":
                    return new THREE.Color(0, 0, 0);
                case "AT-2":
                    return new THREE.Color(1, 1, 0);
                case "AT-3":
                    return new THREE.Color(1, 0, 1);
                case "AT-4":
                    return new THREE.Color(1, 0, 0);
                case "AT-5":
                    return new THREE.Color(0, 1, 1);
                case "AT-6":
                    return new THREE.Color(0, 1, 0);
                case "AT-7":
                    return new THREE.Color(0, 0, 1);
                case "AT-8":
                    return new THREE.Color(0, 0, 0);
                case "AT-9":
                    return new THREE.Color(0, 0, 1);
            }
        },
        height: function (d) {
            return (transferSums[d.iso]?.height ?? 0) * 250;
        }
    };

    const materials = {
        phong: function (color) {
            return new THREE.MeshPhongMaterial({
                color: color,
                side: THREE.DoubleSide,
                opacity: 0.5,
                transparent: true
            });
        },
        phongNotTransparent: function (color) {
            return new THREE.MeshPhongMaterial({
                color: color,
                side: THREE.DoubleSide
            });
        },
        meshLambert: function (color) {
            return new THREE.MeshLambertMaterial({
                color: color,
                //specular: 0x009900,
                //shininess: 30,
                //shading: THREE.SmoothShading,
                transparent: true
            });
        },
        meshWireFrame: function (color) {
            return new THREE.MeshBasicMaterial({
                color: color,
                //specular: 0x009900,
                reflectivity: 30,
                //shading: THREE.SmoothShading,
                wireframe: true,
                transparent: true
            });
        },
        meshBasic: function (color) {
            return new THREE.MeshBasicMaterial({
                color: color,
                //specular: 0x009900,
                reflectivity: 30,
                //shading: THREE.SmoothShading,
                transparent: true
            });
        }
    };

    const addShape = function (group, shape, extrudeSettings, material, color, x, y, z, rx, ry, rz, s, iso) {
        var geometry, mesh;
        geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
        mesh = new THREE.Mesh(geometry, materials[material](color));
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        mesh.position.set(x, y, z);
        mesh.rotation.set(rx, ry, rz);
        mesh.scale.set(s, s, s);
        mesh.userData.bundesland = iso;
        //objects.push(mesh);
        return group.add(mesh);
    };

    const addFeature = function (scene: THREE.Scene, feature, projection) {
        // var amount, color, err, extrudeSettings, group, iso, material, shape;
        const group = new THREE.Group();
        group.name = feature.properties.iso;
        scene.add(group);
        let color;
        let amount;
        try {
            color = defaults.color(feature.properties);
        } catch (error_) {
            color = new THREE.Color(0, 0, 1)
            console.log(error_);
        }
        try {
            amount = defaults.height(feature.properties);
        } catch (error_) {
            amount = 42;
            console.log(error_);
        }
        const extrudeSettings = {
            amount: amount,
            bevelEnabled: false
        };
        const material = 'phongNotTransparent';
        const iso = feature.properties.iso;
        let shape;
        if (feature.geometry.type === 'Polygon') {
            shape = createPolygonShape(feature.geometry.coordinates, projection);
            addShape(group, shape, extrudeSettings, material, color, 0, 0, amount, Math.PI, 0, 0, 1, iso);
        } else if (feature.geometry.type === 'MultiPolygon') {
            feature.geometry.coordinates.forEach(function (polygon) {
                shape = createPolygonShape(polygon, projection);
                return addShape(group, shape, extrudeSettings, material, color, 0, 0, amount, Math.PI, 0, 0, 1, iso);
            });
        } else {
            console.log('This tutorial only renders Polygons and MultiPolygons');
        }
        return group;
    };

    const update = (renderer: THREE.Renderer, camera: THREE.Camera, scene: THREE.Scene, width: number, height: number) => {
        const url = "/data/oesterreich.json";
        return d3.json<ExtendedFeatureCollection>(url).then(data => {
            var functions, projection;
            const json = data;
            //clearGroups();
            functions = {};
            functions.color = defaults.color;
            functions.height = defaults.height;
            if (json?.type === 'FeatureCollection') {
                projection = getProjection(json, width, height);
                json.features.forEach(function (feature) {
                    var group;
                    group = addFeature(scene, feature, projection);
                    return feature['_group'] = group;
                });
                renderer.render(scene, camera);
            } else {
                return console.log('This tutorial only renders GeoJSON FeatureCollections');
            }
        });
    };
    useEffect(() => {
        if (query.from !== periods[pStart] || query.to !== periods[pEnd]) {
            setPeriods([query?.from ? periods.indexOf(query.from) : 0, query?.to ? periods.indexOf(query.to) : 0])
        }
    }, [query, pEnd, pStart, periods])
    useEffect(() => {
        if (periods && periods.length > 0) {
            //console.log("Setting from and to!!!")
            //console.log("Periods: " + JSON.stringify(periods, null, 2))
            const maxPeriod = periods[periods.length - 1]
            //console.log("Maxperiod: " + maxPeriod)
            if (!query.from || !query.to) {
                dispatch(setQuery({
                    ...query,
                    from: maxPeriod,
                    to: maxPeriod
                }))
                setPeriods([maxPeriod, maxPeriod])
            } else {
                dispatch(setNeedsUpdate(true))
            }

        }
    }, [periods, dispatch, query])
    useEffect(() => {
        if (needsUpdate && query.from && query.to) {
            setPending(true);
            getMapData(query).then(data => {
                dispatch(setData(data));
            })
                .catch(
                    err => {
                        setError(err?.response?.data?.error ?? err.message)
                        dispatch(setNeedsUpdate(false))
                    })
                .finally(() => setPending(false))
        } else {
            dispatch(setNeedsUpdate(false));
        }
    }, [needsUpdate, query.from, dispatch, query])
    useEffect(() => {
        if (mapContainerRef.current) {
            /*
            var scene = new THREE.Scene();
            var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
            var renderer = new THREE.WebGLRenderer();
            renderer.setSize(mapContainerRef.current.clientWidth, window.innerHeight);
            // document.body.appendChild( renderer.domElement );
            // use ref as a mount point of the Three.js scene instead of the document.body
            mapContainerRef.current.appendChild(renderer.domElement);
            camera.position.z = 5;
            */

            const render = function () {
                return renderer.render(scene, camera);
            };

            const _height = 500 //mapContainerRef.current.clientHeight

            const onWindowResize = function () {
                if (mapContainerRef.current) {
                    camera.aspect = mapContainerRef.current.clientWidth / _height;
                    camera.updateProjectionMatrix();
                    renderer.setSize(mapContainerRef.current.clientWidth, _height);
                    controls.handleResize();
                    return render();
                }
            };

            const animate = function () {
                requestAnimationFrame(animate);
                return controls.update();
            };

            const camera = new THREE.PerspectiveCamera(80,
                mapContainerRef.current.clientWidth / _height,
                0.1, 10000);
            camera.position.set(0, -370, 380);
            camera.zoom = 0.7;
            const controls = new TrackballControls(camera as any, mapContainerRef.current);
            controls.rotateSpeed = 1.0;
            controls.zoomSpeed = 1.2;
            controls.panSpeed = 0.8;
            controls.noZoom = false;
            controls.noPan = false;
            controls.staticMoving = true;
            controls.dynamicDampingFactor = 0.3;
            controls.keys = [65, 83, 68];
            const scene = new THREE.Scene();


            const light = new THREE.DirectionalLight(0xffffff);
            light.position.set(1, 1, 1);
            scene.add(light);
            const spotLight = new THREE.SpotLight(0xffffff);
            spotLight.position.set(-1000, -1000, 1000);
            spotLight.castShadow = true;
            scene.add(spotLight);
            const ambientLight = new THREE.AmbientLight(0x333333);
            scene.add(ambientLight);
            const renderer = new THREE.WebGLRenderer({
                antialias: true
            });
            renderer.setPixelRatio(window.devicePixelRatio);
            renderer.setSize(mapContainerRef.current.clientWidth, _height);
            mapContainerRef.current.childNodes.forEach(c => c.remove());
            mapContainerRef.current.appendChild(renderer.domElement);
            renderer.shadowMap.enabled = true;
            /*
            renderer.shadowMapSoft = true;
            renderer.shadowCameraNear = 1;
            renderer.shadowCameraFar = camera.far;
            renderer.shadowCameraFov = 60;
            renderer.shadowMapBias = 0.0025;
            renderer.shadowMapDarkness = 0.5;
            renderer.shadowMapWidth = 1024;
            renderer.shadowMapHeight = 1024;
            */
            const onDocumentMouseOver = function (event) {
                let debugMode, geometry, intersects, line, mouse3D, newV, raycaster;
                debugMode = false;
                mouse3D = new THREE.Vector3(event.offsetX / event.target.clientWidth * 2 - 1, -event.offsetY / event.target.clientHeight * 2 + 1, 1.0);
                mouse3D.unproject(camera);
                mouse3D.sub(camera.position);
                mouse3D.normalize();
                raycaster = new THREE.Raycaster(camera.position, mouse3D);
                if (debugMode) {
                    console.log('camera');
                    console.log(camera.position);
                    console.log('mouse');
                    console.log(mouse3D);
                    geometry = new THREE.BufferGeometry()//THREE.Geometry();
                    geometry.vertices.push(raycaster.ray.origin);
                    newV = new THREE.Vector3(raycaster.ray.direction.x, raycaster.ray.direction.y, raycaster.ray.direction.z);
                    newV = newV.multiplyScalar(10000);
                    newV = newV.add(raycaster.ray.origin);
                    geometry.vertices.push(newV);
                    line = new THREE.Line(geometry, materials.phongNotTransparent(new THREE.Color("rgb(241, 176, 0)")));
                    scene.add(line);
                }
                intersects = raycaster.intersectObjects(scene.children.filter(r => r.type === 'Group').reduce((acc, g) => [...acc, ...g.children], [] as THREE.Object3D[]));

                //toolTipDiv = document.getElementById('toolTip');
                if (intersects.length > 0) {
                    console.log("Intersections: ", selection?.federalState)
                    setSelection({
                        federalState: intersects[0].object.userData.bundesland,
                        x: event.clientX,
                        y: event.clientY
                    })
                } else {
                    //console.log("No Selection", selection?.federalState)
                    //if (selection?.federalState) {
                    //  console.log("clear selection")
                    setSelection(null);
                    //}
                    //return toolTipDiv.style.display = 'none';
                }

            };

            mapContainerRef.current.addEventListener('mousemove', onDocumentMouseOver, false)
            //mapContainerRef.current.addEventListener('click', ()=>console.log('clicked', selection))
            renderer.setClearColor(0xffffff, 1);
            controls.addEventListener('change', () => render());
            window.addEventListener('resize', onWindowResize, false);
            onWindowResize();
            render()
            update(renderer, camera, scene, mapContainerRef.current.clientWidth, window.innerHeight)
            animate()
            //renderer.render(scene, camera)
        } else {
            console.log("No element set!")
        }
    }, [transferSums]) //eslint-disable-line react-hooks/exhaustive-deps

    const tooltipStyles: CSSProperties = {
        position: 'fixed',
        zIndex: 5
    }

    const updateTimescale = (v: number[]) => {
        const from = periods[v[0]];
        const to = periods[v[1]]
        if (query.from !== from || query.to !== to) {
            dispatch(setQuery({ ...query, from, to }))
        }
    }

    const updateOrgTypes = (oType: string) => {
        if (!query.orgTypes.includes(oType)) {
            dispatch(setQuery({ ...query, orgTypes: [oType] }))
        }
    }
    return (<>
        <ShowError error={error} onClose={() => setError('')} />
        <ModalLoader isPending={pending} />
        {/*<pre>{JSON.stringify(needsUpdate, null, 2)}</pre>*/}
        {/*<pre>{JSON.stringify(transferSums, null, 2)}</pre>*/}
        <Card className="flows" >
            <Card.Title>{t('Federal States')}</Card.Title>
            <Card.Body>
                <Card.Text>
                    <div className="settings">
                        <div className="row justify-content-between" >
                            <Form className="control-form">
                                <Form>
                                    <Row>
                                        <Col>
                                            <div className="label">{t('Advertising or Funding')}</div>
                                            <ToggleSwitch values={toggleStates} selected={switchState}
                                                onChange={pT => dispatch(setQuery({ ...query, pType: pT === 0 ? [2, 4] : pT }))}
                                            />
                                        </Col>
                                        <Col>
                                            <div className="label">{t('Organisation Type')}</div>
                                            <Form.Control as="select" value={query.orgTypes} onChange={e => updateOrgTypes(e.target.value)}>
                                                <option value={''}>{t('All')}</option>
                                                {orgTypes.map(ot => <option value={ot}>{t(ot)}</option>)}
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                </Form>
                            </Form>


                        </div>

                        <div className="row justify-content-between">
                            <div className="col-12">
                                <div className="label">{t('Period')}</div>
                                <Render when={query.from !== undefined && query.from > 0}>
                                    <Range vertical={false} max={periods.length - 1}
                                        railStyle={{ backgroundColor: "#999" }}
                                        onAfterChange={updateTimescale}
                                        tipFormatter={index => periodToString(periods[index])}
                                        handleStyle={[handleStyle, handleStyle]}
                                        onChange={setPeriods}
                                        marks={periods.reduce((acc, v, i) => ({ ...acc, [i]: periodToString(v) }), {})}
                                        value={[pStart, pEnd]} />
                                </Render>
                            </div>
                        </div>
                    </div>
                    <div className="info float-right">
                        <FontAwesomeIcon icon={faInfoCircle} /> {t("Click on the chart to get more information")}
                    </div>
                    {/*<pre>{JSON.stringify(selection,null,2)}</pre>*/}
                    <div ref={mapContainerRef} style={{ cursor: 'grab' }} /> {/* "position": "fixed"; display: "none"; z-index: 5*/}
                    {selection ?
                        <div id="toolTip" className="panel panel-default" style={{
                            ...tooltipStyles,
                            left: selection.x,
                            top: selection.y,
                            cursor: 'pointer'
                        }} onClick={() => (transferSums[selection.federalState]?.amount ?? 0) > 0 ?
                            goToTop(selection.federalState) : null} >
                            <div className="panel-heading">
                                {t(selection.federalState)}
                            </div>
                            <div className="panel-body">
                                {(transferSums[selection.federalState]?.amount ?? 0).toLocaleString(i18n.language, {
                                    style: 'currency',
                                    currency: 'EUR',
                                })}<br />
                                {((transferSums[selection.federalState]?.percent ?? 0) * 100).toFixed(2)}%
                                {(transferSums[selection.federalState]?.amount ?? 0) > 0 ?
                                    <div
                                        className="clickLink" >{t('Click here for details')}
                                    </div> : null}
                            </div>
                        </div> : null}
                </Card.Text>
            </Card.Body>
        </Card>
    </>
    )

}