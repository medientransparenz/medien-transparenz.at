import { useMemo, useEffect, useState} from 'react';
import { useTranslation } from 'react-i18next';
import DataTable, { TableColumn } from 'react-data-table-component-with-filter';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faCreditCard, faEdit, faNewspaper, faTimes, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Link, useNavigate } from 'react-router-dom';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../index';
import { IfNoError } from '../components/helper-components';
import { IGroupingDocument } from '../../../server/models/grouping';
import { countEntities, getAllGroups, deleteGroup } from '../services/data-service';
import { ConfirmDelete } from '../components/deleteDialog';


type GroupQuery = {
  name?: string;
  type?: string;
  group_type?: string;
}

interface TGroupingsState {
  groupings: IGroupingDocument[];
  needsUpdate: boolean;
  page: number;
  size: number;
  count: number;
  pending: boolean;
  query: GroupQuery;
}

export const groupingSlice = createSlice({
  name: 'users',
  initialState: {
    groupings: [],
    needsUpdate: true,
    page: 1,
    size: 10,
    count: 0,
    query: {},
    pending: false
  } as TGroupingsState,
  reducers: {
    setGroupingsList: (state, action: PayloadAction<IGroupingDocument[]>) => ({
      ...state,
      groupings: action.payload,
      needsUpdate: false
    }),
    setPage: (state, action: PayloadAction<number>) => ({ ...state, page: action.payload, needsUpdate: true }),
    setSize: (state, action: PayloadAction<number>) => ({ ...state, size: action.payload, needsUpdate: true }),
    setCount: (state, action: PayloadAction<number>) => ({ ...state, count: action.payload }),
    setNeedsUpdate: (state, action: PayloadAction<void>) => ({ ...state, needsUpdate: true }),
    setQuery: (state, action: PayloadAction<GroupQuery>) => ({ ...state, query: action.payload }),
    setPending: (state, action: PayloadAction<boolean>) => ({ ...state, pending: action.payload })
  }
})

export const { setGroupingsList, setNeedsUpdate, setCount,
  setPage, setSize, setPending, setQuery } = groupingSlice.actions;



export const ShowAllGroups = () => {
  const { groupings, needsUpdate, page,
    size, count, pending, query } = useSelector<AppState, TGroupingsState>(state => state.grouping)
  const [error, setError] = useState<string>("");
  const [groupToDelete, setGroupToDelete] = useState<IGroupingDocument | null>(null);
  const navigate = useNavigate();
  const [sortBy, setSortBy] = useState<string | null>(null)
  const [sortOrder, setSortOrder] = useState<'desc' | 'asc'>('asc')
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const namesToColumns = useMemo(() => ({
    "Name": "name",
    "Type": "type",
    "Typ": "type",
    "Group Type": "group_type",
    "Gruppentyp": "group_type"
  }), [])


  
  const columns = useMemo<TableColumn<IGroupingDocument>[]>(() => [
    {
      name: t('Name'),
      selector: row => row.name,
      sortable: true,
      filterable: true,
    },
    {
      name: t('Type'),
      selector: row => row.type,
      sortable: true,
      right: false,
      filterable: true,
    },
    {
      name: t('Group Type'),
      selector: row => row.group_type,
      sortable: true,
      filterable: true,
    },
    {
      name: t('Active'),
      selector: row => row.isActive,
      maxWidth: "100px",
      minWidth: "50px",
      cell: row => row.isActive ? <FontAwesomeIcon icon={faCheck} /> : <FontAwesomeIcon icon={faTimes} color="red" />
    },
    {
      name: t('Actions'),
      cell: row => <span>
        <Link className="btn btn-primary btn-sm" to={`/edit_groups/${row._id}`}><FontAwesomeIcon icon={faEdit} /> {t('Edit')}</Link>
        <button className="btn btn-danger btn-sm" onClick={()=>setGroupToDelete(row)}><FontAwesomeIcon icon={faTrashAlt} /> {t('Delete')}</button>
        </span>
    },
  ], [t]);
  useEffect(() => {
    countEntities('/groupings/count/all').then(c => dispatch(setCount(c)))
      .catch(err => err.message ? setError(err.message) : setError(err))
  }, [dispatch])
  useEffect(() => {
    if (groupings.length === 0 || needsUpdate) {
      dispatch(setPending(true))
      Promise.all([
        countEntities('/groupings/count/all', query),
        getAllGroups(page, size, sortBy ?? '', sortOrder, query)
      ])
        .then(([count, groups_]) => {
          dispatch(setCount(count))
          dispatch(setGroupingsList(groups_))
        }
        ).catch(err => err.message ? setError(err.message) : setError(err))
        .finally(() => dispatch(setPending(false)))
    }
  }, [needsUpdate, dispatch, groupings.length, page, query, size, sortBy, sortOrder])
  const onDelete = () => {
      if (groupToDelete && groupToDelete._id) {
        deleteGroup(groupToDelete._id.toString())
          .then(()=>dispatch(setNeedsUpdate()))
          .catch(err => setError(err))
          .finally(()=>setGroupToDelete(null))
      } else {
        setGroupToDelete(null)
      }
  }

  return <div>
    <div>
      <button className="btn btn-primary btn-sm" onClick={() => navigate('/newgroup/media')} ><FontAwesomeIcon icon={faNewspaper}/> {t("New Media Group")}</button>&nbsp;
      <button className="btn btn-primary btn-sm" onClick={() => navigate('/newgroup/org')} ><FontAwesomeIcon icon={faCreditCard}/> {t("New Organisation Group")}</button>
    </div>
    <ConfirmDelete show={!!groupToDelete} elementToDelete={groupToDelete}
     propertyToPresent="name"
     onHide={()=>setGroupToDelete(null)} onDelete={onDelete} />
    <IfNoError error={error}>
      <DataTable
        title={t("Available Groups")}
        filterServer={true}
        onFilter={e => {
          dispatch(setQuery({
            ...Object.values(e).reduce((acc, { column, value }) =>
              ({ ...acc, [namesToColumns[column.name?.toString() ?? "test"]]: value }), {})
          }))
          dispatch(setNeedsUpdate())
        }}
        columns={columns}
        data={groupings}
        pagination={true}
        progressPending={pending}
        paginationServer={true}
        onChangePage={e => dispatch(setPage(e))}
        onChangeRowsPerPage={e => dispatch(setSize(e))}
        paginationTotalRows={count}
        onSort={(column, sortDirection) => {
          setSortOrder(sortDirection)
          setSortBy(namesToColumns[column.name?.toString() ?? "test"])
          dispatch(setNeedsUpdate())
        }}
      />
    </IfNoError>
  </div>


}