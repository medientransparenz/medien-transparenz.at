import React, { useEffect, useMemo, useState } from 'react';
import { useParams } from 'react-router';
import { useTranslation } from 'react-i18next';
import { IfNoError } from '../components/helper-components';
import { BuildForm } from '../components/form-builder/standard-element-factory';
import { faUserPlus, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { FormDescription } from '../components/form-builder/field-types';
import { useDispatch } from 'react-redux';
import { setNeedsUpdate } from './list-users';
import { IStoredGrouping } from '../../../server/models/grouping';
import { getGroup, updateGroup, searchNames } from '../services/data-service';

export const EditGroup: React.FC = () => {
    const { id } = useParams();
    const [group, setGroup] = useState<IStoredGrouping | undefined>(undefined);
    const [error, setError] = useState("");
    const { t } = useTranslation();
    const search = (orgType: 'media' | 'org') => (name: string) => searchNames(name, orgType)
    const dispatch = useDispatch();
    const editGroupFormDescription = useMemo<FormDescription<IStoredGrouping>>(() => ({
        fields: [
            {
                name: "name", type: "text", label: t("Name"),
                validators: { required: "required" }, help: t("Pick a name for this group")
            },
            {
                name: "type", type: "radiobutton", label: "Type", help: t("Select the type of this group"),
                options: ['media', 'org'], disabled: true
            },
            {
                name: "isActive", type: "checkbox", label: t("Active"),
                help: t("Activate or deactivate this group")
            },
            {
                name: "group_type", type: "radiobutton", label: t("Group type"),
                options: ['group', 'class']
            },
            {
                name: "region", type: "select", label: t("Federal State"),
                options: ["EU", "AT", "AT-1", "AT-2", "AT-3", "AT-4", "AT-5", "AT-6", "AT-7", "AT-8", "AT-9"],
                labels: [t("EU"), t("AT"), t("AT-1"), t("AT-2"), t("AT-3"), t("AT-4"),
                         t("AT-5"), t("AT-6"), t("AT-7"), t("AT-8"), t("AT-9")],
                disabled: false
            },
            {
                name: "members", type: "multiselect", onSearch: search(group ? group.type : 'media'),
                emptyRecordMsg: t('Start typing'), placeHolder: ""
            }
        ],
        name: "Edit Group",
        submitHandler: (group_) => {
            updateGroup(group_).then(() => dispatch(setNeedsUpdate()))
        },
        submitAction: { label: t("Update Group"), icon: faUserPlus, nextRoute: "/groups" },
        cancelAction: { label: t("Cancel"), icon: faArrowCircleLeft, nextRoute: 'back' },
        hiddenFields: ['_id']
    }), [group, dispatch, t]);
    useEffect(() => {
        getGroup(id || "").then(setGroup)
            .catch(err => err.message ? setError(err.message) : setError(err))
    }, [id])
    return <IfNoError error={t(error)}>
        <div className="row justify-content-center">
             <div className="col-lg-4 col-md-6 col-sm-9 col-xs-12">
                <BuildForm {...editGroupFormDescription} initialState={group} ></BuildForm>
            </div>
        </div>
        <pre>{/*JSON.stringify(group,null,2)*/}</pre>
    </IfNoError>
} 