import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { FederalStateCode } from '../models/models';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../index';
import { search } from '../services/data-service';
import { IfNoError, ModalLoader } from '../components/helper-components';
import Accordion from 'react-bootstrap/Accordion';
import Badge from 'react-bootstrap/Badge';

import "./search.css"
import { useNavigate } from 'react-router-dom';
import { flowsSlice } from './flows/flows-transfers';
import { TInfoState } from '../App';
import Tooltip from 'react-bootstrap/esm/Tooltip';
import OverlayTrigger from 'react-bootstrap/esm/OverlayTrigger';

type TSearchResultDetails = {
    transferYears: number[],
    fundingYears: number[],
    total: number,
    transferTypes: [2 | 4 | 31],
    name: string,
    organisationType?: string,
    federalState?: FederalStateCode,
}

export type TSearchResult = {
    org: TSearchResultDetails[],
    media: TSearchResultDetails[]
}

export type TSearchState = {
    query: { name: string },
    results: TSearchResult,
    needsUpdate: boolean
}

export const searchSlice = createSlice({
    name: "search",
    initialState: {
        query: { name: '' },
        results: {
            org: [],
            media: []
        },
        needsUpdate: false
    } as TSearchState,
    reducers: {
        setName: (state, action: PayloadAction<string>) => ({
            ...state, query: { name: action.payload }
        }),
        setResult: (state, action: PayloadAction<TSearchResult>) => ({
            ...state, results: action.payload, needsUpdate: false
        }),
        setNeedsUpdate: (state, action: PayloadAction<boolean>) => ({
            ...state, needsUpdate: action.payload
        })
    }
});

const { setName, setResult, setNeedsUpdate } = searchSlice.actions

const MIN_LENGTH = 3;

const flowsActions = flowsSlice.actions;

export const Search = () => {
    const navigate = useNavigate();
    const { periods, periodsFunding } = useSelector<AppState, TInfoState>(state => state.info);
    const { query, results, needsUpdate } = useSelector<AppState, TSearchState>(state => state.search)
    const { t, i18n } = useTranslation();
    const dispatch = useDispatch();
    const [error, setError] = useState<string>('')
    const [pending, setPending] = useState<boolean>(false);
    const openFlow = (idx: number, oType: 'media' | 'org', type: 'transfer' | 'funding' = 'transfer', event: any) => {
        if(type === 'funding') {
          const to = periodsFunding[periodsFunding.length - 1];
          const from = periodsFunding[0];
          const entity = results[oType][idx];
          const queryString =
            `receiver=${encodeURIComponent(entity.name)}&` +
            `from=${from}&to=${to}`;
          navigate(`/flows/fundings?${queryString}`);
          event.stopPropagation();
          return;
        }

        const entity = results[oType][idx];
        //let from = parseInt(entity.years[0].toString()+'1');
        //let to = parseInt(entity.years[entity.years.length-1].toString()+'4');
        //if (periods.length > 0 && to > periods[periods.length-1]) {
        const to = periods[periods.length - 1]
        //}
        //if (periods.length > 0 && from > periods[0]) {
        const from = periods[0]
        //}
        const pType = entity.transferTypes[0];
        const queryString = `${oType === "org" ? 'organisations' : 'media'}=${encodeURIComponent(entity.name)}&` +
            `from=${from}&to=${to}&pType=${pType}`
        navigate(`/flows?${queryString}`);
        dispatch(flowsActions.setQuery({
            organisations: oType === 'org' ? [entity.name] : [],
            media: oType === 'media' ? [entity.name] : [],
            pType,
            from,
            to
        }))
        dispatch(flowsActions.setNeedsUpdate())
    }
    useEffect(() => {
        if (query.name.length >= MIN_LENGTH && needsUpdate) {
            setError('')
            setPending(true)
            search(query.name)
                .then(data => ({
                    org: [...data.org.map(o => ({ ...o, transferYears: o.transferYears.sort((a, b) => a - b) }))],
                    media: [...data.media.map(o => ({ ...o, transferYears: o.transferYears?.sort((a, b) => a - b),
                        fundingYears: o.fundingYears?.sort((a, b) => a - b) }))]
                }))
                .then(data => dispatch(setResult(data)))
                .catch(err => {
                    setError(err?.response?.data ?? err.message);
                }).finally(() => setPending(false))
        } else {
            dispatch(setNeedsUpdate(false))
        }
    }, [needsUpdate, dispatch, query.name])
    return <>
        <ModalLoader isPending={pending} />
        <div  >
            <div className="search-form">
                <div className="form-outline">
                    <input type="search" id="form1"
                        placeholder={t('Search')}
                        onChange={e => {
                            dispatch(setName(e.target.value))
                        }}
                        onKeyPress={e => {
                            console.log(e)
                            if (e.key === 'Enter') {
                                dispatch(setNeedsUpdate(true))
                            }
                        }}
                        value={query.name}
                        className="form-control" />
                </div>
                <button type="button" disabled={query.name.length < MIN_LENGTH}
                    className="btn btn-primary"
                    onClick={() => dispatch(setNeedsUpdate(true))}
                >
                    <FontAwesomeIcon icon={faSearch} />
                </button>
            </div>
            <pre>{/*JSON.stringify(query,null,2)}</pre>
            <pre>{JSON.stringify(needsUpdate,null,2)}</pre>
                <pre>{JSON.stringify(results,null,2)*/}</pre>
            <IfNoError error={error}>
                <Accordion >
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>
                            {t('Hits for Payers')}&nbsp;
                            <Badge bg="info">{results.org.length} {t('Hits')}</Badge>
                        </Accordion.Header>
                        <Accordion.Body>
                            {results.org.map((r, idx) => <div
                                onClick={e => openFlow(idx, 'org', 'transfer', e)}
                                className="d-flex justify-content-between result-line">
                                <div>
                                    {r.name}
                                    {r.transferYears.map(y => <span> <Badge bg="success" >{y}</Badge></span>)}
                                </div>
                                <div>
                                    {r.total.toLocaleString(i18n.language, {
                                        style: 'currency',
                                        currency: 'EUR',
                                    })}
                                </div>
                            </div>
                            )}
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1" data-test-id="beneficiariesHits">
                        <Accordion.Header>
                            {t('Hits for Beneficiaries')}&nbsp;
                            <Badge bg="info">{results.media.length} {t('Hits')}</Badge>
                        </Accordion.Header>

                        <Accordion.Body>

                            {results.media.map((r, idx) => <div
                                onClick={e => openFlow(idx, 'media', 'transfer', e)}
                                className="d-flex justify-content-between result-line">
                                <div>
                                    {r.name}
                                    {r.transferYears?.length > 0 ?
                                    <span style={{marginLeft: '.25em'}}>
                                        <OverlayTrigger placement="top" overlay={<Tooltip id='tooltip'>{r.transferYears.join(", ")}</Tooltip>}>
                                            <Badge  bg="success">{t("Media Transparency")}</Badge>
                                        </OverlayTrigger>
                                    </span> : <></>}
                                    {r.fundingYears?.length > 0 ?
                                        <span style={{marginLeft: '.25em'}}  onClick={e => openFlow(idx, 'media', 'funding', e)}>
                                            <OverlayTrigger placement="top" overlay={<Tooltip id='tooltip'>{r.fundingYears.join(", ")}</Tooltip>}>
                                                <Badge bg="secondary">{t("Fundings")}</Badge>
                                            </OverlayTrigger>
                                        </span> : <></>}
                                    {/* {r.transferYears.map(y => <span> <Badge bg="success" >{y}</Badge></span>)} */}
                                </div>
                                <div>
                                    {r.total.toLocaleString(i18n.language, {
                                        style: 'currency',
                                        currency: 'EUR',
                                    })}
                                </div>
                            </div>
                            )}


                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </IfNoError>
        </div>
    </>
}
