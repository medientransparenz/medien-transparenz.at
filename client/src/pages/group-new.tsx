import React, { useEffect, useMemo , useState} from 'react';
import { useTranslation } from 'react-i18next';
import { IfNoError } from '../components/helper-components';
import { BuildForm } from '../components/form-builder/standard-element-factory';
import { faUserPlus, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { FormDescription } from '../components/form-builder/field-types';
import { useDispatch } from 'react-redux';
import { setNeedsUpdate } from './list-users';
import { IGrouping } from '../../../server/models/grouping';
import { searchNames, createGroup } from '../services/data-service';

type GroupProps = {
    default: 'media' | 'org'
}    

export const NewGroup:React.FC<GroupProps> = (props) => {
    const [group,setGroup] = useState<IGrouping|undefined>(undefined);
    const [error] = useState("");
    const { t } = useTranslation();
    const search = (orgType:'media'|'org') => (name:string) => searchNames(name,orgType)
    const dispatch = useDispatch();
    const editGroupFormDescription  = useMemo<FormDescription<IGrouping>>(()=>({
        fields: [
            {name: "name", type: "text", label: t("Name"), 
                validators: { required: "required"}, help: t("Pick a name for this group")},
            {name: "type", type: "radiobutton", label: "Type", help: t("Select the type of this group"), 
            options:['media','org'], disabled:true},
            {name: "isActive", type: "checkbox", label: t("Active"),
                help: t("Activate or deactivate this group")}, 
            {name: "group_type", type: "radiobutton", label: t("Group type"), 
                options:['group','class']
            },{
                name: "region", type: "select", label: t("Federal State"),
                options: ["EU", "AT", "AT-1", "AT-2", "AT-3", "AT-4", "AT-5", "AT-6", "AT-7", "AT-8", "AT-9"],
                labels: [t("EU"), t("AT"), t("AT-1"), t("AT-2"), t("AT-3"), t("AT-4"),
                         t("AT-5"), t("AT-6"), t("AT-7"), t("AT-8"), t("AT-9")],
                disabled: false
            }, 
            { name: "members", type: "multiselect", onSearch: search(group ? group.type : 'media'),
                 emptyRecordMsg: t('Start typing'), placeHolder: ""}      
        ],
        name: t("Edit Group"),
        submitHandler: (group_) =>{ 
            createGroup(group_)
            .then(res=>dispatch(setNeedsUpdate()))
            .catch(err => {throw err})
        },
        submitAction: { label: t("Create Group"), icon: faUserPlus, nextRoute:"/groups"},
        cancelAction: { label: t("Cancel"), icon: faArrowCircleLeft, nextRoute:'back'},
    }),[group, dispatch, t]);
    useEffect(()=>{
        setGroup({
            name:"",
            type: props.default,
            group_type: 'group',
            region: 'AT',
            isActive: true,
            members: [],
            serverSide: true
        })
    },[props.default])
    return <IfNoError error={t(error)}>
        <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-9 col-xs-12">
            <BuildForm {...editGroupFormDescription} initialState={group} ></BuildForm>
            </div>
        </div> 
        <pre>{/*JSON.stringify(group,null,2)*/}</pre>
    </IfNoError>
} 