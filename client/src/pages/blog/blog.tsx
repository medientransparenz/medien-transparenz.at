import { useEffect, useMemo, useState } from "react";
import { Button, ButtonGroup, Card, Col, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { faNewspaper } from '@fortawesome/free-solid-svg-icons';
import "./blog.css"
import {
  addBlog,
  deleteBlog,
  getBlogData,
  updateBlog,
} from "../../services/data-service";
import BootstrapSwitchButton from "bootstrap-switch-button-react";
import { FormDescription } from "../../components/form-builder/field-types";
import { BuildForm } from "../../components/form-builder/standard-element-factory";


export type Blog = {
  _id?: string;
  titleGerman: string;
  titleEnglish: string;
  contentGerman: string;
  contentEnglish: string;
  createDate: Date;
  isActive: boolean;
};

export type TBlogResult = Blog[];

const Spinner: React.FC<{ visible: boolean, msg: string }> = ({ visible, msg }) =>
    visible ? (<div className="alert alert-primary" role="alert"><div className="spinner-border" role="status">
        <span className="sr-only">{msg}</span>
    </div> {msg}</div>) : null

export const BlogPage = () => {
  const { t, i18n } = useTranslation();
  const [editorData, setEditorData] = useState<Blog>({ isActive: false} as Blog);
  const [existigBlogData, setExistingBlogData] = useState<TBlogResult>();
  const [isPending, setIsPending] = useState(false);
  
  const load = () => {
    setIsPending(true)
    getBlogData()
    .then(entries => setExistingBlogData(entries))
    .finally(() => {
      setIsPending(false)
      setEditorData({ isActive: false} as Blog)
    })
  }

  const editBlogFormDecription = useMemo<FormDescription<Blog>>(() => ({
    fields: [
      {
        name: "titleGerman", type: "text", label: t("Title (German)"),
        validators: { required: "required" }
      },
      {
        name: "contentGerman", type: "html", label: t("Content (German)"),
        validators: { required: "required" }
      },
      {
        name: "titleEnglish", type: "text", label: t("Title (English)"),
        validators: { required: "required" }
      },
      {
        name: "contentEnglish", type: "html", label: t("Content (English)"),
        validators: { required: "required" }
      },
      {
        name: "isActive", type: "checkbox", label: t("is active?")
      }
    ],
    name: t(editorData._id ? "Edit Blog Entry" :"New Blog Entry"),
    submitHandler: (blog_) => (blog_._id ? updateBlog(blog_) : addBlog(blog_))
      .then(load) 
    ,
    submitAction: { label: editorData._id ? t("Edit Blog Entry"): t("Create Blog Entry"), icon: faNewspaper },
    hiddenFields: ['_id']
  }),[editorData,t])

  useEffect(load, []);


  const onDeleteBlog = (id: string) => {
    deleteBlog(id.toString()).then(load);
  };



  return (
    <>
    
      <Spinner visible={isPending} msg={t("Loading Data")} />
      <Card>
        <Card.Body>
          <Card.Title>Blogs & News</Card.Title>
            <Row className="mb-5">
            <Col>
              <BuildForm {...editBlogFormDecription} initialState={editorData} ></BuildForm>
            </Col>
          </Row>
          <Row>
            <h4>Bestehende Einträge</h4>
          </Row>
          <Row className="mb-2">
            <Col md={5}>
              <h5>Deutsch</h5>
            </Col>
            <Col md={5}>
              <h5>English</h5>
            </Col>
            <Col md={"auto"}></Col>
          </Row>
          {existigBlogData?.map((val) => (
            <Row className="mb-2" key={val._id}>
              <Col md={5}>
                <Card>
                  <Card.Title>
                    {new Date(val.createDate).toLocaleDateString(i18n.language,{}) +
                      " | " +
                      val.titleGerman}
                  </Card.Title>
                  <Card.Body>
                    <div
                      dangerouslySetInnerHTML={{ __html: val.contentGerman }}
                    ></div>
                  </Card.Body>
                </Card>
              </Col>
              <Col md={5}>
                <Card>
                  <Card.Title>
                    {new Date(val.createDate).toLocaleDateString("en",{}) +
                      " | " +
                      val.titleEnglish}
                  </Card.Title>
                  <Card.Body>
                    <div
                      dangerouslySetInnerHTML={{ __html: val.contentEnglish }}
                    ></div>
                  </Card.Body>
                </Card>
              </Col>
              <Col md={"auto"}>
                <Col>
                  <ButtonGroup className="mb-3">
                    <Button
                      variant="danger"
                      onClick={() => onDeleteBlog(val?._id ?? "")}
                    >
                      Delete
                    </Button>
                    <Button
                      variant="warning"
                      onClick={() => {
                        setEditorData(val);
                        window.scrollTo({ top: 0, behavior: "smooth" });
                      }}
                    >
                      Edit
                    </Button>
                  </ButtonGroup>
                </Col>
                <Col>
                  <BootstrapSwitchButton
                    checked={val.isActive}
                    width={120}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Enabled")}
                    onChange={(enabled: boolean) => {
                      updateBlog({ ...val, isActive: enabled })
                      .then(load)
                    }}
                    offlabel={t("Disabled")}
                  />
                </Col>
              </Col>
            </Row>
          ))}
        </Card.Body>
      </Card>
    </>
  );
};
