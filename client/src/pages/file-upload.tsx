import { faSearchPlus, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { TUploadResult } from '../../../server/models/types';

import { FormDescription } from '../components/form-builder/field-types';
import { BuildForm } from '../components/form-builder/standard-element-factory';
import { Render } from '../components/helper-components';
import { fileUpload } from '../services/data-service';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';

type TFileUpload = {
    file: FileList;
}

const Result: React.FC<{ data: TUploadResult }> =
    ({ data: { entries, savedEntries, errorEntries, transfersWithoutAddress } }) => {
        const { t } = useTranslation();
        return <>
            <FontAwesomeIcon icon={faThumbsUp} />

            <ul>
                <li>{t("Processed Entries")}: {entries}</li>
                <li>{t("Saved Entries")}: {savedEntries}</li>
                <li>{t("Number of Entries with Errors")}: {errorEntries?.length}
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header as={Button} variant="link" eventKey="0">
                                <FontAwesomeIcon icon={faSearchPlus} />
                            </Accordion.Header>
                            <Accordion.Body >
                                <div className="card card-body">
                                    <ul>
                                        {errorEntries?.map(e => <li>{JSON.stringify(e)}</li>)}
                                    </ul>
                                </div>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </li>

                <Render when={transfersWithoutAddress ? true : false}>
                    <li>{t("Missing Organisation")}: {transfersWithoutAddress?.length}
                        <Accordion>
                            <Accordion.Item eventKey="1">
                                <Accordion.Header as={Button} variant="link" eventKey="1">
                                    <FontAwesomeIcon icon={faSearchPlus} />
                                </Accordion.Header>
                            </Accordion.Item>
                            <Accordion.Body>
                                <div className="card card-body">
                                    <ul>
                                        {transfersWithoutAddress?.map(e => <li>{JSON.stringify(e)}</li>)}
                                    </ul>
                                </div>
                            </Accordion.Body>
                        </Accordion>
                    </li>

                </Render>

            </ul>
        </>
    }

export const FileUpload: React.FC<{ url: string, name: string }> = ({ url, name }) => {
    const { t } = useTranslation();

    const formDef = useMemo<FormDescription<TFileUpload>>(() => ({
        name: t(name),
        fields: [
            {
                name: 'file', validators: { required: true }, type: 'file',
                help: t("Select the file with new entries"),
                label: t("File")
            }

        ],
        submitHandler: (f) => fileUpload(url)(f.file),
        submitAction: {
            label: t("Upload Data")
        }
    }), [name]) // eslint-disable-line react-hooks/exhaustive-deps
    return <>
        <div className="row justify-content-center">
            <div className="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                <BuildForm {...formDef} Presenter={Result} />
            </div>
        </div>
    </>
}
