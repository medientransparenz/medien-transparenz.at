import { useState, useEffect, useMemo } from "react";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { useSelector, useDispatch } from "react-redux";
import { AppState } from "../..";
import {
  searchNames,
  objectToParams,
  getTimelineFundings,
} from "../../services/data-service";
import {
  Help,
  ModalLoader,
  Render,
  ShowError,
} from "../../components/helper-components";
import { ResponsiveContainer } from "recharts";
import qs from "query-string";
import { Multiselect } from "multiselect-react-dropdown";
import Card from "react-bootstrap/esm/Card";
import { useTranslation } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChartLine,
  faInfoCircle,
  faStream,
} from "@fortawesome/free-solid-svg-icons";

import { infoSlice, TInfoState } from "../../App";
import Slider from "rc-slider";
import {
  objectIncluded,
  isTouchSupported,
  isEqual,
  objectNormalize,
  toCurrency,
} from "../../helpers/helpers";
import "./flows-transfers.css";
import { useLocation, useNavigate } from "react-router-dom";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import { getPFColor } from "../../colors";
import DataTable from "react-data-table-component-with-filter";
import { ExportToExcel } from "../../components/excel-exporter";
import BootstrapSwitchButton from "bootstrap-switch-button-react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ReactECharts from "echarts-for-react";
import { addElementIfNotExist } from "../../helpers/flows-mixed.helper";
import { getColorFromPaymentDetailType } from "./flows-mixed";
import {
  getFundingFlows,
  getFundingFlowsLinks,
  TFlowsFundingQuery,
  TFundingFlowsRecord,
  TFundingFlowsResult,
  TTimelineFundingResult,
} from "../../helpers/flows-funding.helper";
/* eslint eqeqeq: 0 */
/* eslint react-hooks/exhaustive-deps: 0 */

const keysToNames = {
  receiver: "receiver",
  receiverGroups: "receiverGroups",
};

let echartRef;

export const getFundingValueFromName = (name: string): number => {
  switch (name) {
    case "Pressefoerderung":
      return 0;
    case "Publizistikfoerderung":
      return 1;
    case "Privatrundfunkfonds":
      return 2;
    case "Nichtkommerzieller Rundfunkfonds":
      return 3;
    case "Fernsehfonds":
      return 4;
    case "Werbung":
      return 22;
    default:
      return -1;
  }
};

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
// defined for new selected values const MAX_LENGTH = 30

type TSankeyDataModel = {
  name: string;
};

type TSankeyLinkModel = {
  source: string;
  target: string;
  value: number;
};

type TSankeyChartModel = {
  data: TSankeyDataModel[];
  links: TSankeyLinkModel[];
  right?: string;
  lineStyle?: {
    color: string;
  };
};

export type TYearlyFundingInfo = {
  name: string;
  entries: number;
  total: number;
};

type TSearchFundingState = {
  media: {
    length: number;
    hits: string[];
  };
};

const toFundingYearEChartsModel = (data_: TTimelineFundingResult) =>
  data_
    .map((item) => {
      return {
        year: item.year,
        ...item.yearsData.reduce(
          (acc, q) => ({
            ...acc,
            [getFundingValueFromName(q.name)]: q.total,
          }),
          { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0 }
        ),
      };
    })
    .sort((a, b) => a.year - b.year);

const receiverGroups: string[] = [];
const receivers: string[] = [];
const pfTypes: string[] = [];
const fundingBaseTypes: string[] = [];
let fundingBaseColors: Map<string, string>;

const getDataModel = (
  acc: {
    name: string;
    itemStyle?: { color: string; borderColor: string };
    label?: { position?: string; offset?: any };
  }[],
  currentValue: TFundingFlowsRecord
): TSankeyDataModel[] => {
  if (currentValue.amount) {
    acc.push({
      name: currentValue.fundingType,
      itemStyle: {
        color: getColorFromPaymentDetailType(currentValue.fundingType),
        borderColor: getColorFromPaymentDetailType(currentValue.fundingType),
      },
    });

    receivers.push(currentValue.receiver);
    acc.push({
      name: currentValue.receiver,
      label: { position: "insideRight", offset: [-20, 0] },
    });

    if (currentValue.receiverGroups) {
      receiverGroups.push(currentValue.receiverGroups);
      acc.push({ name: currentValue.receiverGroups });
    }

    if (currentValue.showFundingBasis && currentValue.fundingBasis) {
      const fundingBaseColor =
        "#" + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
      fundingBaseTypes.push(currentValue.fundingBasis);
      fundingBaseColors.set(currentValue.fundingBasis, fundingBaseColor);
      acc.push({
        name: currentValue.fundingBasis,
        itemStyle: { color: fundingBaseColor, borderColor: fundingBaseColor },
      });
    }

    pfTypes.push(currentValue.fundingType);
  }
  return acc;
};

let payerSumMap: Map<string, number> = new Map();
let receiverGroupSumMap: Map<string, number> = new Map();
let fundingBaseSumMap: Map<string, number> = new Map();
let receiverSumMap: Map<string, number> = new Map();

const dataToSankeyDataModel = (
  result: TFundingFlowsResult
): TSankeyChartModel => {
  fundingBaseColors = new Map();
  const duplicateData = Array.from(new Set(result.reduce(getDataModel, [])));

  const uniqueData = Array.from(
    new Map(duplicateData.map((v) => [JSON.stringify([v.name]), v])).values()
  );

  let res = getFundingFlowsLinks(result,fundingBaseColors);
  payerSumMap = res.payerSumMap;
  receiverGroupSumMap = res.receiverSumGroupMap;
  receiverSumMap = res.receiverSumMap;
  fundingBaseSumMap = res.fundingBaseSumMap;

  return {
    data: uniqueData,
    links: [...res.linkModel],
    right: "5%",
    label: {
      borderWidth: 0,
      color: "#000000",
    },
  } as TSankeyChartModel;
};

export type TFlowsFundingState = {
  data: TFundingFlowsResult;
  timeline: TTimelineFundingResult;
  timelineModel: ReturnType<typeof toFundingYearEChartsModel>;
  sankeyModel: ReturnType<typeof dataToSankeyDataModel>;
  query: TFlowsFundingQuery;
  pending: boolean;
  needsUpdate: boolean;
  selectedOrganisaions: string[];
  organisationsList: string[];
};

export const flowsFundingSlice = createSlice({
  name: "flowsFunding",
  initialState: {
    data: [] as TFundingFlowsResult,
    timeline: [] as any[] as TTimelineFundingResult,
    timelineModel: [],
    sankeyModel: {} as TSankeyChartModel,
    query: {
      fundingType: [0],
      receiver: [],
      receiverGroups: [],
      showFundingBasis: false,
    } as TFlowsFundingQuery,
    pending: false,
    needsUpdate: false,
    selectedOrganisaions: [],
    organisationsList: [],
  } as TFlowsFundingState,
  reducers: {
    setFlowFundingData: (
      state,
      action: PayloadAction<TFundingFlowsResult>
    ) => ({
      ...state,
      data: action.payload,
      needsUpdate: false,
      sankeyModel: dataToSankeyDataModel(action.payload),
    }),
    setFundingTimeline: (
      state,
      action: PayloadAction<TTimelineFundingResult>
    ) => ({
      ...state,
      timeline: action.payload,
      timelineModel: toFundingYearEChartsModel(action.payload),
    }),
    setPending: (state, action: PayloadAction<boolean>) => ({
      ...state,
      pending: action.payload,
    }),
    setQuery: (state, action: PayloadAction<TFlowsFundingQuery>) => ({
      ...state,
      query: { ...state.query, ...action.payload },
    }),
    setNeedsUpdate: (
      state: TFlowsFundingState,
      action: PayloadAction<boolean>
    ) => ({
      ...state,
      needsUpdate: action.payload,
    }),
  },
});

const {
  setFlowFundingData,
  setPending,
  setQuery,
  setNeedsUpdate,
  setFundingTimeline,
} = flowsFundingSlice.actions;

const MAX_OPTION_LENGTH = 40;
const valueDecorator = (v: string) =>
  v.length > MAX_OPTION_LENGTH ? (
    <span>
      {`${v.substring(0, MAX_OPTION_LENGTH)}...`}
      <Help text={v} />
    </span>
  ) : (
    v
  );

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export const FlowsFunding = () => {
  const {
    setFundingGroupsEnabled,
    setOtherReceiverFundingEnabled,
    setShowPaymentTypeFundingDetail,
  } = infoSlice.actions;
  const [error, setError] = useState("");
  const [key, setKey] = useState("flowsFunding");
  const [queryStringParsed, setQueryStringParsed] = useState(false); // eslint-disable-line @typescript-eslint/no-unused-vars
  const [[pFundingStart, pFundingEnd], setFundingPeriods] = useState<number[]>([
    0, 0,
  ]);
  const navigate = useNavigate();
  const queryParams = useQuery();
  // @ts-ignore
  const location = useLocation();
  const {
    periodsFunding,
    fundingGroupsEnabled,
    groups,
    otherReceiverFundingEnabled,
    showPaymentTypeFundingDetail,
  } = useSelector<AppState, TInfoState>((state) => state.info);
  const [search, setSearch] = useState<TSearchFundingState>({
    media: { length: 0, hits: [] },
  });
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const [loadingOptions, setLoadingOptions] = useState(false);
  const {
    pending,
    needsUpdate,
    data,
    query,
    timeline,
    timelineModel,
    sankeyModel,
  } = useSelector<AppState, TFlowsFundingState>((state) => state.flowFundings);
  const updateTimescale = () => {
    const from = periodsFunding[pFundingStart];
    const to = periodsFunding[pFundingEnd];
    if (query.from !== from || query.to !== to) {
      dispatch(setQuery({ ...query, from, to }));
    }
  };

  useEffect(() => {
    document["lastClick"] = Date.now();
  }, []);

  const handleClick = ({ data }) => {
    if (isTouchSupported()) {
      const now = Date.now();
      const lastClick = document["lastClick"];
      if (now - lastClick < 600) {
        return selectPeriod(data.period);
      } else {
        document["lastClick"] = now;
      }
    } else {
      return selectPeriod(data.period);
    }
  };

  const customToolTip = (params) => {
    const fmt = new Intl.NumberFormat(i18n.language, {
      style: "currency",
      currency: "EUR",
      minimumFractionDigits: 2,
    });
    let data = params.data;
    if (data.source) {
      let percentage = 0;
      let targetPercentage = 0;
      if(payerSumMap.has(data.source))
        percentage = (data.value / (payerSumMap.get(data.source) as number)) * 100;
      else if(receiverGroupSumMap.has(data.source))
        percentage = (data.value / (receiverGroupSumMap.get(data.source) as number)) * 100;
      else if(fundingBaseSumMap.has(data.source))
        percentage = (data.value / (fundingBaseSumMap.get(data.source) as number)) * 100;

      if(payerSumMap.has(data.target))
        targetPercentage = (data.value / (payerSumMap.get(data.target) as number)) * 100;
      else if(receiverGroupSumMap.has(data.target))
        targetPercentage = (data.value / (receiverGroupSumMap.get(data.target) as number)) * 100;
      else if(receiverSumMap.has(data.target))
        targetPercentage = (data.value / (receiverSumMap.get(data.target) as number)) * 100;
      else if(fundingBaseSumMap.has(data.target))
        targetPercentage = (data.value / (fundingBaseSumMap.get(data.target) as number)) * 100;

        return `
              <b>${data.source} ${percentage % 100 !== 0 ? '(' + percentage.toFixed(2) + '%)' : ''} → ${data.target}
              ${targetPercentage % 100 !== 0 ? '(' + targetPercentage.toFixed(2) + '%)' : ''}</b><br />
              ${fmt.format(data.value)}
        `;
    } else {
      return `
              <b>${params.data.name}</b><br />
              ${fmt.format(params.value)}
        `;
    }
  };

  const sankeyOptions = useMemo(
    () => ({
      tooltip: {
        trigger: "item",
        triggerOn: "mousemove",
        formatter: customToolTip,
      },
      series: {
        type: "sankey",
        layout: "none",
        emphasis: {
          focus: "adjacency",
        },
        ...sankeyModel,
      },
    }),
    [t, sankeyModel]
  );

  const timelineOptions = useMemo(
    () => ({
      legend: {},
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "cross",
          crossStyle: {
            color: "#999",
          },
        },
        formatter: (params) => {
          params = params instanceof Array ? params : [params];
          const includesForecast =
            params.filter((e) => e.data.prediction).length > 0;
          const fmt = new Intl.NumberFormat(i18n.language, {
            style: "currency",
            currency: "EUR",
            minimumFractionDigits: 2,
          });
          let d = params
            .map(
              (p) =>
                `${p.marker} ${p.seriesName}${
                  p.data["prediction"] ? "*" : ""
                } <span style="float:right;margin-left:20px;font-size:14px;color:${
                  p.data["prediction"] ? "#aaa" : "#666"
                };font-weight:900">${fmt.format(
                  p.data[p.dimensionNames[p.encode.y]]
                )}</span>`
            )
            .join("<br/>");
          if (params.length > 1) {
            if (!includesForecast) {
              const s = params
                .filter((e) => !e.data.prediction)
                .reduce(
                  (acc, p) => acc + p.data[p.dimensionNames[p.encode.y]],
                  0
                );
              d = `${d}<br/><span style="font-weight:900">${t(
                "Total"
              )}</span><span style="float:right;margin-left:20px;font-size:14px;color:#666;font-weight:900">${fmt.format(
                s
              )}</span>`;
            } else {
              const s = params.reduce(
                (acc, p) => acc + p.data[p.dimensionNames[p.encode.y]],
                0
              );
              d = `${d}<br/>${t(
                "Total"
              )}*<span style="float:right;margin-left:20px;font-size:14px;color:#aaa;font-weight:900">${fmt.format(
                s
              )}</span>`;
              d += `<br>*) ${t("Forecast")}`;
            }
          } else if (includesForecast) {
            d += `<br>*) ${t("Forecast")}`;
          }
          return `${params[0].name}<br/>${d}`;
        },
      },
      toolbox: {
        show: true,
        orient: "horizontal",
        left: "center",
        top: "bottom",
        feature: {
          mark: { show: true },
          dataView: { show: false, readOnly: false },
          magicType: { show: true, type: ["line", "bar", "stack"] },
          restore: { show: false },
          saveAsImage: { show: false },
        },
      },
      xAxis: { type: "category" },
      yAxis: {
        axisLabel: {
          formatter: (value) => `${value / 1000000} Mio.`,
        },
      },
      dataset: [
        {
          dimensions: ["year", "0", "1", "2", "3", "4"],
          source: timelineModel,
        },
      ],
      series: [
        {
          type: "bar",
          name: t("pref"),
          emphasis: { focus: "series" },
          color: getPFColor(1, 4),
        },
        {
          type: "bar",
          name: t("pubf"),
          emphasis: { focus: "series" },
          color: getPFColor(2, 4),
        },
        {
          type: "bar",
          name: t("prif"),
          emphasis: { focus: "series" },
          color: getPFColor(3, 4),
        },
        {
          type: "bar",
          name: t("nikorf"),
          emphasis: { focus: "series" },
          color: getPFColor(4, 4),
        },
        {
          type: "bar",
          name: t("ferf"),
          emphasis: { focus: "series" },
          color: getPFColor(5, 4),
        },
      ],
    }),
    [timelineModel, t]
  );

  useEffect(() => {
    if (periodsFunding && periodsFunding.length > 0) {
      const maxPeriodIndex = periodsFunding.length - 1;
      const maxPeriod = periodsFunding[maxPeriodIndex];
      if (!query.from || !query.to) {
        dispatch(
          setQuery({
            ...query,
            from: maxPeriod,
            to: maxPeriod,
          })
        );
        setFundingPeriods([maxPeriodIndex, maxPeriodIndex]);
      } else if(query.from && query.to) {
        const minIndex = periodsFunding.findIndex(it => it === query.from);
        const maxIndex = periodsFunding.findIndex(it => it === query.to);
        dispatch(setNeedsUpdate(true));
        setFundingPeriods([minIndex < 0 ? 0 : minIndex, maxIndex < 0 ? periodsFunding.length -1 : maxIndex]);
      } else {
        dispatch(setNeedsUpdate(true));
      }
    }
  }, [periodsFunding]);

  // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (
      periodsFunding &&
      periodsFunding.length > 0 &&
      query.fundingType.length +
        query.receiver.length +
        (query.receiverGroups ?? []).length >
        0 &&
      query.from &&
      query.to
    ) {
      const queryString = objectToParams(query);
      const currentQueryStringObj = qs.parse(location.search);
      if (
        !isEqual(objectNormalize(currentQueryStringObj), objectNormalize(query))
      ) {
        navigate(`/flows/fundings?${queryString}`);
      }
      //    }
      if(query.from && query.to) {
        const minIndex = periodsFunding.findIndex(it => it === query.from);
        const maxIndex = periodsFunding.findIndex(it => it === query.to);
        setFundingPeriods([minIndex < 0 ? 0 : minIndex, maxIndex < 0 ? periodsFunding.length -1 : maxIndex]);
      }
      dispatch(setNeedsUpdate(true));
    }
  }, [query]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    let q = {
      fundingType: [0],
      receiver: [],
      receiverGroups: [],
    };
    ["receiver", "fundingType", "receiverGroups"].forEach((org) => {
      if (queryParams.getAll(org).length > 0) {
        q[org] = queryParams.getAll(org).filter((e) => e.length > 0);
      }
    });
    ["from", "to"].forEach((org) => {
      if (queryParams.get(org)) {
        q[org] = parseInt(queryParams.get(org) ?? "0");
      }
    });
    if (
      query.receiver.length +
        query.fundingType.length +
        (query.receiverGroups ?? []).length >
        0 &&
      q["from"] &&
      q["to"]
    ) {
      if (!objectIncluded(q, query)) {
        dispatch(setQuery({ ...query, ...q }));
      }
    }
    const startPeriod_ = periodsFunding.indexOf(q["from"]);
    const endPeriod_ = periodsFunding.indexOf(q["to"]);
    if (startPeriod_ >= 0 && endPeriod_ >= 0) {
      setFundingPeriods([startPeriod_, endPeriod_]);
    }
    setQueryStringParsed(true);
    //dispatch(setNeedsUpdate());
  }, [location]);

  useEffect(() => {
    if (
      needsUpdate &&
      query.fundingType.length +
        query.receiver.length +
        (query.receiverGroups ?? []).length >
        0 &&
      query.from &&
      query.to
    ) {
      setError("");
      dispatch(setPending(true));
      Promise.all([
        getTimelineFundings(query),
        getFundingFlows(
          query,
          showPaymentTypeFundingDetail,
          otherReceiverFundingEnabled
        ),
      ])
        .then(([timelineData_, flowData_]) => {
          dispatch(setFlowFundingData(flowData_));
          dispatch(setFundingTimeline(timelineData_));
        })
        .catch((err) => {
          setError(err?.response?.data ?? err.message);
        })
        .finally(() => {
          dispatch(setPending(false));
          dispatch(setNeedsUpdate(false));
        });
    }
  }, [needsUpdate]); // eslint-disable-line react-hooks/exhaustive-deps

  const colums = useMemo(
    () => [
      {
        name: t("PF_Type"),
        selector: (row) => row["fundingType"],
        sortable: true,
      },
      {
        name: t("Beneficiary"),
        selector: (row) => row["receiver"],
        sortable: true,
      },
      {
        name: t("Fundingbasis"),
        selector: (row) => row["fundingBasis"],
        sortable: true,
      },
      /* {
            name: t("Payment type"),
            selector: row => row["transferType"],
            sortable: true,
            format: toPaymentTypeText('transferType'),
            width: "10em"
        },  */ {
        name: t("Amount"),
        selector: (row) => row["amount"],
        sortable: true,
        format: toCurrency("amount"),
        right: true,
      },
    ],
    [t]
  ); // eslint-disable-line react-hooks/exhaustive-deps

  const populateList = (orgType: "org" | "media") => (name: string) => {
    if (loadingOptions) return;
    if (name.length > search[orgType].length && name.length === 3) {
      setLoadingOptions(true);
      searchNames(name, orgType)
        .then((names) =>
          setSearch({
            ...search,
            [orgType]: { length: name.length, hits: names },
          })
        )
        .finally(() => setLoadingOptions(false));
      return ["A", "B", "C"];
    }
    if (name.length < search[orgType].length && name.length < 3) {
      setSearch({ ...search, [orgType]: { length: name.length, hits: [] } });
      return [];
    }
  };
  const updateSelection =
    (orgType: "receiver" | "receiverGroups") => (list: string[]) => {
      setError("");
      dispatch(
        setQuery({
          ...query,
          [keysToNames[orgType]]: list.filter((v) => v.length > 0),
        })
      );
    };

  const selectionBoxStyle = {
    //chips: { background: "red" },
    searchBox: {
      border: "none",
      "border-bottom": "1px solid blue",
      "border-radius": "0px",
      "background-color": "white",
    },
  };
  const selectPeriod = (period) => {
    const p = period.slice(3) + period.slice(1, 2);
    dispatch(setQuery({ ...query, from: p, to: p }));
    setKey("flowsFunding");
  };
  const handleStyle = {
    border: "5px solid #96dbfa",
    width: "20px",
    height: "20px",
  };

  const fundingBases = [
    "Pressefoerderung",
    "Publizistikfoerderung",
    "Privatrundfunkfonds",
    "Nichtkommerzieller Rundfunkfonds",
    "Fernsehfonds",
  ];

  const updateFundingType = (list: string[]) => {
    setError("");
    dispatch(
      setQuery({
        ...query,
        fundingType:
          list.length > 0
            ? list
                .filter((v) => v.length > 0)
                .map((x) => fundingBases.indexOf(x))
            : fundingBases.map((x) => fundingBases.indexOf(x)),
      })
    );
    //dispatch(setNeedsUpdate())
  };

  const onChartMouseMove = (params) => {
    const echartInstance = echartRef.getEchartsInstance();
    echartInstance.getZr().setCursorStyle('pointer');
  }

  const onChartClick = (params) => {
    if (params.data.source?.includes("Andere") || params.data.name?.includes("Andere") || params.data.target?.includes("Andere"))
      return;

    const fundingBaseName = params.data.name ?? params.data.source;
    if(fundingBaseTypes.includes(fundingBaseName)) {
      const link = sankeyOptions.series.links.find(it => it.target === fundingBaseName);
    if(link) {
        if(receiverGroups.includes(params.data.target)) {
          dispatch(setQuery({...query, receiverGroups: addElementIfNotExist(query.receiverGroups, params.data.target),
          receiver: [], fundingType: [getFundingValueFromName(link.source)]}))
        } else if (receivers.includes(params.data.target)) {
          dispatch(setQuery({...query, receiverGroups: [],
            receiver: addElementIfNotExist(query.receiver, params.data.target), fundingType: [getFundingValueFromName(link.source)]}))
        } else {
          dispatch(setQuery({...query, receiverGroups: [],
            receiver: [], fundingType: [getFundingValueFromName(link.source)]}))
        }
      }
    }
    else if ((params.data.name && pfTypes.includes(params.data.name))) {
      const nam = params.data.name ? params.data.name : params.data.source;
        dispatch(
          setQuery({
            ...query,
            fundingType: [getFundingValueFromName(
            nam
          )],
            receiverGroups: [],
            receiver: [],
          })
        )

    } else if ((params.data.source && pfTypes.includes(params.data.source))) {
      const rec = params.data.target;
      const ft = params.data.source;
      if (receiverGroups.includes(rec)) {
        dispatch(
          setQuery({
            ...query,
            receiverGroups: addElementIfNotExist(query.receiverGroups, rec),
            receiver: [],
            fundingType: [getFundingValueFromName(
            ft
          )]
          })
        );
      } else if (receivers.includes(rec)) {
        dispatch(
          setQuery({
            ...query,
            receiver: addElementIfNotExist(query.receiver, rec),
            receiverGroups: [],
            fundingType: [getFundingValueFromName(
            ft
          )]
          })
        );
      }
    }
     else {
      const nam = params.data.name ? params.data.name : params.data.target;
      if (receiverGroups.includes(nam)) {
        dispatch(
          setQuery({
            ...query,
            receiverGroups: addElementIfNotExist(query.receiverGroups, nam),
            receiver: [],
            fundingType: [0,1,2,3,4]
          })
        );
      } else if (receivers.includes(nam)) {
        dispatch(
          setQuery({
            ...query,
            receiver: addElementIfNotExist(query.receiver, nam),
            receiverGroups: [],
            fundingType: [0,1,2,3,4]
          })
        );
      }
    }
  };

  const onEvents = {
    click: onChartClick,
        mousemove: onChartMouseMove
  };

  return (
    <>
      <ShowError error={error} onClose={() => setError("")} />
      <ModalLoader isPending={pending} />
      <Card.Text>
        {/*
                    <pre>{JSON.stringify(periods,null,2)}</pre>
                    <pre>{JSON.stringify(periods,null,2)}</pre>
                    <pre>{startPeriod} - {endPeriod}</pre>
                    */}
        <div className="settings">
          <div className="row justify-content-between">
            <div className="col-lg-4 col-md-5, col-12">
              <div className="label">{t("Funding type")}</div>

              <Multiselect
                id="fundingType"
                options={fundingBases}
                isObject={false}
                style={selectionBoxStyle}
                emptyRecordMsg={t("Start typing")}
                placeholder={t("Select a funding type")}
                selectedValueDecorator={valueDecorator}
                onSelect={updateFundingType}
                onRemove={updateFundingType}
                showCheckbox={true}
                selectedValues={query.fundingType.map((x) => fundingBases[x])}
              />
            </div>
            <div className="col-lg-3 col-md-2 col-12 ">
              <Row>
                <Col>
                  <div className="label">
                    {t("Groupings")}{" "}
                    <Help text="Allows using pre-configured groupings" />
                  </div>
                  <BootstrapSwitchButton
                    checked={fundingGroupsEnabled}
                    width={140}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Enabled")}
                    onChange={(enabled: boolean) => {
                      dispatch(setFundingGroupsEnabled(enabled));

                      dispatch(
                        setQuery({
                          ...query,
                          receiverGroups: [],
                        })
                      );
                    }}
                    offlabel={t("Disabled")}
                  />
                </Col>
                <Col>
                  <div className="label">
                    {t("show_details_funding")}{" "}
                    <Help text={t("Show details funding long")} />
                  </div>
                  <BootstrapSwitchButton
                    checked={showPaymentTypeFundingDetail}
                    width={140}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Displayed")}
                    onChange={(enabled: boolean) => {
                      dispatch(setShowPaymentTypeFundingDetail(enabled));

                      dispatch(
                        setQuery({
                          ...query,
                          showFundingBasis: enabled,
                        })
                      );
                    }}
                    offlabel={t("Hidden")}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="label">
                    {t("Unhide others")} <Help text="Hide other recipients" />
                  </div>
                  <BootstrapSwitchButton
                    checked={otherReceiverFundingEnabled}
                    width={140}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Displayed")}
                    onChange={(enabled: boolean) => {
                      dispatch(setOtherReceiverFundingEnabled(enabled));
                      dispatch(setQuery({ ...query }));
                    }}
                    offlabel={t("Hidden")}
                  />
                </Col>
              </Row>
            </div>
            <div className="col-lg-4 col-md-5, col-12 ">
              <div className="label">{t("Beneficiaries")}</div>
              <Multiselect
                onSearch={populateList("media")}
                id="recList"
                options={search.media.hits}
                isObject={false}
                emptyRecordMsg={t("Start typing")}
                selectedValues={query.receiver}
                style={selectionBoxStyle}
                loading={loadingOptions}
                onSelect={updateSelection("receiver")}
                onRemove={updateSelection("receiver")}
                placeholder={t("Select a Beneficiary")}
                selectedValueDecorator={valueDecorator}
              />
              <Render when={fundingGroupsEnabled}>
                <div className="label">{t("Groups")}</div>
                <Multiselect
                  id="recGroups"
                  options={groups
                    .filter(
                      (g) => g.type === "media" && g.group_type === "group"
                    )
                    .map((g) => g.name)}
                  isObject={false}
                  selectedValues={query.receiverGroups}
                  style={selectionBoxStyle}
                  onSelect={updateSelection("receiverGroups")}
                  onRemove={updateSelection("receiverGroups")}
                  placeholder={t("Select a Group")}
                  selectedValueDecorator={valueDecorator}
                />
              </Render>
            </div>
          </div>
          <div className="row justify-content-between">
            <div className="col-12">
              <div className="label">{t("Period")}</div>
              <Range
                vertical={false}
                max={periodsFunding.length - 1}
                railStyle={{ backgroundColor: "#999" }}
                onAfterChange={updateTimescale}
                tipFormatter={(index) => periodsFunding[index]}
                handleStyle={[handleStyle, handleStyle]}
                onChange={setFundingPeriods}
                marks={periodsFunding.reduce(
                  (acc, v, i) =>
                    v % 2 === 0 || [pFundingStart, pFundingEnd].includes(i)
                      ? { ...acc, [i]: v }
                      : acc,
                  {}
                )}
                value={[pFundingStart, pFundingEnd]}
              />
            </div>
          </div>
        </div>
        <Render when={data?.length === 0}>
          <p></p>
          <div className="alert alert-warning" role="alert">
            <p>{t("No Data found that matches your settings")}.</p>
          </div>
        </Render>
        <Tabs
          id="flow-timeline-tabs"
          activeKey={key}
          onSelect={(k) => setKey(k ?? "flowsFunding")}
        >
          <Tab
            eventKey="flowsFunding"
            title={
              <>
                <FontAwesomeIcon icon={faStream} /> {t("Flows")}
              </>
            }
          >
            {/*<pre>{JSON.stringify(query,null,2)}</pre>*/}
            <Render when={data?.length > 0 && !error}>
              <div className="text-end info">
                <FontAwesomeIcon icon={faInfoCircle} />{" "}
                {t(
                  `${
                    isTouchSupported() ? "Double click" : "Click"
                  } on the chart to get more information`
                )}
              </div>

              <ResponsiveContainer
                width="100%"
                height={
                  data.length * 20 < 1000
                    ? 1000
                    : sankeyOptions.series.links.length * 20
                }
              >
                <ReactECharts
                  option={sankeyOptions}
                  ref={(e) => { echartRef = e; }}
                  style={{ height: "100%", width: "100%" }}
                  onEvents={onEvents}
                />
              </ResponsiveContainer>
              <DataTable
                title={t("Money Flows")}
                pagination={true}
                columns={colums}
                data={data}
                actions={<ExportToExcel data={data} fileName="MoneyFlow" />}
              />
            </Render>
          </Tab>
          <Tab
            eventKey="timeline"
            title={
              <>
                <FontAwesomeIcon icon={faChartLine} /> {t("Timeline")}
              </>
            }
          >
            <Render when={timeline?.length > 0 && !error}>
              <div className="text-end info">
                <FontAwesomeIcon icon={faInfoCircle} />{" "}
                {t(
                  `${
                    isTouchSupported() ? "Double click" : "Click"
                  } on the chart to get more information`
                )}
              </div>
              <ReactECharts
                option={timelineOptions}
                onEvents={{ click: handleClick }}
                style={{ height: 450 }}
              />
            </Render>
          </Tab>
        </Tabs>
      </Card.Text>
    </>
  );
};
