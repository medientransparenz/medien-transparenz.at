import { useState, useEffect, useMemo } from "react";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { useSelector, useDispatch } from "react-redux";
import { AppState } from "../..";
import { objectToParams, searchNames } from "../../services/data-service";
import {
  Help,
  ModalLoader,
  Render,
  ShowError,
} from "../../components/helper-components";
import { ResponsiveContainer } from "recharts";

import { Multiselect } from "multiselect-react-dropdown";
import Card from "react-bootstrap/esm/Card";
import { useTranslation } from "react-i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChartLine,
  faInfoCircle,
  faStream,
} from "@fortawesome/free-solid-svg-icons";

import { infoSlice, TInfoState } from "../../App";
import Slider from "rc-slider";
import {
  objectIncluded,
  isTouchSupported,
  isEqual,
  objectNormalize,
  toCurrency,
} from "../../helpers/helpers";
import "./flows-transfers.css";
import { useLocation, useNavigate } from "react-router-dom";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import { getColor, getPFColor } from "../../colors";
import DataTable from "react-data-table-component-with-filter";
import { ExportToExcel } from "../../components/excel-exporter";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ReactECharts from "echarts-for-react";
import {
  addElementIfNotExist,
  getMixedFlows,
  getMixedFlowsLinks,
  TMixedFlowsQuery,
  TMixedFlowsRecord,
  TMixedFlowsResult,
  getMixedTimeline,
} from "../../helpers/flows-mixed.helper";
import BootstrapSwitchButton from "bootstrap-switch-button-react";
import qs from "query-string";
import {
  getFundingValueFromName,
} from "./flows-funding";
import { TTimelineFundingResult } from "../../helpers/flows-funding.helper";

/* eslint eqeqeq: 0 */
/* eslint react-hooks/exhaustive-deps: 0 */

const keysToNames = {
  receiver: "receiver",
  receiverGroups: "receiverGroups",
  org: "org",
  orgGroups: "orgGroups",
};
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
// defined for new selected values const MAX_LENGTH = 30

let echartRef;

type TSankeyDataModel = {
  name: string;
};

type TSankeyLinkModel = {
  source: string;
  target: string;
  value: number;
};

type TSankeyChartModel = {
  data: TSankeyDataModel[];
  links: TSankeyLinkModel[];
  right?: string;
  lineStyle?: {
    color: string;
  };
};

export type TTimelineResult = { period: string; total: number }[];

type TSearchFundingState = {
  org: {
    length: number;
    hits: string[];
  };
  media: {
    length: number;
    hits: string[];
  };
};


const timelineToEcharModel = (data: TTimelineFundingResult) => {
  const result = data
    .map((item) => {
      return {
        year: item.year,
        ...item.yearsData.reduce(
          (acc, q) => ({
            ...acc,
            [getFundingValueFromName(q.name)]: q.total,
          }),
          { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 22: 0 }
        ),
      };
    })
    .sort((a, b) => a.year - b.year);
  return result;
};

export const getColorFromPaymentDetailType = (paymentType: string): string => {
  switch (paymentType) {
    case "Pressefoerderung":
      return getPFColor(1, 4);
    case "Publizistikfoerderung":
      return getPFColor(2, 4);
    case "Privatrundfunkfonds":
      return getPFColor(3, 4);
    case "Nichtkommerzieller Rundfunkfonds":
      return getPFColor(4, 4);
    case "Fernsehfonds":
      return getPFColor(5, 4);
    case "Werbung":
      return getColor(1, 2);
    default:
      return "#b3bef5";
  }
};

const mediaGroups: string[] = [];
const mtTypes: string[] = [];
const pfTypes: string[] = [];
const fundingBaseTypes: string[] = [];
const receivers: string[] = [];

let fundingBaseColors: Map<string, string>;
const getDataModel = (
  acc: { name: string; itemStyle?: { color: string; borderColor: string }; label?: {position?: string; offset?: any;}}[],
  currentValue: TMixedFlowsRecord
): TSankeyDataModel[] => {
  if (currentValue.amount) {
    acc.push({
      name: currentValue.paymentDetailType,
      itemStyle: {
        color: getColorFromPaymentDetailType(currentValue.paymentDetailType),
        borderColor: getColorFromPaymentDetailType(currentValue.paymentDetailType),
      },
    });

    receivers.push(currentValue.receiverMedia);
    acc.push({ name: currentValue.receiverMedia, label: {position: "insideRight", offset: [-20, 0]} });

    if (currentValue.showFundingBasis && currentValue.fundingBasis) {
      const fundingBaseColor = '#'+(0x1000000+Math.random()*0xffffff).toString(16).substr(1,6);
      fundingBaseTypes.push(currentValue.fundingBasis);
      fundingBaseColors.set(currentValue.fundingBasis, fundingBaseColor);
      acc.push({ name: currentValue.fundingBasis, itemStyle: {color: fundingBaseColor, borderColor: fundingBaseColor} });
    }

    if (currentValue.receiverGroups) {
      mediaGroups.push(currentValue.receiverGroups);
      acc.push({ name: currentValue.receiverGroups});
    }

    if (currentValue.transferType) mtTypes.push(currentValue.paymentDetailType);
    else pfTypes.push(currentValue.paymentDetailType);

  }
  return acc;
};
let payerSumMap: Map<string, number> = new Map();
let receiverGroupSumMap: Map<string, number> = new Map();
let receiverSumMap: Map<string, number> = new Map();
let fundingBaseSumMap: Map<string, number> = new Map();

const dataToSankeyDataModel = (
  result: TMixedFlowsResult
): TSankeyChartModel => {
  fundingBaseColors = new Map();
  const duplicateData = Array.from(new Set(result.reduce(getDataModel, [])));
  const uniqueData = Array.from(
    new Map(duplicateData.map((v) => [JSON.stringify([v.name]), v])).values()
  );

  let res = getMixedFlowsLinks(result, fundingBaseColors);
  payerSumMap = res.payerSumMap;
  receiverGroupSumMap = res.receiverSumGroupMap;
  receiverSumMap = res.receiverSumMap;
  fundingBaseSumMap = res.fundingBaseSumMap;

  return {
    data: uniqueData,
    links: [...res.linkModel],
    right: "5%",
    label: {
      borderWidth: 0,
      color: '#000000'
    }
  } as TSankeyChartModel;
};

export type TMixedFlowsState = {
  data: TMixedFlowsResult;
  timeline: TTimelineFundingResult;
  timelineModel: ReturnType<typeof timelineToEcharModel>;
  sankeyModel: ReturnType<typeof dataToSankeyDataModel>;
  query: TMixedFlowsQuery;
  pending: boolean;
  needsUpdate: boolean;
  selectedOrganisaions: string[];
  organisationsList: string[];
};

export const flowsMixedSlice = createSlice({
  name: "flowsMixed",
  initialState: {
    data: [] as TMixedFlowsResult,
    timeline: [] as any[] as TTimelineFundingResult,
    timelineModel: [],
    sankeyModel: {} as TSankeyChartModel,
    query: {
      receiver: [],
      org: [],
      orgGroups: [],
      receiverGroups: ['Mediengruppe "Österreich"'],
    } as TMixedFlowsQuery,
    pending: false,
    needsUpdate: false,
    selectedOrganisaions: [],
    organisationsList: [],
  } as TMixedFlowsState,
  reducers: {
    setFlowFundingData: (state, action: PayloadAction<TMixedFlowsResult>) => ({
      ...state,
      data: action.payload,
      needsUpdate: false,
      sankeyModel: dataToSankeyDataModel(action.payload),
    }),
    setTimeline: (state, action: PayloadAction<TTimelineFundingResult>) => ({
      ...state,
      timeline: action.payload,
      timelineModel: timelineToEcharModel(action.payload),
    }),
    setPending: (state, action: PayloadAction<boolean>) => ({
      ...state,
      pending: action.payload,
    }),
    setQuery: (state, action: PayloadAction<TMixedFlowsQuery>) => ({
      ...state,
      query: { ...state.query, ...action.payload },
    }),
    setNeedsUpdate: (
      state: TMixedFlowsState,
      action: PayloadAction<boolean>
    ) => ({
      ...state,
      needsUpdate: action.payload,
    }),
    clearNeedsUpdate: (state) => ({
      ...state,
      needsUpdate: false,
    }),
  },
});

const {
  setFlowFundingData,
  setPending,
  setQuery,
  setNeedsUpdate,
  setTimeline,
  clearNeedsUpdate,
} = flowsMixedSlice.actions;

const MAX_OPTION_LENGTH = 40;
const valueDecorator = (v: string) =>
  v.length > MAX_OPTION_LENGTH ? (
    <span>
      {`${v.substring(0, MAX_OPTION_LENGTH)}...`}
      <Help text={v} />
    </span>
  ) : (
    v
  );
function useQuery() {
  return new URLSearchParams(useLocation().search);
}
export const FlowsMixed = () => {
  const {
    setGroupsEnabled,
    setShowPaymentTypeDetail,
    setOtherMediaDisabledEnabled
  } = infoSlice.actions;
  const [error, setError] = useState("");
  const [key, setKey] = useState("flows");
  const [queryStringParsed, setQueryStringParsed] = useState(false); // eslint-disable-line @typescript-eslint/no-unused-vars
  const [[pStart, pEnd], setPeriods] = useState<number[]>([0, 0]);
  const navigate = useNavigate();
  // @ts-ignore
  const queryParams = useQuery();
  const location = useLocation();
  const {
    periods,
    periodsFunding,
    mixedPeriods,
    groupsEnabled,
    groups,
    showPaymentTypeDetail,
    otherMediaDisabled,
  } = useSelector<AppState, TInfoState>((state) => state.info);
  const [search, setSearch] = useState<TSearchFundingState>({
    media: { length: 0, hits: [] },
    org: { length: 0, hits: [] },
  });
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const [loadingOptions, setLoadingOptions] = useState(false);
  const {
    pending,
    needsUpdate,
    data,
    query,
    timeline,
    timelineModel,
    sankeyModel,
  } = useSelector<AppState, TMixedFlowsState>((state) => state.flowsMixed);
  const updateTimescale = () => {
    const from = mixedPeriods[pStart];
    const to = mixedPeriods[pEnd];
    if (query.from !== from || query.to !== to) {
      dispatch(setQuery({ ...query, from, to }));
    }
  };

  useEffect(() => {
    document["lastClick"] = Date.now();
  }, []);

  const handleClick = ({ data }) => {
    if (isTouchSupported()) {
      const now = Date.now();
      const lastClick = document["lastClick"];
      console.log("lastClick", lastClick);
      if (now - lastClick < 600) {
        return selectPeriod(data.period);
      } else {
        document["lastClick"] = now;
      }
    } else {
      return selectPeriod(data.period);
    }
  };

  const customToolTip = (params) => {
    const fmt = new Intl.NumberFormat(i18n.language, {
      style: "currency",
      currency: "EUR",
      minimumFractionDigits: 2,
    });
    let data = params.data;
    if (data.source) {
      let percentage = 0;
      let targetPercentage = 0;
      if(payerSumMap.has(data.source)) 
        percentage = (data.value / (payerSumMap.get(data.source) as number)) * 100;
      else if(receiverGroupSumMap.has(data.source)) 
        percentage = (data.value / (receiverGroupSumMap.get(data.source) as number)) * 100;
      else if(fundingBaseSumMap.has(data.source)) 
        percentage = (data.value / (fundingBaseSumMap.get(data.source) as number)) * 100;

      if(payerSumMap.has(data.target))
        targetPercentage = (data.value / (payerSumMap.get(data.target) as number)) * 100;
      if(receiverGroupSumMap.has(data.target))
        targetPercentage = (data.value / (receiverGroupSumMap.get(data.target) as number)) * 100;
      if(receiverSumMap.has(data.target))
        targetPercentage = (data.value / (receiverSumMap.get(data.target) as number)) * 100;
      if(fundingBaseSumMap.has(data.target))
        targetPercentage = (data.value / (fundingBaseSumMap.get(data.target) as number)) * 100;

      return `
              <b>${data.source} ${percentage % 100 !== 0 ? '(' + percentage.toFixed(2) + '%)' : ''} → ${data.target}
              ${targetPercentage % 100 !== 0 ? '(' + targetPercentage.toFixed(2) + '%)' : ''}</b><br />
              ${fmt.format(data.value)}
        `;
    } else {
      return `
              <b>${params.data.name}</b><br />
              ${fmt.format(params.value)}
        `;
    }
  };

  const sankeyOptions = useMemo(
    () => ({
      tooltip: {
        trigger: "item",
        triggerOn: "mousemove",
        formatter: customToolTip,
      },
      series: {
        type: "sankey",
        layout: "none",
        emphasis: {
          focus: "adjacency",
        },
        ...sankeyModel,
      },
    }),
    [t, sankeyModel]
  );

  const timelineOptions = useMemo(
    () => ({
      legend: {},
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "cross",
          crossStyle: {
            color: "#999",
          },
        },
        formatter: (params) => {
          params = params instanceof Array ? params : [params];
          const includesForecast =
            params.filter((e) => e.data.prediction).length > 0;
          const fmt = new Intl.NumberFormat(i18n.language, {
            style: "currency",
            currency: "EUR",
            minimumFractionDigits: 2,
          });
          let d = params
            .map(
              (p) =>
                `${p.marker} ${p.seriesName}${
                  p.data["prediction"] ? "*" : ""
                } <span style="float:right;margin-left:20px;font-size:14px;color:${
                  p.data["prediction"] ? "#aaa" : "#666"
                };font-weight:900">${fmt.format(
                  p.data[p.dimensionNames[p.encode.y]]
                )}</span>`
            )
            .join("<br/>");
          if (params.length > 1) {
            if (!includesForecast) {
              const s = params
                .filter((e) => !e.data.prediction)
                .reduce(
                  (acc, p) => acc + p.data[p.dimensionNames[p.encode.y]],
                  0
                );
              d = `${d}<br/><span style="font-weight:900">${t(
                "Total"
              )}</span><span style="float:right;margin-left:20px;font-size:14px;color:#666;font-weight:900">${fmt.format(
                s
              )}</span>`;
            } else {
              const s = params.reduce(
                (acc, p) => acc + p.data[p.dimensionNames[p.encode.y]],
                0
              );
              d = `${d}<br/>${t(
                "Total"
              )}*<span style="float:right;margin-left:20px;font-size:14px;color:#aaa;font-weight:900">${fmt.format(
                s
              )}</span>`;
              d += `<br>*) ${t("Forecast")}`;
            }
          } else if (includesForecast) {
            d += `<br>*) ${t("Forecast")}`;
          }
          return `${params[0].name}<br/>${d}`;
        },
      },
      toolbox: {
        show: true,
        orient: "horizontal",
        left: "center",
        top: "bottom",
        feature: {
          mark: { show: true },
          dataView: { show: false, readOnly: false },
          magicType: { show: true, type: ["line", "bar", "stack"] },
          restore: { show: false },
          saveAsImage: { show: false },
        },
      },
      xAxis: { type: "category" },
      yAxis: {
        axisLabel: {
          formatter: (value) => `${value / 1000000} Mio.`,
        },
      },
      dataset: [
        {
          dimensions: ["year", "22", "0", "1", "2", "3", "4"],
          source: timelineModel,
        },
      ],
      series: [
        {
          type: "bar",
          name: "Werbung",
          emphasis: { focus: "series" },
          color: getColor(1, 3),
        },
        {
          type: "bar",
          name: t("pref"),
          emphasis: { focus: "series" },
          color: getPFColor(1, 4),
        },
        {
          type: "bar",
          name: t("pubf"),
          emphasis: { focus: "series" },
          color: getPFColor(2, 4),
        },
        {
          type: "bar",
          name: t("prif"),
          emphasis: { focus: "series" },
          color: getPFColor(3, 4),
        },
        {
          type: "bar",
          name: t("nikorf"),
          emphasis: { focus: "series" },
          color: getPFColor(4, 4),
        },
        {
          type: "bar",
          name: t("ferf"),
          emphasis: { focus: "series" },
          color: getPFColor(5, 4),
        },
      ],
    }),
    [timelineModel, t]
  );

  useEffect(() => {
    if (mixedPeriods && mixedPeriods.length > 0) {
      const maxPeriodIndex = mixedPeriods.length - 1;
      const maxPeriod = mixedPeriods[maxPeriodIndex];
      if (!query.from || !query.to) {
        dispatch(
          setQuery({
            ...query,
            from: maxPeriod,
            to: maxPeriod,
          })
        );
        setPeriods([maxPeriodIndex, maxPeriodIndex]);
      } else if(query.from && query.to) {
        const minIndex = mixedPeriods.findIndex(it => it === query.from);
        const maxIndex = mixedPeriods.findIndex(it => it === query.to);
        dispatch(setNeedsUpdate(true));
        setPeriods([minIndex, maxIndex]);
    }
       else {
        dispatch(setNeedsUpdate(true));
      }
    }
  }, [mixedPeriods]);

  // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (
      mixedPeriods &&
      mixedPeriods.length > 0 &&
      query.receiver.length + (query.receiverGroups ?? []).length > 0 &&
      query.from &&
      query.to
    ) {
      //console.log("Checking query vs. location")
      const queryString = objectToParams(query);
      const currentQueryStringObj = qs.parse(location.search);
      if (
        !isEqual(objectNormalize(currentQueryStringObj), objectNormalize(query))
      ) {
        navigate(`/flows/mixed?${queryString}`);
      }
      //    }
      dispatch(setNeedsUpdate(true));
    }
  }, [query]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    let q = {
      receiver: [],
      org: [],
      orgGroups: [],
      receiverGroups: [],
    };
    ["receiver", "org", "orgGroups", "receiverGroups"].forEach((org) => {
      if (queryParams.getAll(org).length > 0) {
        q[org] = queryParams.getAll(org).filter((e) => e.length > 0);
      }
    });
    ["from", "to"].forEach((org) => {
      if (queryParams.get(org)) {
        q[org] = parseInt(queryParams.get(org) ?? "0");
      }
    });
    //console.log("QUERY:");
    //console.log(q)
    if (
      query.receiver.length + (query.receiverGroups ?? []).length > 0 &&
      q["from"] &&
      q["to"]
    ) {
      if (!objectIncluded(q, query)) {
        dispatch(setQuery({ ...query, ...q }));
      }
    }
    const startPeriod_ = mixedPeriods.indexOf(q["from"]);
    const endPeriod_ = mixedPeriods.indexOf(q["to"]);
    if (startPeriod_ >= 0 && endPeriod_ >= 0) {
      setPeriods([startPeriod_, endPeriod_]);
    }
    setQueryStringParsed(true);
  }, [location]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
      if (
      needsUpdate &&
      query.from &&
      query.to &&
      query.receiver.length + (query.receiverGroups ?? []).length > 0
    ) {
      setError("");
      dispatch(setPending(true));
      Promise.all([
        getMixedFlows(query, showPaymentTypeDetail, otherMediaDisabled),
        getMixedTimeline(query),
      ])
        .then(([flows, timeline]) => {
          dispatch(setFlowFundingData(flows));
          dispatch(setTimeline(timeline));
        })
        .catch((err) => {
          setError(err?.response?.data?.error ?? err.message);
          dispatch(setFlowFundingData([]));
          dispatch(clearNeedsUpdate());
        })
        .finally(() => {
          dispatch(setPending(false));
          dispatch(setNeedsUpdate(false));
        });
    } else if (needsUpdate) {
      dispatch(clearNeedsUpdate());
    }
  }, [needsUpdate]); // eslint-disable-line react-hooks/exhaustive-deps

  const colums = useMemo(
    () => [
      {
        name: t("Payer"),
        selector: (row) => row["organisation"],
        sortable: true,
      },
      {
        name: t("Beneficiary"),
        selector: (row) => row["receiverMedia"],
        sortable: true,
      },
      {
        name: "Art",
        selector: (row) => row["paymentTypeFull"],
        sortable: true,
      },
      {
        name: t("Amount"),
        selector: (row) => row["amount"],
        sortable: true,
        format: toCurrency("amount"),
        right: true,
      },
    ],
    [t]
  ); // eslint-disable-line react-hooks/exhaustive-deps

  const populateList = (orgType: "org" | "media") => (name: string) => {
    if (loadingOptions) return;
    if (name.length > search[orgType].length && name.length === 3) {
      setLoadingOptions(true);
      searchNames(name, orgType)
        .then((names) =>
          setSearch({
            ...search,
            [orgType]: { length: name.length, hits: names },
          })
        )
        .finally(() => setLoadingOptions(false));
      return ["A", "B", "C"];
    }
    if (name.length < search[orgType].length && name.length < 3) {
      setSearch({ ...search, [orgType]: { length: name.length, hits: [] } });
      return [];
    }
  };
  const updateSelection =
    (orgType: "org" | "orgGroups" | "receiver" | "receiverGroups") =>
    (list: string[]) => {
      setError("");
      dispatch(
        setQuery({
          ...query,
          [keysToNames[orgType]]: list.filter((v) => v.length > 0),
        })
      );
    };

  const selectionBoxStyle = {
    //chips: { background: "red" },
    searchBox: {
      border: "none",
      "border-bottom": "1px solid blue",
      "border-radius": "0px",
      "background-color": "white",
    },
  };
  const selectPeriod = (period) => {
    const p = period.slice(3) + period.slice(1, 2);
    dispatch(setQuery({ ...query, from: p, to: p }));
    setKey("flows");
  };
  const handleStyle = {
    border: "5px solid #96dbfa",
    width: "20px",
    height: "20px",
  };


  const onChartMouseMove = (params) => {
    const echartInstance = echartRef.getEchartsInstance();
    echartInstance.getZr().setCursorStyle('pointer');
  }

  const onChartClick = (params) => {
    if (params.data.source?.includes("Andere") || params.data.name?.includes("Andere") || params.data.target?.includes("Andere"))
      return;
    const toPeriod = periods[periods.length - 1];
    const fromPeriod = periods[0];

    const toPeriodFunding = periodsFunding[periodsFunding.length - 1];
    const fromPeriodFunding = periodsFunding[0];

    // handle payer click
    if(params.data.name && mtTypes.includes(params.data.name))
      navigate(`/flows/transfers?from=${fromPeriod}&to=${toPeriod}&pType=2`);
    else if(params.data.name && pfTypes.includes(params.data.name)) {
      navigate(`/flows/fundings?&fundingType=${getFundingValueFromName(
        params.data.name
      )}&from=${fromPeriodFunding}&to=${toPeriodFunding}`);
    }

    // handle funding base type
    const fundingBaseName = params.data.name ?? params.data.source;
    if(fundingBaseTypes.includes(fundingBaseName)) {
      let query = `/flows/fundings?showFundingBasis=true&from=${mixedPeriods[pStart]}&to=${mixedPeriods[pEnd]}`;
      const link = sankeyOptions.series.links.find(it => it.target === fundingBaseName);
      if(link) {
        if (params.data.target && mediaGroups.includes(params.data.target))
          query += `&fundingType=${getFundingValueFromName(link?.source)}&receiverGroups=${params.data.target}`;
        else if (params.data.target && receivers.includes(params.data.target))
          query += `&fundingType=${getFundingValueFromName(link?.source)}&receiver=${params.data.target}`;
        else
          query += `&fundingType=${getFundingValueFromName(link?.source)}`
      }
      navigate(query);
    }else if (params.data.source && (mtTypes.includes(params.data.source) || pfTypes.includes(params.data.source))) {
      if (mtTypes.includes(params.data.source)) {
        let queryString = `from=${mixedPeriods[pStart]+'1'}&to=${mixedPeriods[pEnd]+'4'}`;
        if (mediaGroups.includes(params.data.target))
          queryString += `&pType=2&mediaGroups=${encodeURIComponent(params.data.target)}`;
        else
          queryString += `&pType=2&media=${encodeURIComponent(params.data.target)}`;
        navigate(`/flows/transfers?${queryString}`);
      } else if (pfTypes.includes(params.data.source)) {
        let queryString = `from=${fromPeriodFunding}&to=${toPeriodFunding}`;
        if (mediaGroups.includes(params.data.target))
          queryString += `&fundingType=${getFundingValueFromName(params.data.source)}&receiverGroups=${encodeURIComponent(params.data.target)}`;
        else if(receivers.includes(params.data.target))
          queryString += `&fundingType=${getFundingValueFromName(params.data.source)}&receiver=${encodeURIComponent(params.data.target)}`;
        else 
          queryString += `&fundingType=${getFundingValueFromName(params.data.source)}`;
        navigate(`/flows/fundings?${queryString}`);
      }
    } else {
      const nam = params.data.name ? params.data.name : params.data.target;
      if (mediaGroups.includes(nam)) {
        dispatch(
          setQuery({
            ...query,
            receiverGroups: addElementIfNotExist(query.receiverGroups, nam),
            receiver: [],
          })
        );
      } else if(receivers.includes(nam)) {
        dispatch(
          setQuery({
            ...query,
            receiver: addElementIfNotExist(query.receiver, nam),
            receiverGroups: [],
          })
        );
      }
    }
  };

  const onEvents = {
    click: onChartClick,
    mousemove: onChartMouseMove
  };

  return (
    <>
      <ShowError error={error} onClose={() => setError("")} />
      <ModalLoader isPending={pending} />
      <Card.Text>
        <div className="settings">
          <div className="row justify-content-between">
            <div className="col-lg-4 col-md-5, col-12">
              <div className="label">{t("Beneficiaries")}</div>
              <Multiselect
                onSearch={populateList("media")}
                id="recList"
                options={search.media.hits}
                isObject={false}
                emptyRecordMsg={t("Start typing")}
                selectedValues={query.receiver}
                style={selectionBoxStyle}
                loading={loadingOptions}
                onSelect={updateSelection("receiver")}
                onRemove={updateSelection("receiver")}
                placeholder={t("Select a Beneficiary")}
                selectedValueDecorator={valueDecorator}
              />
            </div>
            <div className="col-lg-3 col-md-5, col-12">
            <Row>
                <Col>
                  <div className="label">
                    {t("Groupings")}{" "}
                    <Help text="Allows using pre-configured groupings" />
                  </div>
                  <BootstrapSwitchButton
                    checked={groupsEnabled}
                    width={140}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Enabled")}
                    onChange={(enabled: boolean) => {
                      dispatch(setGroupsEnabled(enabled));
                      dispatch(
                        setQuery({
                          ...query,
                          receiverGroups: [],
                        })
                      );
                    }}
                    offlabel={t("Disabled")}
                  />
                </Col>
                <Col>
                  <div className="label">
                    {t("show_details_funding")} <Help text={t("Show details funding long")} />
                  </div>
                  <BootstrapSwitchButton
                    checked={showPaymentTypeDetail}
                    width={140}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Displayed")}
                    onChange={(enabled: boolean) => {
                      dispatch(setShowPaymentTypeDetail(enabled));
                      dispatch(setQuery({ ...query }));
                    }}
                    offlabel={t("Hidden")}
                  />
                </Col>
                </Row>
                <Row>
                <Col>
                  <div className="label">
                    {t("Unhide others")} <Help text="Hide other recipients" />
                  </div>
                  <BootstrapSwitchButton
                    checked={otherMediaDisabled}
                    width={140}
                    onstyle="info"
                    offstyle="secondary"
                    onlabel={t("Displayed")}
                    onChange={(enabled: boolean) => {
                      dispatch(setOtherMediaDisabledEnabled(enabled));
                      dispatch(setQuery({ ...query }));
                    }}
                    offlabel={t("Hidden")}
                  />
                </Col>
              </Row>
            </div>
            <div className="col-lg-4 col-md-2 col-12 ">
            <Render when={groupsEnabled}>
                <div className="label">{t("Groups")}</div>
                <Multiselect
                  id="recGroups"
                  options={groups
                    .filter(
                      (g) => g.type === "media" && g.group_type === "group"
                    )
                    .map((g) => g.name)}
                  isObject={false}
                  selectedValues={query.receiverGroups}
                  style={selectionBoxStyle}
                  onSelect={updateSelection("receiverGroups")}
                  onRemove={updateSelection("receiverGroups")}
                  placeholder={t("Select a Group")}
                  selectedValueDecorator={valueDecorator}
                />
              </Render>
            </div>
          </div>
          <div className="row justify-content-between">
            <div className="col-12">
              <div className="label">{t("Period")}</div>
              <Range
                vertical={false}
                max={mixedPeriods.length - 1}
                railStyle={{ backgroundColor: "#999" }}
                onAfterChange={updateTimescale}
                value={[pStart, pEnd]}
                onChange={setPeriods}
                tipFormatter={(index) => mixedPeriods[index]}
                handleStyle={[handleStyle, handleStyle]}
                marks={mixedPeriods.reduce(
                  (acc, v, i) =>
                    v % 2 === 0 || [pStart, pEnd].includes(i)
                      ? { ...acc, [i]: v }
                      : acc,
                  {}
                )}
              />
            </div>
          </div>
        </div>
        <Render when={data?.length === 0}>
          <p></p>
          <div className="alert alert-warning" role="alert">
            <p>{t("No Data found that matches your settings")}.</p>
          </div>
        </Render>
        <Tabs
          id="flow-timeline-tabs"
          activeKey={key}
          onSelect={(k) => setKey(k ?? "flows")}
        >
          <Tab
            eventKey="flows"
            title={
              <>
                <FontAwesomeIcon icon={faStream} /> {t("Flows")}
              </>
            }
          >
            <Render when={data?.length > 0 && !error}>
              <div className="text-end info" style={{ margin: "1em 0" }}>
                <FontAwesomeIcon icon={faInfoCircle} />{" "}
                {t(
                  `${
                    isTouchSupported() ? "Double click" : "Click"
                  } on the chart to get more information`
                )}
              </div>

              <ResponsiveContainer
                width="100%"
                height={
                  data.length * 20 < 1000
                    ? 1000
                    : sankeyOptions.series.links.length * 20
                }
              >
                <ReactECharts
                  ref={(e) => { echartRef = e; }}
                  option={sankeyOptions}
                  style={{ height: "100%", width: "100%", cursor: "pointer !important" }}
                  onEvents={onEvents}
                />
              </ResponsiveContainer>
              <div style={{ margin: "1em 0 0 0" }}>
                <DataTable
                  title={t("Money Flows")}
                  pagination={true}
                  columns={colums}
                  data={data}
                  actions={<ExportToExcel data={data} fileName="MoneyFlow" />}
                />
              </div>
            </Render>
          </Tab>
          <Tab
            eventKey="timeline"
            title={
              <>
                <FontAwesomeIcon icon={faChartLine} /> {t("Timeline")}
              </>
            }
          >
            <Render when={timeline?.length > 0 && !error}>
              <div className="text-end info">
                <FontAwesomeIcon icon={faInfoCircle} />{" "}
                {t(
                  `${
                    isTouchSupported() ? "Double click" : "Click"
                  } on the chart to get more information`
                )}
              </div>
              <ReactECharts
                option={timelineOptions}
                onEvents={{ click: handleClick }}
                style={{ height: 450 }}
              />
            </Render>
          </Tab>
        </Tabs>
      </Card.Text>
    </>
  );
};
