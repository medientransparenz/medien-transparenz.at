import { useState, useEffect, useMemo } from 'react';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../..';
import { getFlow, objectToParams, searchNames, getTimeline } from '../../services/data-service';
import { Help, ModalLoader, Render, ShowError } from '../../components/helper-components';
import { Layer, Rectangle, ResponsiveContainer, Sankey, Tooltip } from 'recharts';

import { Multiselect } from 'multiselect-react-dropdown';
import Card from 'react-bootstrap/esm/Card';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChartLine, faInfoCircle, faStream } from '@fortawesome/free-solid-svg-icons';

import { infoSlice, TInfoState } from '../../App';
import Slider from 'rc-slider';
import qs from 'query-string';
import { periodToString, objectIncluded, isTouchSupported, isEqual, objectNormalize } from '../../helpers/helpers';
import './flows.css'
import { useLocation, useNavigate } from 'react-router-dom';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import { getColor } from '../../colors';
import { ToggleSwitch } from '../../components/three-state/three-state-switch';
import DataTable from 'react-data-table-component-with-filter';
import i18n from '../../i18n';
import { ExportToExcel } from '../../components/excel-exporter';
import BootstrapSwitchButton from 'bootstrap-switch-button-react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ReactECharts from 'echarts-for-react';

/* eslint eqeqeq: 0 */
/* eslint react-hooks/exhaustive-deps: 0 */

const keysToNames = {
    "org": "organisations",
    "media": "media",
    "orgGroup": "orgGroups",
    "mediaGroup": "mediaGroups"
}

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
// defined for new selected values const MAX_LENGTH = 30

type TSankeyModel = {
    nodes: { name: string }[],
    links: { source: number, target: number, value: number, transferType: 2 | 4 | 31 | number[] }[]
}

export type TFlowsResultRecord = {
    amount: number,
    media: string,
    mediaGroup?: string,
    orgGroup?: string,
    organisation: string,
    transferType: 2 | 4 | 31 | number[]
}

const toCurrency = (field: string) => (row: any) =>
    row[field]?.toLocaleString(i18n.language, {
        style: 'currency',
        currency: 'EUR',
    })



export type TFlowResult = TFlowsResultRecord[]

export type TTimelineResult = { period: string, total: number }[]


export type TFlowsQuery = {
    from?: number,
    to?: number,
    media: string[],
    organisations: string[],
    orgGroups?: string[],
    mediaGroups?: string[],
    pType: 2 | 4 | 31 | number[],
    federalState?: string
}


type TSearchState = {
    org: {
        length: number
        hits: string[]

    },
    media: {
        length: number
        hits: string[]

    }
}

const timelineToEcharModel = (data: TTimelineResult) =>
    [
        ...[1, 2].map(q => ({ period: `Q${q}/2012`, quarter: q, year: 2012, total: 0 })),
        ...data.map(e =>
            ({ ...e, quarter: parseInt(e.period.charAt(1)), year: parseInt(e.period.split("/")[1]) }))
    ]

export type TFlowsState = {
    data: TFlowResult,
    timeline: TTimelineResult,
    timelineModel: ReturnType<typeof timelineToEcharModel>,
    query: TFlowsQuery,
    pending: boolean,
    needsUpdate: boolean,
    switchState: number,
    chartModel: TSankeyModel,
    selectedOrganisaions: string[],
    organisationsList: string[]
}



export const flowsSlice = createSlice({
    name: "flows",
    initialState: {
        data: [] as TFlowResult,
        timeline: [] as any[] as TTimelineResult,
        switchState: 2,
        timelineModel: [],
        query: {
            pType: 2,
            organisations: ['Bundeskanzleramt'],
            orgGroups: [],
            media: [],
            mediaGroups: []
        } as TFlowsQuery,
        pending: false,
        needsUpdate: false,
        chartModel: {
            node: [],
            nodes: [],
            links: []
        } as TSankeyModel,
        selectedOrganisaions: [],
        organisationsList: []
    } as TFlowsState,
    reducers: {
        setData: (state, action: PayloadAction<TFlowResult>) => ({
            ...state, data: action.payload, needsUpdate: false,
            chartModel: responseToSankeyModel(action.payload)
        }),
        setTimeline: (state, action: PayloadAction<TTimelineResult>) => ({
            ...state, timeline: action.payload,
            timelineModel: timelineToEcharModel(action.payload)
        }),
        setPending: (state, action: PayloadAction<boolean>) => ({
            ...state, pending: action.payload
        }),
        setQuery: (state, action: PayloadAction<TFlowsQuery>) => ({
            ...state, query: { ...state.query, ...action.payload },
            switchState: Array.isArray(action.payload.pType) ? 0 : action.payload.pType
        }),
        setNeedsUpdate: (state) => ({
            ...state, needsUpdate: true
        }),
        clearNeedsUpdate: (state) => ({
            ...state, needsUpdate: false
        }),
    }
})

const { setData, setPending, setQuery,
    setNeedsUpdate, clearNeedsUpdate, setTimeline } = flowsSlice.actions



const responseToSankeyModel = (data: TFlowResult): TSankeyModel => {
    const rawNodes =
        data.reduce((acc, { organisation, media, orgGroup, mediaGroup }) => {
            acc = ({ ...acc, [organisation]: 'org', [media]: "media" })//.add(orgGroup)
            if (orgGroup) acc = ({ ...acc, [orgGroup]: 'orgGroup' })
            if (mediaGroup) acc = ({ ...acc, [mediaGroup]: 'mediaGroup' })
            return acc
        }, {})
    const nodes = Object.entries(rawNodes).map(([k, v]) => ({ name: k, type: v }))
    const names = nodes.map(v => v.name)
    const linkMaps = {} as {
        [id: string]: {
            [id: string]: { [t: number]: number }
        }
    }
    const addLink = (from, to, tType, amount) => {
        if (linkMaps[from]) {
            if (linkMaps[from][to]) {
                if (linkMaps[from][to][tType]) {
                    linkMaps[from][to][tType] = linkMaps[from][to][tType] + amount
                } else {
                    linkMaps[from][to][tType] = amount
                }
            } else {
                linkMaps[from][to] = { [tType]: amount }
            }
        } else {
            linkMaps[from] = { [to]: { [tType]: amount } }
        }
    }
    //const nodes = rawNodes
    data.forEach(l => {
        let src = l.organisation
        if (l.orgGroup) {
            addLink(l.organisation, l.orgGroup, l.transferType, l.amount)
            src = l.orgGroup
        }
        if (l.mediaGroup) {
            addLink(src, l.mediaGroup, l.transferType, l.amount)
            addLink(l.mediaGroup, l.media, l.transferType, l.amount)
        } else {
            addLink(src, l.media, l.transferType, l.amount)
        }
    })
    const links = Object.entries(linkMaps).reduce((acc, [from, o]) =>
        [...acc, ...Object.entries(o).reduce((acc_, [to, values]) =>
            [...acc_, ...Object.entries(values).reduce((acc2, [tt, amount]) => [...acc2, {
                source: names.indexOf(from),
                target: names.indexOf(to),
                value: amount,
                transferType: tt
            }], [] as any)], [] as any)], [] as any)
    return { nodes, links }

}

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const TransferNode = ({ x, y, width, height, index, payload }: any) => {
    const isOut = payload.type === "media" || payload.type === "mediaGroup"
    const { t } = useTranslation()
    const { query } = useSelector<AppState, TFlowsState>(state => state.flow)
    const dispatch = useDispatch();
    const nodeDetails = () => {
        //console.log("PAYLOAD:", payload)
        return payload.name.startsWith('Other') ? null :
            dispatch(setQuery({
                ...query,
                ...Object.entries(keysToNames).reduce((acc, [k, v]) =>
                    ({ ...acc, [v]: k === payload.type ? [payload.name] : [] }), {} as any)
            }))
    }
    //console.log(payload);
    return (
        <Layer key={`CustomNode${index}`}>
            <Rectangle
                x={x} y={y} width={width} height={height}
                fill="#5192ca" fillOpacity="1"
                cursor={payload.name.startsWith('Other') ? 'default' : 'pointer'}
                onClick={isTouchSupported() ? () => true : nodeDetails}
                onDoubleClick={isTouchSupported() ? nodeDetails : () => true}
            />
            {height >= 0 ?
                <>
                    <text
                        textAnchor={isOut ? 'end' : 'start'}
                        x={isOut ? x - 6 : x + width + 6}
                        y={y + height / 2 + 6}
                        fontSize="12"
                        className="node_label"
                    //stroke="#333"
                    >{payload.name.startsWith('Other') ? t(payload.name) : payload.name}{payload.type.includes('Group') ? " (Grp)" : null}</text>

                </> : null
            }
        </Layer>
    );
}


const TransferLink = ({ sourceX, targetX,
    sourceY, targetY,
    sourceControlX, targetControlX,
    linkWidth,
    index, payload
}: any) => {
    const [fill, setFill] = useState((payload.source.name.startsWith('Other') || payload.target.name.startsWith('Other')) ? 'url(#linkGradient2)' :
        payload.transferType == 2 ? 'url(#linkGradient)' : 'url(#linkGradientFunding)');
    const [cursor, setCursor] = useState('default');
    const { query } = useSelector<AppState, TFlowsState>(state => state.flow)
    useEffect(() => {
        setFill((payload.source.name.startsWith('Other') || payload.target.name.startsWith('Other')) ? 'url(#linkGradient2)' :
            payload.transferType == 2 ? 'url(#linkGradient)' : 'url(#linkGradientFunding)');
    }, [payload.source]) // eslint-disable-line react-hooks/exhaustive-deps
    const dispatch = useDispatch()
    const layerDetails = () => {
        if (payload.source.name.startsWith('Other') || payload.target.name.startsWith('Other')
            || (payload.target.type.includes('Group') && payload.source.type.includes('Group'))) { } else {
            dispatch(setQuery({
                ...query,
                organisations: payload.source.type.includes("Group") ? [] : [payload.source.name],
                media: payload.target.type.includes("Group") ? [] : [payload.target.name],
                orgGroups: [],
                mediaGroups: []

            }
            ))
        }
    }
    return (
        <Layer key={`CustomLink${index}`}>
            <path
                d={`
          M${sourceX},${sourceY + linkWidth / 2}
          C${sourceControlX},${sourceY + linkWidth / 2}
            ${targetControlX},${targetY + linkWidth / 2}
            ${targetX},${targetY + linkWidth / 2}
          L${targetX},${targetY - linkWidth / 2}
          C${targetControlX},${targetY - linkWidth / 2}
            ${sourceControlX},${sourceY - linkWidth / 2}
            ${sourceX},${sourceY - linkWidth / 2}
          Z
        `}
                fill={fill}
                cursor={cursor}
                onClick={isTouchSupported() ? () => true : layerDetails}
                onDoubleClick={isTouchSupported() ? layerDetails : () => true}
                strokeWidth="0"
                opacity={0.5}
                onMouseEnter={() => {
                    if (!payload.source.name.startsWith('Other') && !payload.target.name.startsWith('Other')) {
                        setFill('rgba(0, 136, 254, 0.5)');
                        setCursor((payload.source.name.startsWith('Other') || payload.target.name.startsWith('Other')) ? 'default' : 'pointer')
                    }
                }}
                onMouseLeave={() => {
                    setFill((payload.source.name.startsWith('Other') || payload.target.name.startsWith('Other')) ? 'url(#linkGradient2)' : payload.transferType == 2 ? 'url(#linkGradient)' : 'url(#linkGradientFunding)');
                    setCursor('default')
                }}
            />
        </Layer>
    );
}

const MAX_OPTION_LENGTH = 40
const valueDecorator = (v: string) =>
    v.length > MAX_OPTION_LENGTH ? <span>{`${v.substring(0,MAX_OPTION_LENGTH)}...`}<Help text={v} /></span>  : v

export const FlowsTransfers = () => {
    const { setGroupsEnabled } = infoSlice.actions
    const [error, setError] = useState('');
    const [key, setKey] = useState('flows');
    const [queryStringParsed, setQueryStringParsed] = useState(false) // eslint-disable-line @typescript-eslint/no-unused-vars
    const [[pStart, pEnd], setPeriods] = useState<number[]>([0, 0]);
    const navigate = useNavigate();
    const location = useLocation();
    const { periods, groups, groupsEnabled } = useSelector<AppState, TInfoState>(state => state.info);
    const [search, setSearch] = useState<TSearchState>({
        org: { length: 0, hits: [] },
        media: { length: 0, hits: [] }
    });
    const { t, i18n } = useTranslation()
    const queryParams = useQuery();
    const dispatch = useDispatch();
    const [loadingOptions, setLoadingOptions] = useState(false);
    const { pending, needsUpdate, data, query,
        timeline, chartModel, switchState, timelineModel } = useSelector<AppState, TFlowsState>(state => state.flow)
    const updateTimescale = () => {
        const from = periods[pStart];
        const to = periods[pEnd];
        if (query.from !== from || query.to !== to) {
            dispatch(setQuery({ ...query, from, to }));
        }
    }
    const toPaymentTypeText = (field: string) => (row: any) => {
        switch (row[field]) {
            case 2: return t("Advertising")
            case 4: return t("Funding")
            default: return t("Fees")
        }
    }

    useEffect(() => { document['lastClick'] = Date.now() }, [])

    const handleClick = ({data}) => {
        if (isTouchSupported()) {
            const now = Date.now()
            const lastClick = document['lastClick']
            console.log('lastClick', lastClick)
            if (now - lastClick < 600) {
                return selectPeriod(data.period)
            } else {
                document['lastClick'] = now
            }
        } else {
            return selectPeriod(data.period)
        }
    }

    const timelineOptions = useMemo(() => ({
        legend: {},
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999'
                }
            },
            formatter: (params) => {
                params = params instanceof Array ? params : [params]
                const includesForecast = params.filter(e => e.data.prediction).length > 0
                const fmt = new Intl.NumberFormat(i18n.language, { style: 'currency', currency: 'EUR', minimumFractionDigits: 2 })
                let d = params.map(p => `${p.marker} ${p.seriesName}${p.data["prediction"] ? '*' : ''} <span style="float:right;margin-left:20px;font-size:14px;color:${p.data["prediction"] ? '#aaa' : '#666'};font-weight:900">${fmt.format(p.data[p.dimensionNames[p.encode.y]])}</span>`).join('<br/>')
                if (params.length > 1) {
                    if (!includesForecast) {
                        const s = params.filter(e => !e.data.prediction).reduce((acc, p) => acc + p.data[p.dimensionNames[p.encode.y]], 0)
                        d = `${d}<br/><span style="font-weight:900">${t('Total')}</span><span style="float:right;margin-left:20px;font-size:14px;color:#666;font-weight:900">${fmt.format(s)}</span>`
                    } else {
                        if (includesForecast) {
                            const s = params.reduce((acc, p) => acc + p.data[p.dimensionNames[p.encode.y]], 0)
                            d = `${d}<br/>${t('Total')}*<span style="float:right;margin-left:20px;font-size:14px;color:#aaa;font-weight:900">${fmt.format(s)}</span>`
                            d += `<br>*) ${t('Forecast')}`
                        }
                    }
                    return `${params[0].name}<br/>${d}`
                }
            }
        },
        grid: {
            show: true,

        },
        toolbox: {
            show: true,
            orient: 'horizontal',
            left: 'center',
            top: 'bottom',
            feature: {
                mark: { show: true },
                dataView: { show: false, readOnly: false },
                magicType: { show: true, type: ['line', 'bar', 'stack'] },
                restore: { show: false },
                saveAsImage: { show: false }
            }
        },
        xAxis: { type: 'category' },
        yAxis: {
            axisLabel: {
                formatter: (value) => `${value / 1000000} Mio.`
            }
        },
        dataset: [{
            dimensions: ['year', 'total', 'quarter', 'transferType'],
            source: timelineModel
        }, {
            transform: {
                type: 'filter',
                config: {
                    and: [
                        { dimension: 'quarter', '=': 1 },
                    ]
                },
                print: false
            }
        }, {
            transform: {
                type: 'filter',
                config: {
                    and: [
                        { dimension: 'quarter', '=': 2 },]
                },
                print: false
            }
        }, {
            transform: {
                type: 'filter',
                config: {
                    and: [
                        { dimension: 'quarter', '=': 3 },
                    ]
                },
                print: false
            }
        }, {
            transform: {
                type: 'filter',
                config: {
                    and: [
                        { dimension: 'quarter', '=': 4 },
                    ]
                },
                print: true
            }
        }
        ],
        series: [
            { type: 'bar', datasetIndex: 1, name: "Q1", emphasis: { focus: 'series' }, color: getColor(1, 4), barCategoryGap: '10%' },
            { type: 'bar', datasetIndex: 2, name: "Q2", emphasis: { focus: 'series' }, color: getColor(2, 4), barCategoryGap: '10%' },
            { type: 'bar', datasetIndex: 3, name: "Q3", emphasis: { focus: 'series' }, color: getColor(3, 4), barCategoryGap: '10%' },
            { type: 'bar', datasetIndex: 4, name: "Q4", emphasis: { focus: 'series' }, color: getColor(4, 4), barCategoryGap: '10%' }
        ],
        onClick: () => alert("hi!"),
        animation: process.env.NODE_ENV !== "development"
    }), [t, timelineModel])

    useEffect(() => {
        if (periods && periods.length > 0) {
            //console.log("Setting from and to!!!")
            //console.log("Periods: " + JSON.stringify(periods, null, 2))
            const maxPeriod = periods[periods.length - 1]
            //console.log("Maxperiod: " + maxPeriod)
            if (!query.from || !query.to) {
                dispatch(setQuery({
                    ...query,
                    from: maxPeriod,
                    to: maxPeriod
                }))
                setPeriods([periods.length - 1, periods.length - 1])
            }  else if(query.from && query.to) {
                const minIndex = periods.findIndex(it => it === query.from);
                const maxIndex = periods.findIndex(it => it === query.to);
                dispatch(setNeedsUpdate());
                setPeriods([minIndex, maxIndex]);
              }else {
                dispatch(setNeedsUpdate())
            }
        }
    }, [periods]) // eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        //if (queryStringParsed) {
        //    setQueryStringParsed(false)
        //} else {
        if (periods && periods.length > 0 && (query.organisations.length + query.media.length +
            (query.orgGroups ?? []).length + (query.mediaGroups ?? []).length) > 0 &&
            query.from && query.to) {
            //console.log("Checking query vs. location")
            const queryString = objectToParams(query)
            const currentQueryStringObj = qs.parse(location.search)
            if (!isEqual(objectNormalize(currentQueryStringObj), objectNormalize(query))) {
                navigate(`/flows/transfers?${queryString}`)
            }
            //    }
            dispatch(setNeedsUpdate());
        }
    }, [query]) // eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        //console.log("Location changed")
        let q = {
            pType: [2],
            organisations: [],
            orgGroups: [],
            media: [],
            mediaGroups: []
        };
        ['organisations', 'media', 'pType', 'orgGroups', 'mediaGroups'].forEach(org => {
            if (queryParams.getAll(org).length > 0) {
                q[org] = queryParams.getAll(org).filter(e => e.length > 0)
            }
        });
        ['from', 'to'].forEach(org => {
            if (queryParams.get(org)) {
                q[org] = parseInt(queryParams.get(org) ?? '0')
            }
        });
        //console.log("QUERY:");
        //console.log(q)
        if ((query.organisations.length + query.media.length +
            (query.orgGroups ?? []).length + (query.mediaGroups ?? []).length) > 0 && q['from'] && q['to']) {
            if (!objectIncluded(q, query)) {
                dispatch(setQuery({ ...query, ...q }))
            }

        }
        const startPeriod_ = periods.indexOf(q['from']);
        const endPeriod_ = periods.indexOf(q['to']);
        if (startPeriod_ >= 0 && endPeriod_ >= 0) {
            setPeriods([startPeriod_, endPeriod_]);
        }
        setQueryStringParsed(true)
        //dispatch(setNeedsUpdate());
    }, [location]); // eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        if (needsUpdate && (query.organisations.length + query.media.length +
            (query.orgGroups ?? []).length + (query.mediaGroups ?? []).length) > 0 && query.from && query.to) {
            setError('')
            //console.log("Sending new query")
            dispatch(setPending(true))
            Promise.all([getTimeline(query),
            getFlow(query)]).then(([timeline_, data_]) => {
                dispatch(setTimeline(timeline_.map(({ period, total }) => ({ total, period: periodToString(parseInt(period)) }))));
                dispatch(setData(data_));
            })
                .catch(
                    err => {
                        setError(err?.response?.data?.error ?? err.message)
                        dispatch(clearNeedsUpdate())
                    })
                .finally(() => dispatch(setPending(false)))
        } else if (needsUpdate) { dispatch(clearNeedsUpdate()) }
    }, [needsUpdate]) // eslint-disable-line react-hooks/exhaustive-deps

    const colums = useMemo(() => [
        {
            name: t("Payer"),
            selector: row => row['organisation'],
            sortable: true
        }, {
            name: t("Beneficiary"),
            selector: row => row["media"],
            sortable: true

        }, {
            name: t("Payment type"),
            selector: row => row["transferType"],
            sortable: true,
            format: toPaymentTypeText('transferType'),
            width: "10em"
        }, {
            name: t("Amount"),
            selector: row => row["amount"],
            sortable: true,
            format: toCurrency('amount'),
            right: true
        }
    ], [t]) // eslint-disable-line react-hooks/exhaustive-deps

    const populateList = (orgType: 'org' | 'media') => (name: string) => {
        if (loadingOptions) return;
        if ((name.length > search[orgType].length) && name.length === 3) {
            setLoadingOptions(true)
            searchNames(name, orgType).then(names => setSearch({ ...search, [orgType]: { length: name.length, hits: names } }))
                .finally(() => setLoadingOptions(false))
            return ['A', 'B', 'C'];
        }
        if ((name.length < search[orgType].length) && name.length < 3) {
            setSearch({ ...search, [orgType]: { length: name.length, hits: [] } })
            return [];
        }
    }
    const updateSelection = (orgType: 'org' | 'media' | 'orgGroups' | 'mediaGroups') => (list: string[]) => {
        setError('')
        const fields = {
            'org': 'organisations',
            'media': 'media',
            'orgGroups': 'orgGroups',
            'mediaGroups': 'mediaGroups'
        }
        dispatch(setQuery({ ...query, [fields[orgType]]: list.filter(v => v.length > 0) }))
        //dispatch(setNeedsUpdate())
    }

    const LinkLabel = ({ mode, pl }) => {
        const { t } = useTranslation()
        const orgPercent = (pl.value / (pl.payload.payload.source?.value ?? 100) * 100).toFixed(2);
        const mediaPercent = (pl.value / (pl.payload.payload.target?.value ?? 100) * 100).toFixed(2)
        return <>
            {pl.payload.payload.source.name.startsWith('Other') ?
                t(pl.payload.payload.source.name) : pl.payload.payload.source.name
            }{['org', 'both'].includes(mode) && !pl.payload.payload.source.name.startsWith('Other') ? `${+orgPercent % 100 !== 0 ? '('+orgPercent+'%)' : ''} ` : null}
            &#x2192;
            {pl.payload.payload.target.name.startsWith('Other') ?
                t(pl.payload.payload.target.name) : pl.payload.payload.target.name
            }{['media', 'both'].includes(mode) && !pl.payload.payload.target.name.startsWith('Other') ? `${+mediaPercent % 100 !== 0 ? '('+mediaPercent+'%)' : ''} ` : null}
        </>
    }

    const customToolTip = (mode: 'org' | 'media' | 'both') => ({ active, payload }: any) => {
        if (!active) return null;
        //console.log(payload);
        const pl = payload[0]
        const isLink = pl.payload.payload.source && pl.payload.payload.target
        return active ? (<>
            <div className={`custom-tooltip ${mode} ${isLink ? 'link' : ''}`}>
                <div className="label">
                    {isLink ? <LinkLabel mode={mode} pl={pl} /> : (pl.name.startsWith('Other') ? t(pl.name) : pl.name)}
                </div>
                <div className="intro" >{payload[0].value.toLocaleString(i18n.language, {
                    style: 'currency',
                    currency: 'EUR',
                })}{isLink ? <span className="pType"> ({pl.payload.payload.transferType == 2 ? t('Advertising') : t('Funding')})</span> : null}
                </div>
                {/*<div className=></div>*/}
            </div>
        </>) : null
    }
    const selectionBoxStyle = {
        //chips: { background: "red" },
        searchBox: {
            border: "none",
            "border-bottom": "1px solid blue",
            "border-radius": "0px",
            "background-color": "white"
        }
    }
    const selectPeriod = (period) => {
        const p = period.slice(3) + period.slice(1, 2);
        dispatch(setQuery({ ...query, from: p, to: p }))
        setKey('flows')
    }
    const toggleStates = [
        { label: t('Advertising'), value: 2 },
        { label: t('Funding'), value: 4 },
        { label: t('Both'), value: 0 },
    ];
    const handleStyle = {
        border: "5px solid #96dbfa",
        width: "20px",
        height: "20px"
    }
    const mode = ((query.media?.length > 0) && (query.organisations?.length > 0)) ? 'both' : query.media?.length > 0 ? 'media' : 'org'
    return <>
        <ShowError error={error} onClose={() => setError('')} />
        <ModalLoader isPending={pending} />
                <Card.Text>
                    {
                    /*
                    <pre>{JSON.stringify(periods,null,2)}</pre>
                    <pre>{JSON.stringify(periods,null,2)}</pre>
                    <pre>{startPeriod} - {endPeriod}</pre>
                    */}
                    <div className="settings" data-test-id="flowSettings">
                        <div className="row justify-content-between">
                            <div className="col-lg-4 col-md-5, col-12">
                                <div className="label">{t('Payers')}</div>
                                <Multiselect onSearch={populateList('org')}
                                    id="orgList"
                                    options={search.org.hits}
                                    isObject={false}
                                    selectedValues={query.organisations}
                                    loading={loadingOptions}
                                    style={selectionBoxStyle}
                                    emptyRecordMsg={t('Start typing')}
                                    onSelect={updateSelection('org')}
                                    onRemove={updateSelection('org')}
                                    placeholder={t('Select a Payer')}
                                    selectedValueDecorator={valueDecorator}
                                />
                                <Render when={groupsEnabled}>
                                    <div className="label">{t('Groups')}</div>
                                    <Multiselect
                                        id="orgGroups"
                                        options={groups.filter(g => g.type === "org" && g.group_type === "group").map(g => g.name)}
                                        isObject={false}
                                        selectedValues={query.orgGroups}
                                        style={selectionBoxStyle}
                                        onSelect={updateSelection('orgGroups')}
                                        onRemove={updateSelection('orgGroups')}
                                        placeholder={t('Select a Group')}
                                        selectedValueDecorator={valueDecorator}
                                    />
                                </Render>
                            </div>
                            <div className="col-lg-3 col-md-2 col-12 ">
                                <Row>
                                    <Col data-test-id="paymentTypeSwitch">
                                        <div className="label">{t('Advertising or Funding')}?</div>
                                        <ToggleSwitch values={toggleStates} selected={switchState}
                                            onChange={pT => dispatch(setQuery({ ...query, pType: pT === 0 ? [2, 4] : pT }))}
                                        />
                                    </Col>
                                    <Col data-test-id="groupingsToggle">
                                        <div className="label">{t('Groupings')} <Help text="Allows using pre-configured groupings" /></div>
                                        <BootstrapSwitchButton checked={groupsEnabled}
                                            width={120}
                                            onstyle="info" offstyle="secondary"
                                            onlabel={t("Enabled")}
                                            onChange={(enabled: boolean) => {
                                                dispatch(setGroupsEnabled(enabled))
                                                dispatch(setQuery({ ...query, mediaGroups: [], orgGroups: [] }))
                                            }}
                                            offlabel={t("Disabled")} />
                                    </Col>
                                </Row>
                            </div>
                            <div className="col-lg-4 col-md-5, col-12 ">
                                <div className="label">{t('Beneficiaries')}</div>
                                <Multiselect onSearch={populateList('media')}
                                    id="mediaList"
                                    options={search.media.hits}
                                    isObject={false}
                                    emptyRecordMsg={t('Start typing')}
                                    selectedValues={query.media}
                                    style={selectionBoxStyle}
                                    loading={loadingOptions}
                                    onSelect={updateSelection('media')}
                                    onRemove={updateSelection('media')}
                                    placeholder={t('Select a Beneficiary')}
                                    selectedValueDecorator={valueDecorator}
                                />
                                <Render when={groupsEnabled}>
                                    <div className="label">{t('Groups')}</div>
                                    <Multiselect
                                        id="mediaGroups"
                                        options={groups.filter(g => g.type === "media" && g.group_type === "group").map(g => g.name)}
                                        isObject={false}
                                        selectedValues={query.mediaGroups}
                                        style={selectionBoxStyle}
                                        onSelect={updateSelection('mediaGroups')}
                                        onRemove={updateSelection('mediaGroups')}
                                        placeholder={t('Select a Group')}
                                        selectedValueDecorator={valueDecorator}
                                    />
                                </Render>
                            </div>
                        </div>
                        <div className="row justify-content-between">
                            <div className="col-12">
                                <div className="label">{t('Period')}</div>
                                <Range vertical={false} max={periods.length - 1}
                                    railStyle={{ backgroundColor: "#999" }}
                                    onAfterChange={updateTimescale}
                                    value={[pStart, pEnd]}
                                    onChange={setPeriods}
                                    tipFormatter={index => periodToString(periods[index])}
                                    handleStyle={[handleStyle, handleStyle]}
                                    marks={periods.reduce((acc, v, i) => v.toString().endsWith("1") || [pStart, pEnd].includes(i) ? { ...acc, [i]: periodToString(v) } : acc, {})}
                                />

                            </div>
                        </div>
                    </div>
                    <Render when={data?.length === 0}>
                        <p></p>
                        <div className="alert alert-warning" role="alert">
                            <p>{t('No Data found that matches your settings')}.</p>
                        </div>
                    </Render>
                    <Tabs id="flow-timeline-tabs" activeKey={key}
                        onSelect={(k) => setKey(k ?? 'flows')} >
                        <Tab eventKey="flows" title={<><FontAwesomeIcon icon={faStream} /> {t('Flows')}</>}>
                            {/*<pre>{JSON.stringify(query,null,2)}</pre>*/}
                            <Render when={data?.length > 0 && !error}>
                                <div className="text-end info"><FontAwesomeIcon icon={faInfoCircle} /> {t(`${isTouchSupported() ? "Double click" : "Click"} on the chart to get more information`)}</div>

                                <ResponsiveContainer width="100%" height={(chartModel.links.length * 20 < 1000) ? 1000 : chartModel.links.length * 20}>
                                    <Sankey
                                        data={chartModel as any}
                                        node={<TransferNode i18n={i18n} containerWidth={960} />}
                                        nodePadding={10}
                                        link={<TransferLink />}
                                        margin={{
                                            left: 20,
                                            right: 20,
                                            top: 40,
                                            bottom: 150,
                                        }}
                                    >
                                        <defs>
                                            <linearGradient id={'linkGradient'}>
                                                <stop offset="0%" stopColor="rgba(0, 136, 254, 0.5)" />
                                                <stop offset="100%" stopColor="rgba(0, 197, 159, 0.3)" />
                                            </linearGradient>
                                            <linearGradient id={'linkGradientFunding'}>
                                                <stop offset="0%" stopColor="rgba(68,208,87,0.900078781512605)" />
                                                <stop offset="100%" stopColor="rgba(78,236,208,1)" />
                                            </linearGradient>
                                            <linearGradient id={'linkGradient2'}>
                                                <stop offset="0%" stopColor="rgba(244,223,238,0.900078781512605)" />
                                                <stop offset="100%" stopColor="rgba(164,191,190,1)" />
                                            </linearGradient>
                                        </defs>
                                        <Tooltip content={customToolTip(mode)} />
                                    </Sankey>
                                </ResponsiveContainer>
                                <DataTable
                                    title={t("Money Flows")}
                                    pagination={true}
                                    columns={colums}
                                    data={data}
                                    actions={<ExportToExcel data={data} fileName="MoneyFlow" />}
                                />
                            </Render>

                        </Tab>
                        <Tab eventKey="timeline" title={<><FontAwesomeIcon icon={faChartLine} /> {t('Timeline')}</>} >
                            <Render when={timeline?.length > 0 && !error}>
                            <div className="text-end info"><FontAwesomeIcon icon={faInfoCircle} /> {t(`${isTouchSupported() ? "Double click" : "Click"} on the chart to get more information`)}</div>
                            <ReactECharts option={timelineOptions} onEvents={{'click': handleClick}} style={{height:450}} />
                            </Render>
                        </Tab>
                    </Tabs>

                </Card.Text>
    </>
}
