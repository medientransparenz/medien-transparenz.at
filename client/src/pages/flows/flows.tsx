import { useEffect, useState} from "react";
import { Card, Nav, Tab } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { Link, Outlet, useLocation } from "react-router-dom";
import { InfoTooltip } from "../../components/helper-components";
import "./flows.css";

export const Flows = () => {
  const { t } = useTranslation();
  const [key, setKey] = useState('transfers');
  const location = useLocation();

  useEffect(() => {
    if (location.pathname.includes("fundings")) {
      setKey("fundings")
    } else if (location.pathname.includes("mixed")) {
      setKey("mixed")
    } else {
      setKey("transfers")
    }
  }, [location])
  return (
    <>
       <Card className="flows">
        <Card.Title>{t("Money Flows")}</Card.Title>
        <Card.Body style={{ padding: "0 1rem 1rem 1rem" }}>
      <Tab.Container activeKey={key}  onSelect={(k) => setKey(k ?? "transfers")}>
        <Nav variant="tabs">
          <Nav.Item>
            <Nav.Link eventKey="transfers" as={Link} to={`transfers`}>
              {t("Media Transparency")}<span style={{margin: "0 0 0 .25em"}}><InfoTooltip text="about_MT" /></span>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="fundings" as={Link} to={`fundings`}>
              {t("Fundings")}<span style={{margin: "0 0 0 .25em"}}><InfoTooltip text="about_PF" /></span>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="mixed" as={Link} to={`mixed`}>
            {t("Mixed")}<span style={{margin: "0 0 0 .25em"}}><InfoTooltip text="about_PF_MT" /></span>
            </Nav.Link>
          </Nav.Item>
        </Nav>
      </Tab.Container>
      <Tab.Content>
        <Outlet/>
          </Tab.Content>
          </Card.Body>
        </Card>
      </>
  );
};