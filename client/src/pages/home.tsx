import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import Card from "react-bootstrap/esm/Card";
import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { getActiveBlogData } from "../services/data-service";
import { Blog } from "./blog/blog";
import "./home.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  TwitterShareButton,
  TwitterIcon,
  FacebookShareButton,
  FacebookIcon,
  WhatsappShareButton,
  WhatsappIcon,
  EmailShareButton,
  EmailIcon,
} from "react-share";

// import Swiper core and required modules
import { Navigation, Pagination } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import i18n from "../i18n";
import useBreakpoint from "../helpers/breakpointHook";

export const Home = () => {
  const { t } = useTranslation();
  const mdSize = 6;
  const mdOffset = 0;
  const lgSize = 4;
  const lgOffset = 0;
  const xlSize = 3;
  const xlOffset = 0;
  const breakpoint = useBreakpoint();

  const [blogs, setBlogs] = useState<Blog[]>([]);

  const currentLang: string = i18n.language;
  useEffect(() => {getActiveBlogData().then(entries => setBlogs(entries))}, []);

  const getStyleCount = (): number => {
    if (breakpoint < 830) {
      return 1
    } else if (breakpoint >= 830 && breakpoint < 1145) {
      return 2
    } else if (breakpoint >= 1145 && breakpoint < 1800) {
      return 3
    } else if (breakpoint >= 1800) {
      return 4
    }
    return 3;
  }

  return (
    <>
      {blogs.length > 0 && (
        <Card >
          <Card.Title>News</Card.Title>
          <Card.Body>
            <Row>
              <div
                className="swiper-container"
                style={{ display: blogs.length < getStyleCount() ? "block" : "flex" }}
              >
                {blogs.length > getStyleCount() && (
                  <i className="review-swiper-button-prev">
                    <FontAwesomeIcon icon={faAngleLeft} />
                  </i>
                )}
                <Swiper
                  modules={[Navigation, Pagination]}
                  spaceBetween={20}
                  navigation={{
                    nextEl: ".review-swiper-button-next",
                    prevEl: ".review-swiper-button-prev",
                  }}
                  loop={blogs.length >= (getStyleCount()*2)}
                  pagination={{
                    clickable: true,
                    el: ".swiper-custom-pagination",
                  }}
                  slidesPerView={1}
                  breakpoints={{
                    830: {
                      slidesPerView: blogs.length < 2 ? blogs.length : 2,
                    },
                    1145: {
                      slidesPerView: blogs.length < 3 ? blogs.length : 3,
                    },
                    1800: {
                      slidesPerView: blogs.length < 4 ? blogs.length : 4,
                    },
                  }}
                >
                  {blogs &&
                    blogs.map((blog) => (
                      <SwiperSlide key={blog._id}>
                        <Card className="h-100">
                          <Card.Title>
                            {new Date(blog.createDate).toLocaleDateString(currentLang,{}) +
                              " | " +
                              (currentLang.startsWith("de") ? blog.titleGerman : blog.titleEnglish)}
                          </Card.Title>
                          <Card.Body>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: (currentLang.startsWith("de") ? blog.contentGerman : blog.contentEnglish),
                              }}
                            ></div>
                          </Card.Body>
                          <Card.Footer>
                            <Row>
                              <Col className="align-center">Teilen:</Col>
                              <Col>
                                <TwitterShareButton
                                  url={window.location.href}
                                  title={(currentLang.startsWith("de") ? blog.titleGerman : blog.titleGerman)}
                                >
                                  <TwitterIcon size={32} round />
                                </TwitterShareButton>
                              </Col>
                              <Col>
                                <FacebookShareButton
                                  className="ml-1"
                                  url={window.location.href}
                                  title={(currentLang.startsWith("de") ? blog.titleGerman : blog.titleGerman)}
                                >
                                  <FacebookIcon size={32} round />
                                </FacebookShareButton>
                              </Col>
                              <Col>
                                <WhatsappShareButton
                                  className="ml-1"
                                  url={window.location.href}
                                  title={(currentLang.startsWith("de") ? blog.titleGerman : blog.titleGerman)}
                                >
                                  <WhatsappIcon size={32} round />
                                </WhatsappShareButton>
                              </Col>
                              <Col>
                                <EmailShareButton
                                  className="ml-1"
                                  url={window.location.href}
                                  title={(currentLang.startsWith("de") ? blog.titleGerman : blog.titleGerman)}
                                >
                                  <EmailIcon size={32} round />
                                </EmailShareButton>
                              </Col>
                            </Row>
                          </Card.Footer>
                        </Card>
                      </SwiperSlide>
                    ))}
                </Swiper>
                {blogs.length > getStyleCount() && (
                  <i className="review-swiper-button-next">
                    <FontAwesomeIcon icon={faAngleRight} />
                  </i>
                )}
              </div>
              {blogs.length > getStyleCount() && <div className="swiper-custom-pagination" />}
            </Row>
          </Card.Body>
        </Card>
      )}
      <Card>
        <Card.Title>{t("Background")}</Card.Title>
        <Card.Body>  
          <Card.Text>
            <p>{t("Welcome_Text")}</p>
            <p>{t("Welcome_Text_2")}</p>
          </Card.Text>
        </Card.Body>
      </Card>
      <Row>
        <Col
          xl={{ span: xlSize, offset: xlOffset }}
          lg={{ span: lgSize, offset: lgOffset }}
          sm={{ span: mdSize, offset: mdOffset }}
        >
          <Card>
            <Link to="/overview">
              <Card.Img variant="top" src="/img/Overview.png" />
            </Link>
            <Card.Title>{t("Overview")}</Card.Title>
            <Card.Body>
              <Card.Text>{t("Overview_Text")}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <Link to="/overview">{t("Go to the page!")}</Link>
            </Card.Footer>
          </Card>
        </Col>
        <Col
          sm={{ span: mdSize, offset: mdOffset }}
          lg={{ span: lgSize, offset: lgOffset }}
          xl={{ span: xlSize, offset: xlOffset }}
        >
          <Card>
            <Link to="/top">
              <Card.Img variant="top" src="/img/Top.png" />
            </Link>
            <Card.Title>{t("Top Values")}</Card.Title>
            <Card.Body>
              <Card.Text>{t("Top_Text")}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <Link to="/top">{t("Go to the page!")}</Link>
            </Card.Footer>
          </Card>
        </Col>
        <Col
          sm={{ span: mdSize, offset: mdOffset }}
          lg={{ span: lgSize, offset: lgOffset }}
          xl={{ span: xlSize, offset: xlOffset }}
        >
          <Card>
            <Link to="/flows">
              <Card.Img variant="top" src="/img/Flow.png" />
            </Link>
            <Card.Title>{t("Money Flows")}</Card.Title>
            <Card.Body>
              <Card.Text>{t("Flow_Text")}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <Link to="/flows">{t("Go to the page!")}</Link>
            </Card.Footer>
          </Card>
        </Col>
        <Col
          sm={{ span: mdSize, offset: mdOffset }}
          lg={{ span: lgSize, offset: lgOffset }}
          xl={{ span: xlSize, offset: xlOffset }}
        >
          <Card>
            <Link to="/search">
              <Card.Img variant="top" src="/img/Search.png" />
            </Link>
            <Card.Title>{t("Search")}</Card.Title>
            <Card.Body>
              <Card.Text>{t("Search_Text")}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <Link to="/search">{t("Go to the page!")}</Link>
            </Card.Footer>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <Card.Body>
              <Card.Text>
                {t("Description_Text")}{" "}
                <a
                  href="https://www.ris.bka.gv.at/GeltendeFassung.wxe?Abfrage=Bundesnormen&Gesetzesnummer=20007610 "
                  target="_blank"
                  rel="noreferrer"
                >
                  [1]
                </a>
              </Card.Text>
              <Row>
                <Col sm={{ span: mdSize, offset: mdOffset }}>
                  <Card>
                    <Card.Title>{t("Media Transparency")}</Card.Title>
                    <Card.Body>
                      <Card.Text>{t("MT_Text")}</Card.Text>
                      <Table striped bordered hover>
                        <thead>
                          <tr>
                            <th>{t("MT_table")}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Werbeaufträge und Medienkooperationen</td>
                          </tr>
                          <tr>
                            <td>Förderungen*</td>
                          </tr>
                          <tr>
                            <td>ORF Gebühren</td>
                          </tr>
                        </tbody>
                      </Table>
                      <Card.Text>{t("MT_Text_2")}</Card.Text>
                      <br />
                      <Card.Text>*{t("MT_Text_3")}</Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
                <Col sm={{ span: mdSize, offset: mdOffset }}>
                  <Card>
                    <Card.Title>{t("Fundings")}</Card.Title>
                    <Card.Body>
                      <Card.Text>{t("PF_Text")}</Card.Text>
                      <Table striped bordered hover>
                        <thead>
                          <tr>
                            <th>{t("PF_Type")}</th>
                            <th>
                              {t("Objective")}{" "}
                              <a
                                href="https://www.rtr.at/rtr/service/opendata/OD_Uebersicht.de.html"
                                target="_blank"
                                rel="noreferrer"
                              >
                                [2]
                              </a>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{t("pref")}</td>
                            <td>{t("pref_o")}</td>
                          </tr>
                          <tr>
                            <td>{t("pubf")}</td>
                            <td>{t("pubf_o")}</td>
                          </tr>
                          <tr>
                            <td>{t("prif")}</td>
                            <td>{t("prif_o")}</td>
                          </tr>
                          <tr>
                            <td>{t("nikorf")}</td>
                            <td>{t("nikorf_o")}</td>
                          </tr>
                          <tr>
                            <td>{t("ferf")}</td>
                            <td>{t("ferf_o")}</td>
                          </tr>
                        </tbody>
                      </Table>
                      <Card.Text>{t("PF_Text_2")}</Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
};
