import { TYearlyFundingInfo } from "../pages/flows/flows-funding";
import { getColorFromPaymentDetailType } from "../pages/flows/flows-mixed";
import { getFlowFunding } from "../services/data-service";
import {
  getOtherFundingTypesName,
  getOtherReceiversName,
} from "./flows-mixed.helper";

export type TFundingFlowsRecord = {
  amount: number;
  fundingType: string; // = payer
  receiver: string;
  receiverGroups?: string;
  showFundingBasis?: boolean;
  fundingBasis?: string;
};

export type TFundingFlowsResult = TFundingFlowsRecord[];

export type TTimelineFundingResult = {
  year: number;
  yearsData: TYearlyFundingInfo[];
}[];

export type TFlowsFundingQuery = {
  from?: number;
  to?: number;
  fundingType: number[];
  receiver: string[];
  receiverGroups: string[];
  showFundingBasis?: boolean;
};

export const getFundingFlows = async (
  query: TFlowsFundingQuery,
  showFundingBasis: boolean,
  otherReceiverFundingEnabled: boolean
) => {
  const results = await Promise.all([
    getFlowFunding(query, otherReceiverFundingEnabled, showFundingBasis),
  ]);

  // TODO remove slices
  let fundings: TFundingFlowsResult = results[0].map((item) => {
    return {
      amount: item.amount,
      receiver:
        item.receiver.toLocaleLowerCase() === "other receivers"
          ? getOtherReceiversName()
          : item.receiver,
      receiverGroups: item.receiverGroups,
      fundingType:
        item.fundingType.toLocaleLowerCase() === "other fundingtypes"
          ? getOtherFundingTypesName()
          : item.fundingType,
      paymentDetailType: item.fundingType,
      fundingBasis: item.fundingBasis,
      showFundingBasis: showFundingBasis,
    } as TFundingFlowsRecord;
  });

  return fundings;
};

type TSankeyLinkModel = {
  source: string;
  target: string;
  value: number;
  lineStyle?: {
    color?: string;
  };
};

export const getFundingFlowsLinks = (
  result: TFundingFlowsResult,
  fundingBaseColors: Map<string, string>
) => {
  let linkModel: TSankeyLinkModel[] = [];

  // to calculate percentage in tooltip the sums are saved
  const payerSumMap: Map<string, number> = new Map();
  const receiverSumGroupMap: Map<string, number> = new Map();
  const receiverSumMap: Map<string, number> = new Map();
  const fundingBaseSumMap: Map<string, number> = new Map();

  // key = fundingType+|+fundingBasis+|+receiverGroups
  const receiverGroupMap: Map<string, number> = new Map();

  // key = fundingType+|+receiverGroups+|+receiver
  const receiverMap: Map<string, number> = new Map();

  // key = fundingType+|+fundindBase
  const fundingBaseMap: Map<string, number> = new Map();

  for (const item of result.filter((item) => item.amount)) {
    if (item.receiverGroups) {
      const receiverGroupKey =
        item.fundingType +
        "|" +
        (item.showFundingBasis ? item.fundingBasis + "|" : "|") +
        item.receiverGroups;
      if (receiverGroupMap.has(receiverGroupKey))
        receiverGroupMap.set(
          receiverGroupKey,
          ((receiverGroupMap.get(receiverGroupKey)
            ? receiverGroupMap.get(receiverGroupKey)
            : 0) as number) + item.amount
        );
      else receiverGroupMap.set(receiverGroupKey, item.amount);

      if (receiverSumGroupMap.has(item.receiverGroups)) {
        receiverSumGroupMap.set(
          item.receiverGroups,
          (receiverSumGroupMap.get(item.receiverGroups) as number) + item.amount
        );
      } else {
        receiverSumGroupMap.set(item.receiverGroups, item.amount);
      }
    }
    const receiverKey =
      item.fundingType +
      "|" +
      (item.receiverGroups ? item.receiverGroups : "") +
      "|" +
      (item.showFundingBasis && item.fundingBasis ? item.fundingBasis : "") +
      "|" +
      item.receiver;
    if (receiverMap.has(receiverKey))
      receiverMap.set(
        receiverKey,
        ((receiverMap.get(receiverKey)
          ? receiverMap.get(receiverKey)
          : 0) as number) + item.amount
      );
    else receiverMap.set(receiverKey, item.amount);

    if (receiverSumMap.has(item.receiver))
      receiverSumMap.set(item.receiver, (receiverSumMap.get(item.receiver) as number) + item.amount);
    else receiverSumMap.set(item.receiver, item.amount);

    if (payerSumMap.has(item.fundingType)) {
      payerSumMap.set(
        item.fundingType,
        (payerSumMap.get(item.fundingType) as number) + item.amount
      );
    } else {
      payerSumMap.set(item.fundingType, item.amount);
    }

    if (item.showFundingBasis && item.fundingBasis) {
      if (fundingBaseSumMap.has(item.fundingBasis))
        fundingBaseSumMap.set(item.fundingBasis, (fundingBaseSumMap.get(item.fundingBasis) as number) + item.amount);
      else fundingBaseSumMap.set(item.fundingBasis, item.amount);

      const fundingKey = item.fundingType + "|" + item.fundingBasis;
      if (fundingBaseMap.has(fundingKey))
        fundingBaseMap.set(
          fundingKey,
          ((fundingBaseMap.get(fundingKey)
            ? fundingBaseMap.get(fundingKey)
            : 0) as number) + item.amount
        );
      else fundingBaseMap.set(fundingKey, item.amount);
    }
  }

  for (const key of Array.from(fundingBaseMap.keys())) {
    const paymentDetailType = key.split("|")[0];
    const fundingBase = key.split("|")[1];
    linkModel.push({
      source: paymentDetailType,
      target: fundingBase,
      value: fundingBaseMap.get(key) as number,
      lineStyle: {
        color: getColorFromPaymentDetailType(paymentDetailType),
      },
    });
  }

  for (const key of Array.from(receiverMap.keys())) {
    const paymentDetailType = key.split("|")[0];
    const receiverGroup = key.split("|")[1];
    const fundingBase = key.split("|")[2];
    const receiverMedia = key.split("|")[3];
    let color = getColorFromPaymentDetailType(paymentDetailType);
    if (fundingBase && fundingBaseColors.has(fundingBase)) {
      color = fundingBaseColors.get(fundingBase) as string;
    }
    linkModel.push({
      source: receiverGroup
        ? receiverGroup
        : fundingBase
        ? fundingBase
        : paymentDetailType,
      target: receiverMedia,
      value: receiverMap.get(key) as number,
      lineStyle: {
        color: color,
      },
    });
  }

  for (const key of Array.from(receiverGroupMap.keys())) {
    const paymentDetailType = key.split("|")[0];
    const fundingBase = key.split("|")[1];
    const receiverGroup = key.split("|")[2];
    let color = getColorFromPaymentDetailType(paymentDetailType);
    if (fundingBase && fundingBaseColors.has(fundingBase)) {
      color = fundingBaseColors.get(fundingBase) as string;
    }
    linkModel.push({
      source: fundingBase ? fundingBase : paymentDetailType,
      target: receiverGroup,
      value: receiverGroupMap.get(key) as number,
      lineStyle: {
        color: color,
      },
    });
  }

  return { linkModel, payerSumMap, receiverSumGroupMap, receiverSumMap, fundingBaseSumMap };
};

export const addElementIfNotExist = <T>(oldList: T[], value: T): T[] => {
  return oldList.indexOf(value) ? [...oldList, value] : [...oldList];
};
