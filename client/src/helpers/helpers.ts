import i18n from "../i18n";

export const periodToString = (p: number) =>
  p ? `Q${p.toString().slice(4)}/${p.toString().slice(0, 4)}` : "";

export const objectNormalize = (obj) =>
  Object.entries(obj).reduce(
    (acc, [k, v]) =>
      Array.isArray(v) && v.length === 0
        ? acc
        : { ...acc, [k]: Array.isArray(v) ? v.join(" ") : [v].join(" ") },
    {}
  );

export const isEqual = (obj1, obj2) =>
  Object.keys(obj1).sort().join(" ") === Object.keys(obj2).sort().join(" ") &&
  Object.entries(obj1).reduce(
    (acc, [k, v]) => (v !== obj2[k] ? false : acc),
    true
  );

export const objectIncluded = (
  subObject: { [key: number]: string },
  superObject: { [key: number]: string }
) => {
  if (Object.keys(subObject).length === 0) {
    return false;
  }
  const subKeys = Object.keys(subObject);
  let ok = true;
  Object.keys(superObject).forEach((k) => {
    if (!subKeys.includes(k)) {
      ok = false;
    }
  });
  if (!ok) {
    return false;
  }
  /*
    if (!Object.keys(superObject).sort().join(' ').includes(Object.keys(subObject).sort().join(' '))) {
        return false;
    }*/
  const keys = Object.keys(subObject).sort();
  for (let i = 0; i < keys.length; i++) {
    const k = keys[i];
    if (Array.isArray(subObject[k])) {
      let array = [...subObject[k]];
      if (
        array.sort().join(" ") !==
        (Array.isArray(superObject[k])
          ? [...superObject[k]].sort().join(" ")
          : [superObject[k]].join(" "))
      ) {
        return false;
      }
    } else if (Array.isArray(superObject[k])) {
      if (superObject[k][0] !== subObject[k]) {
        return false;
      }
    } else {
      if (superObject[k] !== subObject[k]) {
        return false;
      }
    }
  }
  return true;
};

export const isTouchSupported = () =>
  "ontouchstart" in window && "ontouchend" in window;
export const zip = (a, b) => a.map((k, i) => [k, b[i]]);

export const toCurrency = (field: string) => (row: any) =>
  row[field]?.toLocaleString(i18n.language, {
    style: "currency",
    currency: "EUR",
  });
