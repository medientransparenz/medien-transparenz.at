import { t } from "i18next";
import { TYearlyFundingInfo } from "../pages/flows/flows-funding";
import { getColorFromPaymentDetailType } from "../pages/flows/flows-mixed";
import { TFlowsQuery } from "../pages/flows/flows-transfers";
import {
  getFlow,
  getFlowFunding,
  getTimeline,
  getTimelineFundings,
} from "../services/data-service";
import {
  TFlowsFundingQuery,
  TTimelineFundingResult,
} from "./flows-funding.helper";

export type TMixedFlowsRecord = {
  amount: number;
  receiverMedia: string;
  organisation: string; // = payer
  organisationGroup: string;
  paymentType: "PF" | "MT";
  paymentTypeFull: "Förderungstransparenz" | "Medientransparenz";
  paymentDetailType: string; // fundingType or transferType
  fundingType?: string;
  transferType?: number; // 2 = Werbung, 4 = Förderung
  receiverGroups?: string;
  showFundingBasis?: boolean;
  fundingBasis?: string;
};

export type TMixedFlowsResult = TMixedFlowsRecord[];

export type TTimelineResult = { period: string; total: number }[];

export type TMixedFlowsQuery = {
  from?: number;
  to?: number;
  receiver: string[];
  receiverGroups: string[];
};

const rtrOrgName = "Rundfunk und Telekom Regulierungs-GmbH (RTR-GmbH)";
const kommAustriaOrgName = 'Kommunikationsbehörde Austria ("KommAustria")';

export const getMixedTimeline = async (query: TMixedFlowsQuery) => {
  const fundingQuery: TFlowsFundingQuery = {
    fundingType: [0, 1, 2, 3, 4],
    receiver: query.receiver,
    receiverGroups: query.receiverGroups,
    from: query.from,
    to: query.to,
  };
  const transferQuery: TFlowsQuery = {
    organisations: [],
    mediaGroups: query.receiverGroups,
    media: query.receiver,
    from: +(query.from + "1"),
    to: +(query.to + "4"),
    pType: [2],
  };

  const results = await Promise.all([
    getTimelineFundings(fundingQuery),
    getTimeline(transferQuery),
  ]);
  const mixedTimeline: {
    year: number;
    yearsData: TYearlyFundingInfo[];
  }[] = [];
  for (let i = 0; i <= (query.to as number) - (query.from as number); i++) {
    const currYear = (query.from as number) + i;
    let fundingTimeline = results[0].find((it) => it.year === currYear);
    const transferTimeline = results[1].filter((it) =>
      it.period.startsWith(currYear + "")
    );

    if (transferTimeline && transferTimeline.length > 0) {
      if (!fundingTimeline) {
        fundingTimeline = {
          year: currYear,
          yearsData: [],
        };
      }
      fundingTimeline?.yearsData.push({
        name: "Werbung",
        entries: transferTimeline.length,
        total: transferTimeline.reduce((acc, it) => acc + it.total, 0),
      });
    }

    if (fundingTimeline) {
      mixedTimeline.push({
        year: fundingTimeline?.year as number,
        yearsData: fundingTimeline?.yearsData as TYearlyFundingInfo[],
      });
    }
  }

  return mixedTimeline as TTimelineFundingResult;
};

export const getMixedFlows = async (
  query: TMixedFlowsQuery,
  showFundingBasis: boolean,
  otherMediaDisabled: boolean
) => {
  const fundingQuery: TFlowsFundingQuery = {
    fundingType: [0, 1, 2, 3, 4],
    receiver: query.receiver,
    receiverGroups: query.receiverGroups,
    from: query.from,
    to: query.to,
  };
  const transferQuery: TFlowsQuery = {
    organisations: [],
    mediaGroups: query.receiverGroups,
    media: query.receiver,
    from: +(query.from + "1"),
    to: +(query.to + "4"),
    pType: [2],
  };

  const results = await Promise.all([
    getFlowFunding(fundingQuery, otherMediaDisabled, showFundingBasis),
    getFlow(transferQuery),
  ]);
  if (!otherMediaDisabled) {
    results[0] = results[0].filter(
      (item) => item.receiver.toLowerCase() !== "other receivers"
    );
    results[1] = results[1].filter(
      (item) => item.media.toLowerCase() !== "other media"
    );
  }

  // TODO remove slices
  let mixed: TMixedFlowsResult = results[0].map((item) => {
    return {
      amount: item.amount,
      receiverMedia:
        item.receiver.toLocaleLowerCase() === "other receivers"
          ? getOtherReceiversName()
          : item.receiver,
      fundingType: item.fundingType,
      paymentType: "PF",
      paymentTypeFull: "Förderungstransparenz",
      paymentDetailType: item.fundingType,
      fundingBasis: item.fundingBasis,
      organisation:
        item.fundingType === "Pressefoerderung" ||
          item.fundingType === "Publizistikfoerderung"
          ? kommAustriaOrgName
          : rtrOrgName,
      showFundingBasis: showFundingBasis,
      receiverGroups: item.receiverGroups,
    } as TMixedFlowsRecord;
  });

  mixed.push(
    ...results[1].map((item) => {
      return {
        amount: item.amount,
        receiverMedia:
          item.media.toLocaleLowerCase() === "other media"
            ? getOtherReceiversName()
            : item.media,
        organisation: item.organisation,
        paymentType: "MT",
        paymentTypeFull: "Medientransparenz",
        paymentDetailType: item.transferType === 2 ? "Werbung" : "Förderung", // TODO check for other methods to get string
        transferType: item.transferType,
        showFundingBasis: showFundingBasis,
        organisationGroup: item.orgGroup,
        receiverGroups: item.mediaGroup,
        fundingBasis: "",
      } as TMixedFlowsRecord;
    })
  );

  return mixed;
};

export const mergeMixedArrayBasedOnProperty = (array: any, property: any) => {
  const newArray = new Map();
  array.forEach((item: any) => {
    const propertyValue = item[property];
    newArray.has(propertyValue)
      ? newArray.set(
        propertyValue,
        getMergedMixedFlowsEntry(item, newArray.get(propertyValue))
      )
      : newArray.set(propertyValue, item);
  });
  return Array.from(newArray.values());
};

const getMergedMixedFlowsEntry = (item, item2) => {
  const mergedItem = { ...item, ...item2 };
  mergedItem.amount = item.amount > item2.amount ? item.amount : item2.amount;
  return { ...item, ...item2 };
};

type TSankeyLinkModel = {
  source: string;
  target: string;
  value: number;
  lineStyle?: {
    color?: string;
  };
};

export const getMixedFlowsLinks = (
  result: TMixedFlowsResult,
  fundingBaseColors: Map<string, string>
) => {
  let linkModel: TSankeyLinkModel[] = [];

  // to calculate percentage in tooltip the sums are saved
  const payerSumMap: Map<string, number> = new Map();
  const receiverSumGroupMap: Map<string, number> = new Map();
  const receiverSumMap: Map<string, number> = new Map();
  const fundingBaseSumMap: Map<string, number> = new Map();

  // key = paymentDetailType+|+fundingBasis+|+receiverGroups
  const receiverGroupMap: Map<string, number> = new Map();

  // key = paymentDetailType+|+receiverGroups+|+receiver
  const receiverMap: Map<string, number> = new Map();

  // key = paymentDetailType+|+fundindBase
  const fundingBaseMap: Map<string, number> = new Map();

  for (const item of result.filter((item) => item.amount)) {
    if (item.receiverGroups) {
      const receiverGroupKey =
        item.paymentDetailType +
        "|" +
        (item.showFundingBasis ? item.fundingBasis + "|" : "|") +
        item.receiverGroups;
      if (receiverGroupMap.has(receiverGroupKey))
        receiverGroupMap.set(
          receiverGroupKey,
          ((receiverGroupMap.get(receiverGroupKey)
            ? receiverGroupMap.get(receiverGroupKey)
            : 0) as number) + item.amount
        );
      else receiverGroupMap.set(receiverGroupKey, item.amount);

      if (receiverSumGroupMap.has(item.receiverGroups))
        receiverSumGroupMap.set(item.receiverGroups, (receiverSumGroupMap.get(item.receiverGroups) as number) + item.amount);
      else receiverSumGroupMap.set(item.receiverGroups, item.amount);
    }
    const receiverKey = item.paymentDetailType + "|" + (item.receiverGroups ? item.receiverGroups : "") +
      "|" + (item.showFundingBasis && item.fundingBasis ? item.fundingBasis : "") +
      "|" + item.receiverMedia;
    if (receiverMap.has(receiverKey))
      receiverMap.set(receiverKey, ((receiverMap.get(receiverKey) ? receiverMap.get(receiverKey): 0) as number) + item.amount);
    else receiverMap.set(receiverKey, item.amount);

    if (receiverSumMap.has(item.receiverMedia))
      receiverSumMap.set(item.receiverMedia, (receiverSumMap.get(item.receiverMedia) as number) + item.amount);
    else receiverSumMap.set(item.receiverMedia, item.amount);

    if (payerSumMap.has(item.paymentDetailType))
      payerSumMap.set(item.paymentDetailType,(payerSumMap.get(item.paymentDetailType) as number) + item.amount);
    else payerSumMap.set(item.paymentDetailType, item.amount);

    if (item.showFundingBasis && item.fundingBasis) {
      if (fundingBaseSumMap.has(item.fundingBasis))
        fundingBaseSumMap.set(item.fundingBasis, (fundingBaseSumMap.get(item.fundingBasis) as number) + item.amount);
      else fundingBaseSumMap.set(item.fundingBasis, item.amount);

      const fundingKey = item.paymentDetailType + "|" + item.fundingBasis;
      if (fundingBaseMap.has(fundingKey))
        fundingBaseMap.set(
          fundingKey,
          ((fundingBaseMap.get(fundingKey)
            ? fundingBaseMap.get(fundingKey)
            : 0) as number) + item.amount
        );
      else fundingBaseMap.set(fundingKey, item.amount);
    }
  }

  for (const key of Array.from(fundingBaseMap.keys())) {
    const paymentDetailType = key.split("|")[0];
    const fundingBase = key.split("|")[1];
    linkModel.push({
      source: paymentDetailType,
      target: fundingBase,
      value: fundingBaseMap.get(key) as number,
      lineStyle: {
        color: getColorFromPaymentDetailType(paymentDetailType),
      },
    });
  }

  for (const key of Array.from(receiverMap.keys())) {
    const paymentDetailType = key.split("|")[0];
    const receiverGroup = key.split("|")[1];
    const fundingBase = key.split("|")[2];
    const receiverMedia = key.split("|")[3];
    let color = getColorFromPaymentDetailType(paymentDetailType);
    if (fundingBase && fundingBaseColors.has(fundingBase)) {
      color = fundingBaseColors.get(fundingBase) as string;
    }
    linkModel.push({
      source: receiverGroup
        ? receiverGroup
        : fundingBase
          ? fundingBase
          : paymentDetailType,
      target: receiverMedia,
      value: receiverMap.get(key) as number,
      lineStyle: {
        color: color,
      },
    });
  }

  for (const key of Array.from(receiverGroupMap.keys())) {
    const paymentDetailType = key.split("|")[0];
    const fundingBase = key.split("|")[1];
    const receiverGroup = key.split("|")[2];
    let color = getColorFromPaymentDetailType(paymentDetailType);
    if (fundingBase && fundingBaseColors.has(fundingBase)) {
      color = fundingBaseColors.get(fundingBase) as string;
    }
    linkModel.push({
      source: fundingBase ? fundingBase : paymentDetailType,
      target: receiverGroup,
      value: receiverGroupMap.get(key) as number,
      lineStyle: {
        color: color,
      },
    });
  }

  return {
    linkModel,
    payerSumMap,
    receiverSumGroupMap,
    receiverSumMap,
    fundingBaseSumMap
  };
};

export const addElementIfNotExist = <T>(oldList: T[], value: T): T[] => {
  return oldList.indexOf(value) ? [...oldList, value] : [...oldList];
};

export const getOtherReceiversName = () => {
  return t("Other receivers");
};

export const getOtherFundingTypesName = () => {
  return t("Other fundingtypes");
};
