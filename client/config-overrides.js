const webpack = require("webpack");
module.exports = {
  webpack: (config) => {
    config.resolve.fallback = {
      url: require.resolve("url"),
      fs: require.resolve("fs"),
      assert: require.resolve("assert"),
      crypto: require.resolve("crypto-browserify"),
      http: require.resolve("stream-http"),
      https: require.resolve("https-browserify"),
      os: require.resolve("os-browserify/browser"),
      buffer: require.resolve("buffer"),
      stream: require.resolve("stream-browserify"),
    };

    config.plugins.push(
      new webpack.ProvidePlugin({
        process: "process/browser",
        Buffer: ["buffer", "Buffer"],
      })
    );
    config.plugins.push(
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
      })
    );

    config.module.rules.unshift({
      test: /\.m?js$/,
      resolve: {
        fullySpecified: false, // disable the behavior
      },
    });

    return config;
  },
  // devServer: (configFunction) => (proxy) => configFunction(proxy, "all"),
};
