module.exports = {
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
      }),
    ],
  },
    resolve: {
      fallback: { "stream": false }
    }
  
};

resolve.fallback = { "stream": require.resolve("stream-browserify") }
config.resolve.fallback = { "stream": require.resolve("stream-browserify") }