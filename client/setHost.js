const os = require('os');
var replace = require('replace');

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const hostRegex = /(https?):\/\/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|localhost)/;

const getLocalExternalIP = () => {
    try {
        return [].concat(...Object.values(os.networkInterfaces()))
            .filter(details => details.family === 'IPv4' && !details.internal)
    } catch (err) { return ['127.0.0.1'] }
}


const update = (address) => { 
    console.log(`Setting tLog host address to: ${address}`);
       replace(
        {
            regex: hostRegex,
            replacement: "$1://" + address,
            paths: ['./src/services/server-config.ts'],
            recursive: false,
            silent: false
        });
    }

const determineAddress = () => {
    const addresses = getLocalExternalIP();
    if (addresses.length > 1) {
        console.log('There are multiple candidate addresses:')
        addresses.forEach((address, idx) => {
            console.log(`${idx + 1}.) ${address.address}`);
        });
        rl.question(`Please select the address to be used (1-${addresses.length}): `, (number) => {
            try {
                const address = addresses[parseInt(number,10)-1].address;
                update(address);
            } catch (err) { 
                console.err(`Could not determine address (err.message)`)
            } finally {
                rl.close();
            }
        })
    } else {
        const address = addresses[0].address;
        update(address);
        process.exit(0);
    }
}

determineAddress();