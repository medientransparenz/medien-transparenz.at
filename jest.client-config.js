const {defaults} = require('jest-config');
//console.log(defaults);
const config = Object.assign(defaults,tsJestPreset);
config.globals['ts-jest'].tsConfig = '<rootDir>/tsconfig.spec.json';
config.moduleNameMapper = {
    '^src/(.*)$': '<rootDir>/$1',
    '^app/(.*)$': '<rootDir>/app/$1',
    '^assets/(.*)$': '<rootDir>/assets/$1',
    '^environments/(.*)$': '<rootDir>/environments/$1'
};


module.exports = config;
