# A Helm Chart for www.medien-transparenz.at

## Install/Upgrade

```
helm upgrade mta . -n media-transparency --set mongodb.auth.rootPassword=secret --install
```