import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import * as jwt from 'jsonwebtoken';
import Event, { IEvent } from '../server/models/events';
import { IUser } from '../server/models/types';


mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;

const clearDB = () => Event.deleteMany({});
  const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);

beforeEach(async () => {await clearDB()})
afterEach(async () => {await clearDB()});
afterAll(async () => db.close())

describe('Create new Event', () => {
    it('should not be possible for unauthenticated users to create events', async () =>{
        const event: IEvent = {
            name: "Event 1",
            startDate: new Date(Date.now()),
            predictable: true,
            type: 'moment',
            region: 'AT-7'
        }
        //const newEventResponse = await supertest(app).post('/api/events').send(event);
        //expect(newEventResponse.status).toBe(403);
        expect(event.predictable).toBeTruthy()
    })
})
