import { IUser, IUserDocument } from '../server/models/types';
import User from '../server/models/user';

import * as jwt from 'jsonwebtoken';
import ZipCodeCtrl from '../server/controllers/zipcodes';
import { promises as fsPromises } from 'fs';
import iconv from "iconv-lite";
import Grouping from '../server/models/grouping';
import { fileUpload } from '../client/src/services/data-service';
import Difference from '../server/models/differences';
import Duplicate from '../server/models/duplicates';


export const range = (size: number): Array<number> =>
  Array.from(new Array(size + 1).keys()).slice(1);

export const createUsers = (number, prefix = 'user', role = 'user'): Array<IUser> =>
  range(number).map(nr => ({
    username: `${prefix}${nr}`,
    email: `${prefix}${nr}@test.com`,
    password: 'topsecret',
    provider: 'local',
    roles: [role],
    active: true
  }));

export const saveUsers = (users: Array<IUser>): Promise<IUserDocument[]> =>
  Promise.all(users.map(u => new User(u).save()));

export const createAndSaveUsers =
  (number, prefix = 'user', role = 'user'): Promise<IUserDocument[]> =>
    saveUsers(createUsers(number, prefix, role));

export const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);

export const preFillZipCodes = async (filename: string) => {
  const zipCtrl = new ZipCodeCtrl();
  const data = await fsPromises.readFile(filename)
  const lines = iconv.decode(data, 'utf8').split('\n').slice(1);
  return zipCtrl.model.insertMany(
    lines.filter(l => l.length > 0)
      .map(zipCtrl.lineToZipCode),
    { ordered: false }
  )
    .catch(err => true);
}

export const preloadGroupings = async () => {
  const data = await fsPromises.readFile('test/files/groupings.json')
  const groupings = JSON.parse(iconv.decode(data, 'utf8'))
  const grps = groupings.map(grp => { delete grp._id; return new Grouping(grp) })
  const result = await Grouping.insertMany(grps)
  return result;
}

export const preloadDifferences = async () => {
  const data = await fsPromises.readFile('test/files/differences.json')
  const differences = JSON.parse(iconv.decode(data, 'utf8'))
  const diffs = differences.map(diff => { delete diff._id; return new Difference(diff) })
  const result = await Difference.insertMany(diffs)
  return result;
}

export const preloadDuplicates = async () => {
  const data = await fsPromises.readFile('test/files/duplicates.json')
  const entries = JSON.parse(iconv.decode(data, 'utf8'))
  const docs = entries.map(entry => { delete entry._id; return new Duplicate(entry) })
  const result = await Duplicate.insertMany(docs)
  return result;
}