/* eslint-disable no-undef */
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import Grouping from '../server/models/grouping';
dotenv.config({path: '.env.test'});

mongoose.connect(process.env.MONGODB_URI );
const db = mongoose.connection;

const clearDB = () => Promise.all([Grouping.deleteMany({})]);

afterAll(async () => {
    await clearDB()
    await db.close()
});

beforeEach(async () => {await clearDB()});

const checkValidationError = (error: Error, field: string, type: string) => {
    expect(error).toHaveProperty(field)
    expect(error[field].kind).toBe(type)
}

describe("Grouping Model", ()=>{
    it('should not be possible to store an empty model', async () => {
        try {
            await new Grouping({}).save();
        } catch (error) {
            const errors = error.errors;
            ['group_type', 'name','type','region'].forEach(field =>
                checkValidationError(errors,field,'required'))
        }
    })
    it('should be possible to store a valid grouping', async () => {
        const grouping = await new Grouping({
            name: 'MyGroup',
            members: ['Member1', 'Member2'],
            type: 'org',
            group_type: 'class',
            region: 'AT-4'
        }).save()
        expect(grouping._id).toBeDefined();
        expect(grouping.name).toBe('MyGroup');
        expect(grouping.isActive).toBeTruthy();
        expect(grouping.serverSide).toBeTruthy();
        expect(grouping.members).toHaveLength(2);
        expect(grouping.members).toContain('Member1');
        expect(grouping.members).toContain('Member2');
        expect(grouping.region).toBe('AT-4');
        expect(grouping.group_type).toBe('class');
        expect(grouping.type).toBe('org');
    })
    it('should not be possible to store grouping with wrong field values', async () => {
        try {
            const grouping = await new Grouping({
                name: 'MyGroup',
                members: ['Member1', 'Member2'],
                type: 'test',
                group_type: 'test',
                region: 'test'
            }).save()
        } catch (error) {
            const errors = error.errors;
            ['type','group_type','region'].forEach(f=>
                checkValidationError(errors,f,'enum')
            )
        }
    })
})
