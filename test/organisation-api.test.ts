import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import User from '../server/models/user';
import {IUser} from '../server/models/types';
import * as jwt from 'jsonwebtoken';
import { createAndSaveUsers, preFillZipCodes } from './helpers';
import ZipCode from '../server/models/zipcodes';
import fs from 'fs';
import Organisation, { IOrganisation, IOrganisationDocument } from '../server/models/organisations';

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;
//(<any>mongoose).Promise = Bluebird;


const clearDB = () => {
  //console.log("About to delete users from Database")
  return Promise.all([User.deleteMany({}),ZipCode.deleteMany(),Organisation.deleteMany()])
};
const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);

beforeEach(async () => {await clearDB(); });
afterEach(async () => await clearDB());
afterAll(async () => db.close())

describe('Organisation Upload', ()=>{
    it('should be possible to upload a list of organisations for admins', async () => {
      const admins = await createAndSaveUsers(1,'admin','admin');
      await preFillZipCodes('test/files/PLZ-Verzeichnis_long.csv');
      const uploadOrganisationResponse = await supertest(app)
        .post(`/api/organisations/import`)
        .set('Authorization', `Bearer ${getToken(admins[0])}`)
        .attach('file', fs.createReadStream('test/files/Rechtstraeger_Stand_15_09_20.csv'));
      expect(uploadOrganisationResponse.status).toBe(200); 
      expect(uploadOrganisationResponse.body.entries).toBe(5689) 
      expect(uploadOrganisationResponse.body.savedEntries).toBe(5689)
      expect(uploadOrganisationResponse.body.errorEntries).toHaveLength(12);
    })
    it('should not be possible to upload a list of organisations for non-admins', async () => {
        const users = await createAndSaveUsers(1);
        await preFillZipCodes('test/files/PLZ-Verzeichnis_long.csv');
        const uploadOrganisationResponse = await supertest(app)
          .post(`/api/organisations/import`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
          .attach('file', fs.createReadStream('test/files/Rechtstraeger_Stand_15_09_20.csv'));
        expect(uploadOrganisationResponse.status).toBe(403); 
      })
})

describe('Create Organisations', ()=>{
  it('should be possible to add new organisations for admins', async () => {
    const admins = await createAndSaveUsers(1,'admin','admin');
    const newOrg : IOrganisation = {
      name: "MyOrg",
      street: "My Street 1",
      city_de: "My City",
      type: "Company",
      federalState: "AT-6",
      country_de: "Austria",
      zipCode: "4711"
    }
    const newOrganisationResponse = await supertest(app)
        .post(`/api/organisations`)
        .set('Authorization', `Bearer ${getToken(admins[0])}`)
        .send(newOrg);
    expect(newOrganisationResponse.status).toBe(200);
    const storedOrganisation = newOrganisationResponse.body as IOrganisationDocument
    expect(storedOrganisation._id).toBeDefined()
    Object.entries(newOrg).forEach(([key,value]) => {
      expect(storedOrganisation[key]).toBe(value)
    })
  })
})

