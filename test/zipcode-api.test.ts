import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import User from '../server/models/user';
import {IUser} from '../server/models/types';
import * as jwt from 'jsonwebtoken';
import { createAndSaveUsers, preFillZipCodes } from './helpers';
import ZipCode from '../server/models/zipcodes';
import ZipCodeCtrl from '../server/controllers/zipcodes';
import fs from 'fs';


mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;
//(<any>mongoose).Promise = Bluebird;


const clearDB = () => {
  //console.log("About to delete users from Database")
  return Promise.all([User.deleteMany({}),ZipCode.deleteMany()])
};
const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);




beforeEach(async () => {await clearDB(); });
afterEach(async () => await clearDB());
afterAll(async () => db.close())

describe('ZipCode Controller', ()=>{
    it('should turn a line into a ZipCode entity',  () => {
        const zipCtrl = new ZipCodeCtrl();
        const result = zipCtrl.lineToZipCode('1000;Wien;W;01.08.2009;;PLZ-Postfach;extern;Nein;Ja');
        expect(result.zipCode).toBe('1000');
        expect(result.federalState).toBe('Vienna');
        expect(result.federalStateCode).toBe('AT-9');
    })
})

describe('Zipcode Upload', ()=>{
  it('should be possible to upload a list of zip codes for admins', async () => {
    const admins = await createAndSaveUsers(1,'admin','admin');
    const uploadZipCodesResponse = await supertest(app)
      .post(`/api/zipcodes/import`)
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
      .attach('file', fs.createReadStream('test/files/PLZ-Verzeichnis_long.csv'));
    expect(uploadZipCodesResponse.status).toBe(200);  
    expect(uploadZipCodesResponse.body.entries).toBe(2547)
    expect(uploadZipCodesResponse.body.savedEntries).toBe(2547)
  })
  it('should be possible to upload incrementally', async ()=> {
    const request = supertest(app);
    const admins = await createAndSaveUsers(1,'admin','admin');
    await preFillZipCodes('test/files/PLZ-Verzeichnis_short.csv');
    const uploadZipCodesResponse = await request
      .post(`/api/zipcodes/import`)
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
      .attach('file', fs.createReadStream('test/files/PLZ-Verzeichnis_long.csv'));
    expect(uploadZipCodesResponse.status).toBe(200);  
    expect(uploadZipCodesResponse.body.entries).toBe(2547)
    expect(uploadZipCodesResponse.body.savedEntries).toBe(2547)
  })
  it("should not be possible to upload zipCodes for non-admin users", async ()=>{
    const request = supertest(app);
    const users = await createAndSaveUsers(1);
    const uploadZipCodesResponse = await request
      .post(`/api/zipcodes/import`)
      .set('Authorization', `Bearer ${getToken(users[0])}`)
      .attach('file', fs.createReadStream('test/files/PLZ-Verzeichnis_long.csv'));
    expect(uploadZipCodesResponse.status).toBe(403);
  })
})

describe('Zipcodes API', ()=>{
  it('should be possible get a list of all zip codes for admins', async ()=> {
    const request = supertest(app);
    const admins = await createAndSaveUsers(1,'admin','admin');
    await preFillZipCodes('test/files/PLZ-Verzeichnis_short.csv');
    const listZipCodesResponse = await request
      .get(`/api/zipcodes`)
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
    expect(listZipCodesResponse.status).toBe(200); 
    expect(listZipCodesResponse.body).toHaveLength(100); 
    expect(listZipCodesResponse.body[10].zipCode).toBe("1024");
    expect(listZipCodesResponse.body[10].federalState).toBe("Vienna");
    expect(listZipCodesResponse.body[10].federalStateCode).toBe("AT-9");
  })
  it('should be possible get a paginated list of zip codes for admins', async ()=> {
    const request = supertest(app);
    const admins = await createAndSaveUsers(1,'admin','admin');
    await preFillZipCodes('test/files/PLZ-Verzeichnis_short.csv');
    const listZipCodesResponse = await request
      .get(`/api/zipcodes?page=2&size=20`)
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
  
    expect(listZipCodesResponse.status).toBe(200); 
    expect(listZipCodesResponse.body).toHaveLength(20); 
    expect(listZipCodesResponse.body[0].zipCode).toBe("1040");
    expect(listZipCodesResponse.body[0].federalState).toBe("Vienna");
    expect(listZipCodesResponse.body[0].federalStateCode).toBe("AT-9");
  })
  it('should be possible get the number of zip codes for admins', async ()=> {
    const request = supertest(app);
    const admins = await createAndSaveUsers(1,'admin','admin');
    await preFillZipCodes('test/files/PLZ-Verzeichnis_short.csv');
    const listZipCodesResponse = await request
      .get(`/api/zipcodes/count`)
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
  
    expect(listZipCodesResponse.status).toBe(200); 
    expect(listZipCodesResponse.body).toBe(100); 
  })
})