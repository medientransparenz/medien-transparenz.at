import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import { promises as fsPromises } from 'fs';
import iconv from "iconv-lite";
import Transfer from '../server/models/transfer';
import { TOverViewResult, TOverViewTransferResult } from '../server/controllers/transfer';
import { createAndSaveUsers, getToken } from './helpers';
import User from '../server/models/user';
import { ITransferDocument } from '../server/models/transfer';



mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;


const preloadTransfer = async () => {
    const data = await fsPromises.readFile('test/files/transfers.json')
    const orgs = JSON.parse(iconv.decode(data, 'utf8'))
    const o = orgs.map(trans => {delete trans._id; delete trans.organisationReference ;return new Transfer(trans)})
    const result = await Transfer.insertMany(o)
    return result;
}   

jest.setTimeout(60000);
    

beforeAll(()=> {
  return Promise.all([Transfer.deleteMany(), User.deleteMany()]).then(preloadTransfer)
});
afterAll(() => Promise.all([Transfer.deleteMany(), User.deleteMany()]).then(()=>db.close()))  
afterEach(() => User.deleteMany())

describe('Transfer Data API', ()=>{
    it('should be possible to get an overview of transfers', async () => {
      const transferOverviewResponse = await supertest(app)
        .get(`/api/transfers/overview`)     
      expect(transferOverviewResponse.status).toBe(200); 
     const overview: TOverViewTransferResult = transferOverviewResponse.body
     expect(overview).toHaveLength(2)
     overview.forEach(o => expect(o.quarters).toHaveLength(12));
    })
})

describe('List Transfers', () => {
  it('should be possible to get transfers paginated', async () => {
    const admins = await createAndSaveUsers(1,'admin','admin');
    const pageResponse = await supertest(app)
      .get('/api/transfers?page=1&size=20&sortBy=media&sortOrder=desc&organisation=joa') 
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
      expect(pageResponse.status).toBe(200)
      expect(pageResponse.body).toHaveLength(20)
  })
  it('should be possible to get a specific transfer', async () => {
    const admins = await createAndSaveUsers(1,'admin','admin');
    const testTransfers = await Transfer.find({}).skip(41).limit(1).exec()
    const testTransfer = testTransfers[0]
    const singleResponse = await supertest(app)
      .get(`/api/transfers/${testTransfer._id}`) 
      .set('Authorization', `Bearer ${getToken(admins[0])}`)
      expect(singleResponse.status).toBe(200)
      const transfer = singleResponse.body as ITransferDocument
      expect(transfer.organisation).toBe(testTransfer.organisation)
      expect(transfer.period).toBe(testTransfer.period)
      expect(transfer.transferType).toBe(testTransfer.transferType)
  })
})

describe('Search Transfers', () => {
  it('should return search results in a case-insensitive order', async () => {
    const singleResponse = await supertest(app)
      .get(`/api/transfers/search?name=orf`) 
    expect(singleResponse.status).toBe(200)
    const {media,org} = singleResponse.body
    expect(media).toHaveLength(31)
    expect(org).toHaveLength(7)
    expect(media[8].name).toBe('oe3.orf.at')
    expect(media[9].name).toBe('ORF')
  })
})