/* eslint-disable no-undef */
import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import { promises as fsPromises } from 'fs';
import iconv from "iconv-lite";
import { createAndSaveUsers, getToken } from './helpers';
import User from '../server/models/user';
import Funding, { IFundingDocument } from '../server/models/funding';
import { TOverViewFundingResult } from '../server/controllers/funding';


mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;

const preloadTransfer = async () => {
    const data = await fsPromises.readFile('test/files/fundings.json')
    const fundings = JSON.parse(iconv.decode(data, 'utf8'))
    const f = fundings.map(funding => {delete funding._id; delete funding.__v ;return new Funding(funding)})
    const result = await Funding.insertMany(f)
    return result;
}

jest.setTimeout(60000);

beforeAll(()=> {
  return Promise.all([Funding.deleteMany(), User.deleteMany()]).then(preloadTransfer)
});
afterAll(() => Promise.all([Funding.deleteMany(), User.deleteMany()]).then(()=>db.close()))
afterEach(() => User.deleteMany())

describe('Funding Data API', ()=>{
    it('should be possible to get an overview of fundings', async () => {
      const fundingOverviewResponse = await supertest(app)
        .get(`/api/fundings/overview`)
      expect(fundingOverviewResponse.status).toBe(200);
     const overview: TOverViewFundingResult = fundingOverviewResponse.body
     expect(overview).toHaveLength(2)
     overview.forEach(f => expect(f.yearsData.length > 0).toBeTruthy());
    })
})

describe('List Fundings', () => {
    it('should be possible to get funding paginated', async () => {
      const admins = await createAndSaveUsers(1,'admin','admin');
      const pageResponse = await supertest(app)
        .get('/api/fundings?page=1&size=20&sortBy=media&sortOrder=desc&organisation=joa')
        .set('Authorization', `Bearer ${getToken(admins[0])}`)
        expect(pageResponse.status).toBe(200)
        expect(pageResponse.body).toHaveLength(20)
    })
    it('should be possible to get a specific funding', async () => {
      const admins = await createAndSaveUsers(1,'admin','admin');
      const testFundings = await Funding.find({}).skip(41).limit(1).exec()
      const testFunding = testFundings[0]
      const singleResponse = await supertest(app)
        .get(`/api/fundings/${testFunding._id}`)
        .set('Authorization', `Bearer ${getToken(admins[0])}`)
        expect(singleResponse.status).toBe(200)
        const funding = singleResponse.body as IFundingDocument
        expect(funding.receiver).toBe(testFunding.receiver)
        expect(funding.fundedObject).toBe(testFunding.fundedObject)
        expect(funding.name).toBe(testFunding.name)
    })
  })

  describe('Search Fundings', () => {
    it('should return search results in a case-insensitive order for searchname salzburg', async () => {
      const singleResponse = await supertest(app)
        .get(`/api/fundings/search?name=salzburg`)
      expect(singleResponse.status).toBe(200)
      const {receiver} = singleResponse.body
      expect(receiver).toHaveLength(2)
      expect(receiver[0].name).toBe('Salzburger Nachrichten Verlagsgesellschaft m.b.H. & Co KG')
      expect(receiver[0].lowerName).toBe('salzburger nachrichten verlagsgesellschaft m.b.h. & co kg')
    })
    it('should return search results in a case-insensitive order for searchname red', async () => {
        const singleResponse = await supertest(app)
          .get(`/api/fundings/search?name=red`)
        expect(singleResponse.status).toBe(200)
        const {receiver} = singleResponse.body
        expect(receiver).toHaveLength(1)
        expect(receiver[0].name).toBe('OÖN Redaktion GmbH & CO KG')
        expect(receiver[0].lowerName).toBe('oÖn redaktion gmbh & co kg')
      })
  })

  describe('Funding Periods', () => {
    it('should be possible to get the beginning and ending periods for all funding', async () => {
      const periodsResponse = await supertest(app)
          .get(`/api/fundings/periods`)
      expect(periodsResponse.status).toBe(200)
      expect(periodsResponse.body).toHaveLength(2)
      const [min,max] = periodsResponse.body
      expect(min).toBe(2010)
      expect(max).toBe(2011)
    })
  })
