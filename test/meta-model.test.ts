import dotenv from 'dotenv';
import mongoose from 'mongoose';
import Grouping from '../server/models/grouping';
dotenv.config({path: '.env.test'});

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;

const clearDB = () => Promise.all([Grouping.deleteMany({})]);

afterAll(async () => {
    await clearDB()
    await db.close()
});

beforeEach(async () => await clearDB());

const checkValidationError = (error: Error, field: string, type: string) => {
    expect(error).toHaveProperty(field)
    expect(error[field].kind).toBe(type)
}

describe("Meta Model", ()=>{

    it('should be possible figure out which fields a model consists of', async () => {
        const model = Grouping;
        const fields = (model.base.models[model.modelName].schema as any).tree
        expect(fields).toHaveProperty('region')
        expect(fields['region'].type.name).toBe('String')
    })

})