import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import { preloadDuplicates } from './helpers';
import Duplicate from '../server/models/duplicates';

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;

beforeAll(async () => await preloadDuplicates())
afterAll(async () => {
    await Duplicate.deleteMany({})
    await db.close()
})

describe('List Differences', ()=>{
    it('should be possible to retrieve a list of differences', async ()=> {
        const getDifferenceResponse = await supertest(app)
            .get('/api/duplicates')
        expect(getDifferenceResponse.status).toBe(200);
        expect(getDifferenceResponse.body).toHaveLength(145);    
    })
})