import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import * as jwt from 'jsonwebtoken';
import Grouping from '../server/models/grouping';
import { IGroupingDocument } from '../server/models/grouping';
import { createAndSaveUsers, preloadGroupings } from './helpers';
import { IUser } from '../server/models/types';
import User from '../server/models/user';

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;

const clearDB = () => Promise.all([Grouping.deleteMany({}),User.deleteMany({})]);
const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);

let users;

beforeAll(async () => {
    await clearDB()
    await preloadGroupings()
    users = await createAndSaveUsers(1, 'user', 'group');
})
afterAll(async () => {
    await clearDB()
    await db.close()
});

describe('Filter and Count Groupings',()=> {
    it('should be possible to retrieve all groupings if in role group', async () => {
        const getAllGroupingsResponse = await supertest(app)
          .get(`/api/groupings/all`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(getAllGroupingsResponse.status).toBe(200);
        const groups = getAllGroupingsResponse.body;
        expect(groups).toHaveLength(14) 
        const oebb = groups[0];
        expect(oebb.name).toBe('ÖBB Group');
        expect(oebb.type).toBe('org');
        expect(oebb.group_type).toBe('group');
        expect(oebb.members).toBeUndefined();
        const countAllResponse = await supertest(app)
            .get('/api/groupings/count/all')
            .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(countAllResponse.status).toBe(200);
        expect(countAllResponse.body).toBe(14) 
    })
    it('should be possible to retrieve pages ', async () => {
        const getAllGroupingsResponse = await supertest(app)
          .get(`/api/groupings/all?page=1&size=5&sortBy=name`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(getAllGroupingsResponse.status).toBe(200);
        const groups = getAllGroupingsResponse.body;
        expect(groups).toHaveLength(5) 
        const oebb = groups[0];
        expect(oebb.name).toBe("Boulevard 1 (Tageszeitungen)");
        expect(oebb.type).toBe('media');
        expect(oebb.group_type).toBe('class');
        expect(oebb.members).toBeUndefined();
        const countAllResponse = await supertest(app)
            .get('/api/groupings/count/all')
            .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(countAllResponse.status).toBe(200);
        expect(countAllResponse.body).toBe(14) 
    })
    it('should be possible to retrieve pages ', async () => {
        const getAllGroupingsResponse = await supertest(app)
          .get(`/api/groupings/all?page=1&size=5&sortBy=name&type=med&name=ule`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(getAllGroupingsResponse.status).toBe(200);
        const groups = getAllGroupingsResponse.body;
        expect(groups).toHaveLength(2) 
        const oebb = groups[0];
        expect(oebb.name).toBe("Boulevard 1 (Tageszeitungen)");
        expect(oebb.type).toBe('media');
        expect(oebb.group_type).toBe('class');
        expect(oebb.members).toBeUndefined();
        const countAllResponse = await supertest(app)
            .get('/api/groupings/count/all?type=med&name=ule')
            .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(countAllResponse.status).toBe(200);
        expect(countAllResponse.body).toBe(2) 
    })
})

