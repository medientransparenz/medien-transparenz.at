/* eslint-disable no-undef */
import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import User from '../server/models/user';
import {IUser} from '../server/models/types';
import * as jwt from 'jsonwebtoken';
import { createAndSaveUsers } from './helpers';
import fs from 'fs';
import Organisation from '../server/models/organisations';
import { promises as fsPromises } from 'fs';
import iconv from "iconv-lite";
import Transfer from '../server/models/transfer';
import { catController } from '../server/logging';

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;
//(<any>mongoose).Promise = Bluebird;


const clearDB = () => {
  //console.log("About to delete users from Database")
  return Promise.all([User.deleteMany({}), Transfer.deleteMany({})])
};
const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);

beforeEach(async () => {await clearDB(); });
afterEach(async () => await clearDB());

const preloadOrganisations = async () => {
    console.log("--------- Populating Organisations ----------")
    const data = await fsPromises.readFile('test/files/organisations.json')
    const orgs = JSON.parse(iconv.decode(data, 'utf8'))
    const o = orgs.map(org => {delete org._id; return new Organisation(org)})
    const result = await Organisation.insertMany(o)
    return result;
}


beforeAll(async ()=> {
  await Organisation.deleteMany()
  await preloadOrganisations()
})
afterAll(async () => {
  await Organisation.deleteMany()
  await db.close()
})

jest.setTimeout(60000);

describe('Transfer API', ()=>{
    it('should be possible to upload a list of transfer for admins', async () => {

        const dataFile = 'test/files/2019-refined-utf8.csv';
        const admins = await createAndSaveUsers(1,'admin','admin');
        const uploadTransferResponse = await supertest(app)
          .post(`/api/transfers/import`)
          .set('Authorization', `Bearer ${getToken(admins[0])}`)
          .attach('file', fs.createReadStream(dataFile));
        expect(uploadTransferResponse.status).toBe(200);
        expect(uploadTransferResponse.body.entries).toBe(9302);
        expect(uploadTransferResponse.body.savedEntries).toBe(9298)
        expect(uploadTransferResponse.body.errorEntries).toHaveLength(4);
        uploadTransferResponse.body.errorEntries.forEach(({msg}) =>
            expect(msg).toEqual(expect.stringContaining('E11000'))
        );
        const transfers = await Transfer.find({});
        catController.info(`Stored ${transfers.length} entries.`)
        expect(transfers.length).toBe(uploadTransferResponse.body.savedEntries);
        //expect(uploadTransferResponse.body.transfersWithoutAddress).toHaveLength(44)
    })
})
