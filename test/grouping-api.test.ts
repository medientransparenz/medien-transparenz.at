import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import * as jwt from 'jsonwebtoken';
import Grouping, { IGroupingDocument } from '../server/models/grouping';
import { createAndSaveUsers, preloadGroupings } from './helpers';
import { IUser } from '../server/models/types';
import User from '../server/models/user';

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;


const clearDB = () => Promise.all([Grouping.deleteMany({}),User.deleteMany({})]);
const getToken = (user: IUser) => jwt.sign({ user: user }, process.env.SECRET_TOKEN);

beforeEach(async () => {await clearDB()})
afterEach(async () => {await clearDB()});

afterAll(async () => db.close())

describe('Create Grouping',()=>{
    it('should be possible to create a new grouping', async () => {
        const group = {
            name:'Group1', 
            region: 'AT',
            type: 'media',
            group_type: 'group'
        }
        const users = await createAndSaveUsers(1,'user','group');
        const createGroupingResponse = await supertest(app)
          .post(`/api/groupings`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
          .send(group)
        expect(createGroupingResponse.status).toBe(200);
        const savedGroup = createGroupingResponse.body as IGroupingDocument;
        expect(savedGroup.name).toBe("Group1");
        expect(savedGroup.members).toHaveLength(0);
        expect(savedGroup.region).toBe("AT");
        expect(savedGroup._id).toBeDefined();
        expect(savedGroup.group_type).toBe('group')
        expect(savedGroup.isActive).toBeTruthy();
    })
    it('it should not be possible to create a grouping without role group', async () => {
        const group = new Grouping({
            name:'Group1', 
            region: 'AT',
            type: 'media',
            group_type: 'group'
        })
        const users = await createAndSaveUsers(1);
        const createGroupingResponse = await supertest(app)
          .post(`/api/groupings`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
          .send(group)
        expect(createGroupingResponse.status).toBe(403);
    })
})

describe('List All Groupings',()=> {
    it('should not be possible to retrieve all groupings if not on role group', async () => {
        const users = await createAndSaveUsers(1);
        const getAllGroupingsResponse = await supertest(app)
          .get(`/api/groupings/all`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(getAllGroupingsResponse.status).toBe(403);  
    })
    it('should be possible to retrieve all groupings if not on role group', async () => {
        await preloadGroupings();
        const users = await createAndSaveUsers(1,'user','group');
        const getAllGroupingsResponse = await supertest(app)
          .get(`/api/groupings/all`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(getAllGroupingsResponse.status).toBe(200);
        const groups = getAllGroupingsResponse.body;
        expect(groups).toHaveLength(14) 
        const oebb = groups[0];
        expect(oebb.name).toBe('ÖBB Group');
        expect(oebb.type).toBe('org');
        expect(oebb.group_type).toBe('group');
        expect(oebb.members).toBeUndefined();
        const countAllResponse = await supertest(app)
            .get('/api/groupings/count/all')
            .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(countAllResponse.status).toBe(200);
        expect(countAllResponse.body).toBe(14) 
    })
    it('should be possible to update a group', async ()=>{
        const group = {
            name:'Group1', 
            region: 'AT',
            type: 'media',
            group_type: 'group'
        }
        const users = await createAndSaveUsers(1, 'user', 'group');
        const createGroupingResponse = await supertest(app)
          .post(`/api/groupings`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
          .send(group)
        const savedGroup: IGroupingDocument = createGroupingResponse.body;
        savedGroup.members.push('Test');
        const updateGroupResponse = await supertest(app)
            .put(`/api/groupings/${savedGroup._id}`)
            .set('Authorization', `Bearer ${getToken(users[0])}`)
            .send(savedGroup)
        const updatedGroup = updateGroupResponse.body;
        expect(updatedGroup.members).toHaveLength(1); 
        expect(updatedGroup.members).toContain('Test');   
    })
})

describe('List Groups' ,() => {
    it('should be possible to get a list of all active groups', async () => {
        await preloadGroupings();
        const getAllGroupingsResponse = await supertest(app)
          .get(`/api/groupings`)
        expect(getAllGroupingsResponse.status).toBe(200);
        const groups = getAllGroupingsResponse.body;
        expect(groups).toHaveLength(11);
        groups.forEach(grp => {
            if (!grp.isActive) { fail('Must not be inactive')}
        }); 
        const countResponse = await supertest(app)
          .get('/api/groupings/count')
        expect(countResponse.status).toBe(200);
        expect(countResponse.body).toBe(11)  
    })
    it('should be possible to get a specific group', async () => {
        await preloadGroupings();
        const oebb = await Grouping.find({
            name: {$in: ['ÖBB Group']}, 
            isActive: true, type: 'org', group_type: 'group'
        })
        expect(oebb).toHaveLength(1)
    })
})

describe('Delete Groups', () => {
    it('should be possible to delete any group if in role group', async () => {
        await preloadGroupings();
        const users = await createAndSaveUsers(1,'user','group');
        const groupsBeforeDeletion = await Grouping.find({})
        expect(groupsBeforeDeletion).toHaveLength(14) 
        const deleteGroupingsResponse = await supertest(app)
          .delete(`/api/groupings/${groupsBeforeDeletion[0]._id}`)
          .set('Authorization', `Bearer ${getToken(users[0])}`)
        expect(deleteGroupingsResponse.status).toBe(200); 
        const groupsAfterDeletion = await Grouping.find({})
        expect(groupsAfterDeletion).toHaveLength(13)
    })
})