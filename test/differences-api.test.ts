import supertest from 'supertest';
import dotenv from 'dotenv';
dotenv.config({path: '.env.test'});
import mongoose from 'mongoose';
import {app} from '../server/app';
import { preloadDifferences } from './helpers';
import Difference, { IDifference } from '../server/models/differences';

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;

beforeAll(async () => {
    await Difference.deleteMany({})
    await preloadDifferences()
})
afterAll(async () => Difference.deleteMany({}).then(()=> db.close()))

describe('List Differences', ()=>{
    it('should be possible to retrieve a list of differences', async ()=> {
        //const r = await preloadDifferences()
        const getDifferenceResponse = await supertest(app)
            .get('/api/differences')
        expect(getDifferenceResponse.status).toBe(200);
        expect(getDifferenceResponse.body).toHaveLength(37);
        const firstEntry = getDifferenceResponse.body[0] as IDifference;
        expect(firstEntry.period).toBe(20123)
        expect(Math.abs(firstEntry.amountMta - 3.689785672E+07)).toBeLessThan(1e-6)
        expect(firstEntry.countMta).toBe(1975)
        expect(firstEntry.countRtr).toBe(1992)
        expect(Math.abs(firstEntry.amountRtr - 3.725658214E+07)).toBeLessThan(1e-6)
        expect(firstEntry.diffAmount).toBe(358725.42)
        expect(firstEntry.diffCount).toBe(17)

    })
})