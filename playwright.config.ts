import type { PlaywrightTestConfig } from "@playwright/test";
import { devices } from "@playwright/test";

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
require("dotenv").config({ path: ".env.test" });

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const config: PlaywrightTestConfig = {
  testDir: "./e2e",
  /* Maximum time one test can run for. */
  timeout: 30 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000,
  },
  /* Run tests in files in parallel */
  fullyParallel: true,
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Retry on CI only */
  retries: process.env.CI ? 2 : 0,
  /* Opt out of parallel tests on CI. */
  workers: process.env.CI ? 2 : undefined,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: process.env.CI ? [["html"], ["junit", { outputFile: "results.xml" }]] : "html",
  /* Path to the global setup file. This file will be required and run before all the tests. It must export a single function. */
  globalSetup: require.resolve("./global-setup"),
  /* Path to the global teardown file. This file will be required and run after all the tests. It must export a single function. */
  globalTeardown: require.resolve("./global-teardown"),
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
  /* The base directory, relative to the config file, for snapshot files created with toMatchSnapshot. Defaults to testConfig.testDir. */
  snapshotDir: "./e2e/snapshots",
  use: {
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 0,
    /* Base URL to use in actions like `await page.goto('/')`. */
    baseURL: process.env.BASE_URL,
    /* Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer */
    trace: "on-first-retry",
    /* Set browser language */
    locale: "de-AT",
  },
  /* Configure projects for major browsers */
  projects: [
    {
      name: "desktop-chrome",
      testIgnore: [/.*mobile.spec.ts/, /.*tablet.spec.ts/],
      use: devices["Desktop Chrome"],
    },
    {
      name: "desktop-firefox",
      testIgnore: [/.*mobile.spec.ts/, /.*tablet.spec.ts/],
      use: devices["Desktop Firefox"],
    },
    {
      name: "desktop-safari",
      testIgnore: [/.*mobile.spec.ts/, /.*tablet.spec.ts/],
      use: devices["Desktop Safari"],
    },
    {
      name: "mobile-chrome",
      testIgnore: [/.*tablet.spec.ts/],
      testMatch: /.*mobile.spec.ts/,
      use: devices["Galaxy S9+"],
    },
    {
      name: "tablet-safari",
      testIgnore: [/.*mobile.spec.ts/],
      testMatch: /.*tablet.spec.ts/,
      use: devices["iPad Pro 11"],
    },
  ]
};

export default config;
