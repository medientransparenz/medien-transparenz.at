import mongoose from "mongoose";

const db = mongoose.connection;

async function globalTeardown() {
  await db.close();
}

export default globalTeardown;
