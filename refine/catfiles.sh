#!/bin/bash
type=$1
# mf = mediaFunding or pf = pressFunding
lines=0
outfile="all-data.csv"
csvFiles=$(ls ../data/transfers/raw/*.csv)
if [ $type = "pf" ]
then 
    outfile="all-fundings.csv"
    csvFiles=$(ls ../data/fundings/raw/*.csv)
    echo '"receiver";"fundedObject";"name";"payer";"amount";"year";"fundingBasis";"street";"zipCode";"city_de";"status";"amountPart1";"amountPart2";"broadcastName";"category";"deadline";"broadcastLength";"productionCosts"' > $outfile
else
    echo '"RECHTSTRÄGER";"QUARTAL";"BEKANNTGABE";"LEERMELDUNG";"MEDIUM_MEDIENINHABER";"EURO"' > $outfile
fi

for f in $csvFiles; do
  # echo $f 
  #lines=$(($lines + $(cat $f | wc -l)))
  tail -n +2 "$f" >> $outfile
done
#echo "original number lines (incl. heading): $lines"
#echo "all-data line numbers: $(cat $outfile | wc -l)"