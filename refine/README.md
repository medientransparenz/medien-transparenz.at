# Integrating new Quarterly Reports

These are instruction on how integrate new reports that are published quarterly be RTR.

## Download new Data Set

Data can be automatically downloaded from RTR open data interface using the script [download-report.mjs](../download-report.mjs).

Before starting the script, the value of variable `quarter` needs to be set to the value of interest.

```javascript
const quarter = 20214;
```

After running command `node download-report.mjs` a new data file can be found in folder [`data/transfers/raw/`](../data/transfers/raw/).

## Download new Funding Set

Data can be automatically downloaded from RTR open data interface using the script [download-fundings.mjs](../download-fundings.mjs).

The year can be passed as an argument or set in the .mjs file directly.

After running command `node download-fundings.mjs <year>` a new data file can be found in folder [`data/fundings/raw/`](../data/fundings/raw/).

## Running Data Cleansing Routines

This requires a local instance of [OpenRefine](https://openrefine.org/) to be running at http://127.0.0.1:3333/.

When using the following command all data located in the [raw data folder](../data/transfers/raw/) will be uploaded to a new OpenRefine project:

```
node refine_data.mjs create [--type pf]
```

This will also apply all operations found in [./operations](./operations/).
Nevertheless it is recommended to inspect the resulting data for further inconsistencies (especially new media entries that might occur in different spellings)

## Getting Refined Data

Now cleansed data needs to be downloaded from OpenRefine. This will eventually result in on file per year.

**Note**: If the new report is for the first quarter, you need to set the value of variable `maxYear` in file [refine_data.mjs](./refine_data.mjs) accordingly before running this command.

```
node refine_data.mjs get --years [--type pf]
```

This will update all file in folder [/data/transfers/refined/](/data/transfers/refined/).

## Saving new OpenRefine Operations

If you had to perform changes to the already cleansed data set (most likely due to new media entries that came with different spellings), don't forget to save these operations for later usage. Therefore run:

```
node refine_data.mjs get --operations [--type pf]
```

## Updating Web-Site Database

In this step data used by the actual web-site will be updated. The script therefore performs the following two steps:

- all existing transfer data gets deleted (by directly accessing the database)
- new/changed data sets get uploaded using the REST-API

The command is:

```
node refine_data.mjs update -u username -p password [--www host] [-db mongo_uri] [--type pf]
```
