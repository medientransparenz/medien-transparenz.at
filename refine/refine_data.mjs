import { OpenRefine } from 'openrefine'
import minimist from 'minimist'
import superagent from 'superagent'
import { writeFile, readFile, unlink, readdir } from 'fs/promises';
import { exec } from 'child_process'
import { promisify } from 'util'
import moment from 'moment'
import { MongoClient } from 'mongodb'


let projectId

const endpoint = 'http://localhost:3333'

let dataType = "pf";
// mf = mediaFunding or pf = pressFunding

const server = OpenRefine(endpoint)

const execP = promisify(exec)

const getProjectID = (projectName) => server.projects_metadata()
    .then(data => {
        for (const [key, value] of Object.entries(data.projects)) {
            if (value.name == projectName) return key
        }
        throw Error(`Project ${projectName} does not exist`)
    })

const getOperations = (id) => superagent.get(`${endpoint}/command/core/get-operations?project=${id}`)
    .then(response => response.body)

const writeResult = (filename, stringify = true) => (content) => writeFile(filename, stringify ? JSON.stringify(content, null, 2) : content)

const args = {
    project: `all-data-${moment().format('YYYY-MM-DD')}`,
    maxYear: "2022",
    refinedDir: "../data/transfers/refined",
    www: "http://localhost:3000",
    db: "mongodb://localhost/developmentdb",
    type: "mf",
    ...minimist(process.argv.slice(2))
}

args['project'] = args['project'].replace(/['"]+/g, '')

const maxYear = parseInt(args['maxYear'])

const range = (size) =>
    Array.from(new Array(size + 1).keys());

const minYear = 2012

const years = range(maxYear - minYear).map(n => n + minYear)

const getFormatOptionsColumns = (names) => {
    const list = [];
    for (const name of names) {
        list.push({
            "name": name, "reconSettings":
                { "output": "entity-name", "blankUnmatchedCells": false, "linkToEntityPages": true },
            "dateSettings": { "format": "iso-8601", "useLocalTimeZone": false, "omitTime": false }
        })
    }
    return list;
}

const formatOptions = {
    "separator": ";", "lineSeparator": "\n",
    "quoteAll": true, "outputColumnHeaders": true,
    "outputBlankRows": false,
    "columns":
        getFormatOptionsColumns(["RECHTSTRÄGER", "QUARTAL", "BEKANNTGABE", "LEERMELDUNG", "MEDIUM_MEDIENINHABER", "EURO"]),
    "limit": -1
}

const getRecords = (year, id) => superagent.post(`${endpoint}/command/core/export-rows`)
    .send(`engine={"facets":[{"type":"text","name":"${dataType == "pf" ? "year" : "QUARTAL"}","columnName":"${dataType == "pf" ? "year" : "QUARTAL"}","mode":"text","caseSensitive":false,"invert":false,"query":"${year}"}],"mode":"row-based"}`)
    .send('project=' + id)
    .send('format=tsv')
    .send('encoding=UTF-8')
    //.send('quoteAll=')
    .send(`options=${JSON.stringify(formatOptions)}`)
    .then(resp => resp.text).then(writeResult(`${args['refinedDir']}/refined-${year}.csv`, false))

const getAllRecords = (years) => (id) =>
    Promise.all(years.map(y => getRecords(y, id)))

const getToken = () => superagent.get(`${endpoint}/command/core/get-csrf-token`)
    .then(resp => resp.body)

const createProject = (projectName) => ({ token }) =>
    superagent.post(`${endpoint}/command/core/create-project-from-upload?csrf_token=${token}`)
        .field('project-name', projectName)
        .field('format', 'text/line-based/*sv')
        .field('options', JSON.stringify({ "encoding": "UTF-8", "separator": ";", "processQuotes": dataType != "pf" }))
        .attach('project-file', dataType == "pf" ? "./all-fundings.csv" : "./all-data.csv")
        .then(resp => resp.redirects[0].replace(endpoint + '/project?project=', ''))

const applyOperations = (fileName) => (projectId) =>
    Promise.all([getToken(), readFile(fileName)]).then(([{ token }, opsJson]) => {
        const ops = JSON.parse(opsJson).entries.filter(e => !!e.operation).map(e => e.operation)
        let applications = Promise.resolve()
        while (ops.length > 0) {
            console.log(`Number of remaining operations: ${ops.length}`)
            const batch = ops.splice(0, 100)
            applications = applications.then(
                () => superagent.post(`${endpoint}/command/core/apply-operations?csrf_token=${token}&project=${projectId}`)
                    .send(`operations=${encodeURIComponent(JSON.stringify(batch))}`)
            )
        }
        return applications
    })


const helpText = `Usage:
node ./refine_data.mjs create [-project project_name] [-ops operations_file] [--type data_type]
Combines all CSV files from folder ../data/transfers/raw/ and creates
an openrefine project with this data. It also applies the operations
from the given file

node ./refine_data.mjs get --years [-project project_name] [-maxYear year]
Download one CSV file per year starting with 2012 up to maxYear

node ./refine_data.mjs get --operations [-project project_name]
Download all operations that haven been applied to the given openrefine project

node ./refine_data.mjs update -u username -p password [--www host] [-db mongo_uri] [-refinedDir dir]
First deletes all entries from the given db and than uploads all files
in the refinedDir using the medien-transparenz.at API

Parameter:
- type: default - mf, mediaFundings,
    possibleValue: pf, pressFunding
`

const getJWT = (host, username, password) =>
    superagent.post(`${host}/api/login`).send({
        email: username, password
    }).then(r => r.body)

const uploadData = (host, token, fileName) =>
    Promise.resolve(console.log(`Uploading ${fileName}`))
        .then(() =>
            superagent.post(`${host}/api/${dataType == "pf" ? "fundings" : "transfers"}/import`)
                .set('Authorization', `Bearer ${token}`)
                .attach('file', `${args['refinedDir']}/${fileName}`)
        )
        .then(resp => {
            console.log(`Response for ${fileName}`)
            console.log(resp.body)
        })


console.log('Using the following arguments:')
args['project']+= `-${args['type']}`
console.log(args)
if (args['h']) {
    console.log(helpText)
    process.exit(0)
}
if (args['type'] && (args['type'] == "pf" || args['type'] == "mf")) {
    dataType = args['type']
    args.refinedDir = "../data/" + (dataType == "pf" ? "fundings" : "transfers") + "/refined";
    formatOptions.columns = getFormatOptionsColumns(dataType == "pf" ? [
        "receiver", "fundedObject", "name", "payer", "amount",
        "year", "fundingBasis", "street", "zipCode", "city_de", "status", "amountPart1", "amountPart2",
        "broadcastName", "category", "deadline", "broadcastLength",
        "productionCosts"
    ] :
        ["RECHTSTRÄGER", "QUARTAL", "BEKANNTGABE", "LEERMELDUNG", "MEDIUM_MEDIENINHABER", "EURO"]);
}
if (args['_'][0] == 'get') {
    if (args['operations']) {
        console.log("downloading operations from openrefine")
        getProjectID(args['project'])
            .then(getOperations)
            .then(writeResult(args['out'] || dataType == "pf" ? "./operations/funding_operations.json" : "./operations/operations.json"))
            .then(() => console.log("Operations saved!"))
    } else if (args['years']) {
        console.log("downloading refined data")
        getProjectID(args['project'])
            .then(getAllRecords(years))
            .then(`Refined data saved ad ${args['refinedDir']}.`)
    } else { console.log(helpText) }
} else if (args['_'][0] == 'create') {
    console.log("Creating new Openrefine Project: " + args['project'])
    execP(`./catfiles.sh ${dataType}`)
        .then(getToken)
        .then(createProject(args['project']))
        .then(r => { projectId = r; return projectId })
        .then(applyOperations(args['ops'] || dataType == "pf" ? "./operations/funding_operations.json" : "./operations/operations.json"))
        .then(r => console.log("Done:", r.statusCode))
        .then(() => unlink(dataType == "pf" ? './all-fundings.csv' : './all-data.csv'))
        .catch(err => console.error(err))
} else if (args['_'][0] == 'update') {
    let token = ''
    let client
    console.log('updating medien-transparenz.at database')
    console.log(`Web server: ${args['www']}`)
    console.log('Web-User: ' + args['u'])
    console.log(`MongoURL: ${args['db']}`)
    MongoClient.connect(args['db'])
        .then(c => { client = c; return c })
        .then(client => client.db().collection(dataType == "pf" ? 'fundings' : 'transfers').deleteMany({}))
        .then(r => console.log("deleted entries:", r.deletedCount))
        .then(() => getJWT(args['www'], args['u'], args['p']))
        .then(r => token = r.token)
        .then(() => readdir(args['refinedDir']))
        .then(files => files.reduce((acc, file) => acc.then(() => uploadData(args['www'], token, file)), Promise.resolve()))
        .then(() => client.close())
        .then(() => console.log("DONE", token))
        .then(() => process.exit(1))

} else {
    console.log(helpText)
}

