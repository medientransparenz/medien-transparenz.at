import * as express from "express";

import UserCtrl from "./controllers/user";
import { PassportStatic } from "passport";
import { Application } from "express";
import * as jwt from "jsonwebtoken";
import multer from "multer";
import ZipCodeCtrl from "./controllers/zipcodes";
import OrganisationCtrl from "./controllers/organisation";
import TransferCtrl from "./controllers/transfer";
import { catSystem, catController } from "./logging";
import GroupingCtrl from "./controllers/groups";
import DifferenceCtrl from "./controllers/difference";
import DuplicateCtrl from "./controllers/duplicate";
import FundingCtrl from "./controllers/funding";
import BlogCtrl from "./controllers/blog";

export default function setRoutes(app: Application, passport: PassportStatic) {
  const router = express.Router();

  const userCtrl = new UserCtrl();
  const zipCodeCtrl = new ZipCodeCtrl();
  const organisationCtrl = new OrganisationCtrl();
  const transferCtrl = new TransferCtrl();
  const groupingCtrl = new GroupingCtrl();
  const upload = multer({ dest: "uploads/" });
  const diffCtrl = new DifferenceCtrl();
  const duplicatesCtrl = new DuplicateCtrl();
  const fundingCtrl = new FundingCtrl();
  const blogCtrl = new BlogCtrl();

  const jwtAuth = passport.authenticate("jwt", { session: false });

  const isOwner =
    (
      extractor: (r: Request) => string // eslint-disable-line no-unused-vars
    ) =>
    (req) =>
      JSON.stringify(req.user._id) === JSON.stringify(extractor(req));
  const isAdmin = (req) => req.user.roles.includes("admin");
  const hasRole = (role: string) => (req) => {
    catSystem.info(req.user.roles);
    catSystem.info(
      `User ${req.user.username} has role ${role}? ${
        req.user.roles.includes(role) ? "yes" : "no"
      }`
    );
    return req.user.roles.includes(role);
  };

  const isAdminOrOwner =
    (
      extractor: (r: Request) => string // eslint-disable-line no-unused-vars
    ) =>
    (req) =>
      isAdmin(req) || isOwner(extractor)(req);
  const checkPermission = (condition) => (req, res, next) =>
    condition(req) ? next() : res.status(403).send();

  const userId = (r) => r.users._id;

  const protectRole = (req, res, next) => {
    if (!isAdmin(req)) {
      if (req.body._doc) {
        delete req.body._doc.roles;
      } else {
        delete req.body.roles;
      }
      //console.log(JSON.stringify(req.body))
    }
    next();
  };

  app.use(passport.initialize());

  // Events

  //zipcodes
  router
    .route("/zipcodes/import")
    .post(
      jwtAuth,
      checkPermission(isAdmin),
      upload.single("file"),
      zipCodeCtrl.uploadZipCode
    );
  router
    .route("/zipcodes")
    .get(jwtAuth, checkPermission(isAdmin), zipCodeCtrl.getList);
  router
    .route("/zipcodes/count")
    .get(jwtAuth, checkPermission(isAdmin), zipCodeCtrl.count);

  //organisations
  router
    .route("/organisations/import")
    .post(
      jwtAuth,
      checkPermission(isAdmin),
      upload.single("file"),
      organisationCtrl.uploadOrganisation
    );
  router.route("/organisations").get(organisationCtrl.getList);
  router
    .route("/organisations")
    .post(
      jwtAuth,
      checkPermission(isAdmin),
      organisationCtrl.insert,
      organisationCtrl.show
    );
  router.route("/organisations/count").get(organisationCtrl.count);
  router.route("/organisations/:orgId").get(organisationCtrl.show);
  router
    .route("/organisations/:orgId")
    .put(
      jwtAuth,
      checkPermission(isAdmin),
      organisationCtrl.update,
      organisationCtrl.show
    );
  router
    .route("/organisations/:orgId")
    .delete(
      jwtAuth,
      checkPermission(isAdmin),
      organisationCtrl.delete,
      organisationCtrl.show
    );

  router.param("orgId", organisationCtrl.load);

  const log = (msg: string) => (req, resp, next) => {
    catController.debug(msg);
    next();
  };

  //tranfers
  router
    .route("/transfers/import")
    .post(
      jwtAuth,
      checkPermission(isAdmin),
      log("Allowed to access Upload"),
      upload.single("file"),
      log("File Upload loaded, ready to process!"),
      transferCtrl.uploadTransfers
    );
  router.route("/transfers/overview").get(transferCtrl.overview);
  router.route("/transfers/top").get(transferCtrl.topEntries);
  router.route("/transfers/periods").get(transferCtrl.periods);
  router.route("/transfers/flows").get(transferCtrl.filteredflows);
  router.route("/transfers/count").get(transferCtrl.count);
  router.route("/transfers/search").get(transferCtrl.search);
  router.route("/transfers/searchNames").get(transferCtrl.searchName);
  router.route("/transfers/timeline").get(transferCtrl.timeline);
  router.route("/transfers/federalStates").get(transferCtrl.federalstates);
  router
    .route("/transfers")
    .get(jwtAuth, checkPermission(isAdmin), transferCtrl.getList);
  router.route("/transfers/:transferId").get(transferCtrl.show);
  router
    .route("/transfers/:transferId")
    .put(
      jwtAuth,
      checkPermission(isAdmin),
      transferCtrl.update,
      transferCtrl.show
    );
  router.param("transferId", transferCtrl.load);
  catSystem.info("Transfer Routes Added!");

  // Users
  router.route("/login").post(userCtrl.login);
  router
    .route("/users")
    .get(jwtAuth, checkPermission(isAdmin), userCtrl.getList);
  router
    .route("/users/count")
    .get(jwtAuth, checkPermission(isAdmin), userCtrl.count);
  router
    .route("/users")
    .post(userCtrl.setRoleAndProvider, userCtrl.insert, userCtrl.show);
  router
    .route("/users/:userId")
    .get(jwtAuth, checkPermission(isAdminOrOwner(userId)), userCtrl.show);
  router
    .route("/users/:userId")
    .put(
      jwtAuth,
      checkPermission(isAdminOrOwner(userId)),
      protectRole,
      userCtrl.update,
      userCtrl.show
    );
  router
    .route("/users/:userId")
    .delete(jwtAuth, checkPermission(isAdmin), userCtrl.delete);

  router.param("userId", userCtrl.load);

  const dump = (req, resp, next) => {
    catSystem.info("Hit this route");
    catSystem.info(JSON.stringify(req.body));
    next();
  };

  // Groupings
  router
    .route("/groupings")
    .post(
      jwtAuth,
      checkPermission(hasRole("group")),
      groupingCtrl.insert,
      groupingCtrl.show
    );
  router
    .route("/groupings/all")
    .get(jwtAuth, checkPermission(hasRole("group")), groupingCtrl.getList);
  router
    .route("/groupings/count/all")
    .get(jwtAuth, checkPermission(hasRole("group")), groupingCtrl.count);
  router.route("/groupings/count").get(groupingCtrl.countActive);
  router.route("/groupings/list").get(groupingCtrl.getGroups);
  router.route("/groupings").get(groupingCtrl.getAll);
  router.route("/groupings/:groupId").get(groupingCtrl.show);
  router
    .route("/groupings/:groupId")
    .delete(jwtAuth, checkPermission(hasRole("group")), groupingCtrl.delete);
  router
    .route("/groupings/:groupId")
    .put(
      dump,
      jwtAuth,
      checkPermission(hasRole("group")),
      groupingCtrl.update,
      groupingCtrl.show
    );
  router.param("groupId", groupingCtrl.load);

  // Difference
  router.route("/differences").get(diffCtrl.getList);
  // Duplicates
  router.route("/duplicates").get(duplicatesCtrl.getList);

  //Funding
  router
    .route("/fundings")
    .post(
      jwtAuth,
      checkPermission(isAdmin),
      fundingCtrl.insert,
      fundingCtrl.show
    );
  router.route("/fundings").get(fundingCtrl.getList);
  router.route("/fundings/overview").get(fundingCtrl.overview);
  router.route("/fundings/top").get(fundingCtrl.topEntries);
  router.route("/fundings/periods").get(fundingCtrl.getOldestAndLatestPeriod);
  router.route("/fundings/count").get(fundingCtrl.count);
  router.route("/fundings/search").get(fundingCtrl.search);
  router.route("/fundings/searchNames").get(fundingCtrl.searchName);
  router.route("/fundings/timeline").get(fundingCtrl.timeline);
  router.route("/fundings/top").get(fundingCtrl.topEntries);
  router.route("/fundings/flows").get(fundingCtrl.filteredFlows);
  router.route("/fundings/:fundingId").get(fundingCtrl.show);
  router
    .route("/fundings/:fundingId")
    .delete(jwtAuth, checkPermission(isAdmin), fundingCtrl.delete);
  router
    .route("/fundings/")
    .delete(jwtAuth, checkPermission(isAdmin), fundingCtrl.deleteManyFromType);
  router
    .route("/fundings/:fundingId")
    .put(
      jwtAuth,
      checkPermission(isAdmin),
      fundingCtrl.update,
      transferCtrl.show
    );
  router
    .route("/fundings/import")
    .post(
      jwtAuth,
      checkPermission(isAdmin),
      upload.single("file"),
      fundingCtrl.uploadFunding
    );

  router.param("fundingId", fundingCtrl.load);

  // GitHub Login
  router.route("/auth/github").get(passport.authenticate("github"));

  router.route("/auth/github/callback").get(
    passport.authenticate("github", {
      failureRedirect: "/login",
      session: false,
    }),
    (req, res) => {
      // Successful authentication, return JWT token.
      const token = jwt.sign({ user: req.user }, process.env.SECRET_TOKEN); // , { expiresIn: 10 } seconds
      res.status(200).json({ token: token });
    }
  );

  // Blogs
  // Get Blog
  router
    .route("/blogs/all")
    .get(jwtAuth, checkPermission(isAdmin), blogCtrl.getList);

  router.route("/blogs").get(blogCtrl.getAllActive);
  // Update blogs
  router
    .route("/blogs/:blogId")
    .put(
      jwtAuth,
      checkPermission(isAdmin),
      protectRole,
      blogCtrl.update,
      blogCtrl.show
    );
  // Create blogs
  router
    .route("/blogs")
    .post(jwtAuth, checkPermission(isAdmin), blogCtrl.insert, blogCtrl.show);

  // Delete Blogs
  router
    .route("/blogs/:blogId")
    .delete(jwtAuth, checkPermission(isAdmin), blogCtrl.delete, blogCtrl.show);

  router.param("blogId", blogCtrl.load);
  // Apply the routes to our application with the prefix /api
  app.use("/api", router);
}
