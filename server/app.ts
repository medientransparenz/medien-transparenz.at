import express from 'express';
import morgan from 'morgan';
import { createWriteStream, mkdir } from 'fs';
import * as path from 'path';

import setRoutes from './routes';
// import * as dotenv from 'dotenv';

import {Strategy, ExtractJwt} from 'passport-jwt';
import passport from 'passport';

import User from './models/user';
import {catSystem} from './logging';

// dotenv.load('.env');
mkdir(path.join(__dirname, '../logs'),{},err=>{catSystem.error("Could not create logs folder",err)})
const logFile = path.join(__dirname, '../logs/access.log')
const accessLogStream = createWriteStream(logFile, { flags: 'a+' })

const jwtOpts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
  secretOrKey: process.env.SECRET_TOKEN
};

const app = express();

app.set('trust proxy', true)

// Register JWT Strategy
passport.use(new Strategy(jwtOpts, (jwtPayload, done) => {
  User.findOne({_id: jwtPayload.user._id})
    .then((user) => done(null, user))
    .catch(done);
}));


app.set('port', (process.env.PORT || 3000));

app.use('/', express.static(path.join(__dirname, '../public')));
app.use(express.json());
app.use(express.urlencoded({extended: false}));



app.use(morgan('combined',{stream: accessLogStream}));


setRoutes(app, passport);
catSystem.info('Registered Routes');
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});


export {app };
