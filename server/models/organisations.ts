import mongoose from 'mongoose';

export interface IOrganisation {
    name: string;
    street: string;
    zipCode: string;
    city_de: string;
    federalState: string;
    country_de: string;
    type: string;    
}

export interface IOrganisationDocument extends IOrganisation, mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
}

export interface IStoredOrganisation extends IOrganisation {
  _id: string;
}

export interface IOrganisationModel extends IOrganisation, mongoose.Model<IOrganisationDocument> {}

const orgSchema = new mongoose.Schema<IOrganisationDocument,IOrganisationModel>({
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      index: 1
      
    },
    street: {
      type: String,
      required: true,
      trim: true
    },
    zipCode: {
      type: String,
      trim: true,
      required: true
    },
    city_de: {
      type: String,
      trim: true,
      required: true
    },
    federalState: {
      type: String,
      trim: true,
      required: true
    },
    country_de: {
      type: String,
      trim: true,
      required: true
    },
    type: {
      type: String,
      trim: true
    }
  });

orgSchema.index({
    name: 1,
    federalState: 1
  }, {
    unique: true
  });
  
orgSchema.indexes();  

const Organisation: IOrganisationModel = mongoose.model<IOrganisationDocument,IOrganisationModel>('Organisation',orgSchema);

export default Organisation;