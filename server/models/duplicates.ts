'use strict';
import mongoose ,{ Document } from 'mongoose';


const Schema = mongoose.Schema;
mongoose.SchemaTypes.String.set('trim', true);


export interface IDuplicate {
    organisation: string;
    transferType: number;
    media: string;
    amount: number;
    year: number;
    quarter: number;
    period: number;
    occurrences: number;
    //organisationReference: mongoose.Schema.Types.ObjectId;
}

export interface IStoredDifference extends IDuplicate {
  _id: string;
}

export type IDuplicateDocument = IDuplicate & Document<IDuplicate>
/*
export interface ITransferDocument extends ITransfer, mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
}
*/

export interface IDuplicateModel extends IDuplicate, mongoose.Model<IDuplicate> {}


/**
* Difference Schema
 */

const DuplicateSchema = new Schema({
  organisation: {
    type: String,
    trim: true,
    required: true,
    index: "hashed"
  },
  transferType: {
    type: Number,
    enum: [2,4,31],
    index: "hashed"
  },
  media: {
    type: String,
    trim: true,
    required: true,
    index: "hashed"
  },
  amount: {
    type: Number,
    index: true
  },
  year: {
    type: Number,
    index: true
  },
  quarter: {
    type: Number,
    index: true
  },
  period: {
    type: Number,
    index: true
  },
  occurrences: {
    type: Number,
    index: true
  },

});

/*
DuplicateSchema.index({
  organisation: 1,
  transferType: 1,
  quarter: 1,
  year: 1,
  media: 1,
  amount: 1,
  occurrense: 1
}, {
  unique: true
});
*/

DuplicateSchema.indexes();

const Duplicate = mongoose.model<IDuplicate>('Duplicate', DuplicateSchema);

export default Duplicate;
