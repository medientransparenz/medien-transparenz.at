import  { Schema, model, Model, Document } from 'mongoose';
import { Region, regions } from './types';

export interface IGrouping {
    name: string,
    type: 'media' | 'org',
    group_type: 'group'|'class',
    region: Region,
    owner?: string,
    members: string[],
    isActive: true,
    serverSide: boolean
}

// export type IGroupingDocument = IGrouping & Document<IGrouping>

export interface IGroupingDocument extends IGrouping, Document {
    _id: Schema.Types.ObjectId;
}

export interface IStoredGrouping extends IGrouping {
  _id: string;
}

export interface IGroupingModel extends IGrouping, Model<IGrouping> {}

/**
* Grouping Schema
 */

const GroupingSchema = new Schema<IGroupingDocument,IGroupingModel>({
  name: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    index: "hashed"
  },
  type: {
    type: String,
    required: true,
    enum: ['media','org']
  },
  group_type: {
    type: String,
    required: true,
    enum: ['group','class']
  },
  region: {
    type: String,
    required: true,
    trim: true,
    enum: regions.map(r=>r)
  },
  owner: {
    type: String,
    trim: true
  },
  members: {
    type: [String],
    required: true
  },
  isActive: {
    type: Boolean,
    required: true,
    default: true
  },
  serverSide: {
    type: Boolean,
    required: true,
    default: true
  }
});

const GroupingModel = model<IGroupingDocument,IGroupingModel>('Grouping', GroupingSchema);

export default GroupingModel;