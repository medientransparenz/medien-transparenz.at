import mongoose from 'mongoose';

/* eslint-disable no-debugger, no-unused-vars */

export const federalStates = ['AT-1', 'AT-2', 'AT-3', 'AT-4',
'AT-5', 'AT-6', 'AT-7', 'AT-8', 'AT-9'] as const

export const federalStatesNamesShort = ['W','N','O','B','Sa','St','V','K','T'] as const
export type FederalStateNameShort = typeof federalStatesNamesShort[number]

export const regions = ['EU', 'AT', ...federalStates ] as const

export type Region = typeof regions[number]
export type FederalStateCode = typeof federalStates[number]

const federalStateNames = ['Burgenland', 'Carinthia', 'Lower Austria', 'Upper Austria',
    'Salzburg', 'Styria', 'Tyrol', 'Vorarlberg', 'Vienna'
] as const

export type FederalStateName = typeof federalStateNames[number];

type TStateNameShortDecoder = {[key in FederalStateNameShort]: {
  code: FederalStateCode,
  name: FederalStateName
  }
}

export const FederalStatesShortDecoder:TStateNameShortDecoder  = {
  'B': { code: 'AT-1', name:'Burgenland'},
  'K': { code: 'AT-2', name:'Carinthia'},
  'N': { code: 'AT-3', name:'Lower Austria'},
  'O': { code: 'AT-4', name:'Upper Austria'},
  'Sa': { code: 'AT-5', name:'Salzburg'},
  'St': { code: 'AT-6', name:'Styria'},
  'T': { code: 'AT-7', name:'Tyrol'},
  'V': { code: 'AT-8', name:'Vorarlberg'},
  'W': { code: 'AT-9', name:'Vienna'},
}

export type TUploadResult = {
    entries: number,
    savedEntries: number,
    errors?: number,
    errorEntries: any[],
    error?: Error,
    transfersWithoutAddress?: string[]
}


/*
 The plain Cat entity type completely agnostic of mongoose
*/
export interface ICat {
  name: string,
  weight: number,
  age: number
}


/*
 This interface represents the document (model instance) returned by
 mongoose. It consists of the document properties and all instance methods
 like save(), remove() and so on
 */
export interface ICatDocument extends ICat, mongoose.Document {
  _id: mongoose.Schema.Types.ObjectId;
}

export interface GitHubUser {
  username: string,
  provider: string,
  emails: {
    value: string,
    primary: boolean,
    verified: boolean
  }[]
}


export interface IUser {
  //_id?: mongoose.Schema.Types.ObjectId,
  username: string,
  email: string,
  password: string,
  roles: [string],
  provider: string,
  active: boolean
}

export interface IStoredUser extends IUser {
  _id: string
}

/*
  Since the User's schema defines a custom instance method
  (with "schema.method.functionName"), we need to declare the type
  of this function here as well, so it can be accessed in the
  rest of the application.
 */
export interface IUserDocument extends IUser, mongoose.Document {
  _id: mongoose.Schema.Types.ObjectId;
  comparePassword: (candidatePassword, callback) => void;
}

/*
 This interface represents the User's model (Model class).
 We need it, since the User's schema defines a custom static
 method, that needs to be declared here as well
 The model interface needs to be registered when creating the
 model from the schema (see ./user.ts for details)
 */
export interface IUserModel extends IUser, mongoose.Model<IUserDocument> {
  findOrCreate: (user: GitHubUser) => Promise<IUserDocument>;
}
