'use strict';
import mongoose ,{ Document } from 'mongoose';
import { FederalStateCode } from './types';

const Schema = mongoose.Schema;
mongoose.SchemaTypes.String.set('trim', true);


export interface ITransfer {
    organisation: string;
    transferType: number;
    media: string;
    amount: number;
    year: number;
    quarter: number;
    period: number;
    federalState: FederalStateCode | 'unknown' | 'N.D.';
    organisationType: string;
    //organisationReference: mongoose.Schema.Types.ObjectId;
}

export interface IStoredTransfer extends ITransfer {
  _id: string;
}

export type ITransferDocument = ITransfer & Document<ITransfer>
/*
export interface ITransferDocument extends ITransfer, mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
}
*/

export interface ITransferModel extends ITransfer, mongoose.Model<ITransfer> {}


/**
* Transfer Schema
 */

const TransferSchema = new Schema({
  organisation: {
    type: String,
    trim: true,
    required: true,
    index: "hashed"
  },
  transferType: {
    type: Number,
    enum: [2,4,31],
    index: "hashed"
  },
  media: {
    type: String,
    trim: true,
    required: true,
    index: "hashed"
  },
  amount: {
    type: Number,
    index: true
  },
  year: {
    type: Number,
    index: true
  },
  quarter: {
    type: Number,
    index: true
  },
  period: {
    type: Number,
    index: true
  },
  organisationReference: {
    type: Schema.Types.ObjectId,
    ref: 'Organisation'
  },
  federalState: {
    type: String,
    trim: true,
    index: "hashed"
  },
  organisationType: {
    type: String,
    trim: true,
    index: "hashed"
  }
});


TransferSchema.index({
  organisation: 1,
  transferType: 1,
  quarter: 1,
  year: 1,
  media: 1,
  amount: 1,
  federalState: 1
}, {
  unique: true
});

TransferSchema.indexes();

const Transfer = mongoose.model<ITransfer>('Transfer', TransferSchema);

export default Transfer;
