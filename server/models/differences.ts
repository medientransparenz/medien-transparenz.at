'use strict';
import mongoose, { Document } from 'mongoose';


const Schema = mongoose.Schema;
mongoose.SchemaTypes.String.set('trim', true);


export interface IDifference {
	period: number;
	amountRtr: number;
	countRtr: number;
	amountMta: number;
	countMta: number;
	diffAmount: number;
	diffCount: number;
    //organisationReference: mongoose.Schema.Types.ObjectId;
}

export interface IStoredDifference extends IDifference {
    _id: string;
}

export type IDifferenceDocument = IDifference & Document<IDifference>
/*
export interface ITransferDocument extends ITransfer, mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
}
*/

export interface IDifferenceModel extends IDifference, mongoose.Model<IDifference> { }


/**
* Difference Schema
 */

const DifferenceSchema = new Schema({
    period: {
        type: Number,
        index: true
    },
    amountRtr: {
        type: Number
    },
    countRtr: {
        type: Number
    },
    amountMta: {
        type: Number
    },
    countMta: {
        type: Number
    },
    diffAmount: {
        type: Number
    },
    diffCount: {
        type: Number
    },
});



DifferenceSchema.indexes();

const Difference = mongoose.model<IDifference>('Difference', DifferenceSchema);

export default Difference;
