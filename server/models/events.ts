import mongoose from 'mongoose';
import { Region, regions } from './types';



const eventTypes = ['period', 'moment']


export interface IEvent {
    name: string;
    type: typeof eventTypes[number]
    startDate: Date;
    endDate?: Date;
    region: Region;
    tags?: Array<string>;
    predictable: boolean;  
}

export interface IEventDocument extends IEvent, mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
  }

export interface IEventModel extends IEvent, mongoose.Model<IEventDocument> {}

/**
* Event Schema
 */

const eventSchema = new mongoose.Schema<IEventDocument,IEventModel>({
  name: {
    type: String,
    required: true,
    trim: true
  },
  startDate: {
    type: Date,
    required: true
  },
  type: {
      type: String,

      required: true,
      default: 'moment'
  },
  endDate: {
    type: Date
  },
  region: {
    type: String,
    enum: regions.map(a=>a),
    required: true,
    "default": 'AT'
  },
  tags: {
    type: [String],
    "default": []
  },
  predictable: {
    type: Boolean,
    "default": true
  }
});

const Event:IEventModel = mongoose.model<IEventDocument,IEventModel>('Event',eventSchema);

export default Event;
