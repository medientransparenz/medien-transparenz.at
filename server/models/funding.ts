import mongoose from "mongoose";

export interface IFunding{
    receiver: string;
    fundedObject: string;
    name: string;
    payer: string;
    amount: number;
    year: number;
    fundingBasis: string;
    street: string;
    zipCode: string;
    city_de: string;
    federalState: string;
    status: string;
    amountPart1: number;
    amountPart2: number;
    broadcastName: string;
    category: string;
    deadline: string;
    broadcastLength: string;
    productionCosts: number;
}

export interface IFundingDocument extends IFunding, mongoose.Document{
    _id: mongoose.Schema.Types.ObjectId;
}

export interface IStoredFunding extends IFunding{
    _id: string;
}

export interface IFundingModel extends IFunding, mongoose.Model<IFundingDocument>{}

const fundingSchema = new mongoose.Schema<IFundingDocument, IFundingModel>({
    receiver: {
        type: String,
        required: true,
        trim: true,
        index: true
    },
    fundedObject: {
        type: String,
        required: true,
        trim: true,
        index: true
    },
    name: {
        type: String,
        required: true,
        trim: true,
        index: true
    },
    payer: {
        type: String,
        required: true,
        trim: true
    },
    amount: {
        type: Number,
        required: true
    },
    year: {
        type: Number,
        required: true,
        index: true
    },
    fundingBasis: {
        type: String,
        required: false,
        trim: true
    },
    street: {
        type: String,
        required: false,
        trim: true
    },
    zipCode: {
        type: String,
        required: false,
        trim: true
    },
    city_de: {
        type: String,
        required: false,
        trim: true
    },
    federalState: {
        type: String,
        required: false,
        trim: true
    },
    status: {
        type: String,
        required: false,
        trim: true
    },
    amountPart1: {
        type: Number,
        required: false
    },
    amountPart2: {
        type: Number,
        required: false
    },
    broadcastName: {
        type: String,
        required: false,
        trim: true
    },
    category: {
        type: String,
        required: false,
        trim: true
    },
    deadline: {
        type: String,
        required: false,
        trim: true
    },
    broadcastLength: {
        type: String,
        required: false,
        trim: true
    },
    productionCosts: {
        type: Number,
        required: false
    }
});

fundingSchema.index({
    fundedObject: 1,
    year: 1,
    amount: 1,
    fundingBasis: 1,
    deadline: 1,
    broadcastName: 1,
    receiver: 1
}, {
    unique: true
});

fundingSchema.indexes();

const Funding = mongoose.model<IFundingDocument, IFundingModel>('Funding', fundingSchema);

export default Funding;
