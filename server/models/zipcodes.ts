import mongoose from 'mongoose';
import { FederalStateCode, federalStates } from './types';


const Schema = mongoose.Schema;

export interface IZipCode {
    zipCode: string;
    federalState: string;
    federalStateCode: FederalStateCode;
}
/**
* ZipCode Schema
 */

export interface IZipCodeDocument extends IZipCode, mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
  }

export interface IZipCodeModel extends IZipCode, mongoose.Model<IZipCodeDocument> {}


const ZipcodeSchema = new Schema<IZipCodeDocument,IZipCodeModel>({
  zipCode: {
    type: String,
    required: true,
    unique: true,
    index: 1
  },
  federalState: {
    type: String,
    required: true
  },
  federalStateCode: {
    type: String,
    enum: federalStates.map(a=>a),
    required: true
  }
});


const ZipCode: IZipCodeModel = mongoose.model<IZipCodeDocument,IZipCodeModel>('ZipCode',ZipcodeSchema);

export default ZipCode;