import { Schema, model, Model, Document } from "mongoose";

export interface IBlog {
  titleGerman: string;
  titleEnglish: string;
  contentGerman: string;
  contentEnglish: string;
  createDate: Date;
  isActive: true;
}

// export type IBlogDocument = IBlog & Document<IBlog>

export interface IBlogDocument extends IBlog, Document {
  _id: Schema.Types.ObjectId;
}

export interface IStoredBlog extends IBlog {
  _id: string;
}

export interface IBlogModel extends IBlog, Model<IBlog> {}

/**
 * Blog Schema
 */

const BlogSchema = new Schema<IBlogDocument, IBlogModel>({
  titleGerman: {
    type: String,
    required: true,
    trim: true,
  },
  contentGerman: {
    type: String,
    required: true,
    trim: true,
  },
  titleEnglish: {
    type: String,
    required: true,
    trim: true,
  },
  contentEnglish: {
    type: String,
    required: true,
    trim: true,
  },
  createDate: {
    type: Date,
    required: true,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    required: true,
    default: true,
  },
});

const BlogModel = model<IBlogDocument, IBlogModel>("Blogs", BlogSchema);

export default BlogModel;
