import BaseCtrl from "./base";
import { catController } from "../logging";
import Blog, { IBlog } from "../models/blog";

export default class BlogCtrl extends BaseCtrl<IBlog> {
  model = Blog;
  projection =
    "_id titleGerman titleEnglish contentGerman contentEnglish createDate isActive";

  // Get all enities of type model
  getAllActive = (req, res) =>
    this.model
      .find({ isActive: true }).sort({ createDate: -1})
      .then((docs) => res.json(docs))
      .catch((err) => {
        catController.error("Error while finding documents", err);
        res.status(500).send({ message: err });
      });
}
