import * as jwt from 'jsonwebtoken';

import User from '../models/user';
import BaseCtrl from './base';
import {IUserDocument} from '../models/types';

export default class UserCtrl extends BaseCtrl<IUserDocument> {

  model = User;
  projection = '_id username email roles active';

  login = (req, res) => {
    this.model.findOne({ email: req.body.email, provider: 'local', active: true })
    .then(user => {
      if (!user) { return res.sendStatus(403) }
      user.comparePassword(req.body.password, (error, isMatch) => {
        if (!isMatch) { return res.sendStatus(403); }
        const token = jwt.sign({ user: user }, process.env.SECRET_TOKEN, { expiresIn: '8h'}); // , { expiresIn: 10 } seconds
        res.status(200).json({ token: token });
      });
    })
    .catch(() => res.sendStatus(403))
  };

  getList = (req, res) =>
    this.model.find({}, this.projection).sort({username:1})
      .then(l => res.json(l))
      .catch(err => res.status(500).json({message: err}));

  insert = (req, res, next) => {
    const obj = new this.model(req.body);
    obj.save()
      .then(m => (Object.prototype.hasOwnProperty.call(this.model, 'load')) ? this.model['load'](m._id) : m)
      .then(m => req[this.model.collection.collectionName] = m)
      .then(() => next())
      .catch(err => err.code === 11000 ?
          res.status(400).json({message: 'Sorry, but this username/email is already in use!'}) :
          res.status(400).json({message: err.message || err}));
  };

  setRoleAndProvider = (req, res, next) =>  {
      req.body.roles = ['user'];
      req.body.provider = 'local';
      next();
  }

}
