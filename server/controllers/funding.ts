import { parse } from "csv-parse";
import fs, { promises as fsPromises } from "fs";
import Funding, { IFundingDocument } from "../models/funding";
import Grouping, { IGrouping, IGroupingDocument } from "../models/grouping";
import { TUploadResult } from "../models/types";
import ZipCode from "../models/zipcodes";
import BaseCtrl from "./base";

const { finished } = require("stream/promises");

const MAX_FLOWS = 1500;

interface ICumulatedFundings {
  total: number;
  receiver: string;
}

interface IGroupedFundings extends ICumulatedFundings {
  isGrouping: boolean;
  children: ICumulatedFundings[];
}

export type TYearlyFundingInfo = {
  name: string;
  entries: number;
  total: number;
};

export type TOverViewFundingResult = [
  {
    year: number;
    yearsData: TYearlyFundingInfo[];
  }
];

export default class FundingCtrl extends BaseCtrl<IFundingDocument> {
  model = Funding;
  projection =
    "_id receiver fundedObject name payer amount year fundingBasis street zipCode city_de status amountPart1 amountPart2 broadcastName category deadline broadcastLength productionCosts";

  private readFile = async (filePath: string, columns: string[]) => {
    const records = [];
    const parser = fs.createReadStream(filePath).pipe(
      parse({
        fromLine: 2,
        delimiter: ";",
        columns: columns,
      })
    );

    parser.on("readable", function () {
      let record;
      while ((record = parser.read())) {
        // eslint-disable-line no-cond-assign
        records.push(record);
      }
    });

    await finished(parser);
    return records;
  };

  deleteManyFromType = async (req, res) => {
    if (!req.query || !req.query.type) {
      return res
        .status(400)
        .send({ message: "'type' query can not be empty." });
    }
    const type = req.query.type;
    this.model
      .deleteMany({ type: type })
      .then((del) => {
        if (del.deletedCount == 0) {
          return res.status(200).send({
            message:
              "No files were deleted, query with type '" +
              type +
              "' matches no entries.",
          });
        }
        res.status(200).send({
          message:
            "Deleted " +
            del.deletedCount +
            " entries matching type: '" +
            type +
            "'",
        });
      })
      .catch((err) => {
        res.status(500).send({ message: err });
      });
  };

  uploadFunding = async (req, res) => {
    const file = req.file;
    const filePath = file.path;
    const columns = [
      "receiver",
      "fundedObject",
      "name",
      "payer",
      "amount",
      "year",
      "fundingBasis",
      "street",
      "zipCode",
      "city_de",
      "status",
      "amountPart1",
      "amountPart2",
      "broadcastName",
      "category",
      "deadline",
      "broadcastLength",
      "productionCosts",
    ];
    const feedback: TUploadResult = {
      entries: 0,
      savedEntries: 0,
      errors: 0,
      errorEntries: [],
    };
    try {
      const records = await this.readFile(filePath, columns);
      feedback.entries = records.length;

      const zipCodes = await ZipCode.find({});
      const zipMap = zipCodes.reduce(
        (acc, z) => ({ ...acc, [z.zipCode]: z.federalStateCode }),
        {}
      );

      const combinations = [];
      let prevValue = "0000";
      const insert = records.map((record) => {
        let value = record.name + record.year;
        if (value !== prevValue) {
          combinations.push([record.year, record.name]);
          prevValue = value;
        }
        return new Funding({
          ...record,
          federalState: record.zipCode ? zipMap[record.zipCode] : undefined,
        });
      });

      let deleteOps = [];
      combinations.some((c) => {
        deleteOps.push(this.model.deleteMany({ year: c[0], name: c[1] }));
      });
      Promise.all(deleteOps);

      const result = await this.model.insertMany(insert, { ordered: false });
      feedback.errorEntries = insert
        .filter((i) => i.errors)
        .concat(insert.filter((i) => i.federalState == "unknown"));
      feedback.savedEntries = result.length;
      return res.status(200).send(feedback);
    } catch (error) {
      if (error.name == "MongoBulkWriteError") {
        feedback.savedEntries = error.insertedDocs.length;
        return res.status(200).send(feedback);
      }
      feedback.errorEntries.push(error);
      return res.status(500).send(feedback);
    } finally {
      await fsPromises.unlink(filePath);
    }
  };

  searchName = (req, res) => {
    const name = req.query.name;
    if (!name) {
      res.status(400).send({
        error: "'name' is required!",
      });
      return;
    }
    const rType = req.query.type;
    if (!rType) {
      res.status(400).send({
        error: "'type' is required!",
      });
      return;
    }
    const fundingFieldName =
      rType == "receiver"
        ? "receiver"
        : rType == "payer"
        ? "payer"
        : "fundedObject";
    this.getNames(fundingFieldName, name)
      .then((results) => res.json(Array.from(results)));
  };

  private getNames( fieldName: string, name: string) {
    return this.model
      .aggregate([
        { $project: { [fieldName]: 1 } },
        { $group: { _id: { name: "$" + fieldName } } },
        {
          $project: {
            name: "$_id.name",
            _id: 0,
          },
        },
        {
          $match: {
            name: {
              $regex: `.*${name}.*`,
              $options: "i",
            },
          },
        },
        {
          $sort: { name: 1}
        }
      ])
      .then((records) => records.map((e) => e.name));
  }

  getOldestAndLatestPeriod = (req, res) => {
    return Funding.aggregate([
      { "$group": {
          "_id": null,
          "max": { "$max": "$year" },
          "min": { "$min": "$year" }
      }}
    ])
    .then( extremes =>
      res.json([ extremes[0]['min'], extremes[0]['max']]))
    .catch((err) => {
        return res.status(500).send(err);
    });
  };

  topEntries = (req, res) => {
    let allPromise, groupingQuery, groupingsPromise;

    const includeGroupings = req.query.groupType ?? "";
    const period = {};
    if (req.query.from) {
      period["$gte"] = parseInt(req.query.from);
    }
    if (req.query.to) {
      period["$lte"] = parseInt(req.query.to);
    }
    let paymentTypes = req.query.fundingType || [0, 1, 2, 3, 4];
    if (!(paymentTypes instanceof Array)) {
      paymentTypes = [paymentTypes];
    }
    const limitOfResults = parseInt(req.query.x || "10");

    const query = {};
    const project = {
      receiver: "$_id.receiver",
      _id: 0,
      total: 1,
    };
    if (period["$gte"] != null || period["$lte"] != null) {
      query["year"] = period;
    }
    query["name"] = {
      $in: paymentTypes.map((e) => this.getFundingNameFromPaymentType(+e)),
    };
    const group = {
      _id: {
        receiver: "$receiver",
      },
      total: {
        $sum: "$amount",
      },
    };
    const pipeline: any[] = [
      { $match: query },
      { $group: group },
      { $sort: { total: -1 } },
    ];
    if (!includeGroupings) {
      pipeline.push({ $limit: limitOfResults });
    }
    pipeline.push({ $project: project });
    const promisesToFulfill = [Funding.aggregate(pipeline)];
    allPromise = Funding.aggregate([
      { $match: query },
      { $project: { amount: 1, _id: 0 } },
      {
        $group: {
          _id: "result",
          total: { $sum: "$amount" },
        },
      },
      { $project: { total: 1, _id: 0 } },
    ]);
    promisesToFulfill.push(allPromise);
    if (includeGroupings) {
      groupingQuery = {};
      groupingQuery.isActive = true;
      groupingQuery.group_type = includeGroupings;
      groupingQuery.type = "media";
      groupingsPromise =
        Grouping.find(groupingQuery).select("name members -_id");
      promisesToFulfill.push(groupingsPromise);
    }
    return Promise.all(promisesToFulfill)
      .then((results) => {
        try {
          const result = {
            top: results[0],
            all: results[1].length > 0 ? results[1][0]["total"] : 0,
            groupings: results[2] ? results[2] : void 0,
          };
          if (result.groupings != null) {
            result.top = this.handleGroupings(
              result.groupings,
              result.top,
              limitOfResults
            );
          }
          return res.send(result);
        } catch (error1) {
          console.log(error1);
          return res.status(500).send("No Data was found!");
        }
      })
      .catch((err) => {
        console.log("Error in Promise.when");
        console.log(err);
        return res.status(500).send("Error " + err.message);
      });
  };

  getFundingNameFromPaymentType = (paymentType: number): string => {
    switch (paymentType) {
      case 0:
        return "Pressefoerderung";
      case 1:
        return "Publizistikfoerderung";
      case 2:
        return "Privatrundfunkfonds";
      case 3:
        return "Nichtkommerzieller Rundfunkfonds";
      case 4:
        return "Fernsehfonds";
      default:
        return "";
    }
  };

  handleGroupings = (
    groupings: IGroupingDocument[],
    fundings: any[],
    limit: number
  ) => {
    console.log("found " + groupings.length + " groupings");
    console.log("found " + fundings.length + " fundings");
    const lookup = groupings.reduce(
      (acc, group) => ({
        ...acc,
        ...group.members.reduce(
          (acc_, m) => ({
            ...acc_,
            [m]: group.name,
          }),
          {}
        ),
      }),
      {} as any
    );
    const groupedFundings = new Map<string, IGroupedFundings>();
    const addMember = (funding: ICumulatedFundings) => {
      if (funding.receiver in lookup) {
        const groupName = lookup[funding.receiver];
        if (groupedFundings.has(groupName)) {
          const g = groupedFundings.get(groupName);
          g.children.push(funding);
          g.total += funding.total;
        } else {
          groupedFundings.set(groupName, {
            receiver: groupName,
            total: funding.total,
            children: [funding],
            isGrouping: true,
          });
        }
        return false;
      } else {
        return true;
      }
    };
    const fundingsWithGroupings = [
      ...fundings.filter(addMember),
      ...Array.from(groupedFundings.values()),
    ];
    return fundingsWithGroupings
      .sort((a, b) => b.total - a.total)
      .splice(0, limit);
  };

  // helper functon for filteredFlows
  getPeriodFromQuery = (req) => {
    const period = {};
    if (req.query.from) {
      period["$gte"] = parseInt(req.query.from);
    }
    if (req.query.to) {
      period["$lte"] = parseInt(req.query.to);
    }
    return period;
  };

  // helper functon for filteredFlows
  // this function gets the receiver with their amount defined in the query params
  // when no receivers are defined, all funding receivers are returned
  getOtherReceivers = (receiver, period, fundingTypes /*receiverGroups*/) => {
    const result = [];
    if (
      receiver &&
      receiver.length > 0 &&
      fundingTypes &&
      fundingTypes.length > 0
    ) {
      const qry = {};
      if (fundingTypes.length > 0) {
        qry["name"] = {
          $in: fundingTypes.map((e) => this.getFundingNameFromPaymentType(+e)),
        };
      }

      if (receiver.length > 0) {
        qry["receiver"] = {
          $nin: receiver,
        };
      }
      if (period.$gte || period.$lte) {
        qry["year"] = period;
      }
      const grp = {
        _id: {
          name: "$name",
        },
        amount: {
          $sum: "$amount",
        },
      };

      return Funding.aggregate([{ $match: qry }, { $group: grp }]).then(
        (rslt) => {
          return rslt.reduce(
            (acc, data) => [
              ...acc,
              {
                amount: data.amount,
                receiver: "Other receivers",
                fundingType: data._id.name,
              },
            ],
            result
          );
        }
      );
    } else {
      return Promise.resolve(result);
    }
  };

  // helper functon for filteredFlows
  // this function gets the receiver with their amount defined in the query params
  // when no receivers are defined, all funding receivers are returned
  getOtherFundingTypes = (
    receiver,
    period,
    fundingTypes /*receiverGroups*/
  ) => {
    const result = [];
    if (
      receiver &&
      receiver.length > 0 &&
      fundingTypes &&
      fundingTypes.length > 0
    ) {
      const qry = {};
      if (fundingTypes.length > 0) {
        qry["name"] = {
          $nin: fundingTypes.map((e) => this.getFundingNameFromPaymentType(+e)),
        };
      }

      if (receiver.length > 0) {
        qry["receiver"] = {
          $in: receiver,
        };
      } /* else if (
        !receiverGroups ||
        (receiverGroups && receiverGroups.length == 0)
      ) {
        qry["receiver"] = {
          $nin: [],
        };
      } */

      if (period.$gte || period.$lte) {
        qry["year"] = period;
      }
      const grp = {
        _id: {
          receiver: "$receiver",
        },
        amount: {
          $sum: "$amount",
        },
      };

      return Funding.aggregate([{ $match: qry }, { $group: grp }]).then(
        (rslt) => {
          return rslt.reduce(
            (acc, data) => [
              ...acc,
              {
                amount: data.amount,
                receiver: data._id.receiver,
                fundingType: "Other fundingtypes",
              },
            ],
            result
          );
        }
      );
    } else {
      return Promise.resolve(result);
    }
  };

  filteredFlows = (req, res) => {
    try {
      const maxLength = parseInt(req.query.maxLength || MAX_FLOWS.toString());
      const period = this.getPeriodFromQuery(req);
      const query = {};

      let fundingTypes = req.query.fundingType || [0, 1, 2, 3, 4];
      if (!(fundingTypes instanceof Array)) {
        fundingTypes = [fundingTypes];
      }
      if (fundingTypes.length > 0) {
        query["name"] = {
          $in: fundingTypes.map((e) => this.getFundingNameFromPaymentType(+e)),
        };
      }

      let receiver = req.query.receiver || [];
      if (!(receiver instanceof Array)) {
        receiver = [receiver];
      }
      if (receiver.length > 0) {
        query["receiver"] = {
          $in: receiver,
        };
      }

      let receiverGroups = req.query.receiverGroups || [];
      if (!(receiverGroups instanceof Array)) {
        receiverGroups = [receiverGroups];
      }

      /*       if (!receiverGroups || (receiverGroups && receiverGroups.length == 0)) {
        query["receiver"] = {
          $nin: receiver,
        };
      } */

      if (period["$gte"] || period["$lte"]) {
        query["year"] = period;
      }

      const group = {
        _id: {
          receiver: "$receiver",
          fundingType: "$name",
          fundingBasis: "$fundingBasis",
        },
        amount: {
          $sum: "$amount",
        },
      };
      // this part gets the receiver in the defined group and merges them with the defined receiver using the getOtherReceivers function
      const mediaGroupType = "group";
      let receiverGroupMembers = {};
      return Promise.all([
        receiverGroups.length > 0
          ? Grouping.find({
              name: { $in: receiverGroups },
              isActive: true,
              type: "media",
              group_type: mediaGroupType,
            })
          : Promise.resolve([]),
      ])
        .then(([receiversInGroup]) => {
          if (receiversInGroup.length > 0) {
            if (!query["receiver"]) {
              query["receiver"] = { $nin: [] };
            }
            // receiverGroupMembers this are the member that are in the receiver group that is passed via the query params
            receiverGroupMembers = (receiversInGroup as IGrouping[]).reduce(
              (acc, o) =>
                o["members"].reduce(
                  (a, m) => ({
                    ...a,
                    [m]: o.name,
                  }),
                  acc
                ),
              {}
            );
            receiver = receiver.concat(receiverGroupMembers);
            if (query["receiver"]["$in"])
              query["receiver"]["$in"] = query["receiver"]["$in"].concat(
                (receiversInGroup as IGrouping[]).reduce(
                  (acc, o) => acc.concat(o["members"]),
                  []
                )
              );
            else {
              query["receiver"] = {
                $in: (receiversInGroup as IGrouping[]).reduce(
                  (acc, o) => acc.concat(o["members"]),
                  []
                ),
              };
            }
          }
        })
        .then(() =>
          Funding.aggregate([
            { $match: query },
            { $group: group },
            {
              $project: {
                fundingType: "$_id.fundingType",
                receiver: "$_id.receiver",
                fundingBasis: "$_id.fundingBasis",
                _id: 0,
                amount: 1,
              },
            },
          ])
        )
        .then((result) => {
          if (result.length == 0) {
            return res.json(result);
          }
          Promise.all([
            this.getOtherReceivers(
              receiver,
              period,
              fundingTypes
              /* receiverGroups */
            ),
            this.getOtherFundingTypes(receiver, period, fundingTypes),
          ])
            .then(
              ([otherReceiver, otherFundingTypes]) => [
                ...result,
                ...otherReceiver,
                ...otherFundingTypes,
              ] // result = receivers from the receiverGroup, otherReceiver = reurn of getOtherReceiver
            )
            .then((result) => {
              if (result.length > maxLength) {
                return res.status(413).send({
                  error:
                    "Your query returns more then the specified maximum of " +
                    maxLength +
                    ". Please refine your query parameters to limit the result",
                  length: result.length,
                });
              } else {
                const r = result.map((t) => {
                  if (t.receiver in receiverGroupMembers) {
                    t["receiverGroups"] = receiverGroupMembers[t.receiver];
                  }
                  return t;
                });
                return res.json(r);
              }
            });
        })
        .catch((err) =>
          res.status(500).send({
            error: "Could not load money flow: " + err,
          })
        );
    } catch (error) {
      return res.status(500).send({
        error: "Could not load money flow: " + error,
      });
    }
  };

  search = (req, res) => {
    const name = req.query.name;
    if (!name) {
      res.status(400).send({
        error: "'name' is required!",
      });
      return;
    }
    const receiverType = ["receiver"];
    const buildRegex = (name, value) => ({
      [name]: {
        $regex: ".*" + value + ".*",
        $options: "i",
      },
    });
    const performQuery = () => {
      const nameField = "receiver";
      const group = {
        _id: {
          name: "$" + nameField,
          type: nameField,
        },
        fundingYears: {
          $addToSet: "$year",
        },
        total: {
          $sum: "$amount",
        },
        transferTypes: {
          $addToSet: "$name",
        },
      };
      const project = {
        name: "$_id.name",
        _id: 0,
        fundingYears: 1,
        total: 1,
        transferTypes: 1,
        lowerName: {
          $toLower: "$_id.name",
        },
      };
      const $or = name.split(" ").map((n) => buildRegex(nameField, n));
      let query = {
        $or,
      };
      return Funding.aggregate([
        { $match: query },
        { $group: group },
        { $project: project },
        { $sort: { lowerName: 1 } },
      ]);
    };

    return Promise.all(receiverType.map(performQuery))
      .then((results) => {
        const result = receiverType.reduce(
          (acc, t, index) => ({ ...acc, [t]: results[index] }),
          {}
        );
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send({
          error: `Could not perform search (${err.message || err})`,
        });
      });
  };

  overview = (req, res) => {
    Funding.aggregate([
      { $match: {} },
      {
        $group: {
          _id: {
            year: "$year",
            name: "$name",
          },
          entries: {
            $sum: 1,
          },
          total: {
            $sum: "$amount",
          },
        },
      },
      {
        $project: {
          year: "$_id.year",
          name: "$_id.name",
          _id: 0,
          entries: 1,
          total: 1,
        },
      },
      {
        $group: {
          _id: {
            year: "$year",
          },
          yearsData: {
            $addToSet: {
              name: "$name",
              entries: "$entries",
              total: "$total",
            },
          },
          total: {
            $sum: "$total",
          },
        },
      },
      {
        $project: {
          year: "$_id.year",
          name: "$_id.name",
          _id: 0,
          yearsData: 1,
          //quarters: 1
        },
      },
    ])
      .then((result) => res.send(result))
      .catch((err) =>
        res.status(500).send("Could not load overview from Database: " + err)
      );
  };

  timeline = (req, res) => {
    //let error, paymentTypes, query, source, target;
    try {
      let fundingTypes = req.query.fundingType || [0, 1, 2, 3, 4];
      if (!(fundingTypes instanceof Array)) {
        fundingTypes = [fundingTypes];
      }
      let receiver = req.query.receiver || [];
      if (!(receiver instanceof Array)) {
        receiver = [receiver];
      }

      let receiverGroups = req.query.receiverGroups || [];
      if (!(receiverGroups instanceof Array)) {
        receiverGroups = [receiverGroups];
      }
      const query = {};
      if (receiver.length > 0) {
        query["receiver"] = {
          $in: receiver,
        };
      }
      if (fundingTypes.length > 0) {
        query["name"] = {
          $in: fundingTypes.map((e) => this.getFundingNameFromPaymentType(+e)),
        };
      }

      const mediaGroupType = "group";
      let receiverGroupMembers = {};
      return Promise.all([
        receiverGroups.length > 0
          ? Grouping.find({
              name: { $in: receiverGroups },
              isActive: true,
              type: "media",
              group_type: mediaGroupType,
            })
          : Promise.resolve([]),
      ])
        .then(([receiversInGroup]) => {
          if (receiversInGroup.length > 0) {
            if (!query["receiver"]) {
              query["receiver"] = { $nin: [] };
            }
            // receiverGroupMembers this are the member that are in the receiver group that is passed via the query params
            receiverGroupMembers = (receiversInGroup as IGrouping[]).reduce(
              (acc, o) =>
                o["members"].reduce(
                  (a, m) => ({
                    ...a,
                    [m]: o.name,
                  }),
                  acc
                ),
              {}
            );
            receiver = receiver.concat(receiverGroupMembers);
            if (query["receiver"]["$in"])
              query["receiver"]["$in"] = query["receiver"]["$in"].concat(
                (receiversInGroup as IGrouping[]).reduce(
                  (acc, o) => acc.concat(o["members"]),
                  []
                )
              );
            else {
              query["receiver"] = {
                $in: (receiversInGroup as IGrouping[]).reduce(
                  (acc, o) => acc.concat(o["members"]),
                  []
                ),
              };
            }
          }
        })
        .then(() =>
          Promise.all([
            Funding.aggregate([
              { $match: query },
              {
                $group: {
                  _id: {
                    year: "$year",
                    name: "$name",
                  },
                  entries: {
                    $sum: 1,
                  },
                  total: {
                    $sum: "$amount",
                  },
                },
              },
              {
                $project: {
                  year: "$_id.year",
                  name: "$_id.name",
                  _id: 0,
                  entries: 1,
                  total: 1,
                },
              },
              {
                $group: {
                  _id: {
                    year: "$year",
                  },
                  yearsData: {
                    $addToSet: {
                      name: "$name",
                      entries: "$entries",
                      total: "$total",
                    },
                  },
                  total: {
                    $sum: "$total",
                  },
                },
              },
              {
                $project: {
                  year: "$_id.year",
                  name: "$_id.name",
                  _id: 0,
                  yearsData: 1,
                  //quarters: 1
                },
              },
            ]),
          ])
        )
        .then(([result]) => res.json(result));
    } catch (error) {
      return res.status(500).send({
        error: "Could not load money flow: " + error,
      });
    }
  };
}
