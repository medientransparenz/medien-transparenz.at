
import BaseCtrl from "./base";
import Grouping, { IGrouping } from '../models/grouping';
import {catController} from '../logging';

export default class GroupingCtrl extends BaseCtrl<IGrouping> {
    model = Grouping;
    projection = '_id name type group_type isActive';

      // Get all enities of type model
  getAll = (req, res) =>
  this.model.find({isActive: true})
    .then(docs => res.json(docs))
    .catch(err => {
      catController.error('Error while finding documents', err);
      res.status(500).send({message: err})
    });

  getGroups = (req, res) => {
    req.query['isActive'] = true;
    return this.getList(req,res)
  }

  countActive = (req, res) => {
    this.model.countDocuments({isActive: true}).then(count => res.json(count))
      .catch(err => res.status(500).json({message: err}));
  };  
}