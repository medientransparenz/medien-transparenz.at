import { IEventDocument } from "../models/events";
import BaseCtrl from "./base";
import Event from "../models/events"

export default class EventCtrl extends BaseCtrl<IEventDocument> {

    model = Event;
    projection = '_id name region tags startDate endDate predictable type';
}    