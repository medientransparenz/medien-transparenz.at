import Difference, { IDifference } from '../models/differences';
import BaseCtrl from './base';


export default class DifferenceCtrl extends BaseCtrl<IDifference> {
    model = Difference;
    projection = '_id period amountRtr countRtr amountMta countMta diffAmount diffCount'
} 