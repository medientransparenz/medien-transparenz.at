
import BaseCtrl from './base';
import Duplicate, { IDuplicate } from '../models/duplicates';



export default class DuplicateCtrl extends BaseCtrl<IDuplicate> {
    model = Duplicate;
    projection = '_id organisation media transferType amount period year quarter occurrences'
} 