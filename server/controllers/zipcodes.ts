import ZipCode, { IZipCodeDocument } from "../models/zipcodes";
import BaseCtrl from "./base";
import { promises as fsPromises } from 'fs';
import iconv from "iconv-lite";
import { catController } from '../logging';

import { FederalStatesShortDecoder, TUploadResult } from '../models/types';



export default class ZipCodeCtrl extends BaseCtrl<IZipCodeDocument> {
    model = ZipCode;
    projection = '_id zipCode federalState federalStateCode'

    lineToZipCode = (line) => {
        const splittedLine = line.split(";");
        if (splittedLine.length <= 3) {
          throw new Error('Upload expects another file format');
        }
        return new ZipCode({
          zipCode:  splittedLine[0],
          federalState: FederalStatesShortDecoder[splittedLine[2]].name,
          federalStateCode: FederalStatesShortDecoder[splittedLine[2]].code
        });
    }

    uploadZipCode = async (req, res) => {
      const file = req.file;
      const response:TUploadResult = {
        entries: 0,
        savedEntries: 0,
        errorEntries: [],
        error: null
      };
      try {
        const data = await fsPromises.readFile(file.path);
        const lines = iconv.decode(data, 'utf8').split('\n').slice(1).filter(l=>l.length>0);
        response.entries = lines.length;
        const entries = lines.map(this.lineToZipCode)
        const deleted =  await this.model.deleteMany({'zipCode': { $in: entries.map(e=>e.zipCode)}})
        catController.info('deleted: '+ JSON.stringify(deleted))
        const result = await this.model.insertMany(entries,{ordered: false})
        response.savedEntries = result.length;
        return res.status(200).send(response);
      } catch (err) {
          if (err.name == 'MongoBulkWriteError') {
            response.savedEntries = err.insertedDocs.length
            return res.status(200).send(response);
          }
          catController.error('Error while uploading ZIP codes',err);
          response.error=err.message || err;
          return res.status(500).send(response);
      } finally {
        fsPromises.unlink(file.path);
      }
    }

}
