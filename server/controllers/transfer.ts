import BaseCtrl from "./base";
import Transfer, { ITransfer, ITransferDocument } from "../models/transfer";
import fs from "fs";
import { parse } from "csv-parse";
import Organisation from "../models/organisations";
import { TUploadResult, federalStates as FEDERALSTATES } from "../models/types";
import { catController } from "../logging";
import Grouping, { IGroupingDocument, IGrouping } from "../models/grouping";
import mongoose from "mongoose";
import { TOverViewFundingResult } from "./funding";

const MAX_FLOWS = 1500;

export type TQuarterlyInfo = {
  quarter: number;
  entries: number;
  total: number;
  transferType: number;
  prediction?: boolean;
};

export type TOverViewTransferResult = [
  {
    year: number;
    quarters: TQuarterlyInfo[];
  }
];

export type TOverViewResult = {
  transfers: TOverViewTransferResult;
  fundings: TOverViewFundingResult;
};

interface ICumulatedTransfers {
  total: number;
  organisation: string;
}

interface IGroupedTransfers extends ICumulatedTransfers {
  isGrouping: boolean;
  children: ICumulatedTransfers[];
}

export default class TransferCtrl extends BaseCtrl<ITransfer> {
  model = Transfer;
  projection = "_id organisation media transferType amount period federalState";

  private processFile = async (filename: string, orgsMap) =>
    new Promise((resolve, reject) => {
      //const batchSize = 1000;
      let jobs: Promise<number> = Promise.resolve(0);
      const missingOrganisation = new Set<string>();
      const feedback: TUploadResult = {
        entries: 0,
        savedEntries: 0,
        errors: 0,
        errorEntries: [],
        transfersWithoutAddress: [],
      };
      const recordToTransfer = (record) => {
        const [organisation, period, transferType, media, amount] = record;
        const lookup = orgsMap[organisation];
        if (lookup == undefined || lookup == null) {
          missingOrganisation.add(organisation);
          catController.info("No Organisation for " + organisation);
        }
        return new Transfer({
          organisation,
          period: parseInt(period),
          transferType: parseInt(transferType),
          media,
          year: parseInt(period.slice(0, 4)),
          quarter: parseInt(period.slice(4)),
          federalState: lookup?.federalState ?? "unknown",
          organisationType: lookup?.organisationType,
          organisationReference: lookup?._id,
          amount: parseFloat(amount),
        });
      };

    const saveBatch = async (batch: ITransferDocument[]) => {
      try {
        //console.log(`Storing batch of size ${batch.length}`)
        batch.forEach(t => {
          if (t.media.startsWith('www.')) {
            t.media = t.media.slice(4)
          }
        })
        const result = await Transfer.insertMany(batch, { ordered: false });
        //console.log(`Saved ${result['length']} entries to database`)
        feedback.savedEntries += result['length'] ?? 0;
        return result['length'] ?? 0;
      } catch (error) {
        //console.error(error.name);
        if (error.name == 'MongoBulkWriteError') {
          const entries = error.insertedDocs.length as number
          //console.log(`Saved ${entries} entries with errors to database`)
          feedback.savedEntries += entries
          feedback.errorEntries.push(...error.writeErrors
            .map(e => ({ msg: e.errmsg || e.err.errmsg, doc: e.err.op })))
          return entries
        } else {
          throw error;
        }
      }
    }

      const parser = parse({
        fromLine: 2,
        delimiter: ";",
        encoding: "utf8",
        from_line: 2,
        on_record: (c) =>
          c[3] == "0" ? [0, 1, 2, 4, 5].map((i) => c[i]) : undefined,
        //columns: ['organisation','period','transferType','media','amount']
      });
      parser.on("readable", function () {
        let batch = [];
        let record;
        while ((record = parser.read())) {
          // eslint-disable-line no-cond-assign
          feedback.entries++;
          const transfer = recordToTransfer(record);
          if (transfer.errors) {
            feedback.errorEntries.push(transfer);
          } else {
            batch.push(transfer);
          }
        }
        if (batch.length > 0) {
          jobs = jobs.then((s) => saveBatch(batch).then((s2) => s + s2));
        }
      });
      // Catch any error
      parser.on("error", (err) => {
        fs.unlink(filename, () => {});
        console.error(err);
        reject(err);
      });
      // When we are done, test that the parsed output matched what expected
      parser.on("end", function () {
        feedback.transfersWithoutAddress = Array.from(
          missingOrganisation.values()
        );
        jobs.then(() => {
          fs.unlink(filename, () => {});
          resolve(feedback);
        });
      });
      fs.createReadStream(filename).pipe(parser);
    });

  uploadTransfers = async (req, res) => {
    const file = req.file;
    catController.debug(`Uploading new transfer file: ${file.path}`);
    console.log(`Uploading new transfer file: ${file.path}`);
    let feedback;
    try {
      const db = mongoose.connection;
      const orgsCursor = await db
        .collection("organisations")
        .find({}, { timeout: false }); //await Organisation.find({}, {timeout: false })
      const orgs = await orgsCursor.toArray();
      const orgsMap = orgs.reduce(
        (acc, org) => ({
          ...acc,
          [org.name]: {
            federalState: org.federalState,
            organisationType: org.type,
            _id: org._id.toString(),
          },
        }),
        {}
      );
      feedback = await this.processFile(file.path, orgsMap);
      return res.send(feedback);
    } catch (error) {
      catController.error("Error while processing new transfers", error);
      return res.status(500).send(feedback);
    }
  };

  overview = (req, res) => {
    catController.debug("Calculating overview");
    Transfer.aggregate([
      { $match: {} },
      {
        $group: {
          _id: {
            quarter: "$quarter",
            year: "$year",
            transferType: "$transferType",
          },
          entries: {
            $sum: 1,
          },
          total: {
            $sum: "$amount",
          },
        },
      },
      {
        $project: {
          quarter: "$_id.quarter",
          year: "$_id.year",
          transferType: "$_id.transferType",
          _id: 0,
          entries: 1,
          total: 1,
        },
      },
      {
        $group: {
          _id: {
            year: "$year",
          },
          quarters: {
            $addToSet: {
              quarter: "$quarter",
              transferType: "$transferType",
              entries: "$entries",
              total: "$total",
            },
          },
          total: {
            $sum: "$total",
          },
        },
      },
      {
        $project: {
          year: "$_id.year",
          transferType: "$_id.transferType",
          _id: 0,
          quarters: 1,
        },
      },
    ])
      .then((result) => res.send(result))
      .catch((err) =>
        res.status(500).send("Could not load overview from Database: " + err)
      );
  };

  handleGroupings = (
    groupings: IGroupingDocument[],
    transfers: any[],
    limit: number
  ) => {
    //var grouping, groupingTotalAmount, groupingTransfersAmount, groupingTransfersNames, j, len, transfer, transfersWithGrouping;
    console.log("found " + groupings.length + " gropings");
    console.log("found " + transfers.length + " transfers");
    //let transfersWithGrouping = transfers;
    const lookup = groupings.reduce(
      (acc, group) => ({
        ...acc,
        ...group.members.reduce(
          (acc_, m) => ({ ...acc_, [m]: group.name }),
          {}
        ),
      }),
      {} as any
    );
    const groupedTransfers = new Map<string, IGroupedTransfers>();
    const addMember = (transfer: ICumulatedTransfers) => {
      if (transfer.organisation in lookup) {
        const groupName = lookup[transfer.organisation];
        if (groupedTransfers.has(groupName)) {
          const g = groupedTransfers.get(groupName);
          g.children.push(transfer);
          g.total += transfer.total;
        } else {
          groupedTransfers.set(groupName, {
            organisation: groupName,
            total: transfer.total,
            children: [transfer],
            isGrouping: true,
          });
        }
        return false;
      } else {
        return true;
      }
    };
    const transfersWithGroupings = [
      ...transfers.filter(addMember),
      ...Array.from(groupedTransfers.values()),
    ];
    /*
    for (let j = 0, len = groupings.length; j < len; j++) {
      const grouping = groupings[j];
      const groupingTransfersAmount = (() => {
        var k, len1, results1;
        results1 = [];
        for (k = 0, len1 = transfersWithGrouping.length; k < len1; k++) {
          const transfer = transfersWithGrouping[k];
          if (grouping.members.includes(transfer.organisation)) {
            results1.push(transfer.total);
          }
        }
        return results1;
      })();
      const groupingTransfersNames = (() => {
        var k, len1, results1;
        results1 = [];
        for (k = 0, len1 = transfersWithGrouping.length; k < len1; k++) {
          const transfer = transfersWithGrouping[k];
          if (grouping.members.includes(transfer.organisation)) {
            results1.push(transfer.organisation);
          }
        }
        return results1;
      })();
      const groupingTotalAmount = groupingTransfersAmount.reduce((function (total, sum) {
        return total + sum;
      }), 0);
      transfersWithGrouping = transfersWithGrouping.filter(transfer =>
        !groupingTransfersNames.includes(transfer.organisation))
      transfersWithGrouping.push({
        total: groupingTotalAmount,
        organisation: grouping.name,
        isGrouping: true
      });
    }
    */
    return transfersWithGroupings
      .sort((a, b) => b.total - a.total)
      .splice(0, limit);
  };
  /*
   returns the top x biggest entries
  */
  topEntries = (req, res) => {
    let allPromise,
      federalState,
      groupingQuery,
      groupingsPromise,
      orgCategories;
    if (
      req.query.federalState &&
      FEDERALSTATES.includes(req.query.federalState)
    ) {
      federalState = req.query.federalState;
    }
    const includeGroupings = req.query.groupType ?? "";

    const period = {};
    if (req.query.from) {
      period["$gte"] = parseInt(req.query.from);
    }
    if (req.query.to) {
      period["$lte"] = parseInt(req.query.to);
    }
    const resultType = req.query.resultType || "org";
    let paymentTypes = req.query.pType || ["2"];
    if (!(paymentTypes instanceof Array)) {
      paymentTypes = [paymentTypes];
    }
    const limitOfResults = parseInt(req.query.x || "10");
    if (req.query.orgCategories) {
      orgCategories = req.query.orgCategories;
    }
    if (!(orgCategories instanceof Array) && req.query.orgCategories) {
      orgCategories = [orgCategories];
    }
    const query = {};
    const project = {
      organisation: "$_id.organisation",
      _id: 0,
      total: 1,
    };
    if (period["$gte"] != null || period["$lte"] != null) {
      query["period"] = period;
    }
    query["transferType"] = {
      $in: paymentTypes.map((e) => parseInt(e)),
    };
    if (federalState != null) {
      query["federalState"] = federalState;
    }
    if (orgCategories != null) {
      query["organisationType"] = {
        $in: orgCategories,
      };
    }
    const group = {
      _id: {
        organisation: resultType === "org" ? "$organisation" : "$media",
      },
      total: {
        $sum: "$amount",
      },
    };
    //const options = ;
    //options['query'] = query;
    const pipeline: any[] = [
      { $match: query },
      { $group: group },
      { $sort: { total: -1 } },
    ];
    if (!includeGroupings) {
      pipeline.push({ $limit: limitOfResults });
    }
    pipeline.push({ $project: project });

    const promisesToFullfill = [Transfer.aggregate(pipeline)];
    allPromise = Transfer.aggregate([
      { $match: query },
      { $project: { amount: 1, _id: 0 } },
      {
        $group: {
          _id: "result",
          total: { $sum: "$amount" },
        },
      },
      { $project: { total: 1, _id: 0 } },
    ]);
    promisesToFullfill.push(allPromise);
    if (includeGroupings) {
      groupingQuery = {};
      groupingQuery.isActive = true;
      groupingQuery.group_type = includeGroupings;
      groupingQuery.type = resultType;
      //groupingQuery.region = federalState ? federalState : 'AT';
      groupingsPromise = Grouping.find(groupingQuery).select(
        "name owner members -_id"
      );
      promisesToFullfill.push(groupingsPromise);
    }
    return Promise.all(promisesToFullfill)
      .then((results) => {
        try {
          //console.log(JSON.stringify(results))
          const result = {
            top: results[0],
            all: results[1].length > 0 ? results[1][0]["total"] : 0,
            groupings: results[2] ? results[2] : void 0,
          };
          if (result.groupings != null) {
            result.top = this.handleGroupings(
              result.groupings,
              result.top,
              limitOfResults
            );
          }
          return res.send(result);
        } catch (error1) {
          const error = error1;
          console.log(error);
          return res.status(500).send("No Data was found!");
        }
      })
      .catch((err) => {
        console.log("Error in Promise.when");
        console.log(err);
        return res.status(500).send("Error " + err.message);
      });
  };

  periods = (req, res) =>
    Transfer.aggregate([
      { $project: { period: 1 } },
      { $group: { _id: { period: "$period" } } },
      { $project: { period: "$_id.period", _id: 0 } },
      { $sort: { period: 1 } },
    ])
      .then((result) => result.map((r) => r.period))
      .then((result) => res.send(result))
      .catch((err) => res.status(500).send("Error " + err.message));

  flows = (req, res) => {
    //var error, federalState, filter, group, maxLength, name, orgType, paymentTypes, period, query;
    try {
      const maxLength = parseInt(req.query.maxLength || "1500");

      const period = {};
      if (req.query.from) {
        period["$gte"] = parseInt(req.query.from);
      }
      if (req.query.to) {
        period["$lte"] = parseInt(req.query.to);
      }
      let paymentTypes = req.query.pType || [];
      if (!(paymentTypes instanceof Array)) {
        paymentTypes = [paymentTypes];
      }
      const orgType = req.query.orgType || "org";
      const name = req.query.name;
      const query = {};
      if (paymentTypes.length > 0) {
        query["transferType"] = {
          $in: paymentTypes.map((e) => parseInt(e)),
        };
        if (name) {
          query[orgType === "org" ? "organisation" : "media"] = name;
        }
        if (period["$gte"] || period["$lte"]) {
          query["period"] = period;
        }
        if (req.query.filter) {
          const filter = req.query.filter;
          query["$or"] = [
            {
              organisation: {
                $regex: ".*" + filter + ".*",
                $options: "i",
              },
            },
            {
              media: {
                $regex: ".*" + filter + ".*",
                $options: "i",
              },
            },
          ];
        }
        if (req.query.federalState) {
          query["federalState"] = req.query.federalState;
        }
        const group = {
          _id: {
            organisation: "$organisation",
            transferType: "$transferType",
            media: "$media",
          },
          amount: {
            $sum: "$amount",
          },
        };
        return Transfer.aggregate([
          { $match: query },
          { $group: group },
          {
            $project: {
              organisation: "$_id.organisation",
              transferType: "$_id.transferType",
              media: "$_id.media",
              _id: 0,
              amount: 1,
            },
          },
        ])
          .then((result) => {
            if (result.length > maxLength) {
              return res.status(413).send({
                error:
                  "Your query returns more then the specified maximum of " +
                  maxLength +
                  ". Please refine your query parameters to limit the result",
                length: result.length,
              });
            } else {
              return res.json(result);
            }
          })
          .catch((err) => {
            return res.status(500).send({
              error: "Could not load money flow: " + err,
            });
          });
      }
    } catch (error) {
      return res.status(500).send({
        error: "Could not load money flow: " + error,
      });
    }
  };

  getPopulateInformation = (sourceForPopulate, path) =>
    Organisation.populate(sourceForPopulate, {
      path: path,
      select: "-_id",
    });

  filteredflows = (req, res) => {
    //var error, federalState, getOtherMedia, getOtherOrganisations, group, maxLength, media, organisations, paymentTypes, period, query;
    const getOtherMedia = (organisations, media, period, paymentTypes) => {
      //var grp, qry, result;
      const result = [];
      if (
        organisations &&
        organisations.length > 0 &&
        media &&
        media.length > 0
      ) {
        const qry = {};
        if (paymentTypes.length > 0) {
          qry["transferType"] = {
            $in: paymentTypes.map((e) => parseInt(e)),
          };
        }
        if (organisations.length > 0) {
          qry["organisation"] = {
            $in: organisations,
          };
        }
        if (media.length > 0) {
          qry["media"] = {
            $nin: media,
          };
        }
        if (period.$gte || period.$lte) {
          qry["period"] = period;
        }
        const grp = {
          _id: {
            organisation: "$organisation",
            organisationReference: "$organisationReference",
            transferType: "$transferType",
          },
          amount: {
            $sum: "$amount",
          },
        };
        return Transfer.aggregate([{ $match: qry }, { $group: grp }]).then(
          (rslt) =>
            rslt.reduce(
              (acc, data) => [
                ...acc,
                {
                  amount: data.amount,
                  organisation: data._id.organisation,
                  transferType: data._id.transferType,
                  media: "Other media",
                },
              ],
              result
            )
        );
      } else {
        return Promise.resolve(result);
      }
    };
    const getOtherOrganisations = (
      organisations,
      media,
      period,
      paymentTypes
    ) => {
      //var grp, qry, result;
      const result = [];
      if (
        media &&
        media.length > 0 &&
        organisations &&
        organisations.length > 0
      ) {
        const qry = {};
        if (paymentTypes.length > 0) {
          qry["transferType"] = {
            $in: paymentTypes.map((e) => parseInt(e)),
          };
        }
        if (organisations.length > 0) {
          qry["organisation"] = {
            $nin: organisations,
          };
        }
        if (media.length > 0) {
          qry["media"] = {
            $in: media,
          };
        }
        if (period.$gte != null || period.$lte != null) {
          qry["period"] = period;
        }
        const grp = {
          _id: {
            media: "$media",
            transferType: "$transferType",
          },
          amount: {
            $sum: "$amount",
          },
        };
        return Transfer.aggregate([{ $match: qry }, { $group: grp }]).then(
          (rslt) =>
            rslt.reduce(
              (acc, data) => [
                ...acc,
                {
                  amount: data.amount,
                  media: data._id.media,
                  transferType: data._id.transferType,
                  organisation: "Other organisations",
                },
              ],
              result
            )
        );
      } else {
        return Promise.resolve(result);
      }
    };
    try {
      const maxLength = parseInt(req.query.maxLength || MAX_FLOWS.toString());
      let federalState = req.query.federalState || "";
      if (federalState == "AT") {
        federalState = "";
      }
      const period = {};
      if (req.query.from) {
        period["$gte"] = parseInt(req.query.from);
      }
      if (req.query.to) {
        period["$lte"] = parseInt(req.query.to);
      }
      let paymentTypes = req.query.pType || [];
      if (!(paymentTypes instanceof Array)) {
        paymentTypes = [paymentTypes];
      }
      const query = {};
      if (federalState) {
        query["federalState"] = federalState;
      }
      if (paymentTypes.length > 0) {
        query["transferType"] = {
          $in: paymentTypes.map((e) => parseInt(e)),
        };
      }
      let organisations = req.query.organisations || [];
      if (!(organisations instanceof Array)) {
        organisations = [organisations];
      }
      let orgGroups = req.query.orgGroups || [];
      if (!(orgGroups instanceof Array)) {
        orgGroups = [orgGroups];
      }
      let media = req.query.media || [];
      if (!(media instanceof Array)) {
        media = [media];
      }
      let mediaGroups = req.query.mediaGroups || [];
      if (!(mediaGroups instanceof Array)) {
        mediaGroups = [mediaGroups];
      }
      if (organisations.length > 0) {
        query["organisation"] = {
          $in: organisations,
        };
      }
      if (media.length > 0) {
        query["media"] = {
          $in: media,
        };
      }
      if (period["$gte"] || period["$lte"]) {
        query["period"] = period;
      }
      const group = {
        _id: {
          organisation: "$organisation",
          organisationReference: "$organisationReference",
          transferType: "$transferType",
          media: "$media",
        },
        amount: {
          $sum: "$amount",
        },
      };
      // TODOs: Groups
      const mediaGroupType = "group";
      let orgGroupMembers = {};
      let mediaGroupMembers = {};
      return Promise.all([
        orgGroups.length > 0
          ? Grouping.find({
              name: { $in: orgGroups } /*,
          isActive: true,
          type: 'org',
          group_type: orgGroupType*/,
            })
          : Promise.resolve([]),
        mediaGroups.length > 0
          ? Grouping.find({
              name: { $in: mediaGroups },
              isActive: true,
              type: "media",
              group_type: mediaGroupType,
            })
          : Promise.resolve([]),
      ])
        .then(([orgs, medias]) => {
          if (orgs.length > 0) {
            if (!query["organisation"]) {
              query["organisation"] = { $in: [] };
            }
            orgGroupMembers = (orgs as IGrouping[]).reduce(
              (acc, o) =>
                o["members"].reduce((a, m) => ({ ...a, [m]: o.name }), acc),
              {}
            );
            organisations = organisations.concat(orgGroupMembers);
            query["organisation"]["$in"] = query["organisation"]["$in"].concat(
              (orgs as IGrouping[]).reduce(
                (acc, o) => acc.concat(o["members"]),
                []
              )
            );
          }
          if (medias.length > 0) {
            if (!query["media"]) {
              query["media"] = { $in: [] };
            }
            mediaGroupMembers = (medias as IGrouping[]).reduce(
              (acc, o) =>
                o["members"].reduce((a, m) => ({ ...a, [m]: o.name }), acc),
              {}
            );
            media = media.concat(mediaGroupMembers);
            query["media"]["$in"] = query["media"]["$in"].concat(
              (medias as IGrouping[]).reduce(
                (acc, o) => acc.concat(o["members"]),
                []
              )
            );
          }
        })
        .then(() =>
          Transfer.aggregate([
            { $match: query },
            { $group: group },
            {
              $project: {
                organisation: "$_id.organisation",
                //organisationReference: "$_id.organisationReference",
                transferType: "$_id.transferType",
                media: "$_id.media",
                _id: 0,
                amount: 1,
              },
            },
          ])
        )
        .then((result) => {
          //var populatedPromise;
          //return populatedPromise = this.getPopulateInformation(result, 'organisationReference')
          //.then(isPopulated => {
          //if (federalState) {
          //  result = result.filter(r => r.organisationReference.federalState_en === federalState)
          //}
          if (result.length == 0) {
            return res.json(result);
          }
          Promise.all([
            getOtherMedia(organisations, media, period, paymentTypes),
            getOtherOrganisations(organisations, media, period, paymentTypes),
          ])
            .then(([otherMedia, otherOrganisations]) => [
              ...result,
              ...otherMedia,
              ...otherOrganisations,
            ])
            .then((result) => {
              if (result.length > maxLength) {
                return res.status(413).send({
                  error:
                    "Your query returns more then the specified maximum of " +
                    maxLength +
                    ". Please refine your query parameters to limit the result",
                  length: result.length,
                });
              } else {
                const r = result.map((t) => {
                  if (t.organisation in orgGroupMembers) {
                    t["orgGroup"] = orgGroupMembers[t.organisation];
                  }
                  if (t.media in mediaGroupMembers) {
                    t["mediaGroup"] = mediaGroupMembers[t.media];
                  }
                  return t;
                });
                return res.json(r);
              }
            });
        })
        .catch((err) =>
          res.status(500).send({
            error: "Could not load money flow: " + err,
          })
        );
    } catch (error) {
      return res.status(500).send({
        error: "Could not load money flow: " + error,
      });
    }
  };

  search = (req, res) => {
    //var all, buildRegex, federalState, name, performQuery, types;
    const name = req.query.name;
    if (!name) {
      res.status(400).send({
        error: "'name' is required!",
      });
      return;
    }
    const types = req.query.orgType ? [req.query.orgType] : ["org", "media"];
    const buildRegex = (name, value) => ({
      [name]: {
        $regex: ".*" + value + ".*",
        $options: "i",
      },
    });
    const performQuery = (orgType) => {
      //var $or, group, nameField, project, query;
      const nameField = orgType === "org" ? "organisation" : "media";
      const group = {
        _id: {
          name: "$" + nameField,
          type: orgType,
        },
        transferYears: {
          $addToSet: "$year",
        },
        total: {
          $sum: "$amount",
        },
        transferTypes: {
          $addToSet: "$transferType",
        },
      };
      const project = {
        name: "$_id.name",
        _id: 0,
        transferYears: 1,
        total: 1,
        transferTypes: 1,
        lowerName: {
          $toLower: "$_id.name",
        },
      };
      if (orgType === "org") {
        group._id["organisationType"] = "$organisationType";
        group._id["federalState"] = "$federalState";
        project["organisationType"] = "$_id.organisationType";
        project["federalState"] = "$_id.federalState";
      }
      const $or = name.split(" ").map((n) => buildRegex(nameField, n));
      let query = {};
      if (!req.query.federalState) {
        query = {
          $or,
        };
      } else {
        query = {
          $and: [$or, { federalState: req.query.federalState }],
        };
      }
      return Transfer.aggregate([
        { $match: query },
        { $group: group },
        { $project: project },
        { $sort: { lowerName: 1 } },
      ]);
    };
    return Promise.all(types.map(performQuery))
      .then((results) => {
        const result = types.reduce(
          (acc, t, index) => ({ ...acc, [t]: results[index] }),
          {}
        );
        return res.json(result);
      })
      .catch((err) => {
        return res.status(500).send({
          error: `Could not perform search (${err.message || err})`,
        });
      });
  };

  searchName = (req, res) => {
    const name = req.query.name;
    if (!name) {
      res.status(400).send({
        error: "'name' is required!",
      });
      return;
    }
    const rType = req.query.type;
    if (!rType) {
      res.status(400).send({
        error: "'type' is required!",
      });
      return;
    }
    const fieldName = rType == "org" ? "organisation" : "media";
    Transfer.aggregate([
      { $project: { [fieldName]: 1 } },
      { $group: { _id: { name: "$" + fieldName } } },
      {
        $project: {
          name: "$_id.name",
          _id: 0,
          lowerName: {
            $toLower: "$_id.name",
          },
        },
      },
      {
        $match: {
          name: {
            $regex: `.*${name}.*`,
            $options: "i",
          },
        },
      },
      { $sort: { lowerName: 1 } },
    ])
      .then((records) => records.map((e) => e.name))
      .then((results) => res.json(results));
  };

  timeline = (req, res) => {
    //let error, paymentTypes, query, source, target;
    try {
      let paymentTypes = req.query.pType || ["2"];
      if (!(paymentTypes instanceof Array)) {
        paymentTypes = [paymentTypes];
      }
      let source = req.query.organisations || [];
      let target = req.query.media || [];
      if (!(source instanceof Array)) {
        source = [source];
      }
      if (!(target instanceof Array)) {
        target = [target];
      }
      let orgGroups = req.query.orgGroups || [];
      if (!(orgGroups instanceof Array)) {
        orgGroups = [orgGroups];
      }
      let mediaGroups = req.query.mediaGroups || [];
      if (!(mediaGroups instanceof Array)) {
        mediaGroups = [mediaGroups];
      }
      const query = {};
      if (source.length > 0) {
        query["organisation"] = {
          $in: source,
        };
      }
      if (target.length > 0) {
        query["media"] = {
          $in: target,
        };
      }
      if (paymentTypes.length > 0) {
        query["transferType"] = {
          $in: paymentTypes.map((e) => parseInt(e)),
        };
      }
      const mediaGroupType = "group";
      let orgGroupMembers = {};
      let mediaGroupMembers = {};
      return Promise.all([
        orgGroups.length > 0
          ? Grouping.find({
              name: { $in: orgGroups } /*,
          isActive: true,
          type: 'org',
          group_type: orgGroupType*/,
            })
          : Promise.resolve([]),
        mediaGroups.length > 0
          ? Grouping.find({
              name: { $in: mediaGroups },
              isActive: true,
              type: "media",
              group_type: mediaGroupType,
            })
          : Promise.resolve([]),
      ])
        .then(([orgs, medias]) => {
          if (orgs.length > 0) {
            if (!query["organisation"]) {
              query["organisation"] = { $in: [] };
            }
            orgGroupMembers = (orgs as IGrouping[]).reduce(
              (acc, o) =>
                o["members"].reduce((a, m) => ({ ...a, [m]: o.name }), acc),
              {}
            );
            source = source.concat(orgGroupMembers);
            query["organisation"]["$in"] = query["organisation"]["$in"].concat(
              (orgs as IGrouping[]).reduce(
                (acc, o) => acc.concat(o["members"]),
                []
              )
            );
          }
          if (medias.length > 0) {
            if (!query["media"]) {
              query["media"] = { $in: [] };
            }
            mediaGroupMembers = (medias as IGrouping[]).reduce(
              (acc, o) =>
                o["members"].reduce((a, m) => ({ ...a, [m]: o.name }), acc),
              {}
            );
            target = target.concat(mediaGroupMembers);
            query["media"]["$in"] = query["media"]["$in"].concat(
              (medias as IGrouping[]).reduce(
                (acc, o) => acc.concat(o["members"]),
                []
              )
            );
          }
        })
        .then(() =>
          Promise.all([
            Transfer.aggregate([
              { $project: { period: 1 } },
              { $group: { _id: { period: "$period" } } },
              { $project: { period: "$_id.period", _id: 0 } },
              { $sort: { period: 1 } },
            ]),
            Transfer.aggregate([
              { $match: query },
              { $project: { period: 1, amount: 1 } },
              {
                $group: {
                  _id: { period: "$period" },
                  total: { $sum: "$amount" },
                },
              },
              { $project: { period: "$_id.period", total: 1, _id: 0 } },
              { $sort: { period: 1 } },
            ]),
          ])
        )
        .then(([periods, totals]) => {
          const periodsObj = periods.reduce(
            (acc, { period }) => ({ ...acc, [period]: 0 }),
            {}
          );
          const totalsObj = totals.reduce(
            (acc, { period, total }) => ({ ...acc, [period]: total }),
            periodsObj
          );
          return Object.entries(totalsObj).map(([period, total]) => ({
            period,
            total,
          }));
        })
        .then((result) => res.json(result));
    } catch (error) {
      return res.status(500).send({
        error: "Could not load money flow: " + error,
      });
    }
  };

  federalstates = (req, res) => {
    //let error, group, organisationTypes, paymentTypes, period, query;
    try {
      const period = {};
      if (req.query.from) {
        period["$gte"] = parseInt(req.query.from);
      }
      if (req.query.to) {
        period["$lte"] = parseInt(req.query.to);
      }
      let paymentTypes = req.query.pType || [2, 4];
      if (!(paymentTypes instanceof Array)) {
        paymentTypes = [paymentTypes];
      }
      let organisationTypes = req.query.orgTypes || [];
      if (!(organisationTypes instanceof Array)) {
        organisationTypes = [organisationTypes];
      }
      const query = { federalState: { $in: FEDERALSTATES } };
      if (paymentTypes.length > 0) {
        query["transferType"] = {
          $in: paymentTypes.map((e) => parseInt(e)),
        };
      }
      if (organisationTypes.length > 0) {
        query["organisationType"] = {
          $in: organisationTypes.map(function (e) {
            return e;
          }),
        };
      }
      if (period["$gte"] != null || period["$lte"] != null) {
        query["period"] = period;
      }
      const group = {
        _id: {
          federalState: "$federalState",
        },
        amount: {
          $sum: "$amount",
        },
      };
      Transfer.aggregate([
        { $match: query },
        { $group: group },
        {
          $project: {
            federalState: "$_id.federalState",
            _id: 0,
            amount: 1,
          },
        },
        { $sort: { federalState: 1 } },
      ])
        .then((data) => {
          return res.status(200).send(data);
        })
        .catch((error) => {
          console.log("Error query data for map: " + error);
          return res.status(500).send({
            error: "Could not get data for map " + error,
          });
        });
    } catch (error) {
      console.log(error);
      return res.status(500).send("Error with query for map");
    }
  };
}
