import BaseCtrl from "./base";
import Organisation, { IOrganisationDocument } from '../models/organisations';
import fs, { promises as fsPromises } from 'fs';
import {parse} from 'csv-parse';

import ZipCode from '../models/zipcodes';
import { TUploadResult } from '../models/types';
import { catController } from '../logging';

const { finished } = require('stream/promises');

export default class OrganisationCtrl extends BaseCtrl<IOrganisationDocument> {

    model = Organisation;
    projection = '_id name city_de federalState';

    private processFile = async (filename:string) => {
        const records = []
        const parser = fs
        .createReadStream(filename)
        .pipe(parse({
          // CSV options if any
          fromLine: 2,
          delimiter: ";",
          columns: ['name','street','zipCode','city_de','country_de'],
        }));
        parser.on('readable', function(){
          let record;
          while (record = parser.read()) { // eslint-disable-line no-cond-assign
            records.push(record)
          }
        });

        await finished(parser);
        return records
      }

    private determineOrganisationType = (organisationName:string, feedback) => {
        let returnValue = 'Undetermined';
        const regexCompany = /(.+G(?:es|esellschaft)?\.?m\.?b\.?H\.?.?$)|.*Gesellschaft?.*|.*AG$|.*OG$|.*KG$|(.* d.o.o?.).*|.*s.r.o?.$|.*Sp.? z?.*|.*spol.r.s.o.|.*Sp.z.o.o..*|.* S\.R\.L\.$|.* in Liq.*|.*unternehmung|.*Limited.*|.*AD$|.*S.P.A.*|.*S.P.R.L.|.*Iberica SL|.*likvidaci.*|.*p\.l\.c\./i;
        const regexIncorporatedCompany = /.* AG.*/;
        const regexAssociation = /.*(Verband).*|.*(Verein).*/i;
        const regexFoundation = /.*(Stiftung).*|.*(Holding)/i;
        const regexCity = /^Stadt .+|.*Stadtwerke.*/i;
        const regexMunicipality = /^(?:Markt)?gemeinde?.*|Stadtgemeinde .*|.*Sanitäts.*/i;
        const regexState = /^Land .+/;
        const regexMinistry = /^(?:Bundesministerium|Bundeskanzleramt)/;
        const regexAgency = /.*(Bundesamt|Patentamt|Parlamentsdirektion|Präsidentschaftskanzlei|Verfassungsgerichtshof|Volksanwaltschaft|.*Agency.*|Arbeitsmarktservice|Agentur.*)/i;
        const regexFund = /.*Fonds?.*/i;
        const regexChamber = /.*?Kammer?.*/i;
        const regexPolicyRelevant = /^(Alternativregion).*|.*BIFIE|.*FMA|.*Sprengel?.*|^Kleinregion .*|Arbeitsmarktservice|Verwaltungsgerichtshof/i;
        const regexEducation = /.*(Alumni).*|.*(Universit).*|.*(Hochsch).*|.*Mittelschul.*|.*Schul.*|.*Päda.*/i;
        const regexMuseum = /Albertina|.*Museum.*|.*Belvedere.*/i;
        if (organisationName === 'Stadt Wien') {
          returnValue = 'Federal state';
        } else if (organisationName.match(regexCompany)) {
          returnValue = 'Company';
        } else if (organisationName.match(regexIncorporatedCompany)) {
          returnValue = 'Company';
        } else if (organisationName.match(regexAssociation)) {
          returnValue = 'Association';
        } else if (organisationName.match(regexChamber)) {
          returnValue = 'Chamber';
        } else if (organisationName.match(regexEducation)) {
          returnValue = 'Education';
        } else if (organisationName.match(regexFoundation)) {
          returnValue = 'Foundation';
        } else if (organisationName.match(regexMunicipality)) {
          returnValue = 'Municipality';
        } else if (organisationName.match(regexFund)) {
          returnValue = 'Fund';
        } else if (organisationName.match(regexPolicyRelevant)) {
          returnValue = 'Policy-relevant';
        } else if (organisationName.match(regexMinistry)) {
          returnValue = 'Ministry';
        } else if (organisationName.match(regexCity)) {
          returnValue = 'City';
        } else if (organisationName.match(regexState)) {
          returnValue = 'Federal state';
        } else if (organisationName.match(regexAgency)) {
          returnValue = 'Agency';
        } else if (organisationName.match(regexMuseum)) {
          returnValue = 'Museum';
        }
        if (returnValue === 'undetermined') {
          console.log("Undetermined organisation type for: " + organisationName);
        }
        const typeAsKey = returnValue.replace('-','_').replace(' ','_');
        feedback[typeAsKey] ? feedback[typeAsKey]=feedback[typeAsKey]+1 : feedback[typeAsKey] = 1;
        return returnValue;
      };

    uploadOrganisation = async (req, res) => {
        const file = req.file;
        const feedback:TUploadResult = {
            entries: 0,
            savedEntries: 0,
            errors: 0,
            errorEntries: []
          };
        try {
            catController.info("Starting Organisation Upload")
            const entries = await this.processFile(file.path);
            feedback.entries = entries.length;
            const zipCodes = await ZipCode.find({});
            const zipMap = zipCodes.reduce((acc,z)=>({...acc, [z.zipCode]: z.federalStateCode}),{})
            const orgsWithFederalStates = entries.map(e => e.country_de == 'Österreich' ?
                {...e, federalState: zipMap[e.zipCode]?zipMap[e.zipCode]:'unknown', type: this.determineOrganisationType(e.name,feedback)} :
                { ...e, federalState: 'N.D.', type: this.determineOrganisationType(e.name, feedback)}).map(o => new Organisation(o))
            await this.model.deleteMany({"name": { "$in": orgsWithFederalStates.map(org=>org.name)}})
            const results = await this.model.insertMany(orgsWithFederalStates,{ordered: false});
            feedback.errorEntries = orgsWithFederalStates.filter(o=>o.errors).concat(orgsWithFederalStates.filter(o=>o.federalState=='unknown'))
            feedback.savedEntries = results.length;
            return res.send(feedback);
        }  catch (error) {
            if (error.name == 'MongoBulkWriteError') {
                feedback.savedEntries = error.insertedDocs.length
                return res.status(200).send(feedback);
              }
            feedback.errorEntries.push(error);
            return res.status(500).send(feedback)
        } finally {
            fsPromises.unlink(file.path);
        }

    }
}
