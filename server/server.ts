import dotenv from 'dotenv';
import mongoose from 'mongoose';
dotenv.config({ path: '.env' });
import { app } from './app';
import { catSystem } from './logging';
import Net from 'net'

const isPortTaken = (port) => new Promise<boolean>((resolve, reject) => {
  const tester = Net.createServer()
    .once('error', err => (err['code'] == 'EADDRINUSE' ? resolve(false) : reject(err)))
    .once('listening', () => tester.once('close', () => resolve(true)).close())
    .listen(port)
})

const dbOptions = {} 
if (process.env.MONGODB_URI.indexOf('tls=true') > -1) {
  dbOptions['tlsCAFile'] = `${__dirname}/certs/ca.crt`
  dbOptions['tlsCertificateKeyFile'] = `${__dirname}/certs/key.pem`
}
mongoose.connect(process.env.MONGODB_URI, dbOptions)
  .then(() => {
    catSystem.info(`Connected to MongoDB: ${mongoose.connection.host}/${mongoose.connection.db.databaseName}`);
    isPortTaken(app.get('port')).then(free => {
      if (free) {
        app.listen(app.get('port'), () => {
          catSystem.info('React Full Stack listening on port ' + app.get('port'));
        });
      }
    })
  })




