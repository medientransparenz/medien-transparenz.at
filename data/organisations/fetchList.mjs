import superagent from 'superagent'
import { writeFile } from 'fs/promises';

const host = "https://www.rechnungshof.gv.at"
const url =`${host}/rh/home/was-wir-tun/was-wir-tun/Pruefen_und_Empfehlen.html`
//const url = 'https://www.rechnungshof.gv.at/rh/home/was-wir-tun/was-wir-tun/Rechtstraeger_Pruefungsobligo_Stand_01_01_23.csv'
const nameTemplate = 'Rechtstraeger_der_Rechnungshofkontrolle_unterliegend_Stand_'

const today = new Date()

const reg = /<a .*? href="(.*)" .*?>Rechtsträger.csv/gm
let cvsLink = ""

superagent.get(url)
.then(resp => reg.exec(resp.text))
.then(r => cvsLink = r[1])
.then(() => superagent.get(host + cvsLink))
.then(resp => resp.text)
.then(data => 
    writeFile(`./${cvsLink.split("/").at(-1)}`,data))
.then(()=> console.log(`Data written to ${cvsLink.split("/").at(-1)}`))    
.catch(err => 
    console.error(`Could not get new list of organisations: ${err}`))