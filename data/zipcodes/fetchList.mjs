import superagent from 'superagent'
import { writeFile } from 'fs/promises';

//TODO: Download from https://www.post.at/g/c/postlexikon!!!

const url = 'https://data.rtr.at/api/v1/tables/plz.csv'
const nameTemplate = 'PLZ-Verzeichnis_Stand_'

const today = new Date()

superagent.get(url)
.then(resp => resp.text)
.then(data => writeFile(`./${nameTemplate}${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}.csv`,data))