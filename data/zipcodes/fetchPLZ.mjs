import fetch from 'node-fetch'
import xlsx from 'node-xlsx';
import { writeFile } from 'fs/promises';

const url = 'https://www.post.at/g/c/postlexikon'
const nameTemplate = 'PLZ-Verzeichnis_Stand_'

const today = new Date()


const wb2TSV = (wb) => 
   wb[0].data.reduce((acc,line) => acc+line.join(";")+"\n","")

const pattern = /.*(http.*\/PLZ_Verzeichnis.*)".*/g
fetch(url)
.then(resp => resp.text())
.then(text => pattern.exec(text)[1])
.then(fetch)
.then(resp => resp.blob())
.then(blob => blob.arrayBuffer())
.then(blob => xlsx.parse(blob))
.then(wb2TSV)
.then(tsv => writeFile(`./${nameTemplate}${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}.csv`,tsv))
.then(()=>console.log("DONE"))
.catch(err=>console.error(err))
//.then(data => writeFile(`./${nameTemplate}${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}.csv`,data))