/**
 * Created by salho on 16.12.16.
 */

import * as http from 'https';
import * as fs from 'fs';


const quarter = 20224;

const previousQuarter = (quarter_) => {
  let pq = quarter_-1;
  if (parseInt(pq.toString()[4])===0){
   pq = pq-6
  }
  return pq;
};


const download = () => http.get(
    "https://data.rtr.at/api/v1/tables/MedKFTGBekanntgabe.json?quartal=" + quarter + "&leermeldung=0&page=0&size=0",
    res => {
        let rawData = "";
        res.on('data', (chunk) => {rawData += chunk});
        res.on('end', () => {
            try {
                let transfers = JSON.parse(rawData).data;
                let csv = "";
                // console.log(transfers[2]);
                //transfers.filter(s=>s.rechtstraeger.includes('UNESCO')).forEach(console.log);
                transfers.forEach(transfer =>
                    csv+=`"${transfer.rechtstraeger.replaceAll("\"","\"\"")}";"${transfer.quartal}";"${transfer.bekanntgabe}";"${transfer.leermeldung}";"${transfer.mediumMedieninhaber.replaceAll("\"","\"\"")}";"${transfer.euro}"\n`)
                 fs.writeFileSync(`./data/transfers/raw/${quarter}-raw.csv`, csv);
                console.log(`Found ${transfers.length} records.`)
            } catch (e) {
                console.log(e.message);
            }
        });
    }).on('error', (e) => {
        console.log(`Got error: ${e.message}`);
});

const pQuarter = previousQuarter(quarter);
console.log(pQuarter);
download();

