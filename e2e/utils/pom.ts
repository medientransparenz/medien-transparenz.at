import { expect, Locator, Page } from "@playwright/test";

export class Pom {
  readonly page: Page;
  readonly navBar: Locator;
  readonly overviewLink: Locator;
  readonly topValuesLink: Locator;
  readonly moneyFlowsLink: Locator;
  readonly searchLink: Locator;

  constructor(page: Page) {
    this.page = page;
    this.navBar = page.locator("[data-test-id=\"navbar\"]");
    this.overviewLink = page.locator("[data-test-id=\"overview\"]");
    this.topValuesLink = page.locator("[data-test-id=\"topValues\"]");
    this.moneyFlowsLink = page.locator("[data-test-id=\"moneyFlows\"]");
    this.searchLink = page.locator("[data-test-id=\"search\"]");
  }

  async navigation() {
    await expect(this.navBar).toBeVisible();
  }

  async overview() {
    await this.navigation();
    await this.overviewLink.click();
  }

  async topValues() {
    await this.navigation();
    await this.topValuesLink.click();
  }

  async moneyFlows() {
    await this.navigation();
    await this.moneyFlowsLink.click();
  }

  async search() {
    await this.navigation();
    await this.searchLink.click();
  }
}
