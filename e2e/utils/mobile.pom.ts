import { expect, Locator, Page } from '@playwright/test';
import { Pom } from "./pom";

export class MobilePom extends Pom {
  readonly mobileNavigationToggle: Locator;

  constructor(page: Page) {
    super(page);
    this.mobileNavigationToggle = page.locator('[data-test-id="mobileNavigationToggle"]');
  }

  async navigation() {
    await expect(this.mobileNavigationToggle).toBeVisible();
    await this.mobileNavigationToggle.click();
  }
}
