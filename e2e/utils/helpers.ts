import { promises as fsPromises } from "fs";
import iconv from "iconv-lite";
import Transfer from "../../server/models/transfer";
import Organisation from "../../server/models/organisations";
import Grouping from "../../server/models/grouping";
import { expect, Page } from "@playwright/test";

const preloadOrganisations = async () => {
  const data = await fsPromises.readFile("e2e/files/organisations.json");
  const orgs = JSON.parse(iconv.decode(data, "utf8"));
  const o = orgs.map(org => {
    delete org._id;
    return new Organisation(org);
  });
  return await Organisation.insertMany(o);
}

const preloadGroupings = async () => {
  const data = await fsPromises.readFile("e2e/files/groupings.json");
  const groupings = JSON.parse(iconv.decode(data, "utf8"));
  const grps = groupings.map(grp => {
    delete grp._id;
    return new Grouping(grp);
  });
  return await Grouping.insertMany(grps)
}

const preloadTransfers = async () => {
  const data = await fsPromises.readFile("e2e/files/transfers.json");
  const orgs = JSON.parse(iconv.decode(data, "utf8"));
  const o = orgs.map(trans => {
    delete trans._id;
    delete trans.organisationReference;
    return new Transfer(trans);
  })
  return await Transfer.insertMany(o);
}

export const preloadData = () => Promise.all([
  preloadOrganisations(), preloadGroupings(), preloadTransfers()
]);

const clearOrganisations = () => Organisation.deleteMany({});
const clearGroupings = () => Grouping.deleteMany({});
const clearTransfers = () => Transfer.deleteMany({});

export const clearDB = () => Promise.all([
  clearOrganisations(), clearGroupings(), clearTransfers()
]);

export const matchScreenshot = async (page: Page, id = "canvas", wait = true) => {
  // Wait for loading spinner
  wait && await waitForLoadingSpinner(page);

  await expect(
    await page
      .locator(id)
      .screenshot({ mask: [page.locator("footer")], animations: "disabled" })
  ).toMatchSnapshot({ maxDiffPixelRatio: 0.01 });
}

export const waitForLoadingSpinner = async (page: Page) => {
  await expect(page.locator(".modal-open")).toBeHidden();
}
