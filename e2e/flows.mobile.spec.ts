import { expect, test } from "@playwright/test";
import { MobilePom } from "./utils/mobile.pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Money flows", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new MobilePom(page);
    await mediaTransparency.moneyFlows();
  })

  /* Test Case ID: 22 */
  test("should display all filter options on small screens", async ({ page }) => {
    // Screenshot settings
    await matchScreenshot(page, "[data-test-id=\"flowSettings\"]");
  });

  /* Test Case ID: 26 */
  test("should show funding money flows from specific payer group", async ({ page }) => {
    // Click in payers selection
    await page.locator("#orgGroups input").click();

    // Choose "Landesregierungen (ohne Wien)"
    await page.locator("text=Landesregierungen (ohne Wien)").click();
    await expect(page).toHaveURL("/flows?pType=2&organisations=Bundeskanzleramt&orgGroups=Landesregierungen%20(ohne%20Wien)&from=20222&to=20222");

    // Click remove icon
    await page.locator("#orgList img.icon_cancel ").click();

    // Choose "Förderung"
    await page.locator("[data-test-id=\"paymentTypeSwitch\"] label:has-text(\"Förderung\")").click();
    await expect(page).toHaveURL("/flows?pType=4&orgGroups=Landesregierungen%20(ohne%20Wien)&from=20222&to=20222");

    // Click in the middle of the chart
    await page.locator("g:nth-child(3) > .recharts-layer > .recharts-rectangle").click();

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });

  /* Test Case ID: 27 */
  test("should show all money flows to specific beneficiary", async ({ page }) => {
    // Click remove icon
    await page.locator("#orgList img.icon_cancel ").click();

    // Click into beneficiary input
    await page.locator("#mediaList input").click();

    // Type in beneficiary input
    await page.locator("#mediaList input").type("Sound");

    // Choose "Radio Soundportal"
    await page.locator("#mediaList .optionListContainer >> text=\"Radio Soundportal\"").click();
    await expect(page).toHaveURL("/flows?pType=2&media=Radio%20Soundportal&from=20222&to=20222");

    // Choose "Beides"
    await page.locator("text=Beides").click();
    await expect(page).toHaveURL("/flows?pType=2&pType=4&media=Radio%20Soundportal&from=20222&to=20222");

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });

  /* Test Case ID: 29 */
  test("should filter money flow by clicking on payer", async ({ page }) => {
    // Click remove icon
    await page.locator("#orgList img.icon_cancel ").click();

    // Click into beneficiary group input
    await page.locator("#mediaGroups input").click();

    // Choose "Red Bull Media House"
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/flows")),
      page.locator("#mediaGroups .optionListContainer >> text=\"Red Bull Media House\"").click()
    ]);
    await expect(page).toHaveURL("/flows?pType=2&mediaGroups=Red%20Bull%20Media%20House&from=20222&to=20222");

    // Click on "Land Kärnten" in chart
    const parent = await page.locator("g.recharts-sankey-nodes > g.recharts-layer > g.recharts-layer", { has: page.locator("text=\"Stadt Graz\"") });
    await parent.locator("path").dblclick();

    // Check payers input
    await expect(page.locator("#orgList div >> nth=0")).toHaveText("Stadt Graz");

    // Hover on the left side of the chart
    await page.locator(".recharts-rectangle").first().click();

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });
});
