import { expect, test } from "@playwright/test";
import { Pom } from "./utils/pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Top values", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new Pom(page);
    await mediaTransparency.topValues();
  })

  /* Test Case ID: 6 */
  test("should display all filter options on large screens", async ({ page }) => {
    // Screenshot settings
    await matchScreenshot(page, "[data-test-id=\"topPlayerSettings\"]");
  });

  /* Test Case ID: 9 */
  test("should display correct tooltip hint for desktop devices", async ({ page }) => {
    // Check tooltip hint
    const locator = page.locator("[data-test-id=\"tooltipHint\"]");
    await expect(locator).toContainText("Klicken Sie auf das Diagram");
  });

  /* Test Case ID: 11 */
  test("should display top payers incl. tooltip", async ({ page }) => {
    // Hover over "Wirtschaftskammer" in the canvas
    await page.locator("canvas").hover({ position: { x: 384, y: 440 } });

    // Screenshot chart
    await matchScreenshot(page, "canvas", false);
  });

  /* Test Case ID: 13 */
  test("should display top beneficiaries", async ({ page }) => {
    // Click on "Zahler"
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("[data-test-id=\"payersOrBeneficiariesToggle\"] span:text-is(\"Zahler\")").click()
    ]);

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 14 */
  test("should display top funding payers", async ({ page }) => {
    // Click on "Förderung"
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("[data-test-id=\"advertisingOrFundingToggle\"] label:text-is(\"Förderung\")").click()
    ]);

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 15 */
  test("should display top advertising and funding payers", async ({ page }) => {
    // Choose "Beides"
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("[data-test-id=\"advertisingOrFundingToggle\"] label:text-is(\"Beides\")").click()
    ]);

    // Hover over "Stadt Wien" in the canvas
    await page.locator("canvas").hover({ position: { x: 392, y: 625 } });

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 16 */
  test("should display top advertising payers in Styria", async ({ page }) => {
    // Choose "Styria"
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("[data-test-id=\"regionToggle\"] select").selectOption("AT-6")
    ]);

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 17 */
  test("should display top advertising payers chart without groupings", async ({ page }) => {
    // Deactivate groupings
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("[data-test-id=\"groupingsToggle\"] span:text-is(\"aktiviert\")").click()
    ]);

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 18 */
  test("should display top advertising payers in 2022", async ({ page }) => {
    // Click on Q1/2022
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("[data-test-id=\"periodRange\"] .rc-slider-step span:nth-child(5)").click()
    ]);

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 19 */
  test("should go from top payers to money flows (incl. filters)", async ({ page }) => {
    // Deactivate groupings
    await page.locator("[data-test-id=\"groupingsToggle\"] span:text-is(\"aktiviert\")").click();

    // Click on "Stadt Wien" in canvas
    await page.locator("canvas").click({ position: { x: 358, y: 472 } });
    await expect(page).toHaveURL("/flows?pType=2&organisations=Stadt%20Wien&from=20222&to=20222");

    // Check groupings setting
    await expect(page.locator("[data-test-id=\"groupingsToggle\"] >> text=deaktiviert")).toBeVisible();

    // Check payers setting
    await expect(page.locator("#orgList div >> nth=0")).toHaveText("Stadt Wien");
  });
});
