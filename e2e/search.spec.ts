import { expect, test } from "@playwright/test";
import { Pom } from "./utils/pom";

test.describe("Money flows", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new Pom(page);
    await mediaTransparency.search();
  })

  /* Test Case ID: 32 */
  test("should find advertising payments by using the search page", async ({ page }) => {
    // Fill search input
    await page.locator("[placeholder=\"Suche\"]").fill("Presse");

    // Press Enter
    await page.locator("[placeholder=\"Suche\"]").press("Enter");

    // Check hits
    await expect(page.locator("[data-test-id=\"beneficiariesHits\"] .badge.bg-info")).toHaveText("17 Treffer");

    // Open beneficiaries hits
    await page.locator("[data-test-id=\"beneficiariesHits\"]").click();

    // Click on "Die Presse"
    await page.locator("[data-test-id=\"beneficiariesHits\"] >> text=\"Die Presse\"").click();
    await expect(page).toHaveURL("/flows?media=Die%20Presse&from=20181&to=20222&pType=2");

    // Check beneficiary input
    await expect(page.locator("#mediaList div >> nth=0")).toHaveText("Die Presse");
  });
});
