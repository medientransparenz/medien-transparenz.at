import { test } from "@playwright/test";
import { MobilePom } from "./utils/mobile.pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Top values", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new MobilePom(page);
    await mediaTransparency.topValues();
  })

  /* Test Case ID: 8 */
  test("should display all filter options on medium screens", async ({ page }) => {
    // Screenshot settings
    await matchScreenshot(page, "[data-test-id=\"topPlayerSettings\"]");
  });
});
