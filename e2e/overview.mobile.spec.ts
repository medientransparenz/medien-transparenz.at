import { test } from "@playwright/test";
import { MobilePom } from "./utils/mobile.pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Mobile overview", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new MobilePom(page);
    await mediaTransparency.overview();
  })

  /* Test Case ID: 2 */
  test("should display forecast in timeline incl. tooltip", async ({ page }) => {
    // Enable forecast
    await page.locator("[data-test-id=\"timelineForecastToggle\"] >> text=Off").click();

    // Click on last bar in timeline canvas
    await page.locator("[data-test-id=\"timelineChart\"] canvas").click({ position: { x: 224, y: 165 } });

    // Screenshot chart
    await matchScreenshot(page, "[data-test-id=\"timelineChart\"] canvas");
  });

  /* Test Case ID: 4 */
  test("should display forecast in payment chart incl. tooltip", async ({ page }) => {
    // Enable forecast
    await page.locator("[data-test-id=\"paymentForecastToggle-4\"] >> text=Off").click();

    // Click on last bar in  payment §4 chart canvas
    await page.locator("[data-test-id=\"paymentChart-4\"] canvas").click({ position: { x: 224, y: 218 } });

    // Screenshot chart
    await matchScreenshot(page, "[data-test-id=\"paymentChart-4\"] canvas");
  });
});
