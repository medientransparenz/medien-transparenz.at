import { expect, test } from "@playwright/test";
import { MobilePom } from "./utils/mobile.pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Top values", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new MobilePom(page);
    await mediaTransparency.topValues();
  })

  /* Test Case ID: 7 */
  test("should display all filter options on small screens", async ({ page }) => {
    // Screenshot settings
    await matchScreenshot(page, "[data-test-id=\"topPlayerSettings\"]");
  });

  /* Test Case ID: 10 */
  test("should display correct tooltip hint for mobile devices", async ({ page }) => {
    // Check tooltip hint
    const locator = page.locator("[data-test-id=\"tooltipHint\"]");
    await expect(locator).toContainText("Doppel-Klicken Sie auf das Diagram");
  });

  /* Test Case ID: 12 */
  test.skip("should display top payers chart incl. tooltip", async ({ page }) => {
    // Workaround needed otherwise element gets clicked twice
    await page.locator("canvas").scrollIntoViewIfNeeded();
    
    // Click on "Wirtschaftskammer" in canvas
    await page.locator("canvas").click({ position: { x: 45, y: 471 } });

    // Screenshot chart
    await matchScreenshot(page);
  });

  /* Test Case ID: 20 */
  test("should go from top payers to money flows (incl. filters)", async ({ page }) => {
    // Click span:has-text("Zahler")
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/top")),
      page.locator("span:has-text(\"Zahler\")").click()
    ]);

    // Click on "ORF (alle)" in canvas
    await page.locator("canvas").dblclick({ position: { x: 122, y: 496 } });
    await expect(page).toHaveURL("/flows?pType=2&mediaGroups=ORF%20(alle)&from=20222&to=20222");

    // Check payers setting
    await expect(page.locator("#mediaGroups div >> nth=0")).toHaveText("ORF (alle)");
  });
});
