import { expect, test } from "@playwright/test";
import { Pom } from "./utils/pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Money flows", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new Pom(page);
    await mediaTransparency.moneyFlows();
  })

  /* Test Case ID: 21 */
  test("should display all filter options on large screens", async ({ page }) => {
    // Screenshot settings
    await matchScreenshot(page, "[data-test-id=\"flowSettings\"]");
  });

  /* Test Case ID: 24 */
  test("should show money flows from specific organisation", async ({ page }) => {
    // Click remove icon
    await page.locator("#orgList img.icon_cancel ").click();

    // Click into payer input
    await page.locator("#orgList input").click();

    // Type in payer input
    await page.locator("#orgList input").type("fh j");

    // Choose "FH JOANNEUM Gesellschaft mbH"
    await page.locator("#orgList .optionListContainer >> text=\"FH JOANNEUM Gesellschaft mbH\"").click();
    await expect(page).toHaveURL("/flows?pType=2&organisations=FH%20JOANNEUM%20Gesellschaft%20mbH&from=20222&to=20222");

    // Hover over left side of the chart
    await page.locator(".recharts-rectangle").first().hover();

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });

  /* Test Case ID: 25 */
  test("should show money flows to specific beneficiary group", async ({ page }) => {
    // Click remove icon
    await page.locator("#orgList img.icon_cancel ").click();

    // Click into beneficiary group input
    await page.locator("#mediaGroups input").click();

    // Type in beneficiary group input
    await page.locator("#mediaGroups input").type("fac");

    // Choose "Facebook (alle)"
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/flows")),
      page.locator("#mediaGroups .optionListContainer >> text=\"Facebook (alle)\"").click()
    ]);
    await expect(page).toHaveURL("/flows?pType=2&mediaGroups=Facebook%20(alle)&from=20222&to=20222");

    // Hover over right side of the chart
    await page.locator("g:nth-child(2) > .recharts-layer > .recharts-rectangle").hover();

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });

  /* Test Case ID: 27 */
  test("should show money flows to specific beneficiary in the last two years", async ({ page }) => {
    // Click remove icon
    await page.locator("#orgList img.icon_cancel").click();

    // Click into beneficiary input
    await page.locator("#mediaList input").click();

    // Type in beneficiary input
    await page.locator("#mediaList input").type("Breg");

    // Choose "Bregenzer Blättle"
    await page.locator("#mediaList .optionListContainer >> text=\"Bregenzer Blättle\"").click();
    await expect(page).toHaveURL("/flows?pType=2&media=Bregenzer%20Bl%C3%A4ttle&from=20222&to=20222");

    // Click on Q1/2020 in range
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/flows")),
      page.locator(".rc-slider-step > span:nth-child(3)").click()
    ]);
    await expect(page).toHaveURL("/flows?pType=2&media=Bregenzer%20Bl%C3%A4ttle&from=20201&to=20222");

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });

  /* Test Case ID: 30 */
  test("should filter money flow by clicking on beneficiary", async ({ page }) => {
    // Click on ProSieben Austria
    const parent = await page.locator("g.recharts-sankey-nodes > g.recharts-layer > g.recharts-layer", { has: page.locator("text=\"ProSieben Austria\"") });
    await parent.locator("path").click();
    await expect(page).toHaveURL("/flows?pType=2&media=ProSieben%20Austria&from=20222&to=20222");

    // Check beneficiary input
    await expect(page.locator("#mediaList div >> nth=0")).toHaveText("ProSieben Austria");

    // Screenshot chart
    await matchScreenshot(page, ".recharts-wrapper");
  });

  /* Test Case ID: 31 */
  test("should filter by quarter by clicking on the timeline", async ({ page }) => {
    // Go to "Ausgabenentwicklung" tab
    await page.locator(".nav-tabs >> text=\"Ausgabenentwicklung\"").click();

    // Click on Q1/2020 in timeline chart
    await Promise.all([
      page.waitForResponse(async (resp) => resp.url().includes("/api/transfers/flows")),
      page.locator("canvas").click({ position: { x: 627, y: 349 } })
    ]);
    await expect(page).toHaveURL("/flows?pType=2&organisations=Bundeskanzleramt&from=20201&to=20201");

    // Screenshot settings
    await matchScreenshot(page, "[data-test-id=\"flowSettings\"]");
  });
});
