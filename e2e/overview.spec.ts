import { expect, test } from "@playwright/test";
import { Pom } from "./utils/pom";
import { matchScreenshot } from "./utils/helpers";

test.describe("Overview", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("");
    const mediaTransparency = new Pom(page);
    await mediaTransparency.overview();
  })

  /* Test Case ID: 1 */
  test("should display forecast in timeline incl. tooltip", async ({ page }) => {
    // Enable forecast
    await page.locator("[data-test-id=\"timelineForecastToggle\"] >> text=Off").click();

    // Hover last bar in timeline canvas
    await page.locator("[data-test-id=\"timelineChart\"] canvas").hover({ position: { x: 1019, y: 213 } });

    // Screenshot chart
    await matchScreenshot(page, "[data-test-id=\"timelineChart\"] canvas");
  });

  /* Test Case ID: 3 */
  test("should display forecast in payment chart incl. tooltip", async ({ page }) => {
    // Enable forecast
    await page.locator("[data-test-id=\"paymentForecastToggle-2\"] >> text=Off").click();

    // Hover over last bar in payment §2 chart canvas
    await page.locator("[data-test-id=\"paymentChart-2\"] canvas").hover({ position: { x: 1062, y: 219 } });

    // Screenshot chart
    await matchScreenshot(page, "[data-test-id=\"paymentChart-2\"] canvas");
  });

  /* Test Case ID: 5 */
  test("should export payment data", async ({ page }) => {
    // Click "Export"
    const download = await Promise.all([
      page.waitForEvent("download"),
      page.locator("[data-test-id=\"paymentChart-31\"] >> text=Export").click()
    ]);

    // Check file name
    const fileName = download[0].suggestedFilename();
    await expect(fileName).toMatch("Spendings_31.xlsx");
  });
});
