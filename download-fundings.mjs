/**
 * Created by salho on 16.12.16.
 */

import * as http from 'https';
import * as fs from 'fs';

const year = (
    process.argv.find(value => /^(19|20)\d{2}$/.test(value)) ?? 2021
);

const FundingType = {
    presse: 0,
    publizistik: 1,
    prrf: 2,
    nkrf: 3,
    ffat: 4
};

const getFileLine = function (type, funding) {
    if (type === FundingType.presse)
        return `"${funding.foerderungswerber}";"${funding.zeitung.replace(";", " -")}";"Pressefoerderung";"KommAustria";` +
            `"${funding.foerderbetrag}";"${funding.jahr}";"${funding.gesetzlichegrundlage}";"${funding.strasse}";` +
            `"${funding.plz}";"${funding.ort}";"${funding.status}";` +
            `"${funding.teilbetrag1}";"${funding.teilbetrag2}";"";"${funding.foerderungsart}";"";"";""`;
    else if (type === FundingType.publizistik)
        return `"${funding.foerderungswerber.replace(";", "-")}";"${funding.zeitschrift}";"Publizistikfoerderung";"KommAustria";` +
            `"${funding.foerderbetrag}";"${funding.jahr}";"${funding.gesetzlichegrundlage}";"${funding.strasse}";` +
            `"${funding.plz}";"${funding.ort}";"${funding.status}";"";"";"";"";"";"";""`;
    else if (type === FundingType.prrf)
        return `"${funding.foerderungsnehmer}";"${funding.gefoerdertes_objekt}";"Privatrundfunkfonds";"RTR";` +
            `"${funding.zugesagte_foerderung}";"${funding.jahr}";"${funding.foerderungsart}";"";"";"";"";"";"";"${funding.sendername}";` +
            `"${funding.kategorie}";"${funding.antragstermin}";"";""`;
    else if (type === FundingType.nkrf)
        return `"${funding.foerderungsnehmer}";"${funding.gefoerdertes_objekt}";"Nichtkommerzieller Rundfunkfonds";"RTR";` +
            `"${funding.zugesagte_foerderung}";"${funding.jahr}";"${funding.foerderungsart}";"";"";"";"";"";"";"${funding.sendername}";` +
            `"${funding.kategorie}";"${funding.antragstermin}";"";""`;
    else if (type === FundingType.ffat)
        return `"${funding.foerderungswerber}";"${funding.titel}";"Fernsehfonds";"RTR";` +
            `"${funding.foerderbetrag}";"${funding.jahr}";"${funding.kategorie}";"";"";"";"";"";"";"${funding.titel}";` +
            `"${funding.kategorie}";"${funding.antragstermin}";"${funding.laenge}";"${funding.gesamtherstellungskosten}"`;
    else
        return null;
}

const download = function (url, type) {
    return new Promise((resolve, reject) => {
        http.get(url,
            res => {
                let rawData = "";
                res.on('data', (chunk) => { rawData += chunk });
                res.on('end', () => {
                    try {
                        resolve({
                            type,
                            fundings: JSON.parse(rawData).data
                        });
                    } catch (e) {
                        console.log(e.message);
                        reject([]);
                    }
                });
            }).on('error', (e) => {
                console.log(`Got error: ${e.message}`);
            });
    })
}

const promises = [];
promises.push(download("https://data.rtr.at/api/v1/tables/presse.json?jahr=" + year + "&size=0", FundingType.presse));
promises.push(download("https://data.rtr.at/api/v1/tables/publizistik.json?jahr=" + year + "&size=0", FundingType.publizistik));
promises.push(download("https://data.rtr.at/api/v1/tables/prrf.json?jahr=" + year + "&size=0", FundingType.prrf));
promises.push(download("https://data.rtr.at/api/v1/tables/nkrf.json?jahr=" + year + "&size=0", FundingType.nkrf));
promises.push(download("https://data.rtr.at/api/v1/tables/ffat.json?jahr=" + year + "&size=0", FundingType.ffat));

Promise.all(promises).then((values) => {
    let csvContent = "receiver;fundedObject;name;payer;amount;year;fundingBasis;" +
        "street;zipCode;city_de;status;amountPart1;amountPart2;broadcastName;" +
        "category;deadline;broadcastLength;productionCosts\n";
    for (const value of values) {
        value.fundings.forEach(item => csvContent += getFileLine(value.type, item) + "\n");
    }
    csvContent = csvContent.trimEnd("\n");
    fs.writeFileSync(`./data/fundings/raw/${year}-raw.csv`, csvContent,
        {
            flag: "w+"
        });
    console.log(`Found ${csvContent.split("\n").length - 1} fundings.`)
});
