#FROM node:15.5.1-buster
FROM node:16.13.0-buster AS builder

ENV GENERATE_SOURCEMAP=false

WORKDIR /media-transparency
COPY . /media-transparency
RUN rm -fr ./node_modules ./client/node_modules && npm install --force
WORKDIR /media-transparency/client
RUN npm install --force && yarn build
WORKDIR /media-transparency 
RUN npm install --force && yarn compile && cp -R client/build dist/public

FROM node:16.13.0-alpine3.12
WORKDIR /media-transparency
COPY --from=builder /media-transparency/dist ./dist
COPY --from=builder /media-transparency/node_modules ./node_modules
EXPOSE 4200
CMD [ "node", "dist/server/server.js" ]
