# Development Documentation

This project contains of a [server-](./server) and a [client-side](./client) part. Both parts are written in Typescript and use the following technology stacks / frameworks:

**Server**

* Express
* Mongoose

**Client**

* ReactJS

## Prereqisites

In order to start developing you need to have [node](https://nodejs.dev/download/) installed (recommended version 16.13.0)

Besides this, you also need a running [mongodb](https://www.mongodb.com/) instance. The best way to get started with mongodb is to use [docker](https://www.docker.com/products/docker-desktop). Run the following commands in order to get a running instance:

```bash
docker volume create mongo
docker run -d --name mongo -p 27017:27017 -v mongo:/data/db mongo:5.0.5 
```
This will install and run a mongodb.

It is also recommended to use a mongo UI tool like [Robo 3T](https://robomongo.org/).

## Building the System

After checking out the source code, you need install all required dependencies for the server- and client part using `yarn`:

```
yarn
cd ./client
yarn
```

Once this is done, you can run the application in development mode by using the following command from the project's root folder:

```
yarn dev
```

The application will become available at http://localhost:3000.

## Populating the Database

Before you can load data into the database, you need to create an admin account. Therefore first register a new account at http://localhost:3000/register_.

After doing so, you need to activate and upgrade this account by directly editing the corresponding document in the mongodb's `user` collection wihtin the `developmentdb` database:

```json
{
    "_id" : ObjectId("61c1848e7be7451963c28c81"),
    "username" : "johnny",
    "email" : "jd@test.com",
    "password" : "$2a$10$R3aw9vtYBXWXMBqmHcd25.n70FZDYKnl.qePlZ.Ky1HoSnVi0JK96",
    "roles" : [ 
        "user",
        "admin",
        "group"
    ],
    "active" : true,
    "provider" : "local",
    "__v" : 0
}
```
You need to change the values of field `active` to true and add `admin` and `group` to the list of roles.

You can now login using http://localhost:3000/login_. 

Reqired data can be found in folder [./data](./data). Using the `Administration` interface you now need to import all *CSV* files (from oldest ot latest) found the following folders using the corresponding upload dialogs:

* [`zipcodes`](./data/zipcodes)
* [`organisations`](./data/organisations)
* [`transfers` (refined)](./data/transfers/refined)

Transfers can also be added to the database following an alternative approach, that is also used to update data after the release of a new quarterly data set. Inside folder `./refine` you can execute the following command:

```
node refine_data.mjs update  -u [your account's email] -p [your account's password] --db "mongodb://localhost:27017/developmentdb" --www localhost:3000
```

The last step is to import the groupings definitions found in file [`groupings.json`](./data/groupings). This requires to have the [mongo database tools](https://www.mongodb.com/try/download/database-tools) installed. To import these entries run the following command:

```
mongoimport mongodb://localhost/developmentdb ./data/groupings/groupings.json
```

## Executing Tests

Currently only tests for the server-side part of the application exist. You can run them from the project's root folder using `yarn test-srv`.

## Debugging the Application

You can use your favorite IDE to debug the application. Here's a `launch.json` file that can be used with Visual Studio Code:

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "firefox",
            "request": "launch",
            "name": "Launch Firefox",
            "url": "http://localhost:3000"
        },
        {
            "name": "fetchPLZ",
            "program": "${workspaceFolder}/data/zipcodes/fetchPLZ.mjs",
            "request": "launch",
            "skipFiles": [
                "<node_internals>/**"
            ],
            "type": "pwa-node"
        },
        {
            "type": "node",
            "request": "attach",
            "name": "Node: Nodemon",
            "processId": "${command:PickProcess}",
            "restart": true,
            "protocol": "inspector"
        },
        {
            "name": "Launch Chrome",
            "request": "launch",
            "type": "pwa-chrome",
            "url": "http://localhost:3000",
            "webRoot": "${workspaceFolder}"
        },
        {
            "type": "node",
            "name": "Jest Selected File",
            "request": "launch",
            "args": [
                "${fileBasenameNoExtension}",
                "--runInBand",
                "--config",
                "jest.server.config.json"
            ],
            "cwd": "${workspaceFolder}",
            "console": "integratedTerminal",
            "internalConsoleOptions": "neverOpen",
            "disableOptimisticBPs": true,
            "program": "${workspaceFolder}/node_modules/jest/bin/jest",
            "windows": {
                "program": "${workspaceFolder}/node_modules/jest/bin/jest"
            }
        },
        {
            "type": "node",
            "name": "Jest Selected Test",
            "request": "launch",
            "args": [
                "${fileBasenameNoExtension}",
                "-t ${selectedText}",
                "--runInBand",
                "--config",
                "jest.server.config.json"
            ],
            "cwd": "${workspaceFolder}",
            "console": "integratedTerminal",
            "internalConsoleOptions": "neverOpen",
            "disableOptimisticBPs": true,
            "program": "${workspaceFolder}/node_modules/jest/bin/jest",
            "windows": {
                "program": "${workspaceFolder}/node_modules/jest/bin/jest"
            }
        },
        {
            "type": "node",
            "request": "launch",
            "name": "Launch Server Program",
            "program": "${workspaceFolder}/server/server.ts",
            "preLaunchTask": "tsc: build - tsconfig.json",
            "outFiles": [
                "${workspaceFolder}/dist/**/*.js"
            ]
        }
    ]
}
```

