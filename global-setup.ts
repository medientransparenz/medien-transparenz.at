import mongoose from "mongoose";
import { clearDB, preloadData } from "./e2e/utils/helpers";

mongoose.connect(process.env.MONGODB_URI);

async function globalSetup() {
  await clearDB();
  await preloadData();
}

export default globalSetup;
