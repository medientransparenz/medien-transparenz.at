# Media Transparency Austria

This site is hosting the current source code and the raw data of the web site [www.medien-transparenz.at](https://www.medien-transparenz.at). Up to a major refactoring in 2021 it was located [here](https://github.com/AnotherCodeArtist/medien-transparenz.at).

[![Screenshot](./docs/web-site.png)](https://www.medien-transparenz.at)

The goal of this project is provide easily accessible insights into the data according to the [Austrian Media Transparency Act](https://www.ris.bka.gv.at/eli/bgbl/i/2011/125/P0/NOR40134532), which is published quarterly by the RTR(https://www.rtr.at/medien/was_wir_tun/medientransparenz/startseite.de.html).

It recently integrated more detailed funding data as well.

The actual data is regularly fetched from [RTR's open data interface](https://www.rtr.at/rtr/service/opendata/Medientransparenz/OpenDataMTGDaten20212.de.html). After downloading, data get processed using [OpenRefine](https://openrefine.org/).

The raw version of all available data so far (reaching back to 2012) can be found in folder [data/transfers/raw](data/transfers/raw) while the improved version of the same data (typos fixed, duplicates removed, ...) can be found at [data/transfers/refined](data/transfers/refined).
